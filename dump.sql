﻿-- phpMyAdmin SQL Dump
-- version 3.3.3
-- http://www.phpmyadmin.net
--
-- Servidor: mysql05-farm57.uni5.net
-- Tempo de Geração: Mar 30, 2018 as 05:40 PM
-- Versão do Servidor: 5.5.43
-- Versão do PHP: 5.3.28

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `proatlhetica01`
--
CREATE DATABASE `eccotrade` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `eccotrade`;

-- --------------------------------------------------------

--
-- Estrutura da tabela `Agenda_contatos`
--

CREATE TABLE IF NOT EXISTS `Agenda_contatos` (
  `CON_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CON_Nome` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `CON_Telefone_Fixo` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `CON_Telefone_Movel` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `CON_Email` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `CON_Observacoes` varchar(255) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`CON_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Agenda_contatos`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Aux_Rede_Filial`
--

CREATE TABLE IF NOT EXISTS `Aux_Rede_Filial` (
  `Aux_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Aux_Rede_ID` int(11) DEFAULT NULL,
  `Aux_Filial_ID` int(11) DEFAULT NULL,
  `Aux_Promotor_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`Aux_ID`),
  KEY `Aux_Rede_ID` (`Aux_Rede_ID`),
  KEY `Aux_Filial_ID` (`Aux_Filial_ID`),
  KEY `Aux_Promotor_ID` (`Aux_Promotor_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Aux_Rede_Filial`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Clientes`
--

CREATE TABLE IF NOT EXISTS `Clientes` (
  `Cli_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Cli_Nome` varchar(145) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_RazaoSocial` varchar(145) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_TelefoneFixo` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_TelefoneRecado` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_TelefoneComercial` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_TelefoneMovel` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Email` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_CEP` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Endereco` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Numero` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Cidade` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Estado` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_RG` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_CPF` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_CNPJ` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Bairro` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Complemento` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_PJ_PF` varchar(3) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Cli_Coordenador_ID` int(11) DEFAULT NULL,
  `bit_deletado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`Cli_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Extraindo dados da tabela `Clientes`
--

INSERT INTO `Clientes` (`Cli_ID`, `Cli_Nome`, `Cli_RazaoSocial`, `Cli_TelefoneFixo`, `Cli_TelefoneRecado`, `Cli_TelefoneComercial`, `Cli_TelefoneMovel`, `Cli_Email`, `Cli_CEP`, `Cli_Endereco`, `Cli_Numero`, `Cli_Cidade`, `Cli_Estado`, `Cli_RG`, `Cli_CPF`, `Cli_CNPJ`, `Cli_Bairro`, `Cli_Complemento`, `Cli_PJ_PF`, `Cli_Coordenador_ID`, `bit_deletado`) VALUES
(3, 'EMBALIXO ', 'EMBALIXO COMERCIO DE EMBALAGENS', '(19) 3716-8699', NULL, NULL, '(19) 9 8189-3093', 'marketing@embalixo.com.br', '13082-030', 'Rua Francisco CearÃ¡ Barbosa', '110', 'Campinas', 'SP', NULL, NULL, '19.572.597/0001-77', 'ChÃ¡caras Campos dos Amarais', NULL, NULL, 9, '0'),
(4, 'PARMISSIMO ALIMENTOS', 'PARMISSIMO COMERCIO DE ALIMENTOS', '(51) 3340-0823', NULL, NULL, '51 9755-5218', 'danielkunzler@parmissimo.com.br', '94420-990', 'Rua CÃ¢ndido Pinheiro Barcelos', '1055', 'ViamÃ£o', 'RS', NULL, NULL, '93.647.881/0001-18', 'ViamÃ£o', NULL, NULL, 6, '0'),
(5, 'ALCOOL TUPI', 'Calamaris LTDA', '16 99229-5838', NULL, NULL, '16 99994-2422', 'marketing@familiatupi.com.br', '14815-000', 'AntÃ´nio Donatoni', '161', 'IbatÃ©', 'SP', NULL, NULL, '01.932.232/0002-21', 'Parque Industrial', NULL, NULL, 10, '0'),
(6, 'NEWAGE DISTRIBUIDORA DE BEBIDAS LTDA', 'NEWAGE DISTRIBUIDORA DE BEBIDAS LTDA', '(19) 3573-6600', NULL, NULL, '11 98176-3365', 'cleber.lourenco@newagebebidas.com.br', '13612-410', 'Avenida Ferdinando Marchi', '1', 'Leme', 'SP', NULL, NULL, '17.896.947/0003-97', 'Distrito Industrial', NULL, NULL, 11, '0'),
(7, 'VITA MUNDI', 'FUNCIONAL FABRICAÃ‡ÃƒO DE SUCOS LTDA', '19) 3802-2816', NULL, NULL, '19 99204-8029', 'alan@vitamundi.com.br', '13825-000', 'RUA DOS ANTURIOS ', '33', 'Holambra', 'SP', NULL, NULL, '21.164.527/0001-21', 'JD HOLANDA', NULL, NULL, 6, '0'),
(8, '7 PETS', 'LEGANA COMERCIAL LTDA', '(21) 9962-9937', NULL, NULL, '(21) 9962-9937', 'leila@7pets.com.br', '22621-200', 'Avenida OlegÃ¡rio Maciel', '331b', 'Rio de Janeiro', 'RJ', NULL, NULL, '16.729.231/0002-51', 'Barra da Tijuca', NULL, NULL, 6, '0'),
(9, 'DEMARCHI', 'CACO COMERCIAL DE FRUTAS LTDA', '(19) 3800-3111', NULL, NULL, '19 99381-8450', 'nfe@demarchicampinas.com.br', '13178-546', 'Rua CambaÃ­', '64', 'SumarÃ©', 'SP', NULL, NULL, '60.795.978/0002-08', 'Loteamento Industrial Veccon Zeta', NULL, NULL, 7, '0'),
(10, 'XTAPA', 'KORBACH VOLLET ALIMENTOS LTDA', '(19) 3881-7070', NULL, NULL, '19 99385-9351', 'nfe@kvaalimentos.com.br;eder.zicolau@kvaalime', '13271-570', 'Rua Luiz Spiandorelli Neto', '30 SALA 308', 'Valinhos', 'SP', NULL, NULL, '04.082.337/0001-46', 'Loteamento PaiquerÃª', NULL, NULL, 11, '0'),
(11, 'LA BASQUE ', 'LA BASQUE ALIMENTOS LTDA', '(19) 3256-9294', NULL, NULL, '19 99133-1908', 'luana.silva@labasque.com.br;sergio.urbano@lab', '13087-607', 'Rua Josefina Gori Fiorani', '220', 'Campinas', 'SP', NULL, NULL, '44.930.204/0001-05', 'Parque Rural Fazenda Santa CÃ¢ndida', NULL, NULL, 7, '0'),
(12, 'DEMARCHI BRASILIA', 'Valemix Distribuicao de Alimentos Ltda (De Marchi Brasilia)', '61) 3232-5588', NULL, NULL, '', 'vainer@demarchibrasilia.com.br', ' 71989000', 'A Ade Conjunto 19, Sn, : Lote 12/13; : Lojas ', '19', 'BRASILIA', 'Selecione', NULL, NULL, '07.646.239/0001-82', ' Area De Des. Econom', NULL, NULL, 8, '0'),
(13, 'HIKARI ', 'HIKARI INDUSTRIA E COMERCIO LTDA', '(11) 4674-6000', NULL, NULL, '(11) 9 6651-799', 'coordenadorsp@hikari.com.br', '08540-215', 'Rua AntÃ´nio Ruvolo', '653', 'Ferraz de Vasconcelos', 'SP', NULL, NULL, '61.155.511/0001-77', 'Jardim Soeiro', NULL, NULL, 9, '0'),
(14, 'ZINHO PÃƒO DE ALHO', 'ZINHO INDUSTRIA E COMERCIO DE PÃƒES DE ALHO LTDA', '(16) 9915-4880', NULL, NULL, '(16) 3966-6887', 'renata.oliveira@zinhoalimentos.com.br', '14077-440', 'Rua Marcelo Pinto de Moraes', '105', 'RibeirÃ£o Preto', 'SP', NULL, NULL, '06.268.413/0001-38', 'Parque Industrial Avelino Alves Palma', NULL, NULL, 4, '0'),
(15, 'DASP ', 'DASP SERVIÃ‡OS TEMPORARIOS', ' (11) 2093-086', NULL, NULL, '11 9.4005-7548', 'gerencia@dasp.com.br', '03087-020', 'Rua Jacinto JosÃ© de AraÃºjo', '335', 'SÃ£o Paulo', 'SP', NULL, NULL, '62.445.960/0001-12', 'Parque SÃ£o Jorge', NULL, NULL, 6, '0'),
(16, 'GAUPE COMPARTILHADO', 'GAUPE COMPARTILHADO', '(11) 5524-4983', NULL, NULL, '11 95252-8175', 'francisco@gauperh.com.br', ' 01023-00', 'Rua BarÃ£o de Duprat', '250', 'SÃƒO PAULO', 'SP', NULL, NULL, '14.393.589/0001-40', 'SANTO AMARO', NULL, NULL, 6, '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Contas_pagar`
--

CREATE TABLE IF NOT EXISTS `Contas_pagar` (
  `CONP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONP_Valor` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `conp_data_vencimento` datetime DEFAULT NULL,
  `CONP_Cliente_ID` int(11) DEFAULT NULL,
  `CONP_Descricao` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `CONP_Status` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `CONP_Data_Pagamento` datetime DEFAULT NULL,
  `bit_deletado` varchar(2) COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`CONP_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Contas_pagar`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Formas_recebimento`
--

CREATE TABLE IF NOT EXISTS `Formas_recebimento` (
  `FORR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `FORR_Nome` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) COLLATE latin1_general_ci DEFAULT '0',
  `FORR_Servico_ID` int(11) DEFAULT NULL,
  `FORR_Valor` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `FORR_Status` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `FORR_Obs` longtext COLLATE latin1_general_ci,
  `FORR_Loja` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `FORR_notaFiscal` varchar(35) COLLATE latin1_general_ci DEFAULT NULL,
  `FORR_Data` date DEFAULT NULL,
  `FORR_Rede_ID` int(11) DEFAULT NULL,
  `FORR_Filial_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`FORR_ID`),
  KEY `FORR_Servico_ID` (`FORR_Servico_ID`),
  KEY `FORR_Filial_ID` (`FORR_Filial_ID`),
  KEY `FORR_Rede_ID` (`FORR_Rede_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Formas_recebimento`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Jobs`
--

CREATE TABLE IF NOT EXISTS `Jobs` (
  `JOB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `JOB_Cliente_ID` int(11) DEFAULT NULL,
  `JOB_Rede_ID` int(11) DEFAULT NULL,
  `JOB_Filial_ID` int(11) DEFAULT NULL,
  `JOB_Promotor_ID` int(11) DEFAULT NULL,
  `JOB_Data_Job` date DEFAULT NULL,
  `JOB_Hora_Job` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `JOB_Foto` varchar(52000) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `JOB_Cadastrador_ID` int(11) DEFAULT NULL,
  `JOB_Valor` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `JOB_Status` varchar(11) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0',
  `JOB_Data_Hora_Input` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `JOB_Obs` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  PRIMARY KEY (`JOB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Jobs`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Mensagens`
--

CREATE TABLE IF NOT EXISTS `Mensagens` (
  `MSG_ID` int(11) NOT NULL AUTO_INCREMENT,
  `MSG_De_ID` int(11) DEFAULT NULL,
  `MSG_Para_ID` int(11) DEFAULT NULL,
  `MSG_Status` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `MSG_Data_Envio` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `MSG_Data_Leitura` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `MSG_Mensagem` longtext CHARACTER SET latin1 COLLATE latin1_general_ci,
  `MSG_Prioridade` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`MSG_ID`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Mensagens`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Promotores`
--

CREATE TABLE IF NOT EXISTS `Promotores` (
  `PRO_ID` int(11) NOT NULL AUTO_INCREMENT,
  `PRO_Nome` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `PRO_Telefone` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `PRO_Status` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `PRO_Coordenador_ID` int(11) DEFAULT NULL,
  `PRO_Banco` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `PRO_Agencia` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `PRO_Conta` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`PRO_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Promotores`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Rede`
--

CREATE TABLE IF NOT EXISTS `Rede` (
  `RED_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RED_Nome` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`RED_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=83 ;

--
-- Extraindo dados da tabela `Rede`
--

INSERT INTO `Rede` (`RED_ID`, `RED_Nome`, `bit_deletado`) VALUES
(1, 'WALMART', '0'),
(2, 'TENDA ATACADO', '0'),
(3, 'ASSAI ATACADO', '0'),
(4, 'SÃƒO VICENTE', '0'),
(5, 'ARENA ATACADO', '0'),
(6, 'ATACADÃƒO', '0'),
(7, 'CARREFOUR', '0'),
(8, 'CONFIANÃ‡A MAX', '0'),
(9, 'EXTRA', '0'),
(10, 'JAU SERVE', '0'),
(11, 'PAULISTÃƒO', '0'),
(12, 'TAUSTE', '0'),
(13, 'TONIN', '0'),
(14, 'MAXXI', '0'),
(15, 'COMERCIAL ESPERANÃ‡A', '0'),
(16, 'EXTRA', '0'),
(17, 'FLEX ATACADO', '0'),
(18, 'MAKRO ATACADO', '0'),
(19, 'PAGUE MENOS', '0'),
(20, 'ROLDÃƒO ATACADO', '0'),
(21, 'SAVEGNAGO', '0'),
(22, 'PÃƒO DE AÃ‡UCAR', '0'),
(23, 'GOOD BOM', '0'),
(24, 'SEMPRE VALE', '0'),
(25, 'COVABRA', '0'),
(26, 'AKKI ATACADO', '0'),
(27, 'LAVAPÃ‰S', '0'),
(28, 'BIG BOM', '0'),
(29, 'DELTA ', '0'),
(30, 'ENXUTO', '0'),
(31, 'DALBEM', '0'),
(32, 'GRAAL ', '0'),
(33, 'SONDA', '0'),
(34, 'SAMÂ´S CLUB', '0'),
(35, 'PÃƒO DE MEL', '0'),
(36, 'LOPES', '0'),
(37, 'MERCADO TAQUARAL', '0'),
(38, 'BOA', '0'),
(39, 'CAETANO', '0'),
(40, 'IGA', '0'),
(41, 'MULFATTO', '0'),
(42, 'OBA HORTI FRUT', '0'),
(43, 'SM SÃƒO ROQUE', '0'),
(44, 'ZEFERINO', '0'),
(45, 'GALASSI', '0'),
(46, 'COCERQUI', '0'),
(47, 'COOPIDEAL', '0'),
(48, 'COOP', '0'),
(49, 'TONIN ATACADISTA', '0'),
(50, 'ANTONELLI', '0'),
(51, 'ROFATTO', '0'),
(52, 'DELALANA', '0'),
(53, 'PÃ“NTO NOVO', '0'),
(54, 'SEMAR', '0'),
(55, 'MAXIMO', '0'),
(56, 'PINHEIRÃƒO', '0'),
(57, 'GIGA', '0'),
(58, 'UNIÃƒO', '0'),
(59, 'INFANGER', '0'),
(60, 'CREMA', '0'),
(61, 'CATO', '0'),
(62, 'SUMER BOL', '0'),
(63, 'AMIGÃƒO', '0'),
(64, 'IQUEGAMI', '0'),
(65, 'CATRICALA', '0'),
(66, 'NUTRISAN', '0'),
(67, 'MARANHÃƒO', '0'),
(68, 'LARANJÃƒO', '0'),
(69, 'PORECATU', '0'),
(70, 'PROENÃ‡A', '0'),
(71, 'ANTUNES', '0'),
(72, 'BORBOM', '0'),
(73, 'BRETAS', '0'),
(74, 'COOPERCICA', '0'),
(75, 'CUBATÃƒO', '0'),
(76, 'KURODA', '0'),
(77, 'LOUVEIRA', '0'),
(78, 'MENK', '0'),
(79, 'PARAZZE', '0'),
(80, 'TOKIO', '0'),
(81, 'VENCEDOR', '0'),
(82, 'ZAFFARI', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Tipos_servicos`
--

CREATE TABLE IF NOT EXISTS `Tipos_servicos` (
  `TIPS_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TIPS_Nome` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`TIPS_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Tipos_servicos`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `Usuarios`
--

CREATE TABLE IF NOT EXISTS `Usuarios` (
  `Usr_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Usr_Nome` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Login` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Senha` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Nivel` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Realiza_Audiencia` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Email` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Telefone` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Skype` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Data_Nascimento` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Avatar` varchar(200) COLLATE latin1_general_ci DEFAULT NULL,
  `Usr_Status` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`Usr_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=12 ;

--
-- Extraindo dados da tabela `Usuarios`
--

INSERT INTO `Usuarios` (`Usr_ID`, `Usr_Nome`, `Usr_Login`, `Usr_Senha`, `Usr_Nivel`, `Usr_Realiza_Audiencia`, `Usr_Email`, `Usr_Telefone`, `Usr_Skype`, `Usr_Data_Nascimento`, `Usr_Avatar`, `Usr_Status`, `bit_deletado`) VALUES
(1, 'ROBSON LEME', 'Robson', 'c77g68bk', 'Admin', 'NULL', 'robson.rocha@mnti.com.br', '(19) 99152-4209', 'medianewsit', '27/09/1985', 'NULL', 'Ativo', '0'),
(2, 'MARCIO BIZARRO', 'MARCIO', '328463577', 'Admin', 'NULL', 'marcio@eccotrade.com.br', '', '', '', 'NULL', 'Ativo', '0'),
(3, 'SILVANA GUEDES', 'SILVANA', '@GUEDES', 'Admin', 'NULL', 'silvana@eccotrade.com.br', 'NULL', 'NULL', ' NULL', 'NULL', 'Ativo', '0'),
(4, 'NATALIA FONSECA', 'NATALIA', '@FONSECA', 'Coordenador', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'Ativo', '0'),
(5, 'Administrador', 'admin', 'eccotrade2018', 'Admin', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'NULL', 'Ativo', '0'),
(6, 'LARISSA BISCOLA', 'LARISSA', '@BISCOLA', 'Coordenador', NULL, 'larissa@eccotrade.com.br', NULL, NULL, NULL, NULL, 'Ativo', '0'),
(7, 'VICTOR ALMEIDA', 'VICTOR', '@ALMEIDA', 'Coordenador', NULL, NULL, NULL, NULL, NULL, NULL, 'Ativo', '0'),
(8, 'WESLEY GALVAO', 'WESLEY', '@GALVAO', 'Coordenador', NULL, NULL, NULL, NULL, NULL, NULL, 'Ativo', '0'),
(9, 'SARA PRADO', 'SARA', '@PRADO', 'Coordenador', NULL, NULL, NULL, NULL, NULL, NULL, 'Ativo', '0'),
(10, 'NATHALIA RODRIGUES', 'NATHALIA', '@RODRIGUES', 'Coordenador', NULL, NULL, NULL, NULL, NULL, NULL, 'Ativo', '0'),
(11, 'MARLENE BOTELHO', 'MARLENE', '@BOTELHO', 'Coordenador', NULL, NULL, NULL, NULL, NULL, NULL, 'Ativo', '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `Valores_job`
--

CREATE TABLE IF NOT EXISTS `Valores_job` (
  `VJOB_ID` int(11) NOT NULL AUTO_INCREMENT,
  `VJOB_Cliente_ID` int(11) DEFAULT NULL,
  `VJOB_Rede_ID` int(11) DEFAULT NULL,
  `VJOB_Filial_ID` int(11) DEFAULT NULL,
  `VJOB_Valor` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`VJOB_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `Valores_job`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `configs`
--

CREATE TABLE IF NOT EXISTS `configs` (
  `Conf_ID` int(11) NOT NULL AUTO_INCREMENT,
  `Conf_Empresa` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Conf_CNPJ` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Conf_Endereco` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Conf_Telefone` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Conf_Email` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `Conf_System_Color` int(11) DEFAULT NULL,
  `bit_deletado` varchar(2) COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`Conf_ID`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=3 ;

--
-- Extraindo dados da tabela `configs`
--

INSERT INTO `configs` (`Conf_ID`, `Conf_Empresa`, `Conf_CNPJ`, `Conf_Endereco`, `Conf_Telefone`, `Conf_Email`, `Conf_System_Color`, `bit_deletado`) VALUES
(1, 'Ecco Trade Merchandising', '999999999', 'Av. Amoreiras', '3244-5555', 'contato@advocaciaraujo.com.br', 1, '0');

-- --------------------------------------------------------

--
-- Estrutura da tabela `inativacao_promotores`
--

CREATE TABLE IF NOT EXISTS `inativacao_promotores` (
  `IP_ID` int(11) NOT NULL AUTO_INCREMENT,
  `IP_Promotor_ID` int(11) DEFAULT NULL,
  `IP_Solicitante_ID` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `IP_Efetivador_ID` int(11) DEFAULT NULL,
  `IP_Data_Solicitacao` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `IP_Hora_Solicitacao` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `IP_Data_Efetivacao` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `IP_Hora_Efetivacao` varchar(45) COLLATE latin1_general_ci DEFAULT NULL,
  `IP_Status` varchar(45) COLLATE latin1_general_ci DEFAULT 'Pendente',
  `bit_deletado` varchar(2) COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`IP_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `inativacao_promotores`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `otr`
--

CREATE TABLE IF NOT EXISTS `otr` (
  `OTR_ID` int(11) NOT NULL AUTO_INCREMENT,
  `OTR_Anexo` varchar(100) DEFAULT NULL,
  `OTR_Descricao` varchar(255) DEFAULT NULL,
  `OTR_Data_Cadastro` varchar(50) DEFAULT NULL,
  `OTR_Data_Ocorrencia` varchar(50) DEFAULT NULL,
  `OTR_Coordenador_ID` int(11) NOT NULL,
  `OTR_Usuario_ID` int(11) NOT NULL,
  `bit_deletado` varchar(2) DEFAULT '0',
  `OTR_Loja` varchar(50) DEFAULT NULL,
  `OTR_Filial_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`OTR_ID`),
  KEY `OTR_Filial_ID` (`OTR_Filial_ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

--
-- Extraindo dados da tabela `otr`
--


-- --------------------------------------------------------

--
-- Estrutura da tabela `rede_filial`
--

CREATE TABLE IF NOT EXISTS `rede_filial` (
  `RFIL_ID` int(11) NOT NULL AUTO_INCREMENT,
  `RFIL_Rede_ID` int(11) DEFAULT NULL,
  `RFIL_Nome` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Encarregado` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Telefone` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_CEP` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Endereco` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Bairro` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Cidade` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Estado` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Numero` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `RFIL_Complemento` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `bit_deletado` varchar(2) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT '0',
  PRIMARY KEY (`RFIL_ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=40 ;

--
-- Extraindo dados da tabela `rede_filial`
--

INSERT INTO `rede_filial` (`RFIL_ID`, `RFIL_Rede_ID`, `RFIL_Nome`, `RFIL_Encarregado`, `RFIL_Telefone`, `RFIL_CEP`, `RFIL_Endereco`, `RFIL_Bairro`, `RFIL_Cidade`, `RFIL_Estado`, `RFIL_Numero`, `RFIL_Complemento`, `bit_deletado`) VALUES
(1, 19, 'PAGUE MENOS JD. EUROPA', 'Benedito', '(19) 3 3072 436', '13050-120', 'Rua Augusto de Carvalho Asbahr', 'Jardim Santa AmÃ¡lia', 'Campinas', 'SP', '150', NULL, '1'),
(2, 1, 'WALLMART AMOREIRAS', 'NÃ£o sei', '(19) 9 9999 999', '13050-045', 'Rua das DÃ¡lias', 'Vila Mimosa', 'Campinas', 'SP', '10', NULL, '1'),
(3, 2, 'JUNDIAI', '  (11) 3010-3414', '  (11) 3010-3414', '13209-430', 'Avenida Prefeito LuÃ­s Latorre', 'Vila das HortÃªncias', 'JundiaÃ­', 'SP', '4860', NULL, '0'),
(4, 2, 'AMOREIRAS ', ' ', '19) 3772-4999', '13030-680', 'Rua JoÃ£o Felipe Xavier da Silva', 'SÃ£o Bernardo', 'Campinas', 'SP', '68-190', NULL, '0'),
(5, 2, 'CARAPICUIBA', ' ', '11) 3556-6200', '06328-330', 'Avenida Desembargador Doutor Eduardo Cunha de', 'Vila Municipal', 'CarapicuÃ­ba', 'SP', '495', NULL, '0'),
(6, 2, 'GUARULHOS MATRIZ', ' ', '11) 2489-2900', '07243-580', 'Rua Professor JoÃ£o Cavaleiro SalÃ©m', 'Parque das NaÃ§Ãµes', 'Guarulhos', 'SP', '365', NULL, '0'),
(7, 2, 'GUARAPIRANGA', ' ', ' (11) 2124-8181', '04762-001', 'Avenida Guarapiranga', 'Vila Socorro', 'SÃ£o Paulo', 'SP', '900', NULL, '0'),
(8, 2, 'DOM PEDRO CEASA', ' ', '19) 3716-8700', '13080-395', 'Rodovia Dom Pedro I', 'Jardim Santa Genebra', 'Campinas', 'SP', 'SN', NULL, '0'),
(9, 2, 'DIADEMA', ' ', '11) 4070-6310', '09941-202', 'Avenida FÃ¡bio Eduardo Ramos Esquivel', 'Canhema', 'Diadema', 'SP', '3000', NULL, '0'),
(10, 2, 'AV ATLÃ‚NTICA SP', ' ', '11) 5693-5844', '04768-100', 'Avenida AtlÃ¢ntica', 'Socorro', 'SÃ£o Paulo', 'SP', '1000', NULL, '0'),
(11, 2, 'INDAIATUBA', ' ', '(19) 3825-8888', '13344-580', 'Alameda Filtros Mann', 'Jardim Tropical', 'Indaiatuba', 'SP', '670', NULL, '0'),
(12, 2, 'HORTOLANDIA', ' ', ' (19) 3865-8210', '13184-273', 'Rua Pietro Rocchi', 'Jardim das Paineiras', 'HortolÃ¢ndia', 'SP', '05', NULL, '0'),
(13, 0, 'SUMARÃ‰', ' ', '19) 3883-8585', '13175-115', 'Rua PetrÃ³polis', 'Jardim Bela Vista', 'SumarÃ©', 'SP', 'SN', NULL, '0'),
(14, 2, 'SANTA BARBARA DÂ´ OSTE', ' ', '(19) 3457-9777', '13456-625', 'Rua do Osmio', 'Jardim Mollon', 'Santa BÃ¡rbara D''Oeste', 'SP', '915', NULL, '0'),
(15, 2, 'ITU', ' ', ' (11) 2118-0200', '13313-100', 'Avenida Nove de Julho', 'Jardim Padre Bento', 'Itu', 'SP', '897', NULL, '0'),
(16, 2, 'SALTO', ' ', '(11) 4602-8010', '13324-240', 'Rodovia da ConvenÃ§Ã£o', 'Salto de SÃ£o JosÃ©', 'Salto', 'SP', '760 ', NULL, '0'),
(17, 2, 'SÃƒO JODE DOS CAMPOS', ' ', '(12) 3212-0133', '12230-000', 'Avenida AndrÃ´meda', 'Jardim SatÃ©lite', 'SÃ£o JosÃ© dos Campos', 'SP', '200 ', NULL, '0'),
(18, 2, 'BOM SUCESSO', ' ', ': (11) 2489-2700', ' 07252000', ' Estr. Juscelino Kubtischek de Oliveira', 'BOM SUCESSO', 'GUARULHOS', 'SP', '5308 ', NULL, '0'),
(19, 2, 'PIRACICABA', ' ', '(19) 3124-3640', '13420-823', 'Rua Guerino Lubiani', 'Dois CÃ³rregos', 'Piracicaba', 'SP', '770', NULL, '0'),
(20, 2, 'VILA GALVÃƒO GUARULHOS', ' ', ' (11) 3010-3404', '07074000.', 'Avenida Pedro de Souza Lopes', 'Vila GalvÃ£o', 'Guarulhos', 'SP', '900', NULL, '0'),
(21, 2, 'JUNDIAI', ' ', '(11) 3010-3414', '13209-430', 'Avenida Prefeito LuÃ­s Latorre', 'Vila das HortÃªncias', 'JundiaÃ­', 'SP', '4860', NULL, '1'),
(22, 2, 'BAURU', '  ', '(14) 3235-2020 /', '17020-310', 'Avenida Nuno de Assis', 'Centro', 'Bauru', 'SP', '21-100', NULL, '1'),
(23, 2, 'BOTUCATU', ' ', '14) 3235-2020', '17020-310', 'Avenida Nuno de Assis', 'Centro', 'Bauru', 'SP', 'SN', NULL, '0'),
(24, 2, 'GUARETINGUETA', '  ', '12) 3128-5100', '12505-300', 'Avenida Juscelino Kubitschek de Oliveira', 'Campo do GalvÃ£o', 'GuaratinguetÃ¡', 'SP', ' 351', NULL, '0'),
(25, 2, 'TAUBATE', ' ', '12) 3624-2762', '12040-000', 'Avenida Charles Schnneider', 'Parque Senhor do Bonfim', 'TaubatÃ©', 'SP', '850', NULL, '0'),
(26, 2, 'JACAREI', ' ', '12) 2127-2100 ', '12306-090', 'PraÃ§a Charles Gates', 'Jardim das IndÃºstrias', 'JacareÃ­', 'SP', '90', NULL, '0'),
(27, 2, 'BAURU', ' ', '14) 3221-3450 ', ' 17020-31', 'End: Av. Nuno de Assis, 19-100 Comercio 1', 'CENTRO', 'BAURU', 'SP', '19-100', NULL, '0'),
(28, 2, 'MOGI GUAÃ‡U', '  ', ' (19) 3811-8500 ', '13845-180', 'Rua Francisco Franco de Godoy Bueno', 'ImÃ³vel Pedregulhal', 'Mogi GuaÃ§u', 'SP', '801', NULL, '0'),
(29, 2, 'SÃƒO CARLOS', ' ', '16) 3366-3214', '13571-513', 'Estrada Municipal Rubens Fernando Monte Ribei', 'Jardim Novo Horizonte', 'SÃ£o Carlos', 'SP', '1 PARTE A', NULL, '0'),
(30, 2, 'ITANHAEM', ' ', '(13) 3421-1770', '11740-000', 'End: R JosÃ© Ernesto Bechelli ', 'JD SABAUNA', 'ItanhaÃ©m', 'SP', '1351', NULL, '0'),
(31, 2, 'PINDAMONHAGABA', ' ', ' (12) 3644-210', '12411-010', 'Avenida Manoel CÃ©sar Ribeiro', 'Ipiranga', 'Pindamonhangaba', 'SP', '321', NULL, '0'),
(32, 2, 'RIBEIRÃƒO PRETO', ' ', '0', '14080-130', 'Avenida Marechal Costa e Silva', 'Campos ElÃ­seos', 'RibeirÃ£o Preto', 'SP', '1231', NULL, '0'),
(33, 2, 'SÃƒO JOSE RIO PRETO', ' ', '0', '14080-130', 'Avenida Marechal Costa e Silva', 'Campos ElÃ­seos', 'RibeirÃ£o Preto', 'SP', '4775', NULL, '0'),
(34, 2, 'ITAQUAQUECETUBA', ' ', '0', '08577-000', 'Estrada SÃ£o Paulo-Mogi', 'Vila Monte Belo', 'Itaquaquecetuba', 'SP', 'SN', NULL, '0'),
(35, 2, 'FERRAZ DE VASCONSELOS', ' ', '0', '08527-000', 'Avenida Governador JÃ¢nio Quadros', 'Parque Dourado', 'Ferraz de Vasconcelos', 'SP', '51', NULL, '0'),
(36, 2, 'SUZANO', ' ', '0', '08675-000', 'Rua Baruel', 'Vila Costa', 'Suzano', 'SP', '315', NULL, '0'),
(37, 2, 'PRES. DUTRA', ' ', '0', '07171-000', 'Rua Bela Vista do ParaÃ­so', 'Jardim Presidente Dutra', 'Guarulhos', 'SP', '1901', NULL, '0'),
(38, 2, 'SÃƒO MATHEUS', ' ', '0', '07171-000', 'Rua Bela Vista do ParaÃ­so', 'Jardim Presidente Dutra', 'SÃƒO MATHEUS', 'SP', '195', NULL, '0'),
(39, 2, 'PRAIA GRANDE', ' ', '0', '11718-180', 'Rua SavÃ©rio Fittipaldi', 'Quietude', 'Praia Grande', 'SP', '150', NULL, '0');

--
-- Restrições para as tabelas dumpadas
--

--
-- Restrições para a tabela `Aux_Rede_Filial`
--
ALTER TABLE `Aux_Rede_Filial`
  ADD CONSTRAINT `Aux_Promotor_ID` FOREIGN KEY (`Aux_Promotor_ID`) REFERENCES `Promotores` (`PRO_ID`),
  ADD CONSTRAINT `Aux_Filial_ID` FOREIGN KEY (`Aux_Filial_ID`) REFERENCES `rede_filial` (`RFIL_ID`),
  ADD CONSTRAINT `Aux_Rede_ID` FOREIGN KEY (`Aux_Rede_ID`) REFERENCES `Rede` (`RED_ID`);

--
-- Restrições para a tabela `otr`
--
ALTER TABLE `otr`
  ADD CONSTRAINT `otr_ibfk_1` FOREIGN KEY (`OTR_Filial_ID`) REFERENCES `rede_filial` (`RFIL_ID`);
