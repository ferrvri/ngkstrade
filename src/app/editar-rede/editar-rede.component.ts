import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-editar-rede',
  templateUrl: './editar-rede.component.html',
  styleUrls: ['./editar-rede.component.css']
})
export class EditarRedeComponent implements OnInit {

  rede: any = [];

  nomeRede: string = "";

  errorNomeRede = { required: null };

  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _activedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this._activedRoute.paramMap.subscribe((data) => {
      this.rede.push(data);
    });

    this._http.post(
      'selectRedes.php',
      {
        unique: true,
        id: this.rede[0].params.id
      }
    ).subscribe((response) => {
      console.log(response);
      this.nomeRede = response.result[0].RED_Nome;
    });
  }

  cadRede(): void {
    let confirmation = window.confirm("KSTrade - Editar Rede\r\n\r\nDeseja realmente editar?");
    if (this.nomeRede.length < 1) {
      this.errorNomeRede.required = true;
    } else {
      if (confirmation == true) {
        this._http.post(
          'editRede.php',
          {
            id: this.rede[0].params.id,
            nome: this.nomeRede,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Rede[' + this.rede[0].params.id + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert("KSTrade - Edição de Rede\r\n\r\nRede editada com sucesso!");
            this._router.navigate(['/main/listaRedes']);
          } else if (response.status == '0x102') {
            window.alert('KSTrade - Edição de Rede\r\n\r\nJá existe uma rede com esse nome.');
          } else {
            window.alert("Erro inesperado!");
          }
        });
      }
    }
  }

}
