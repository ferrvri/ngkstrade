import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditarRedeComponent } from './editar-rede.component';

describe('EditarRedeComponent', () => {
  let component: EditarRedeComponent;
  let fixture: ComponentFixture<EditarRedeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditarRedeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditarRedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
