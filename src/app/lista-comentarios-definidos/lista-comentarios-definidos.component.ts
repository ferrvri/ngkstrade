import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-lista-comentarios-definidos',
  templateUrl: './lista-comentarios-definidos.component.html',
  styleUrls: ['./lista-comentarios-definidos.component.css']
})
export class ListaComentariosDefinidosComponent implements OnInit {

  comentarios = []
  p: number = 0;

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService,) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this._http.post(
      'selectComentariosDefinidos.php',
      {
        all: true
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.comentarios = response.result
      }
    });
  }

  deleteComentario(c, index){
    let cc = confirm("KSTrade Merchandising - Lista de Comentarios\r\n\r\nDeseja realmente remover o comentario?");
    if (cc == true){
      this._http.post(
        'removeComentarioDefinido.php',
        {
          id: c.comdef_id,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Comentario pre-definido[' + c.comdef_id + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104'){
          this.comentarios.splice(index, 1);
        }else{
          window.alert('KSTrade Merchandising - Lista de Comentarios\r\n\r\nErro ao remover comentario! Por favor contate-nos')
        }
      })
    }
  }

}
