import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaComentariosDefinidosComponent } from './lista-comentarios-definidos.component';

describe('ListaComentariosDefinidosComponent', () => {
  let component: ListaComentariosDefinidosComponent;
  let fixture: ComponentFixture<ListaComentariosDefinidosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaComentariosDefinidosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaComentariosDefinidosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
