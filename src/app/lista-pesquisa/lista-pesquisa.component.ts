import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { elementAttribute } from '@angular/core/src/render3/instructions';
import { GeolocationServiceService } from '../geolocation-service.service';
import { ExcelService } from '../excel.service';
declare var $: any;

@Component({
  selector: 'app-lista-pesquisa',
  templateUrl: './lista-pesquisa.component.html',
  styleUrls: ['./lista-pesquisa.component.css']
})

export class ListaPesquisaComponent implements OnInit {
  p: number = 0;
  p2: number = 0;

  pageParams: any;

  dict = {
    pes_data2: "Data realizacao",
    PRO_Nome: "Promotor",
    RED_Nome: "Rede",
    RFIL_Nome: "Filial",
    pes_prateleira: "Em Area de venda?",
    pes_estoque: "Em Estoque?",
    pes_rupturatotal: "Em Ruptura total?",
    pes_produto_id: "Produto"
  }

  userData: any = [];
  pesquisas = [];
  resultsFilterPesquisas = [];

  emptyResult: boolean = false;
  alreadySearch: boolean = false;
  onlyMarket: boolean = false;
  selectAll: boolean = false;
  alreadyFiltered: boolean = false;

  latLong = '';
  constructor(
    private _excel: ExcelService,
    private _geo: GeolocationServiceService,
    private _http: HttpService,
    private _router: Router,
    private _activatedRoute: ActivatedRoute) { }

  daysBetween(date1, date2) {
    let one_day = 1000 * 60 * 60 * 24;
    let date1_ms = new Date(date1).getTime();
    let date2_ms = new Date(date2).getTime();
    let difference_ms = date2_ms - date1_ms;

    date2 = date2.split('-')[0] + '-' + date2.split('-')[2] + '-' + date2.split('-')[1];

    return Math.round(difference_ms / one_day);
  }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat + ';' + c.lng)
    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);

    $('#dataInit').datepicker();
    $('#dataEnd').datepicker();

    if (this.pageParams.params.forId && this.pageParams.params.forId.length > 0) {
      this._http.post(
        'selectPesquisas_2.php',
        {
          cliente: this.pageParams.params.forId,
          dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
          datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.resultsFilterPesquisas = this.pesquisas = response.result
        }
      });
    } else {
      if (this.userData[0].Usr_Nivel == 'Coordenador') {
        this._http.post(
          'selectPesquisas_2.php',
          {
            clientes: JSON.parse(localStorage.getItem('clientes')),
            dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
            datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.resultsFilterPesquisas = this.pesquisas = response.result
          }
        });
      } else {
        this._http.post(
          'selectPesquisas_2.php',
          {
            dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
            datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.resultsFilterPesquisas = this.pesquisas = response.result;
          }
        });
      }
    }
  }

  clearData() {
    this.alreadySearch = false;
    this.resultsFilterPesquisas = this.pesquisas;
    (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
  }

  setDateOne() {
    if ((<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataInit')).value =
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2];
    }
  }

  setDateTwo() {
    if ((<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataEnd')).value =
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2];
    }
  }

  removeProduto(fc) {
    let c = confirm('KSTrade Merchandising - Pesquisas\r\n\r\nDeseja realmente remover este produto?\n\nIsto apagará os demais registros de pesquisa');
    if (c == true) {
      this._http.post(
        'removeProdutoPesquisa.php',
        {
          pro_id: fc.pro_id,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Produto Pesquisa[' + fc.pro_id + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising - Pesquisas\r\n\r\nRemovido com sucesso!');
          this.resultsFilterPesquisas = [];
          this.pesquisas = [];
          this.ngOnInit();
        }
      });
    }
  }

  searchPesquisas() {
    this.resultsFilterPesquisas = [];
    this.alreadySearch = true;
    if (this.userData[0].Usr_Nivel == 'Coordenador') {
      JSON.parse(localStorage.getItem('clientes')).forEach(e1 => {
        this._http.post(
          'selectPesquisas.php',
          {
            cliente: e1.Cli_ID
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            response.result.forEach(element => {
              element.content.forEach(element2 => {
                element2.pesquisas = [];
                this._http.post(
                  'selectPesquisas.php',
                  {
                    pesquisa: true,
                    pro_id: element2.pro_id,
                    dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2]
                      + '-' + (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1]
                      + '-' + (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
                    datatwo:
                      (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2]
                      + '-' + (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1]
                      + '-' + (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
                  }
                ).subscribe((response2) => {
                  if (response2.status == '0x104') {
                    response2.result.forEach(element3 => {
                      element2.pesquisas.push(element3);
                    });
                  }
                });
              });

              this.resultsFilterPesquisas.push(element);
            });
          }
        });
      });
    } else {
      this._http.post(
        'selectPesquisas.php',
        {
          // type: 'jobs'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          response.result.forEach(element => {
            element.content.forEach(element2 => {
              element2.pesquisas = [];
              this._http.post(
                'selectPesquisas.php',
                {
                  pesquisa: true,
                  pro_id: element2.pro_id,
                  dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2]
                    + '-' + (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1]
                    + '-' + (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
                  datatwo:
                    (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2]
                    + '-' + (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1]
                    + '-' + (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
                }
              ).subscribe((response2) => {
                if (response2.status == '0x104') {
                  response2.result.forEach(element3 => {
                    element2.pesquisas.push(element3);
                  });
                }
              });
            });

            this.resultsFilterPesquisas.push(element);
          });
        }
      });
    }
  }

  queryClientes: string = '';

  filterArrayClientes(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterPesquisas = arr.filter((e) => {
      return regexp.test(e.cliente_nome);
    });
  }

  searchClientes(string) {
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
      this.alreadyFiltered = false;
    } else {
      this.filterArrayClientes(string, this.pesquisas);
    }
  }

  queryProdutos: string = '';

  filterArrayProdutos(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach(element2 => {
        if (regexp.test(element2.pro_nome)) {
          o.content.push(element2);
        }
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  searchProdutos(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayProdutos(string, this.pesquisas);
    }
  }

  searchPesquisaRede(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaRedes(string, this.pesquisas);
    }
  }

  queryPesquisaRedes: string = '';

  filterArrayPesquisaRedes(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana,
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RED_Nome)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  searchPesquisaFilial(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaFilial(string, this.pesquisas);
    }
  }

  queryPesquisaFilial: string = '';

  filterArrayPesquisaFilial(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana,
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RFIL_Nome)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  queryPesquisaCidade: string = '';

  searchPesquisaCidade(string) {
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaCidade(string, this.pesquisas);
    }
  }

  filterArrayPesquisaCidade(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana || ";",
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RFIL_Cidade)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  onlyCrt: boolean = false;

  onlyCritic() {
    if (this.onlyMarket) {
      this.onlyMarket = false;
      this.resultsFilterPesquisas = this.pesquisas
    }
    if (!this.onlyCrt) {
      this.onlyCrt = true;
      this.resultsFilterPesquisas = [];
      this.pesquisas.forEach((element) => {
        let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
        element.content.forEach((element2, index) => {
          o.content.push({
            pesquisas: [],
            pro_dias_semana: element2.pro_dias_semana || ";",
            pro_id: element2.pro_id,
            pro_nome: element2.pro_nome
          });

          element2.pesquisas.forEach(element3 => {
            if (element3.hasDif) {
              console.log('has dif', element3);
              o.content[index].pesquisas.push(element3);
            } else {
              console.log('no dif', element3);
            }
          });
        });
        this.resultsFilterPesquisas.push(o);
      });
      console.log(this.resultsFilterPesquisas);
    } else {
      this.onlyCrt = false;
      this.resultsFilterPesquisas = this.pesquisas;
    }
  }

  sendCritcEmail(ff, f) {
    let c = confirm('KSTrade Merchandising\r\n\r\nDeseja enviar o e-mail');
    if (c == true) {
      this._http.post(
        'enviarCriticEmail.php',
        {
          rede: f.RED_ID,
          redeNome: f.RED_Nome,
          filial: f.RFIL_ID,
          filialNome: f.RFIL_Nome,
          cliente: f.JOB_Cliente_ID,
          "obj": { "produto": ff.pro_nome, "rede": f.RED_Nome, "filial": f.RFIL_Nome, "datavencimento": f.pes_data_vencimento, "quantidade": f.pes_quantidade, "nfrentes": f.pes_numero_frentes }
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising - Lista de Pesquisa\r\n\r\nE-mail enviado com sucesso!');
        } else {
          window.alert('KSTrade Merchandising - Lista de Pesquisa\r\n\r\nUm erro ocorreu por favor contate-nos!');
        }
      });
    }
  }

  deletePesquisa(infos) {
    let c = confirm('KSTrade Merchandising - Lista de Pesquisa\r\n\r\nDeseja realmente remover esta pesquisa?');
    if (c == true) {
      this._http.post(
        'removePesquisa.php',
        {
          id: infos.pes_id,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Pesquisa[' + infos.pes_id + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising - Lista de Pesquisa\r\n\r\nRemovido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaPesquisa']);
          }, 1500);
        } else {
          window.alert('KSTrade Merchandising - Lista de Pesquisa\r\n\r\nProblemas ao remover, por favor contate-nos');
        }
      });
    }
  }

  deleteSelected() {
    if (this.selectAll) {
      let c = confirm('KSTrade Merchandising - Lista de pesquisa\r\n\r\nDeseja realmente apagar selecionados?');
      if (c == true) {
        let donear = [];

        this.resultsFilterPesquisas.forEach((element, index) => {
          element.content.forEach((element1, index1) => {
            element1.pesquisas.forEach((element2, index2) => {
              try {
                if ((<HTMLInputElement>document.getElementById('pesquisa-' + index + '-' + index1 + '-' + index2)).checked == true) {
                  this._http.post(
                    'removePesquisa.php',
                    {
                      id: element2.pes_id,

                      //log
                      usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
                      usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
                      usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
                      nome_pc: this._geo.GetComputerName(),
                      latlong: this.latLong,
                      funcao: 'Deleção - Pesquisa[' + element2.pes_id + ']'
                    }
                  ).subscribe((response) => {
                    if (response.status == '0x104') {
                      donear.push('true');
                    } else {
                      donear.push('false');
                    }
                  });
                }
              } catch (TypeError) {
                return;
              }
            });
          });
        });

        if ('false' in donear) {
          window.alert('KSTrade Merchandising - Lista de Pesquisa\r\n\r\nProblemas ao remover, por favor contate-nos');
        } else {
          window.alert('KSTrade Merchandising - Lista de Pesquisa\r\n\r\nRemovidos com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaPesquisa']);
          }, 1500);
        }
      }
    }
  }

  onlyMarketShare() {
    if (this.onlyCrt) {
      this.onlyCrt = false;
      this.resultsFilterPesquisas = this.pesquisas
    }
    if (!this.onlyMarket) {
      this.onlyMarket = true;
      this.resultsFilterPesquisas = [];
      this.pesquisas.forEach((element) => {
        let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
        element.content.forEach((element2, index) => {
          o.content.push({
            pesquisas: [],
            pro_dias_semana: element2.pro_dias_semana || ";",
            pro_id: element2.pro_id,
            pro_nome: element2.pro_nome
          });

          element2.pesquisas.forEach(element3 => {
            if ((parseInt(element3.pes_numero_frentes) > 0 && element3.pes_numero_frentes !== null && element3.pes_numero_frentes != 'null')
              &&
              (element3.pes_numero_max_frentes > 0 && element3.pes_numero_max_frentes !== null && element3.pes_numero_max_frentes != 'null')) {
              // ){
              element3.hasDif = false;
              element3.hasMarket = true;
              element3.marketNum = (element3.pes_numero_frentes * 100) / element3.pes_numero_max_frentes;
              o.content[index].pesquisas.push(element3);
              console.log(element3);
            } else {
              console.log('no dif', element3);
            }
          });
        });
        this.resultsFilterPesquisas.push(o);
      });
    } else {
      this.onlyMarket = false;
      this.ngOnInit();
    }
  }


  limparPesquisa(data) {
    let c = confirm('KSTrade Merchandising - Pesquisa\r\n\r\nDeseja realmente remover toda a pesquisa?')
    if (c == true) {
      let r = []
      data.content.forEach(element => {
        this._http.post(
          'removeProdutoPesquisa.php',
          {
            pro_id: element.pro_id,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Deleção - Produto pesquisa[' + element.pro_id + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            r.push(true)
          } else {
            r.push(false)
          }
        });
      });

      let x = {}
      try {
        r.forEach((e) => {
          if (e == false) {
            throw x;
          }
        });
        window.alert('KSTrade Merchandising - Pesquisas\r\n\r\nPesquisa removida com sucesso!')
        this.pesquisas = []
        this.ngOnInit()
      } catch (x) {
        window.alert('KSTrade Merchandising - Pesquisas\r\n\r\nErro ao remover pesquisa.')
      }
    }
  }

  makeExcel() {
    sweetAlert({
      title: 'Gerando excel...',
      icon: 'info',
      buttons: [
        {
          text: 'OK!',
          closeModal: false,
          visible: true,
        }
      ] as any,
      closeOnEsc: false,
      closeOnClickOutside: false,
      text: 'O sistema está gerando o Excel, pode levar alguns segundos...',
      timer: 4000
    }).then(() => {
      let r = [];
      r.push({ '': this.resultsFilterPesquisas[0].cliente_nome });

      this.resultsFilterPesquisas[0].content.forEach(element => {
        element.pesquisas.forEach(element2 => {
          let content = {}
          for (let k in element2) {
            if (
              (k.indexOf('pes') != -1 ||
                k.indexOf('PRO_Nome') != -1 ||
                k.indexOf('RED_Nome') != -1 ||
                k.indexOf('pes_produto_id') != -1 ||
                k.indexOf('RFIL_Nome') != -1) &&

              k.indexOf('pes_job') == -1 && k.indexOf('pro_id') == -1
            ) {

              if (element2[k] !== null && k != 'pes_data' && k != 'pesquisas' && k != 'pes_id' && k != 'pes_status' && k != 'pes_numero_frentes' && k != 'pes_numero_max_frentes') {
                if (k == 'pes_produto_id') {
                  content[this.dict[k]] = element.pro_id == element2[k] ? element.pro_nome : 'SEM PRODUTO'
                } else {
                  content[this.dict[k]] = element2[k] == 0 ? 'Não' : element2[k] == 1 ? 'Sim' : element2[k]
                }
              }
            }
          }
          r.push(content)
        });
      });

      setTimeout(() => {
        this._excel.exportAsExcelFile(r, 'Pesquisas' + new Date(Date.now()).toISOString().split('T')[0]);
      }, 2000)
    })
  }
}