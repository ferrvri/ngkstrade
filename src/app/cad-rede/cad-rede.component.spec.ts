import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadRedeComponent } from './cad-rede.component';

describe('CadRedeComponent', () => {
  let component: CadRedeComponent;
  let fixture: ComponentFixture<CadRedeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadRedeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadRedeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
