import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-cad-rede',
  templateUrl: './cad-rede.component.html',
  styleUrls: ['./cad-rede.component.css']
})
export class CadRedeComponent implements OnInit {

  nomeRede: string = "";
  errorNomeRede = { required: null };

  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition( c => this.latLong = c.lat+';'+c.long );
  }

  cadRede(): void {
    if (this.nomeRede.length < 1) {
      this.errorNomeRede.required = true;
    } else {
      this._http.post(
        'insertNovaRede.php',
        {
          nome: this.nomeRede,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Inserção - Rede[' + this.nomeRede + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert("KSTrade - Cadastro de Rede\r\n\r\nRede cadastrada com sucesso!");
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/cadastrarRede']);
          }, 1000); this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/cadastrarRede']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert("Erro inesperado!");
        } else if (response.status == '0x102') {
          window.alert('KSTrade - Cadastro de Rede\r\n\r\nRede já cadastrada');
        }
      });
    }
  }

}
