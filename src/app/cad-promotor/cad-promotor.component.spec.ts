import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadPromotorComponent } from './cad-promotor.component';

describe('CadPromotorComponent', () => {
  let component: CadPromotorComponent;
  let fixture: ComponentFixture<CadPromotorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadPromotorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadPromotorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
