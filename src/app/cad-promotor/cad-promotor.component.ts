import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-cad-promotor',
  templateUrl: './cad-promotor.component.html',
  styleUrls: ['./cad-promotor.component.css']
})
export class CadPromotorComponent implements OnInit {

  pageParams: any;

  isPj = false
  isFree = false

  filialIndex: number = 0;
  redIndex: number = 0;
  coordIndex: number = 0;

  coordenadores = [];
  redes = [];
  filiais = [];

  errorNome = { required: null };
  errorTelefoneMovel = { required: null };
  errorEmail = { required: null };
  errorStatus = { required: null };
  errorCoordenador = { required: null };
  errorBanco = { required: null };
  errorAgencia = { required: null };
  errorConta = { required: null };
  errorRede = { required: null };
  errorFilial = { required: null };

  nome: string = "";
  telefoneMovel: string = "";
  email: string = "";
  status: string = "Selecione";
  coordenador: string = "Selecione";
  banco: string = "Selecione";
  agencia: string = "";
  conta: string = "";
  rede: string = '';
  filial: string = '';

  statusOK: boolean = false;
  bancoOK: boolean = false;
  coordenadoresOK: boolean = false;
  redeOK: boolean = false;
  filialOK: boolean = false;

  redeIndex: number = 0;


  cadastroPromotor: FormGroup = new FormGroup({
    nome: new FormControl(this.nome, [Validators.required, Validators.minLength(6)]),
    telefoneMovel: new FormControl(this.telefoneMovel, [Validators.required, Validators.minLength(6)]),
    email: new FormControl(this.email, [Validators.required, Validators.minLength(6)]),
    status: new FormControl(this.status, [Validators.required, Validators.minLength(6)]),
    rede: new FormControl(this.rede, [Validators.required, Validators.minLength(6)]),
    filial: new FormControl(this.filial, [Validators.required, Validators.minLength(6)]),
    // coordenador : new FormControl(this.coordenador, [Validators.required, Validators.minLength(6)]),
    banco: new FormControl(this.banco, [Validators.required, Validators.minLength(6)]),
    agencia: new FormControl(this.agencia, [Validators.required, Validators.minLength(6)]),
    conta: new FormControl(this.conta, [Validators.required, Validators.minLength(6)])
  });


  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition( c => this.latLong = c.lat+';'+c.lng);
    
    $('#inputTelefoneMovelPromotor').mask('(00) 0 0000 0000');
    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar' && this.pageParams.params.proId.length > 0) {
      this._http.post(
        'selectPromotor.php',
        {
          id: this.pageParams.params.proId
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.nome = response.result[0].PRO_Nome;
          if (this.nome.indexOf('PJ') != -1) {
            this.isPj = true
          } else if (this.nome.indexOf('FREE') != -1) {
            this.isFree = true
          }
          // this.coordenador = response.result[0].Usr_Nome;
          // this.coordIndex = response.result[0].Usr_ID;
          this.rede = response.result[0].RED_Nome;
          this.redeIndex = response.result[0].RED_ID;
          this.filial = response.result[0].RFIL_Nome;
          this.filialIndex = response.result[0].RFIL_ID;
          this.conta = response.result[0].PRO_Conta;
          this.banco = response.result[0].PRO_Banco;
          this.agencia = response.result[0].PRO_Agencia;
          this.telefoneMovel = response.result[0].PRO_Telefone;
          this.status = response.result[0].PRO_Status;
          this.email = response.result[0].PRO_Email;
        } else if (response.status == '0x101') {
          window.alert('Erro na seleção de promotor!');
        }
      });
    }

    // this._http.post(
    //   'selectCoordenadores.php',
    //   {}
    // ).subscribe( (response) => {
    //   if (response.status == '0x104'){
    //     response.result.forEach(element => {
    //       this.coordenadores.push(element);
    //     });
    //   }else{
    //     window.alert('Não foi possível buscar os coordenadores disponiveis');
    //   }
    // });

    this._http.post(
      'selectRedes.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.redes.push(element);
        });
        this.resultFilterRedes = this.redes;
      } else {
        window.alert('Não foi possível buscar os coordenadores disponiveis');
      }
    });
  }

  validation() {
    try {
      if (this.nome.length < 1) {
        this.errorNome = { required: true };
      } else {
        this.errorNome = { required: null };
      }
      if (this.telefoneMovel.length < 1) {
        this.errorTelefoneMovel = { required: true }
      } else {
        this.errorTelefoneMovel = { required: null };
      }
      if (this.status.length < 1) {
        this.errorStatus = { required: true };
      } else {
        this.errorStatus = { required: null };
      }
      if (this.rede.length < 1) {
        this.errorRede = { required: true };
      } else {
        this.errorRede = { required: null };
      }
      if (this.filial.length < 1) {
        this.errorFilial = { required: true };
      } else {
        this.errorFilial = { required: null };
      }
      if (this.banco.length < 1) {
        this.errorBanco = { required: true };
      } else {
        this.errorBanco = { required: null };
      }
      if (this.agencia.length < 1) {
        this.errorAgencia = { required: true }
      } else {
        this.errorAgencia = { required: null };
      }
      if (this.conta.length < 1) {
        this.errorConta = { required: true };
      } else {
        this.errorConta = { required: null };
      }
      if (this.email.length < 1) {
        this.errorEmail = { required: true };
      } else {
        this.errorEmail = { required: null };
      }
      return true;
    } catch (e) {
      console.log(e.toString());
    }
  }

  checkPJFree(t) {
    switch (t) {
      case 'f': {
        this.isFree = true;
        this.isPj = false;
        break
      }
      case 'p': {
        this.isPj = true;
        this.isFree = false;
        break
      }
    }
  }

  cadPromotor(): void {
    console.log(this.isFree, this.isPj);
    if (this.pageParams.params.page == 'editar' && this.pageParams.params.proId.length > 0) {
      if (this.validation() && (this.errorNome.required == null && this.errorStatus.required == null &&
        this.errorTelefoneMovel.required == null && this.errorCoordenador.required == null && this.errorConta.required == null &&
        this.errorBanco.required == null && this.errorAgencia.required == null && this.errorEmail.required == null)) {
        this._http.post(
          'editarPromotor.php',
          {
            id: this.pageParams.params.proId,
            nome: this.nome,
            telefoneMovel: this.telefoneMovel,
            email: this.email,
            status: this.status,
            // coordenador: this.coordIndex,
            rede: this.redeIndex,
            filial: this.filialIndex,
            agencia: this.agencia,
            banco: this.banco,
            conta: this.conta,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Promotor[' + this.pageParams.params.proId + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('Promotor editado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadPromotor']);
            }, 1000);
          } else if (response.status == '0x101') {
            window.alert('Erro de inserção ao banco');
          }
        });
      }
    } else {
      if (this.validation() && (this.errorNome.required == null && this.errorStatus.required == null &&
        this.errorTelefoneMovel.required == null && this.errorCoordenador.required == null && this.errorConta.required == null &&
        this.errorBanco.required == null && this.errorAgencia.required == null && this.errorEmail.required == null)) {
        this._http.post(
          'insertPromotor.php',
          {
            pro_id: this.pro_index,
            nome: this.nome + (this.isFree == true && this.nome.indexOf('FREE') == -1 ? ' FREE' : (this.isPj && this.nome.indexOf('PJ') == -1 ? ' PJ' : '')),
            telefoneMovel: this.telefoneMovel,
            email: this.email,
            status: this.status,
            // coordenador: this.coordIndex,
            rede: this.redeIndex,
            filial: this.filialIndex,
            agencia: this.agencia,
            banco: this.banco,
            conta: this.conta,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Inserção - Promotor[' + this.nome + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade - Cadastro de Promotor\r\n\r\nPromotor cadastrado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadPromotor']);
            }, 1000);
          } else if (response.status == '0x101') {
            window.alert('Erro de inserção ao banco');
          } else if (response.status == '0x102') {
            window.alert('KSTrade - Cadastro de Promotor\r\n\r\nPromotor já cadastrado!');
          } else if (response.status == '0x103') {
            window.alert('Erro ao relacionar promotor com rede e filial');
          }
        });
      }
    }
  }

  setCoordenador(index): void {
    this.coordIndex = this.coordenadores[index].Usr_ID;
    this.coordenador = this.coordenadores[index].Usr_Nome;
    this.coordenadoresOK = false;
  }

  setBanco(banco): void {
    this.banco = banco;
    this.bancoOK = false;
  }

  setStatus(status): void {
    this.status = status;
    this.statusOK = false;
  }

  setRede(index): void {
    this.rede = index.RED_Nome;
    this.redeIndex = index.RED_ID;
    this._http.post(
      'selectFilial.php',
      {
        id: this.redeIndex
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.filiais = [];
        response.result.forEach(element => {
          this.filiais.push(element);
        });
        this.resultFilterFiliais = this.filiais;
        this.redeOK = false;
      } else if (response.status == '0x101') {
        window.alert('Erro ao selecionar filiais.');
      }
    });
  }

  setFilial(index): void {
    this.filial = index.RFIL_Nome;
    this.filialIndex = index.RFIL_ID;
    this.filialOK = false;
  }

  resultFilterFiliais: any = [];

  filterFilial(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterFiliais = this.filiais;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterFiliais = arr.filter((e) => {
        return regexp.test(e.RFIL_Nome.split('')[0]);
      });
    }
  }

  resultFilterRedes: any = [];

  filterRede(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterRedes = this.redes;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterRedes = arr.filter((e) => {
        return regexp.test(e.RED_Nome.split('')[0]);
      });
    }
  }

  pro_index: number = 0;

  getPromotorInf(n) {
    this._http.post(
      'selectPromotor.php',
      {
        uniqueName: true,
        nome: n
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.pro_index = parseInt(response.result[0].PRO_ID);
        this.nome = response.result[0].PRO_Nome;

        if (this.nome.indexOf('PJ') != -1) {
          this.isPj = true
          this.isFree = false
        } else if (this.nome.indexOf('FREE') != -1) {
          this.isFree = true
          this.isPj = false
        }

        this.telefoneMovel = response.result[0].PRO_Telefone;
        this.conta = response.result[0].PRO_Conta;
        this.agencia = response.result[0].PRO_Agencia;
        this.banco = response.result[0].PRO_Banco;
        this.status = response.result[0].PRO_Status;
        this.email = response.result[0].PRO_Email;
      }
    });
  }

}
