import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { HttpService } from '../http.service';

declare var $: any;

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  @ViewChild('iconEl') iconEl: HTMLElement;
  userData: any = [];
  yearNow = new Date().getFullYear();

  queryCliente: string = "";
  userImage: string = "";

  mensagens: any = [];
  mensagensPromo: any = [];
  atendimentos = []

  doneLoading = false;
  intervalAtendimento = null;

  queryClienteError: boolean = false;

  constructor(private _router: Router, private _http: HttpService,) { }

  ngOnInit() {
    if (!(localStorage.getItem('session') && JSON.parse(localStorage.getItem('session'))[0].Usr_Nome)) {
      this._router.navigate(['/login']);
    } else if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel != 'Vendedor') {
      this._router.navigate(['main/dashboard']);
      this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);

      if ((this.userData[0].Usr_Avatar === null || this.userData[0].Usr_Avatar === 'null' || this.userData[0].Usr_Avatar === 'NULL')) {
        this.userImage = './assets/defaultuser.png';
      } else {
        this.userImage = environment.apiUrl+'uploads/users/' + this.userData[0].Usr_Avatar;
      }


      // this.intervalAtendimento = setInterval( () => {
      this._http.post(
        'selectAtendimentosE.php',
        {
          simple: true,
          data: new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().split('T')[0]
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          if (this.atendimentos.length != response.result.length && response.result.length > 0) {
            this.atendimentos = response.result;
            (<HTMLElement>document.getElementById('iconEl')).classList.toggle('bit');
            (<HTMLAudioElement>document.getElementById('message_tone')).play();
          }
        }
      });

      this._http.post(
        'selectUsrStatus.php',
        {
          id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          if (response.result[0].Usr_Net_Status == 3 || response.result[0].Usr_Net_Status == '3') {
            localStorage.removeItem('session')
            setTimeout(() => {
              window.location.reload()
            }, 2000)
          }
        }
      })
      // }, 5000);




      // setInterval( () => {
      //   this._http.post(
      //     'selectMensagens.php',
      //     {
      //       myid: this.userData[0].Usr_ID
      //     }
      //   ).subscribe( (response) => {
      //     if (response.status == '0x104'){
      //       let r = [];
      //       let ring = false;
      //       response.result.forEach(element => {
      //         if (element.MSG_Status == 'Nao Lido' && element.MSG_Para_ID == this.userData[0].Usr_ID){
      //           ring = true;
      //         }

      //         if (element.MSG_De_ID != this.userData[0].Usr_ID){
      //           r.push(element);
      //         }
      //       });

      //       if (this.mensagens.length < r.length ){
      //         this.mensagens = r;
      //         if (localStorage.getItem('messages')){
      //           localStorage.removeItem('messages');
      //         }
      //         localStorage.setItem('messages', JSON.stringify(this.mensagens));
      //         if (this.userData[0].Usr_Nivel == 'Admin'){
      //           setTimeout( () => {
      //             if (ring){
      //               (<HTMLAudioElement>document.getElementById('message_tone')).play();
      //               document.getElementById('msgIcon').className =  "heartbit";
      //               ring = false
      //             }else{
      //               document.getElementById('msgIcon').className = '';
      //             }
      //           }, 500);  
      //         }
      //       }

      //       this._http.post(
      //         'selectMensagens.php',
      //         {
      //           toPromo: true,
      //           myid: this.userData[0].Usr_ID
      //         }
      //       ).subscribe( (response) => {
      //         if (response.status == '0x104'){
      //           let ring = false;
      //           if (this.mensagensPromo.length < response.result.length ){
      //             this.mensagensPromo = [];
      //             response.result.forEach(element => {
      //               if (element.MSG_Status == 'Nao Lido' && element.MSG_Para_ID == this.userData[0].Usr_ID){
      //                 ring = true;
      //               }

      //               this.mensagensPromo.push(element);
      //             });
      //             if (localStorage.getItem('messagesPromo')){
      //               localStorage.removeItem('messagesPromo');
      //             }
      //             localStorage.setItem('messagesPromo', JSON.stringify(this.mensagensPromo));
      //             if (this.userData[0].Usr_Nivel == 'Admin'){
      //               setTimeout( () => {
      //                 if (ring){
      //                   (<HTMLAudioElement>document.getElementById('message_tone')).play();
      //                   document.getElementById('msgIcon').className =  "heartbit";
      //                   ring = false
      //                 }else{
      //                   document.getElementById('msgIcon').className = '';
      //                 }
      //               }, 500);  
      //             }
      //           }else{
      //             if (this.userData[0].Usr_Nivel == 'Admin'){
      //               document.getElementById('msgIcon').className = '';
      //             }
      //             this.mensagensPromo = [];
      //             response.result.forEach(element => {
      //               this.mensagensPromo.push(element);
      //             });
      //             if (localStorage.getItem('messagesPromo')){
      //               localStorage.removeItem('messagesPromo');
      //             }
      //             localStorage.setItem('messagesPromo', JSON.stringify(this.mensagensPromo));
      //           }
      //         }
      //       });
      //     }
      //   });
      // }, 30000);

    } else if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Vendedor') {
      this._router.navigate(['/cliente/dashboard']);
      this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
      if ((this.userData[0].Usr_Avatar === null || this.userData[0].Usr_Avatar === 'null' || this.userData[0].Usr_Avatar === 'NULL')) {
        this.userImage = './assets/defaultuser.png';
      } else {
        this.userImage = this.userData[0].Usr_Avatar;
      }
    }


  }

  logOut(): void {
    localStorage.removeItem('messages');
    if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador') {
      localStorage.removeItem('clientes');
    }
    localStorage.removeItem('session');
    this._router.navigate(['/loading']);
    setTimeout(() => {
      this._router.navigate(['login']);
    }, 1500);
  }

  searchCliente(e, string) {
    switch (e.which || e.keyCode) {
      case 13:
        if (string.length < 1) {
          this.queryClienteError = true;
          setTimeout(() => {
            this.queryClienteError = false;
          }, 1500);
        } else {
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['main/listaClientes', { page: 'search', cliente: string }]);
          }, 1000);
        }
        break;
      case 8:
        if (string.length == 0) {
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['main/listaClientes', { page: 'search', cliente: '' }]);
          }, 1000);
        }
        break;
    }
  }

  goto(t): void {
    switch (t) {
      case 'pagar':
        this._router.navigate(['/main/innerload']);
        setTimeout(() => {
          this._router.navigate(['/main/listaContas', { page: 'pagar' }]);
        }, 1000);
        break;
      case 'pagas':
        this._router.navigate(['/main/innerload']);
        setTimeout(() => {
          this._router.navigate(['/main/listaContas', { page: 'pagas' }]);
        }, 1000);
        break;
      case 'listaAReceber':
        this._router.navigate(['/main/innerload']);
        setTimeout(() => {
          this._router.navigate(['/main/listaContasReceber', { page: 'areceber' }]);
        }, 1000);
        break;
      case 'listaRecebidas':
        this._router.navigate(['/main/innerload']);
        setTimeout(() => {
          this._router.navigate(['/main/listaContasReceber', { page: 'recebido' }]);
        }, 1000);
        break;
      case 'listaFilial':
        this._router.navigate(['/main/innerload']);
        setTimeout(() => {
          this._router.navigate(['/main/listaFilial']);
        }, 1000);
        break;
    }
  }

  openPendentes() {
    this._router.navigate(['/main/innerload']);
    setTimeout(() => {
      this._router.navigate(['/main/listaPendentes'])
    }, 1000);
  }

  closeMenu() {
    $('#menu-order').click();
  }

  openMensagem(m, ofP): void {
    if (ofP == 'ofPromo') {
      this._router.navigate(['/main/innerload']);
      setTimeout(() => {
        this._router.navigate(['/main/viewMensagem', { msg: m.MSG_ID, deid: m.MSG_De_ID, paraid: m.MSG_Para_ID, toPromo: true }])
      }, 1000);
    } else {
      this._router.navigate(['/main/innerload']);
      setTimeout(() => {
        this._router.navigate(['/main/viewMensagem', { msg: m.MSG_ID, deid: m.MSG_De_ID, paraid: m.MSG_Para_ID }])
      }, 1000);
    }
  }

  openAtendimentosEmergenciais(el: HTMLElement) {

    if (el.classList.contains('bit')) {
      el.classList.remove('bit')
    }

    // clearInterval(this.intervalAtendimento);
    this._router.navigate(['/main/atendimentos', {}])
  }

}
