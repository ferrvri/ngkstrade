import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-lista-contato',
  templateUrl: './lista-contato.component.html',
  styleUrls: ['./lista-contato.component.css']
})
export class ListaContatoComponent implements OnInit {

  emptyResult:boolean = false;
  
  queryContatos : string = '';
  contatos = [];
  resultsFilterContatos:any = [];
  p: number = 0

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this._http.post(
      'selectContatos.php',
      {}
    ).subscribe( (response) => {
      if(response.status == '0x104'){
        response.result.forEach(element => {
          this.contatos.push(element);
        });
        this.resultsFilterContatos = this.contatos;
      }else if (response.status == '0x101'){
        this.emptyResult = true;
      }
    });
  }

  deleteContato(contato):void{
    let c = confirm('KSTrade - Deletar contato\r\n\r\nDeseja realmente excluir?');
    if (c == true){
      this._http.post(
        'deleteContato.php',
        {
          id: contato.CON_ID,
          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Contato[' + contato.CON_ID + ']'
        }
      ).subscribe( (response) => {
        if(response.status == '0x104'){
          window.alert('Contato excluido!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaContato']);
          }, 1000);
        }else if (response.status == '0x101'){
          window.alert('Erro na deleção do contato.');
        }
      });
    }
  }

  filterArrayContatos(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterContatos = arr.filter( (e) => {
      return regexp.test(e.CON_Nome);
    });
  }

  searchContatos(string){
    this.filterArrayContatos(string, this.contatos);
  }

}
