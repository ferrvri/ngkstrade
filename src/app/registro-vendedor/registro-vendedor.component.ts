import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

declare var html2canvas: any;
declare var $: any;

@Component({
  selector: 'app-registro-vendedor',
  templateUrl: './registro-vendedor.component.html',
  styleUrls: ['./registro-vendedor.component.css']
})
export class RegistroVendedorComponent implements OnInit {

  emptyResult = false;
  vendedores = [];
  resultsFilterVendedores: any = [];

  queryVendedores: string = '';
  p: number = 0; 

  constructor( private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._http.post(
      'selectVendedorRegistro.php',
      {}
    ).subscribe( (response) => {
      if(response.status == '0x104'){
        response.result.forEach( (e) => {
          this.vendedores.push(e);
        });
        setTimeout( () => {
          $('#dataInit').datepicker();
          $('#dataEnd').datepicker();
        }, 1000);
        this.filterArrayVendedores(this.queryVendedores, this.vendedores);
      }else if(response.status == '0x101'){
        this.emptyResult = true;
      }
    });
  }


  filterArrayVendedores(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterVendedores = arr.filter( (e) => {
      return regexp.test(e.Usr_Nome);
    });
  }

  searchVendedores(string){
    this.filterArrayVendedores(string, this.vendedores);
  }

  makePDF() {
    document.getElementById('registroTable').style.display = 'block';
    html2canvas(document.getElementById('registroTable')).then( (canvas) => {
      let w = window.open("",'_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="'+(<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1)+'" /></body></html>');
      document.getElementById('registroTable').style.display = 'none';
    });
  }

  setDateOne(){
    (<HTMLInputElement>document.getElementById('dataInit')).value = 
    (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '/' +
    (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0] + '/' +
    (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2];

    if( (<HTMLInputElement>document.getElementById('dataEnd')).value.length > 0){
      this._http.post(
        'selectVendedorRegistro.php',
        {
          filter: 'data',
          dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2] + '-' +
          (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '-' +
          (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
          datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2] + '-' +
          (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '-' +
          (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
        }
      ).subscribe( (response) => {
        if(response.status == '0x104'){
          this.vendedores = [];
          if (this.emptyResult){
            this.emptyResult = false;
          }
          response.result.forEach( (e) => {
            this.vendedores.push(e);
          });
          setTimeout( () => {
            $('#dataInit').datepicker();
            $('#dataEnd').datepicker();
          }, 1500);
          this.filterArrayVendedores(this.queryVendedores, this.vendedores);
        }else if(response.status == '0x101'){
          this.emptyResult = true;
        }
      });
    }
  }

  setDateTwo(){
    (<HTMLInputElement>document.getElementById('dataEnd')).value = 
    (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '/' +
    (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0] + '/' +
    (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2];
    if( (<HTMLInputElement>document.getElementById('dataInit')).value.length > 0){
      this._http.post(
        'selectVendedorRegistro.php',
        {
          filter: 'data',
          dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2] + '-' +
          (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '-' +
          (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
          datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2] + '-' +
          (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '-' +
          (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
        }
      ).subscribe( (response) => {
        if(response.status == '0x104'){
          this.vendedores = [];
          if (this.emptyResult){
            this.emptyResult = false;
          }
          response.result.forEach( (e) => {
            this.vendedores.push(e);
          });
          setTimeout( () => {
            $('#dataInit').datepicker();
            $('#dataEnd').datepicker();
          }, 1500);
          this.filterArrayVendedores(this.queryVendedores, this.vendedores);
        }else if(response.status == '0x101'){
          this.emptyResult = true;
        }
      });
    }
  }

  clearData(){
    (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
    this.vendedores = []
    this.ngOnInit();
    this.emptyResult =  false;

  }



}
