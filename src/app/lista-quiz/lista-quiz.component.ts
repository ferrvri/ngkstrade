import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var html2canvas: any;

@Component({
  selector: 'app-lista-quiz',
  templateUrl: './lista-quiz.component.html',
  styleUrls: ['./lista-quiz.component.css']
})
export class ListaQuizComponent implements OnInit {

  p: number = 0;

  doneLoad = false;
  perguntas = [];
  
  resultsFilterQuiz = [];
  queryPerguntas = [];

  tempToPrint = [];

  latLong = '';
  constructor( private _geo: GeolocationServiceService, private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router ) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this._http.post(
      'selectQuiz.php',
      {
        all: true
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.perguntas = response.result;
        this.doneLoad = true;
        this.resultsFilterQuiz = this.perguntas;
      }
    });
  }

  searchPerguntas(e, string){
    if (string.length < 1){
      this.resultsFilterQuiz = this.perguntas;
    }else{
      this.filterPerguntas(this.perguntas, string);
    }
  }

  filterPerguntas(arr, model){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterQuiz = arr.filter( (e) => {
      return regexp.test(e.pergunta);
    });  
  }

  inactivePer(p){
    let c = confirm('KSTrade Merchandising - Quiz\r\n\r\nDeseja realmente inativar essa pergunta?');
    if (c == true){
      this._http.post(
        'inativarPergunta.php',
        {
          per_id: p.pergunta_id,
          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Inativação - Pergunta quiz[' + p.pergunta_id + ']'
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade Merchandising - Quiz\r\n\r\nPergunta inativada com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaQuiz']);
          }, 1500);
        }
      });
    }
  }

  makePDF() {
    this.tempToPrint[0] = (this.resultsFilterQuiz[this.p-1]);

    setTimeout( () => {
      document.getElementById('listaQuizTable').style.display = 'block';
      html2canvas(document.getElementById('listaQuizTable')).then( (canvas) => {
        let w = window.open("",'_blank');
        w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="'+(<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1)+'" /></body></html>');
        document.getElementById('listaQuizTable').style.display = 'none';
      });
    }, 500);
  }

  apagarPergunta(p, index){
    let c = confirm('KSTrade Merchandising - Quiz\r\n\r\nDeseja realmente remover essa pergunta?');
    if (c == true){
      this._http.post(
        'inativarPergunta.php',
        {
          delete: true,
          per_id: p.pergunta_id,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Pergunta quiz[' + p.pergunta_id + ']'
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade Merchandising - Quiz\r\n\r\nPergunta removida com sucesso!');
          this.resultsFilterQuiz.splice(index, 1)
        }
      });
    }
  }
}
