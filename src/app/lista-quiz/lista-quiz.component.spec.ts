import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaQuizComponent } from './lista-quiz.component';

describe('ListaQuizComponent', () => {
  let component: ListaQuizComponent;
  let fixture: ComponentFixture<ListaQuizComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaQuizComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaQuizComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
