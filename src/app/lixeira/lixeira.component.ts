import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-lixeira',
  templateUrl: './lixeira.component.html',
  styleUrls: ['./lixeira.component.css']
})
export class LixeiraComponent implements OnInit {

  userData;
  emptyResult: boolean = false;
  p: any;

  lixeira = []
  resultsLixeira = []

  constructor(
    private _http: HttpService,
  ) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem("session"));

    this._http.post(
      'selectLixeira.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.lixeira = this.resultsLixeira = response.result;
      }else{
        this.emptyResult = true;
      }
    })
  }

  restaurar(obj, index){
    this._http.post(
      'moverParaLixeira.php',
      {
        restaurar: true,
        red_id: obj.RED_ID, 
        rfil_id: obj.RFIL_ID,
        pro_id: obj.PRO_ID,
        cli_nome: obj.Cli_Nome,
        lixeira_id: obj.lixeira_id
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.resultsLixeira.splice(index, 1);
      }
    })
  }

  pagar(obj, index){
    let c = confirm('KS Trade Merchandising - Lixeira \r\n\r\nDeseja realmente marcar como pago?')
    if (c == true){
      this._http.post(
        'pagarLixeira.php',
        {
          lixeira_id: obj.lixeira_id
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          this.ngOnInit();
        }
      })
    }
  }

}
