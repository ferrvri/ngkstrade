import { Component, OnInit } from '@angular/core';
import { HttpService } from './http.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  
  tempo: number = 0;
  horaDeSaida = undefined;

  constructor(private _http: HttpService, private _router: Router) {} 

  ngOnInit(){
    this._router.navigate(['login']);
    
    if (localStorage.getItem('session') && JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador'){
      console.log('ok');
      setInterval( () => {
        if (document.hasFocus()){
          if (this.tempo >= 15){
            let test = new Date(Date.now())
            test.setHours(new Date().getHours() - 3);
            this.sendInactivity(test)
            this.tempo = 0;
          }else{
            this.tempo = 0;
          }
        }else{
          this.tempo++;
          if (this.horaDeSaida === undefined){
            let test = new Date(Date.now()) 
            test.setHours(new Date().getHours() - 3);
            this.horaDeSaida = test;
          }
        }
      }, 1000);
    }
  }

  sendInactivity(hora){
    this._http.post(
      'insertCoodenadorInatividade.php',
      {
        coord: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
        horaSaida: this.horaDeSaida,
        horaVolta: hora,
        tempo: this.tempo
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        console.log('Registered');
        this.horaDeSaida = undefined;
        this.tempo = 0;
      }
    });
  }
}
