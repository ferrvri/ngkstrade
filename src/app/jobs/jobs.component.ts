import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

declare var $:any;
declare var jQuery:any;
declare var escape: any;

@Component({
  selector: 'app-jobs',
  templateUrl: './jobs.component.html',
  styleUrls: ['./jobs.component.css']
})
export class JobsComponent implements OnInit {

  pageParams: any;

  promotorIndex:number = 0;
  statusIndex:number = 0;
  redeIndex:number = 0;
  filialIndex:number = 0;

  promotores =  [];
  filiais = [];
  redes = [];

  filial: string = 'Selecione';
  promotor: string = 'Selecione';
  status: string = 'Realizado';
  rede: string = 'Selecione';
  data: string = '';
  horario:string = '';
  anexo: string  = '';
  obs: string = '';

  toUploadImage:any = [
    // 'iVBORw0KGgoAAAANSUhEUgAAAMoAAACbCAIAAACs1ZOtAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABySURBVHhe7cEBDQAAAMKg909tDjcgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAgB81b5QAAUGdPzoAAAAASUVORK5CYII='
  ];
  nameOfFile:string = '';

  filialOK:boolean = false;
  promotorOK: boolean = false;
  redeOK: boolean = false;
  statusOK: boolean = false;

  doneCad: boolean = false;

  constructor(private _http: HttpService, private _router: Router, private _activedRoute: ActivatedRoute) { }

  decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }

  ngOnInit() {
    this._activedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    jQuery('.mydatepicker, #datepicker').datepicker();

    $('.dropify').dropify();

    // Used events
    var drEvent = $('#input-file-events').dropify();

    drEvent.on('dropify.beforeClear', function(event, element) {
        return confirm("Você deseja realmente apagar \"" + element.file.name + "\" ?");
    });

    drEvent.on('dropify.afterClear', function(event, element) {
        alert('Arquivo apagado');
    });

    drEvent.on('dropify.errors', function(event, element) {
        console.log('Erro!');
    });

    var drDestroy = $('#input-file-to-destroy').dropify();
    drDestroy = drDestroy.data('dropify');
    $('#toggleDropify').on('click', function(e) {
        e.preventDefault();
        if (drDestroy.isDropified()) {
            drDestroy.destroy();
        } else {
            drDestroy.init();
        }
    })

    this.horario = new Date().getHours().toString() + ':' +  (new Date().getMinutes().toString().length == 1 ? '0'+new Date().getMinutes().toString():new Date().getMinutes().toString());

    this._http.post(
      'selectRedes.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          this.redes.push(element);
        });
        this.resultFilterRedes = this.redes; 
        // if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador'){
        //   this._http.post(
        //     'selectPromotor.php',
        //     {
        //       uniqueid: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
        //     }
        //   ).subscribe( (response2) => {
        //     if(response2.status == '0x104'){
        //       response2.result.forEach( (element) => {
        //         this.promotores.push(element);
        //       });
        //     }else if (response2.status == '0x101'){
        //       window.alert('Erro ao selecionar promotores');
        //     }
        //   });
        // }else if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Admin'){
          // this._http.post(
          //   'selectPromotor.php',
          //   {
          //   }
          // ).subscribe( (response2) => {
          //   if(response2.status == '0x104'){
          //     response2.result.forEach( (element) => {
          //       this.promotores.push(element);
          //     });
          //   }else if (response2.status == '0x101'){
          //     window.alert('Erro ao selecionar promotores');
          //   }
          // });
        // }
      }else if (response.status == '0x101'){
        window.alert('Erro ao selecionar redes');
      }
    });
  }

  errorRede = {required: null};
  errorPromotor = {required: null};
  errorFilial = {required: null};
  errorStatus = {required: null};
  errorData = {required: null};
  errorHora = {required: null};
  errorAnexo = {required: null};
  errorObs = {required: null};

  cadastroJob: FormGroup = new FormGroup({
    rede: new FormControl(this.rede, [Validators.required, Validators.minLength(6)]),
    promotor: new FormControl(this.promotor, [Validators.required, Validators.minLength(6)]),
    filial: new FormControl(this.filial, [Validators.required, Validators.minLength(6)]),
    status: new FormControl(this.status, [Validators.required, Validators.minLength(6)]),
    data : new FormControl(this.data, [Validators.required, Validators.minLength(6)]),
    hora: new FormControl(this.horario, [Validators.required, Validators.minLength(6)]),
    anexo: new FormControl(this.anexo, [Validators.required, Validators.minLength(6)]),
    obs: new FormControl(this.obs, [Validators.required, Validators.minLength(6)])
  });

  uploadChanges(event){
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      //this.nameOfFile = event.target.files[0].name;
      reader.readAsDataURL(file);
      reader.onload = () => {
          this.toUploadImage.push(reader.result.toString().split(',')[1]);
          // alert(this.toUploadImage[0]);
      };
    }
    // console.log(this.toUploadImage);
  }
  
  validation(){
    try{
      if (this.rede.length < 1 || this.rede == 'Selecione'){
        this.errorRede = {required: true};
      }else{
        this.errorRede = {required: null};
      }
      if (this.promotor.length < 1 || this.promotor == 'Selecione' ){
        this.errorPromotor = {required: true}
      }else{
        this.errorPromotor = {required: null};
      }
      if (this.filial.length < 1 || this.filial == 'Selecione'){
        this.errorFilial = {required : true}
      }else{
        this.errorFilial = {required: null};
      }
      if (this.status.length < 1 || this.status == 'Selecione'){
        this.errorStatus = {required: true};
      }else{
        this.errorStatus = {required: null};
      }
      if (this.data.length < 1){
        this.errorData = {required: true};
      }else{
        this.errorData = {required: null};
      }
      if (this.horario.length < 1){
        this.errorHora = {required: true};
      }else{
        this.errorHora = {required: null};
      }
       if (this.toUploadImage.length < 1){
        this.errorAnexo = {required: true}
      }else{
        this.errorAnexo = {required: null};
      }
       if (this.obs.length < 1){
        this.errorObs = {required: true};
      }else{
        this.errorObs = {required: null};
      }
      return true;
    }catch(e){
      console.log(e.toString());
    }
  }

  getInitAndFimSemana() {
    let init = '';
    let fim = '';
    switch (new Date().getDay()) {
      case 0: {
        init = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 6).toISOString().substring(0, 10);
        break;
      }
      case 1: {
        init = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 1).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 5).toISOString().substring(0, 10);
        break;
      }
      case 2: {
        init = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 2).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 4).toISOString().substring(0, 10);
        break;
      }
      case 3: {
        init = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 3).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 3).toISOString().substring(0, 10);
        break;
      }
      case 4: {
        init = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 4).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 2).toISOString().substring(0, 10);
        break;
      }
      case 5: {
        init = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 5).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1).toISOString().substring(0, 10);
        break;
      }
      case 6: {
        init = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() - 6).toISOString().substring(0, 10);
        fim = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate()).toISOString().substring(0, 10);
        break;
      }
    }
    return {
      init: init,
      end: fim
    }
  }

  cadJob():void{
    if(this.validation() && (this.errorStatus.required == null && this.errorRede.required == null &&
    this.errorPromotor.required == null && this.errorObs.required == null &&
    this.errorFilial.required == null && this.errorAnexo.required ==  null)){
      // this.doneCad = true;

      this.nameOfFile = Math.ceil(Math.random() * 1000)+this.pageParams.params.clienteNome.replace(' ', '')+new Date( new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0,10)+'.jpg'
      let nameOfFile = this.nameOfFile.trim();

      let data = new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10);
      let hora = (new Date().getHours().valueOf() < 10 ? '0' + new Date().getHours() : new Date().getHours()) + ':' + (new Date().getMinutes().valueOf() < 10 ? '0' + new Date().getMinutes() : new Date().getMinutes());
      let data_inicial = new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-01').toISOString().substring(0, 10);

      this._http.post(
        'insertJob.php', 
        {
            title: nameOfFile,
            anexo: this.toUploadImage,
            cliente: parseInt(this.pageParams.params.clienteId),
            rede: this.redeIndex,
            promotor: this.promotorIndex,
            filial: this.filialIndex,
            status: 'Realizado',
            data: data,
            data_inicial: data_inicial,
            ano: new Date().getFullYear(),
            mes: (new Date().getMonth() + 1),
            hora: hora,
            obs: this.obs,
            foto: nameOfFile
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          this._http.post(
            'app/selectProdutos.php',
            {
              cliente: this.pageParams.params.clienteId
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              if (response.result[0].pro_dias_semana.length > 0 && response.result[0].pro_dias_semana.split(';').length > 0){
                window.alert('KSTrade - Cadastro de Job\r\n\r\nJob cadastrado com sucesso!');
  
                let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
                let dayName = days[new Date().getDay() ];
  
                let d = response.result[0].pro_dias_semana || '';
                let diassemana = d.split(';') || [];
  
                let doneExp1 = {};
                try{
                  diassemana.forEach(element => {
                    if (element == dayName){
                      throw doneExp1;
                    }
                  });
                }catch(doneExp1){
                  this._router.navigate(['/main/innerload']);
                  setTimeout( () => {
                    this._router.navigate(['/main/cadPesquisaJob',
                      {
                        cliente: this.pageParams.params.clienteId,
                        rede: this.redeIndex,
                        promotor: this.promotorIndex,
                        filial: this.filialIndex,
                        data: data,
                        hora: hora
                      }
                    ]);
                  }, 250);
                }
              }else{
                window.alert('KSTrade - Cadastro de Job\r\n\r\nJob cadastrado com sucesso!');
                if (response.result[0].pro_numero_vezes > 0 && response.result[0].pro_numero_vezes !== null){                  this._http.post(
                    'selectCountPesquisa.php',
                    {
                      cliente: this.pageParams.params.clienteId,
                      rede: this.redeIndex,
                      promotor: this.promotorIndex,
                      filial: this.filialIndex,
                      dataone: this.getInitAndFimSemana().init,
                      datatwo: this.getInitAndFimSemana().end
                    }
                  ).subscribe( (response2) => {
                    if (response2.status == '0x104'){
                      if (response.result[0].pro_numero_vezes > response2.result[0].numero_pesquisas){
                        this._router.navigate(['/main/innerload']);
                        setTimeout( () => {
                          this._router.navigate(['/main/cadPesquisaJob',
                            {
                              cliente: this.pageParams.params.clienteId,
                              rede: this.redeIndex,
                              promotor: this.promotorIndex,
                              filial: this.filialIndex,
                              data: data,
                              hora: hora
                            }
                          ]);
                        }, 250);
                      }
                    }
                  });
                }else{
                  this._router.navigate(['/main/innerload']);
                  setTimeout( () => {
                    this._router.navigate(['/main/listaClientes', {}]);
                  }, 1000);
                }
              }
            }else{
              window.alert('KSTrade - Cadastro de Job\r\n\r\nJob cadastrado com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout( () => {
                this._router.navigate(['/main/listaClientes', {}]);
              }, 1000);
            }
          });
          this.doneCad = false;
        }else if (response.status == '0x101'){
          window.alert('Erro de inserção ao banco');
        }else if (response.status == '0x102'){
          window.alert('KSTrade - Cadastro de Job\r\n\r\nO arquivo já existe no sistema!');
        }else if(response.status == '0x103'){
          window.alert('KSTrade - Cadastro de Job\r\n\r\nOcorreu um erro no salvamento do arquivo.');
        }else if (response.status == '0x110'){
          window.alert('KSTrade - Cadastro de Job\r\n\r\nO Job não pode ser inserido, pois o valor ainda não foi cadastrado!');
        }else if (response.status == '0x105'){
          window.alert('KSTrade - Cadastro de Job\r\n\r\nEste job já foi feito hoje!');
        }
      });
    }
  }

  removeJobImage(index){
    this.toUploadImage.splice(index, 1);
    if (index == 0){
      $(".dropify-clear").click();
    }
  }

  setPromotor(i):void{
    this.promotorIndex = i.PRO_ID;
    this.promotor = i.PRO_Nome;
    this.promotorOK = false;
  }

  setRede(i):void{
    this.redeIndex = i.RED_ID;
    this.rede = i.RED_Nome;
    this._http.post(
      'selectFilial.php',
      {
        id: i.RED_ID
      }
    ).subscribe( (response) => {
      if(response.status == '0x104'){
        this.filiais = [];
        response.result.forEach( (e) => {
          e.RFIL_Nome = this.decode_utf8(e.RFIL_Nome)
          this.filiais.push(e);
        });
        this.resultFilterFiliais = this.filiais;
        this.redeOK = false;
      }else if (response.status == '0x101'){
        window.alert("Erro ao selecionar as filiais");
      }
    });
  }

  setFilial(i):void{
    this.filialIndex = i.RFIL_ID;
    this.filial = i.RFIL_Nome;
    this.promotores = [];
    this.filialOK = false;
    this._http.post(
      'selectPromotor.php',
      {
        filialId: this.filialIndex
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          this.promotores.push(element);
        });
        this.resultFilterPromotor = this.promotores;
      }else if (response.status == '0x101'){
        window.alert('Erro ao selecionar promotor');
      }
    });
  }

  setStatus(s):void{
    this.status = s;
    this.statusOK = false;
  }

  setData(){
    if ((<HTMLInputElement>document.getElementById('dataJob')).value === undefined || (<HTMLInputElement>document.getElementById('dataJob')).value == ''){
      (<HTMLInputElement>document.getElementById('dataJob')).value = '';
    }else{
      this.data = (<HTMLInputElement>document.getElementById('dataJob')).value.split('/')[1] +'/' + (<HTMLInputElement>document.getElementById('dataJob')).value.split('/')[0] +'/' + (<HTMLInputElement>document.getElementById('dataJob')).value.split('/')[2]
    }
  }

  resultFilterPromotor: any = [];

  filterPromotor(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterPromotor = this.promotores;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterPromotor = arr.filter( (e) => {
        return regexp.test(e.PRO_Nome.split('')[0]);
      });
    }
  }


  resultFilterFiliais : any = [];

  filterFilial(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterFiliais = this.filiais;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterFiliais = arr.filter( (e) => {
        return regexp.test(e.RFIL_Nome.split('')[0]);
      });
    }
  }

  resultFilterRedes : any = [];

  filterRede(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterRedes = this.redes;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterRedes = arr.filter( (e) => {
        return regexp.test(e.RED_Nome.split('')[0]);
      });
    }
  }


  removeFiles(){
    $(".dropify-clear").click();
    this.toUploadImage = [];
  }
}
