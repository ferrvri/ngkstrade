import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from '../../environments/environment';

declare var dialogPolyfill: any;
declare var showDialog: any;
declare var Date: any;
declare var $: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  feedbacks = [];
  userData: any = [];
  mensagens: any = [];
  otrs: any = [];
  top5Promo: any = [];
  checkins = [];
  resultsFilterOtrs: any = [];

  doneLoading = false;

  onlyEntrada = false;
  onlySaida = false;

  aReceberHoje: number = 0;
  aPagarHoje: number = 0;
  pagasTotal: number = 0;
  totalValorJobsDash: number = 0;
  totalRede: number = 0;
  totalFilial: number = 0;
  totalCliente: number = 0;
  totalPromotoresAtivos: number = 0;
  totalPromotoresInativos: number = 0;
  totalJobs: number = 0;
  totalUsuarios: number = 0;
  totalContasRecebidas: number = 0;
  mediaJobs: number = 0;

  emptyResult: boolean = true;
  showOTRImage: boolean = false;

  otrImage: string = '';
  otrTitle: string = '';

  totalValorJobs: number = 0;

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    localStorage.removeItem('messages');
    if (localStorage.getItem('session')) {
      this.userData = JSON.parse(localStorage.getItem('session'));
    }
    let ci = null;


    if (this.userData[0].Usr_Nivel == 'Coordenador') {
      this._http.post(
        'selectQuiz.php',
        {
          tipo: 1
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this._http.post(
            'selectQuiz.php',
            {
              already: true,
              per_id: response.result[0].PER_ID,
              usr_id: this.userData[0].Usr_ID
            }
          ).subscribe((response2) => {
            if (response2.status != '0x102') {
              let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
              let dayName = days[new Date().getDay()];

              response.result.forEach(element => {
                let elem = element.PER_Dias_Semana.split(';');
                elem.forEach(element2 => {
                  if (element2 == dayName) {
                    let arr = [];

                    clearInterval(ci);
                    arr.push(element.PER_ID);
                    this._router.navigate(['/main/cadQuizUser', { per_id: arr }]);
                  }
                });
              });
            }
          });
        }
      });

      this._http.post(
        'selectClientes.php',
        {
          coordenador: 'true',
          coordId: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          console.log('cls', response);
          let r = [];
          response.result.forEach((e) => {
            let obj = {
              Cli_ID: e.Cli_ID,
              Cli_Nome: e.Cli_Nome,
              Cli_Email: e.Cli_Email,
              Cli_CNPJ: e.Cli_CNPJ,
              Cli_TelefoneMovel: e.Cli_TelefoneMovel,
              Usr_Nome: e.Usr_Nome
            };
            r.push(obj);
            localStorage.setItem('clientes', JSON.stringify(r));
          });
        }
        this.getInfos();
      });
    } else if (this.userData[0].Usr_Nivel == 'Externo') {
      this._http.post(
        'selectClientes.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          let r = [];
          response.result.forEach((e) => {
            let obj = {
              Cli_ID: e.Cli_ID,
              Cli_Nome: e.Cli_Nome,
              Cli_Email: e.Cli_Email,
              Cli_CNPJ: e.Cli_CNPJ,
              Cli_TelefoneMovel: e.Cli_TelefoneMovel,
              Usr_Nome: e.Usr_Nome
            };
            r.push(obj);
            localStorage.setItem('clientes', JSON.stringify(r));
          });

        }
        this.getInfos();
      });
    } else {
      this.getInfos();
    }
  }

  getInfos() {
    if (this._router.url == '/main/dashboard' && !this.proOn) {
      this._http.post(
        'selectTotais.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.doneLoading = true;
          this.totalCliente = response.result.totalCliente;
          this.totalFilial = response.result.totalFiliais;
          this.totalJobs = response.result.totalJobs;
          this.totalPromotoresAtivos = response.result.totalPromotoresAtivos;
          this.totalPromotoresInativos = response.result.totalPromotoresInativos;
          this.totalRede = response.result.totalRedes;
          this.totalUsuarios = response.result.totalUsuarios;
        }
      });

      if (this.userData[0].Usr_Nivel == 'Coordenador') {
        this.doneLoading = true;
        this.emptyResult = false;

        this._http.post(
          'selectJobs.php',
          {
            clientes: JSON.parse(localStorage.getItem('clientes')),
            filter: 'data',
            dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 0).toISOString().substring(0, 10),
            datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            let t = []
            response.result.forEach((e) => {
              t.push(e);
            });
            if (t.length > this.otrs.length) {
              this.otrs = t;
              this.resultsFilterOtrs = this.otrs;
            }

            this._http.post(
              'selectTotais.php',
              {}
            ).subscribe((response) => {
              if (response.status == '0x104') {
                this.totalJobs = response.result.totalJobs;
                if (this.filterPromotoresStrCheckin.length < 1 && !this.alreadyFiltered) {
                  this._http.post(
                    'selectCheckin.php',
                    {}
                  ).subscribe((response) => {
                    if (response.status == '0x104') {
                      this.checkins = [];
                      this.checkins = response.result;
                      // setTimeout(() => {
                      //   this.getInfos();
                      // }, 7000);
                    }
                  });
                }
              }
            });
          }
        });
      } else if (this.userData[0].Usr_Nivel == 'Auxiliar') {
        this.doneLoading = true;

        this._http.post(
          'selectJobs.php',
          {
            kind: 'pagar',
            filter: 'data',
            dataone: new Date(new Date().getFullYear(), new Date().getMonth(), '01').toISOString().substring(0, 10),
            datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().substring(0, 10)
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            let t: number = 0;
            this.otrs = [];
            this.emptyResult = false;
            response.result.forEach((e) => {
              this.otrs.push(e);
              t += parseFloat(e.JOB_Valor);
            });
            if (this.totalValorJobs != t) {
              this.totalValorJobs = t;
            }
            this.resultsFilterOtrs = this.otrs;

            if (this.filterPromotoresStrCheckin.length < 1 && !this.alreadyFiltered) {
              this._http.post(
                'selectCheckin.php',
                {}
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  this.checkins = [];
                  this.checkins = response.result;
                  this._http.post(
                    'selectFeedbacks.php',
                    {
                      dataone: new Date(new Date().getFullYear(), new Date().getMonth(), '01').toISOString().substring(0, 10),
                      datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().substring(0, 10)
                    }
                  ).subscribe((response) => {
                    if (response.status == '0x104') {
                      this.feedbacks = [];
                      this.feedbacks = response.result;
                      // setTimeout(() => {
                      //   this.getInfos();
                      // }, 7000);
                    }
                  });
                }
              });
            }
          } else if (response.status == '0x101') {
            this.emptyResult = true;
          }
        });
      } else {
        this._http.post(
          'selectDashboard.php',
          {}
        ).subscribe((response) => {
          if (response.status == true) {

            let t = 0;
            this.otrs = [];

            this.emptyResult = false;

            response.jobs.forEach((e) => {
              this.otrs.push(e);
              t += parseFloat(e.JOB_Valor);
            });
            if (this.totalValorJobs != t) {
              this.totalValorJobs = t;
            }
            this.resultsFilterOtrs = this.otrs;

            ////
            let tt: number = 0;
            response.jobs.forEach((e) => {
              tt += parseFloat(e.JOB_Valor);
            });
            if (this.totalValorJobsDash != tt) {
              this.totalValorJobsDash = tt;
            }

            ////
            let rrr: number = 0;
            response.contasReceber.forEach(element => {
              rrr += parseFloat(element.FORR_Valor.replace(',', ''));
            });

            if (this.aReceberHoje < rrr || this.aReceberHoje > rrr) {
              this.aReceberHoje = rrr;
            }

            ////
            let rrrr: number = 0;
            response.contasPagar.forEach(element => {
              rrrr += parseFloat(element.CONP_Valor.replace(',', ''));
            });

            if (this.aPagarHoje < rrrr || this.aPagarHoje > rrrr) {
              this.aPagarHoje = rrrr;
            }

            ////
            let rrrrr: number = 0;
            response.contasPagas.forEach(element => {
              rrrrr += parseFloat(element.CONP_Valor.replace(',', ''));
            });

            if (this.pagasTotal < rrrrr || this.pagasTotal > rrrrr) {
              this.pagasTotal = rrrrr;
            }

            ////
            let ttt: number = 0;
            response.contasRecebidas.forEach(element => {
              ttt += parseFloat(element.FORR_Valor.replace(',', ''));
            });

            if (this.totalContasRecebidas < ttt || this.totalContasRecebidas > ttt) {
              this.totalContasRecebidas = ttt;
            }

            if (this.filterPromotoresStrCheckin.length < 1 && !this.alreadyFiltered) {
              this._http.post(
                'selectCheckin.php',
                {}
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  this.checkins = [];
                  this.checkins = response.result;
                }
              });

              this._http.post(
                'selectFeedbacks.php',
                {
                  dataone: new Date(new Date().getFullYear(), new Date().getMonth(), '01').toISOString().substring(0, 10),
                  datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().substring(0, 10)
                }
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  this.feedbacks = [];
                  this.feedbacks = response.result;

                  // setTimeout(() => {
                  //   this.getInfos();
                  // }, 7000);
                }
              });
            }
          } else {
            this.emptyResult = true;
          }
        })
      }
    }
  }

  images = []

  openJOBImage(j) {
    this._http.post(
      'selectFotosExtras.php',
      {
        job_id: j.JOB_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.otrImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.otrTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;

        this.images = [];
        this.images.push(environment.apiUrl+'uploads/' + j.JOB_Foto);

        response.result.forEach(element => {
          this.images.push(environment.apiUrl+'uploads/' + element.JFOT_Foto);
        });

        if ((j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
          (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
          (j.JOB_Latitude === null && j.JOB_Longitude === null)) {
          showDialog({
            title: this.otrTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.otrTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      } else {
        this.otrImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.otrTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;

        if ((j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
          (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
          (j.JOB_Latitude === null && j.JOB_Longitude === null)) {
          showDialog({
            title: this.otrTitle,
            image: this.otrImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.otrTitle,
            image: this.otrImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      }
    });
  }

  closeOTRImage() {
    // (<HTMLDialogElement>document.querySelector('#otrImageDialog')).close();
    this.showOTRImage = false;
    // this.showOTRImage = false;
    // this.otrImage = '';
    // document.getElementById('body').style.overflow =  "auto";
  }

  deleteOtr(index): void {
    let c = confirm("KSTrade - Dashboard\r\n\r\nDeseja realmente excluir OTR?");
    if (c == true) {
      this._http.post(
        'deleteOtr.php',
        {
          id: this.resultsFilterOtrs[index].OTR_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/dashboard']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert('Erro na exclusão de otr');
        }
      });
    }
  }

  openMensagem(m): void {
    this._http.post(
      'updateMensagem.php',
      {
        id: m.MSG_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this._router.navigate(['/main/innerload']);
        setTimeout(() => {
          this._router.navigate(['/main/viewMensagem', { msg: m.MSG_ID, deid: m.MSG_De_ID, paraid: m.MSG_Para_ID }])
        }, 1000);
      }
    });
  }

  testTone() {
    if (document.getElementById('message_tone')) {
      (<HTMLAudioElement>document.getElementById('message_tone')).play();
    } else {
      console.log('no audio');
    }
  }

  proOn: boolean = false;

  proOnline(arr) {
    if (this.proOn) {
      this.resultsFilterOtrs = this.otrs;
      this.proOn = false;
    } else {
      this.proOn = true;
      this.resultsFilterOtrs = arr.filter((e) => {
        return (e.PRO_Net_Status == '1' || e.PRO_Net_Status === true) ? true : false;
      });
    }
  }

  filtroPromotorCheckIn = false;
  filterPromotoresStrCheckin = '';

  filterPromotoresCheckin(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.checkins = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
  }

  alreadyFiltered = false;
  filterEntrada(type) {
    this.alreadyFiltered = true;
    this.checkins = this.checkins.filter((a) => {
      if (a.type == type) {
        return true;
      }
    });
  }
}
