import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../http.service';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-cad-grupo-noticia',
  templateUrl: './cad-grupo-noticia.component.html',
  styleUrls: ['./cad-grupo-noticia.component.css']
})
export class CadGrupoNoticiaComponent implements OnInit {

  clientes = [];
  resultFilterClientes = [];
  cliente: any = 'Selecione um cliente';
  cli_obj: any = {};
  nomeGrupo = '';

  is_guest = false

  clienteOK = false;

  errorCliente = { required: null }
  errorNome = { required: null }

  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition( c => this.latLong = c.lat+';'+c.lng)

    this._http.post(
      'selectClientes.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.clientes.push(element);
        });
        this.resultFilterClientes = this.clientes;
      }
    });
  }

  setCliente(c) {
    this.clienteOK = false;
    this.cliente = c.Cli_Nome;
    this.cli_obj = c;
  }

  filterCliente(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterClientes = this.clientes;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterClientes = arr.filter((e) => {
        return regexp.test(e.Cli_Nome.split('')[0]);
      });
    }
  }

  validation() {
    // if (this.cliente == 'Selecione um cliente'){
    //   this.errorCliente.required = true;
    // }

    if (this.nomeGrupo == '' || this.nomeGrupo.length < 1) {
      this.errorNome.required = true;
    }
  }

  cadastrar() {
    this.validation();
    if (this.errorNome.required == null) {
      this._http.post(
        'insertGrupoNoticia.php',
        {
          cliente_id: this.cli_obj.Cli_ID || null,
          is_guest: this.is_guest == true ? 1 : 0,
          nome_grupo: this.nomeGrupo,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Inserção - Grupo noticia[' + this.nomeGrupo + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising - Cadastro de Grupo\r\n\r\nGrupo cadastrado com sucesso!');
          this._router.navigate(['/main/loading']);
          setTimeout(() => {
            this._router.navigate(['/main/cadGrupoNoticia']);
          }, 1000);
        }
      });
    }
  }

}
