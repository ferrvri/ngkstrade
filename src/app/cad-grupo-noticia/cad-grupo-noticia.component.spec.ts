import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadGrupoNoticiaComponent } from './cad-grupo-noticia.component';

describe('CadGrupoNoticiaComponent', () => {
  let component: CadGrupoNoticiaComponent;
  let fixture: ComponentFixture<CadGrupoNoticiaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadGrupoNoticiaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadGrupoNoticiaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
