import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaPromotorComponent } from './lista-promotor.component';

describe('ListaPromotorComponent', () => {
  let component: ListaPromotorComponent;
  let fixture: ComponentFixture<ListaPromotorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaPromotorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaPromotorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
