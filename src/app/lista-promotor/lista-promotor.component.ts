import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var html2canvas: any;

@Component({
  selector: 'app-lista-promotor',
  templateUrl: './lista-promotor.component.html',
  styleUrls: ['./lista-promotor.component.css']
})
export class ListaPromotorComponent implements OnInit {

  canSelect: boolean = false;
  emptyResult: boolean = false;
  enableTotal: boolean = false;

  resultFilterPromotores = [];
  promotores = [];

  tempToPrintList: any = [];

  queryPromotores: string = '';
  queryPromotoresTel: string = '';
  queryPromotoresConta: string = '';
  queryPromotoresFil: string = '';

  totalAtendimento: number = 0;

  doneLoading: boolean = false;

  userData: any = [];

  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat + ';' + c.lng)
    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    this._http.post(
      'selectPromotor2.php',
      {
        group: true
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.resultFilterPromotores = this.promotores = response.result;
        setTimeout(() => {
          this.doneLoading = true;
        }, 5000);
      } else if (response.status == '0x101') {
        this.emptyResult = true;
      }
    });
  }

  filterArrayPromotores(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
  }

  filterArrayPromotoresConta(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.PRO_Conta);
    });
  }

  filterArrayPromotoresTelefone(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.PRO_Telefone);
    });
  }

  filterArrayPromotoresFilial(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.RFIL_Nome);
    });
  }

  searchPromotores(p, param) {
    switch (param) {
      case 'nome':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotores(p, this.promotores);
        break;
      case 'conta':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotoresConta(p, this.promotores);
        break;
      case 'telefone':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotoresTelefone(p, this.promotores);
        break;
      case 'filial':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotoresFilial(p, this.promotores);
        break;
    }
  }

  deletePromotor(p, kind, firstindex?, secondindex?) {
    let c = confirm('KSTrade - Lista de Promotores\r\n\r\nDeseja realmente remover?');
    if (c == true) {
      let o = {
        id: 0,
        nome: '',
        RED_ID: 0,
        RFIL_ID: 0,

        //log
        usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
        usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
        usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
        nome_pc: this._geo.GetComputerName(),
        latlong: this.latLong,
        funcao: 'Deleção - Promotor[' + p.PRO_Nome + ']'
      }

      switch (kind) {
        case 'total': {
          o.nome = p.PRO_Nome
          delete o.id
          break;
        }

        case 'partial': {
          o.id = p.PRO_ID
          o.RED_ID = p.RED_ID;
          o.RFIL_ID = p.RFIL_ID;
          delete o.nome
          break;
        }
      }

      this._http.post(
        'deletePromotor.php',
        o
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Lista de promotores\r\n\r\nRemovido com sucesso!');
          if (kind == 'partial') {
            this.resultFilterPromotores[firstindex].content.splice(secondindex, 1);
          } else {
            this.resultFilterPromotores.splice(firstindex, 1);
          }
        } else {
          window.alert('KSTrade Merchandising - Lista de Promotores\r\nErro na remoção do promotor!');
        }
      });
    }
  }

  deleteSelected(kind) {
    switch (kind) {
      case 'redes': {
        let r = []
        let c = confirm('KSTrade - Lista de Promotores\r\n\r\nDeseja realmente remover os selecionados?');
        if (c == true) {
          this.resultFilterPromotores.forEach((elem1, index1) => {
            elem1.content.forEach((elem2, index2) => {
              if ((<HTMLInputElement>document.getElementById('pro-' + index1 + '-' + index2)).checked == true) {
                this._http.post(
                  'deletePromotor.php',
                  {
                    id: elem2.PRO_ID,
                    //log
                    usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
                    usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
                    usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
                    nome_pc: this._geo.GetComputerName(),
                    latlong: this.latLong,
                    funcao: 'Deleção - Pesquisa[' + elem2.PRO_Nome + ']'
                  }
                ).subscribe((response) => {
                  if (response.status == '0x104') {
                    r.push('true');
                  } else {
                    r.push('false');
                  }
                });
              }
              if (index1 == (this.promotores.length - 1)) {
                if ('false' in r) {
                  window.alert('Erro na remocao de promotor!');
                } else {
                  window.alert('KSTrade - Lista de promotores\r\n\r\nRemovido com sucesso!');
                  this.resultFilterPromotores[index1].content.splice(index2, 1);
                }
              }
            });
          });

        }
        break;
      }
      case 'promotores': {
        let r = []
        let c = confirm('KSTrade - Lista de Promotores\r\n\r\nDeseja realmente remover os selecionados?');
        if (c == true) {
          this.resultFilterPromotores.forEach((elem1, index1) => {
            if ((<HTMLInputElement>document.getElementById('selectPRO_' + index1)) !== null && (<HTMLInputElement>document.getElementById('selectPRO_' + index1)).checked == true) {
              this._http.post(
                'deletePromotor.php',
                {
                  nome: elem1.PRO_Nome,
                  //log
                  usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
                  usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
                  usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
                  nome_pc: this._geo.GetComputerName(),
                  latlong: this.latLong,
                  funcao: 'Deleção - Pesquisa[' + elem1.PRO_Nome + ']'
                }
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  r.push('true');
                  this.resultFilterPromotores.splice(index1, 1);
                } else {
                  r.push('false');
                }
              });
            }

            if (index1 == (this.promotores.length - 1)) {
              if ('false' in r) {
                window.alert('Erro na remoção de promotor!');
              } else {
                window.alert('KSTrade - Lista de promotores\r\n\r\nRemovido com sucesso!');
              }
            }
          });
        }
        break;
      }
    }

  }

  makePDF() {
    this._http.post(
      'selectPromotor.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.tempToPrintList.push(element);
        });
        console.log(this.tempToPrintList);
      }
    });

    setTimeout(() => {
      document.getElementById('promotorTable').style.display = 'block';
      html2canvas(document.getElementById('promotorTable')).then((canvas) => {
        console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
        let w = window.open("", '_blank');
        w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
        document.getElementById('promotorTable').style.display = 'none';
      }, (err) => {
        console.log(err);
      });
    }, 3000);
  }

  inativatePromotor(fc) {
    let c = confirm('KSTrade - Lista de promotores\r\n\r\nDeseja realmente inativar esse promotor?');
    if (c == true) {
      this._http.post(
        'inativatePromotor.php',
        {
          pro_id: fc.PRO_ID,
          pro_nome: fc.PRO_Nome
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Lista de promotores\r\n\r\nInativado com sucesso!');
        }
      });
    }
  }

  proOn: boolean = false;

  proOnline(arr) {
    if (this.proOn) {
      this.resultFilterPromotores = this.promotores;
      this.proOn = false;
    } else {
      this.proOn = true;
      this.resultFilterPromotores = arr.filter((e) => {
        return (e.PRO_Net_Status == '1' || e.PRO_Net_Status === true) ? true : false;
      });
    }
  }

}
