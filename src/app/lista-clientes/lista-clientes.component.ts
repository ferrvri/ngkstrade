import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var jsPDF: any;
declare var html2canvas: any;
declare var $: any;
declare var PptxGenJS: any;

@Component({
  selector: 'app-lista-clientes',
  templateUrl: './lista-clientes.component.html',
  styleUrls: ['./lista-clientes.component.css']
})
export class ListaClientesComponent implements OnInit {

  p: number = 0;
  userData: any = [];
  clientes = [];
  resultsFilterClientes = [];

  emptyResult = false;
  showJobMenu = false;
  enableSearch = false;

  queryClientes: string = '';

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _router: Router, private _activedRoute: ActivatedRoute, private _http: HttpService,) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    if (localStorage.getItem('session')) {
      this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    }

    let pageParams;

    this._activedRoute.paramMap.subscribe((data) => {
      pageParams = data;
    });

    if (pageParams.params.page == 'search') {
      this.enableSearch = false;
      this._http.post(
        'selectClientes.php',
        {
          unique: true,
          cliente: pageParams.params.cliente
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          response.result.forEach((e) => {
            let obj = {
              Cli_ID: e.Cli_ID,
              Cli_Nome: e.Cli_Nome,
              Cli_Email: e.Cli_Email,
              Cli_CNPJ: e.Cli_CNPJ,
              Cli_TelefoneMovel: e.Cli_TelefoneMovel,
              Usr_Nome: e.Usr_Nome,
              menuRow: false
            };
            this.clientes.push(obj);
          });
          this.resultsFilterClientes = this.clientes;
        } else if (response.status == '0x101') {
          this.emptyResult = true;
        }
      });
    } else {
      if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador') {
        this.enableSearch = true;
        this._http.post(
          'selectClientes.php',
          {
            coordenador: 'true',
            coordId: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.emptyResult = false;
            response.result.forEach((e) => {
              let obj = {
                Cli_ID: e.Cli_ID,
                Cli_Nome: e.Cli_Nome,
                Cli_Email: e.Cli_Email,
                Cli_CNPJ: e.Cli_CNPJ,
                Cli_TelefoneMovel: e.Cli_TelefoneMovel,
                Usr_Nome: e.Usr_Nome,
                menuRow: false
              };
              this.clientes.push(obj);
              this.resultsFilterClientes = this.clientes;
              localStorage.setItem('clientes', JSON.stringify(this.clientes));
            });
          } else if (response.status == '0x101') {
            this.emptyResult = true;
          } else if (response.result.length < 1) {
            this.emptyResult = true;
          }
        });
      } else {
        this.enableSearch = true;
        this._http.post(
          'selectClientes.php',
          {
            innerCliente: true
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            response.result.forEach((e) => {
              let obj = {
                Cli_ID: e.Cli_ID,
                Cli_Nome: e.Cli_Nome,
                Cli_Email: e.Cli_Email,
                Cli_CNPJ: e.Cli_CNPJ,
                Cli_TelefoneMovel: e.Cli_TelefoneMovel,
                Usr_Nome: e.Usr_Nome,
                Cli_Logo: e.Cli_Logo,
                menuRow: false
              };
              this.clientes.push(obj);
              this.resultsFilterClientes = this.clientes;
            });
          } else if (response.status == '0x101') {
            this.emptyResult = true;
          } else if (response.result.length < 1) {
            this.emptyResult = true;
          }
        });
      }
    }
  }

  novoJob(c): void {
    this._router.navigate(['main/novoJob', { clienteId: c.Cli_ID, clienteNome: c.Cli_Nome }]);
  }

  filterArrayClientes(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterClientes = arr.filter((e) => {
      return regexp.test(e.Cli_Nome);
    });
  }

  searchClientes(string) {
    this.filterArrayClientes(string, this.clientes);
  }

  novoBook(cliente) {
    this._router.navigate(['/main/cadBook', cliente])
  }

  deleteCliente(c): void {
    let confirmation = confirm('KSTrade - Clientes\r\n\r\nDeseja realmente remover o cliente?\r\n' + c.Cli_Nome);
    if (confirmation == true) {
      this._http.post(
        'deleteCliente.php',
        {
          id: c.Cli_ID,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Cliente[' + c.Cli_ID + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Clientes\r\n\r\nCliente removido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaClientes']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert('Erro na remoção de cliente');
        }
      });
    }
  }

  imprimirLista() {
    // html2canvas(document.getElementById('clienteTable')).then( canvas =
    //   , {
    //     onrendered: function(canvas) {         
    //         var imgData = canvas.toDataURL(
    //             'image/png');              
    //         var doc = new jsPDF('p', 'mm');
    //         doc.addImage(imgData, 'PNG', 10, 10);
    //         doc.save('sample-file.pdf');
    //     }
    // });
    const pdf = new jsPDF('', '', 'a4');

    html2canvas(document.getElementById("clienteTable")).then((canvas) => {
      pdf.addImage(canvas.toDataURL('image/jpeg'), 'JPEG', 25, 15);
      // pdf.fromHTML(document.getElementById('clienteTable').innerHTML, 15, 30, {
      //   'width': 300
      // });
      pdf.output('dataurlnewwindow');
    });

    // pdf.addHTML(document.getElementById("clienteTable"), {
    //   format: 'JPEG',
    //   pagesplit: true,
    //   "background": '#000'
    // });

    // pdf.output('dataurlnewwindow');

  }

  makePDF() {
    document.getElementById('clienteTable').style.display = 'block';
    html2canvas(document.getElementById('clienteTable')).then((canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("", '_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
      document.getElementById('clienteTable').style.display = 'none';
    });
  }
}
