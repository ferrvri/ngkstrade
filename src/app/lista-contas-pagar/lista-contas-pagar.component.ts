import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var $: any;
declare var jQuery: any;
declare var html2canvas: any;

@Component({
  selector: 'app-lista-contas-pagar',
  templateUrl: './lista-contas-pagar.component.html',
  styleUrls: ['./lista-contas-pagar.component.css']
})
export class ListaContasPagarComponent implements OnInit {

  selectAll: boolean = false;
  selectAllConta: boolean = false;

  filterUser: string = '';
  filterBanco: string = '';
  filterCliente: string = '';

  dataFilterOne: string = '';
  dataFilterTwo: string = '';

  dataFilterOneJob: string = '';
  dataFilterTwoJob: string = '';

  bancoFilterChoose: string = '';
  bancoOK: boolean = false;

  pageParams: any;

  emptyResult: boolean = false;
  emptyResultContas: boolean = false;

  filtroBancoCheck: boolean = false;
  filtroClienteCheck: boolean = false;
  filtroUsuarioCheck: boolean = false;
  filtroDataCheck: boolean = false;
  filtroDataCheckJob: boolean = false;

  contas: any = [];
  jobs: any = [];

  resultsFilterJobs: any = []

  totalJobs: number = 0;
  totalContas: number = 0;


  p: number = 0;
  p2: number = 0;


  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)

    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'pagar') {
      this._http.post(
        'selectContas.php',
        {
          kind: 'pagar'
        }
      ).subscribe((response) => {
        this.contas = [];
        if (response.status == '0x104') {
          this.totalContas = 0;
          response.result.forEach(element => {
            let o = {
              CONP_ID: element.CONP_ID,
              CONP_Data_Vencimento2: element.CONP_Data_Vencimento2,
              CONP_Valor: element.CONP_Valor,
              CONP_Descricao: element.CONP_Descricao,
              CONP_Status: element.CONP_Status,
              menuRow: false
            }
            // if (o.CONP_Valor.split(',' || '.')[0].length == 3){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '.'));
            // }else if (o.CONP_Valor.split(',' || '.')[0].length == 1){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '').replace('.00', ',00'));
            // }else{
            //   this.totalContas += parseFloat(o.CONP_Valor);
            // }

            this.totalContas += parseFloat(o.CONP_Valor.replace(',', ''));
            this.contas.push(o);
          });
          this.emptyResultContas = false;
        } else if (response.status == '0x101') {
          this.emptyResultContas = true;
        }
        this._http.post(
          'selectJobs.php',
          {
            kind: 'pagar'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalJobs = 0;
            response.result.forEach(element => {
              // if (element.JOB_Valor.split(',' || '.')[0].length == 3){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
              // }else if (element.JOB_Valor.split(',' || '.')[0].length == 1){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalJobs += parseFloat(element.JOB_Valor);
              // }
              this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
              this.jobs.push(element);
            });
            this.emptyResult = false;
          } else if (response.status == '0x101') {
            this.emptyResult = true;
          }
        });
      });
    } else if (this.pageParams.params.page == 'pagas') {
      this.contas = [];
      this._http.post(
        'selectContas.php',
        {
          kind: 'pagas'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.emptyResultContas = false;
          this.totalContas = 0;
          response.result.forEach(element => {
            let o = {
              CONP_ID: element.CONP_ID,
              CONP_Data_Vencimento2: element.CONP_Data_Vencimento2,
              CONP_Valor: element.CONP_Valor,
              CONP_Descricao: element.CONP_Descricao,
              CONP_Status: element.CONP_Status,
              menuRow: false
            }
            this.contas.push(o);
            // if (o.CONP_Valor.split(',' || '.')[0].length == 3){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '.'));
            // }else if (o.CONP_Valor.split(',' || '.')[0].length == 1){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '').replace('.00', ',00'));
            // }else{
            //   this.totalContas += parseFloat(o.CONP_Valor);
            // }
            this.totalContas += parseFloat(o.CONP_Valor.replace(',', ''));
          });
          this.emptyResultContas = false;
        } else if (response.status == '0x101') {
          this.emptyResultContas = true;
        }
        this._http.post(
          'selectJobs.php',
          {
            kind: 'pagas'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.emptyResult = false;
            this.totalJobs = 0;
            response.result.forEach(element => {
              this.jobs.push(element);
            });
            this.resultsFilterJobs = [];
            this.jobs.forEach(element => {
              this.resultsFilterJobs.push(element);
              // if (element.JOB_Valor.split(',' || '.')[0].length == 3){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
              // }else if (element.JOB_Valor.split(',' || '.')[0].length == 1){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalJobs += parseFloat(element.JOB_Valor);
              // }
              this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
            });
            this.emptyResult = false;
          } else if (response.status == '0x101') {
            this.emptyResult = true;
          }
        });
      });
    } else if (this.pageParams.params.page == 'adfiltro') {
      this.contas = [];
      this._http.post(
        'selectContas.php',
        {
          kind: 'adfiltro'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.totalContas = 0;
          response.result.forEach(element => {
            let o = {
              CONP_ID: element.CONP_ID,
              CONP_Data_Vencimento2: element.CONP_Data_Vencimento2,
              CONP_Valor: element.CONP_Valor,
              CONP_Descricao: element.CONP_Descricao,
              CONP_Status: element.CONP_Status,
              menuRow: false
            }
            // if (o.CONP_Valor.split(',' || '.')[0].length == 3){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '.'));
            // }else if (o.CONP_Valor.split(',' || '.')[0].length == 1){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '').replace('.00', ',00'));
            // }else{
            //   this.totalContas += parseFloat(o.CONP_Valor);
            // }
            this.totalContas += parseFloat(o.CONP_Valor.replace(',', ''));
            this.contas.push(o);
          });
          this.emptyResult = false;
        } else if (response.status == '0x101') {
          this.emptyResult = true;
        }
      });
    } else if (this.pageParams.params.page == 'all') {
      this._http.post(
        'selectContas.php',
        {
          kind: 'all'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.totalContas = 0;
          response.result.forEach(element => {
            let o = {
              CONP_ID: element.CONP_ID,
              CONP_Data_Vencimento2: element.CONP_Data_Vencimento2,
              CONP_Valor: element.CONP_Valor,
              CONP_Descricao: element.CONP_Descricao,
              CONP_Status: element.CONP_Status,
              menuRow: false
            }
            // if (o.CONP_Valor.split(',' || '.')[0].length == 3){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '.'));
            // }else if (o.CONP_Valor.split(',' || '.')[0].length == 1){
            //   this.totalContas += parseFloat(o.CONP_Valor.replace(',', '').replace('.00', ',00'));
            // }else{
            //   this.totalContas += parseFloat(o.CONP_Valor);
            // }
            this.totalContas += parseFloat(o.CONP_Valor.replace(',', ''));
            this.contas.push(o);
          });
          this.emptyResult = false;
        } else if (response.status == '0x101') {
          this.emptyResult = true;
        }
      });
    }
    this.resultsFilterJobs = this.jobs;
  }

  enableDateFilter(f) {
    if (f == true) {
      this.emptyResultContas = false;
      this.filtroDataCheck = false;
      this.jobs = [];
      this.contas = [];
      this.totalContas = 0;
      this.totalJobs = 0;
      this.ngOnInit();
    } else {
      this.filtroDataCheck = true;
      setTimeout(() => {
        $('#dataContasPagarInit').datepicker();
        $('#dataContasPagarEnd').datepicker();
      }, 100);
    }
  }

  setDateOne() {
    if (this.pageParams.params.page == 'pagar') {
      if ((<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value = (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2];
        this._http.post(
          'selectContas.php',
          {
            kind: 'pagar',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalContas = 0;
            this.emptyResultContas = false;
            this.contas = [];
            response.result.forEach(element => {
              this.contas.push(element);
              // if (element.CONP_Valor.split(',')[0].length == 3){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '.'));
              // }else if (element.CONP_Valor.split(',')[0].length == 1){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalContas += parseFloat(element.CONP_Valor);
              // }
              this.totalContas += parseFloat(element.CONP_Valor.replace(',', ''));
            });
          } else if (response.status == '0x101') {
            this.emptyResultContas = true;
            this.totalContas = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value = (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2];
      }
    } else if (this.pageParams.params.page == 'pagas') {
      if ((<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value = (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2];
        this._http.post(
          'selectContas.php',
          {
            kind: 'pagas',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalContas = 0;
            this.emptyResultContas = false;
            this.contas = [];
            response.result.forEach(element => {
              this.contas.push(element);
              // if (element.CONP_Valor.split(',')[0].length == 3){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '.'));
              // }else if (element.CONP_Valor.split(',')[0].length == 1){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalContas += parseFloat(element.CONP_Valor);
              // }
              this.totalContas += parseFloat(element.CONP_Valor.replace(',', ''));
            });
          } else if (response.status == '0x101') {
            this.emptyResultContas = true;
            this.totalContas = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value = (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2];
      }
    }

  }

  setDateTwo() {
    if (this.pageParams.params.page == 'pagar') {
      if ((<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value = (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2];
        this._http.post(
          'selectContas.php',
          {
            kind: 'pagar',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalContas = 0;
            this.emptyResultContas = false;
            this.contas = [];
            response.result.forEach(element => {
              this.contas.push(element);
              // if (element.CONP_Valor.split(',')[0].length == 3){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '.'));
              // }else if (element.CONP_Valor.split(',')[0].length == 1){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalContas += parseFloat(element.CONP_Valor);
              // }
              this.totalContas += parseFloat(element.CONP_Valor.replace(',', ''));
            });
          } else if (response.status == '0x101') {
            this.emptyResultContas = true;
            this.totalContas = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value = (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2];
      }
    } else if (this.pageParams.params.page == 'pagas') {
      if ((<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value = (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2];
        this._http.post(
          'selectContas.php',
          {
            kind: 'pagas',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalContas = 0;
            this.emptyResultContas = false;
            this.contas = [];
            response.result.forEach(element => {
              this.contas.push(element);
              // if (element.CONP_Valor.split(',')[0].length == 3){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '.'));
              // }else if (element.CONP_Valor.split(',')[0].length == 1){
              //   this.totalContas += parseFloat(element.CONP_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalContas += parseFloat(element.CONP_Valor);
              // }
              this.totalContas += parseFloat(element.CONP_Valor.replace(',', ''));
            });
          } else if (response.status == '0x101') {
            this.emptyResultContas = true;
            this.totalContas = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value = (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2];
      }
    }
  }

  enableDateFilterJob(fj) {
    if (fj == true) {
      this.emptyResult = false;
      this.filtroDataCheckJob = false;
      this.jobs = [];
      this.contas = [];
      this.totalContas = 0;
      this.totalJobs = 0;
      this.ngOnInit();
    } else {
      this.filtroDataCheckJob = true;
      setTimeout(() => {
        $('#dataJobInit').datepicker();
        $('#dataJobEnd').datepicker();
      }, 100);
    }
  }

  setDataOneJob() {
    if (this.pageParams.params.page == 'pagar') {
      if ((<HTMLInputElement>document.getElementById('dataJobEnd')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataJobEnd')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataJobInit')).value = (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2];
        this._http.post(
          'selectJobs.php',
          {
            kind: 'pagar',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalJobs = 0;
            this.emptyResult = false;
            this.jobs = [];
            response.result.forEach(element => {
              this.jobs.push(element);
              // if (element.JOB_Valor.split(',')[0].length == 3){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
              // }else if (element.JOB_Valor.split(',')[0].length == 1){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalJobs += parseFloat(element.JOB_Valor);
              // }
              this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
            });
            this.resultsFilterJobs = this.jobs;
          } else if (response.status == '0x101') {
            this.emptyResult = true;
            this.totalJobs = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataJobInit')).value = (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2];
      }
    } else if (this.pageParams.params.page == 'pagas') {
      if ((<HTMLInputElement>document.getElementById('dataJobEnd')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataJobEnd')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataJobInit')).value = (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2];
        this._http.post(
          'selectJobs.php',
          {
            kind: 'pagas',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalJobs = 0;
            this.emptyResult = false;
            this.jobs = [];
            response.result.forEach(element => {
              this.jobs.push(element);
              // if (element.JOB_Valor.split(',')[0].length == 3){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
              // }else if (element.JOB_Valor.split(',')[0].length == 1){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalJobs += parseFloat(element.JOB_Valor);
              // }
              this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
            });
            this.resultsFilterJobs = this.jobs;
          } else if (response.status == '0x101') {
            this.emptyResult = true;
            this.totalJobs = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataJobInit')).value = (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2];
      }
    }
  }

  setDataTwoJob() {
    if (this.pageParams.params.page == 'pagar') {
      if ((<HTMLInputElement>document.getElementById('dataJobInit')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataJobInit')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataJobEnd')).value = (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2];
        this._http.post(
          'selectJobs.php',
          {
            kind: 'pagar',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalJobs = 0;
            this.emptyResult = false;
            this.jobs = [];
            response.result.forEach(element => {
              this.jobs.push(element);
              // if (element.JOB_Valor.split(',')[0].length == 3){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
              // }else if (element.JOB_Valor.split(',')[0].length == 1){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalJobs += parseFloat(element.JOB_Valor);
              // }
              this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
            });
            this.resultsFilterJobs = this.jobs;
          } else if (response.status == '0x101') {
            this.emptyResult = true;
            this.totalJobs = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataJobEnd')).value = (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2];
      }
    } else if (this.pageParams.params.page == 'pagas') {
      if ((<HTMLInputElement>document.getElementById('dataJobInit')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataJobInit')).value !== undefined) {
        (<HTMLInputElement>document.getElementById('dataJobEnd')).value = (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2];
        this._http.post(
          'selectJobs.php',
          {
            kind: 'pagas',
            filter: 'data',
            dataone: (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobInit')).value.split('/')[0],
            datatwo: (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '-' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0]
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.totalJobs = 0;
            this.emptyResult = false;
            this.jobs = [];
            response.result.forEach(element => {
              this.jobs.push(element);
              // if (element.JOB_Valor.split(',')[0].length == 3){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
              // }else if (element.JOB_Valor.split(',')[0].length == 1){
              //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
              // }else{
              //   this.totalJobs += parseFloat(element.JOB_Valor);
              // }
              this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
            });
            this.resultsFilterJobs = this.jobs;
          } else if (response.status == '0x101') {
            this.emptyResult = true;
            this.totalJobs = 0;
          }
        });
      } else {
        (<HTMLInputElement>document.getElementById('dataJobEnd')).value = (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[1] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[0] + '/' + (<HTMLInputElement>document.getElementById('dataJobEnd')).value.split('/')[2];
      }
    }
  }

  changeState(s, index, type?: string, obj?) {
    if (type !== undefined) {
      switch (type) {
        case 'jobs': {
          switch (s) {
            case 'pago': {
              let c = confirm("KSTrade - Pagar conta\r\n\r\nDeseja realmente marcar como pago?");
              if (c == true) {
                this._http.post(
                  'editJob.php',
                  {
                    status: 'Pago',
                    id: this.jobs[index].JOB_ID
                  }
                ).subscribe((response) => {
                  if (response.status == '0x104') {
                    window.alert('KSTrade - Contas a pagar\r\n\r\nJob pago com sucesso!');
                    this.contas.splice(index, 1);
                  } else if (response.status == '0x101') {
                    window.alert('Erro na alteração de job');
                  }
                });
              }
              break;
            }
            case 'pagar': {
              let c = confirm("KSTrade - Pagar conta\r\n\r\nDeseja realmente reabrir esse Job?");
              if (c == true) {
                this._http.post(
                  'editJob.php',
                  {
                    status: 'A pagar',
                    id: this.jobs[index].JOB_ID
                  }
                ).subscribe((response) => {
                  if (response.status == '0x104') {
                    window.alert('KSTrade - Contas a pagar\r\n\r\Job aberto com sucesso!');
                    this._router.navigate(['/main/innerload']);
                    setTimeout(() => {
                      this._router.navigate(['/main/listaContas', { page: 'pagar' }]);
                    }, 1000);
                  } else if (response.status == '0x101') {
                    window.alert('Erro na alteração de job');
                  }
                });
              }
              break;
            }
          }
          break;
        }
        case '': {
          switch (s) {
            case 'pago': {
              let c = confirm("KSTrade - Pagar conta\r\n\r\nDeseja realmente marcar como pago?");
              if (c == true) {
                this._http.post(
                  'editConta.php',
                  {
                    status: 'Pago',
                    id: obj.CONP_ID,

                    //log
                    usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
                    usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
                    usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
                    nome_pc: this._geo.GetComputerName(),
                    latlong: this.latLong,
                    funcao: 'Conta aberta - Conta marcada como paga[' + obj.CONP_ID + ' / ' + JSON.parse(localStorage.getItem('session'))[0].Usr_Nome + ']'
                  }
                ).subscribe((response) => {
                  if (response.status == '0x104') {
                    window.alert('KSTrade - Contas a pagar\r\n\r\nConta paga com sucesso!');
                    this.contas.splice(index + 1, 1);
                  } else if (response.status == '0x101') {
                    window.alert('Erro na alteração de conta');
                  }
                });
              }
              break;
            }
            case 'pagar': {
              let c = confirm("KSTrade - Pagar conta\r\n\r\nDeseja realmente reabrir essa conta?");
              if (c == true) {
                this._http.post(
                  'editConta.php',
                  {
                    status: 'A pagar',
                    id: obj.CONP_ID,

                    //log
                    usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
                    usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
                    usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
                    nome_pc: this._geo.GetComputerName(),
                    latlong: this.latLong,
                    funcao: 'Conta aberta - Conta marcada como a pagar[' + obj.CONP_ID + ' / ' + JSON.parse(localStorage.getItem('session'))[0].Usr_Nome + ']'
                  }
                ).subscribe((response) => {
                  if (response.status == '0x104') {
                    window.alert('KSTrade - Contas a pagar\r\n\r\nConta aberta com sucesso!');
                    this._router.navigate(['/main/innerload']);
                    setTimeout(() => {
                      this._router.navigate(['/main/listaContas', { page: 'pagar' }]);
                    }, 1000);
                  } else if (response.status == '0x101') {
                    window.alert('Erro na alteração de conta');
                  }
                });
              }
              break;
            }
          }
        }
          break;
      }
    }
  }

  applyFiltroBanco(m) {
    this.jobs = [];
    if (m == true) {
      this.ngOnInit();
    } else {
      this.ngOnInit();
    }
  }

  setFilterBanco(b): void {
    this.bancoFilterChoose = b;
    if (b == 'Selecione o banco') {
      this.ngOnInit();
    } else {
      this.filterArrayBanco(this.bancoFilterChoose, this.jobs);
    }
    this.bancoOK = false;
  }

  filterArrayBanco(model, arr) {
    this.totalJobs = 0;
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterJobs = arr.filter((e) => {
      return regexp.test(e.PRO_Banco);
    });
    this.resultsFilterJobs.forEach(element => {
      // if (element.JOB_Valor.split(',')[0].length == 3){
      //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
      // }else if (element.JOB_Valor.split(',')[0].length == 1){
      //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
      // }else{
      //   this.totalJobs += parseFloat(element.JOB_Valor);
      // }
      this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
    });
  }

  filterArrayCliente(model, arr) {
    this.totalJobs = 0;
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterJobs = arr.filter((e) => {
      return regexp.test(e.Cli_Nome);
    });
    this.resultsFilterJobs.forEach(element => {
      // if (element.JOB_Valor.split(',')[0].length == 3){
      //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
      // }else if (element.JOB_Valor.split(',')[0].length == 1){
      //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
      // }else{
      //   this.totalJobs += parseFloat(element.JOB_Valor);
      // }
      this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
    });
  }

  filterArrayUsuario(model, arr) {
    this.totalJobs = 0;
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterJobs = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
    this.resultsFilterJobs.forEach(element => {
      // if (element.JOB_Valor.split(',')[0].length == 3){
      //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '.'));
      // }else if (element.JOB_Valor.split(',')[0].length == 1){
      //   this.totalJobs += parseFloat(element.JOB_Valor.replace(',', '').replace('.00', ',00'));
      // }else{
      //   this.totalJobs += parseFloat(element.JOB_Valor);
      // }
      this.totalJobs += parseFloat(element.JOB_Valor.replace(',', ''));
    });
  }


  removeConta(c): void {
    let conf = confirm('KSTrade - Contas a pagar\r\n\r\nDeseja realmente remover?');
    if (conf == true) {
      this._http.post(
        'editConta.php',
        {
          id: c.CONP_ID,
          unique: true
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Contas a pagar\r\n\r\nConta removida com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaContas', { page: 'pagar' }]);
          }, 1500);
        } else if (response.status == '0x101') {
          window.alert('Erro na remoção de conta!');
        }
      });
    }
  }


  makePDF() {
    document.getElementById('contasPagarTable').style.display = 'block';
    html2canvas(document.getElementById('contasPagarTable')).then((canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("", '_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
      document.getElementById('contasPagarTable').style.display = 'none';
    });
  }

  pagarTodosJobs() {
    let c = confirm('KSTrade - Lista de contas a pagar\r\n\r\nDeseja realmente marcar todos como pago?');
    if (c == true) {
      for (let i = 0; i < this.resultsFilterJobs.length; i++) {
        if ((<HTMLInputElement>document.getElementById('jobCheckBox_' + i)).checked == true) {
          this._http.post(
            'editJob.php',
            {
              status: 'Pago',
              id: this.resultsFilterJobs[i].JOB_ID
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              console.log('ok');
            }
          });
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaContas', { page: 'pagar' }]);
          }, 1500);
        }
      }
    }
  }

  abrirTodosJobs() {
    let c = confirm('KSTrade - Lista de contas a pagar\r\n\r\nDeseja realmente marcar todos como aberto?');
    if (c == true) {
      for (let i = 0; i < this.resultsFilterJobs.length; i++) {
        if ((<HTMLInputElement>document.getElementById('jobCheckBox_' + i)).checked == true) {
          this._http.post(
            'editJob.php',
            {
              status: 'A pagar',
              id: this.resultsFilterJobs[i].JOB_ID
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              console.log('ok');
            }
          });
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaContas', { page: 'pagas' }]);
          }, 1500);
        }
      }
    }
  }

  pagarTodosConta() {
    let c = confirm('KSTrade - Lista de contas a pagar\r\n\r\nDeseja realmente marcar todos como pago?');
    if (c == true) {
      for (let i = 0; i < this.contas.length; i++) {
        if ((<HTMLInputElement>document.getElementById('contaCheckBox_' + i)).checked == true) {
          this._http.post(
            'editConta.php',
            {
              status: 'Pago',
              id: this.contas[i].CONP_ID
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              console.log('ok');
            }
          });
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaContas', { page: 'pagar' }]);
          }, 1500);
        }
      }
    }
  }

  abrirTodosConta() {
    let c = confirm('KSTrade - Lista de contas a pagar\r\n\r\nDeseja realmente marcar todos como aberto?');
    if (c == true) {
      for (let i = 0; i < this.contas.length; i++) {
        if ((<HTMLInputElement>document.getElementById('contaCheckBox_' + i)).checked == true) {
          this._http.post(
            'editConta.php',
            {
              status: 'A pagar',
              id: this.contas[i].CONP_ID
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              console.log('ok');
            }
          });
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaContas', { page: 'pagas' }]);
          }, 1500);
        }
      }
    }
  }

  isSelectedAllJobs: boolean = false;

  selectAllJobs() {
    if (this.isSelectedAllJobs == true) {
      for (let i = 0; i < this.resultsFilterJobs.length; i++) {
        (<HTMLInputElement>document.getElementById('jobCheckBox_' + i)).checked = false;
      }
      this.isSelectedAllJobs = false;
    } else {
      this.isSelectedAllJobs = true;
      for (let i = 0; i < this.resultsFilterJobs.length; i++) {
        (<HTMLInputElement>document.getElementById('jobCheckBox_' + i)).checked = true;
      }
    }
  }

  isSelectedAllContas: boolean = false;

  selectAllContas() {
    if (this.isSelectedAllContas == true) {
      for (let i = 0; i < this.contas.length; i++) {
        (<HTMLInputElement>document.getElementById('contaCheckBox_' + i)).checked = false;
      }
      this.isSelectedAllContas = false;
    } else {
      this.isSelectedAllContas = true;
      for (let i = 0; i < this.contas.length; i++) {
        (<HTMLInputElement>document.getElementById('contaCheckBox_' + i)).checked = true;
      }
    }
  }
}
