import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaContasPagarComponent } from './lista-contas-pagar.component';

describe('ListaContasPagarComponent', () => {
  let component: ListaContasPagarComponent;
  let fixture: ComponentFixture<ListaContasPagarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaContasPagarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaContasPagarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
