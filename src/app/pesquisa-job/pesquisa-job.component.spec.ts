import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PesquisaJobComponent } from './pesquisa-job.component';

describe('PesquisaJobComponent', () => {
  let component: PesquisaJobComponent;
  let fixture: ComponentFixture<PesquisaJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PesquisaJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PesquisaJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
