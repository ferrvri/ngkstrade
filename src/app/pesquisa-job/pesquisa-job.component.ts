import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../http.service';
import { UtilsService } from '../utils.service';

declare var $: any;

@Component({
  selector: 'app-pesquisa-job',
  templateUrl: './pesquisa-job.component.html',
  styleUrls: ['./pesquisa-job.component.css']
})
export class PesquisaJobComponent implements OnInit {

  pageParams: any;
  cartoes = [];

  constructor( private _utils: UtilsService, private _activatedRoute: ActivatedRoute, private _http: HttpService, private _router: Router ) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    console.log(this.pageParams);

    this._http.post(
      'app/selectProdutos.php',
      {
        cliente: this.pageParams.params.cliente
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          element.rupturaTotal = false;
          element.prateleira = false;
          element.estoque = false;
          element.data_vencimento = [
            {
              id: 1,
              value : ''
            }
          ];
          element.quantidade = [
            {
              id: 1,
              value : ''
            }
          ];
          element.data_vencimento_estoque = '';
          element.preco = '';
          element.n_frentes = '';
          element.n_frentes_concorrente = '';
          this.cartoes.push(element);
        });
        setTimeout( () => {
          $('.data_vencimentoInput').mask('00/00/0000');
          $('.data_vencimento_lote').mask('00/00/0000');
          $('#title').text($('#title').text()+this.cartoes[0].Cli_Nome)
        }, 150);
      }
    });
  }

  addData(index){
    this.cartoes[index].data_vencimento.push({
      id: this.cartoes.length + 1,
      value : ''
    });
    setTimeout( () => {
      $('.data_vencimentoInput').mask('00/00/0000');
      $('.data_vencimento_lote').mask('00/00/0000');
    }, 150);
  }

  removeData(index, indexx){
    this.cartoes[index].data_vencimento.splice(indexx, 1);
  }

  addQuant(index){
    this.cartoes[index].quantidade.push({
      id: this.cartoes.length + 1,
      value : ''
    });
  }

  removeQuant(index, indexx){
    this.cartoes[index].quantidade.splice(indexx, 1);
  }

  applyMoneyMask(c){
    c.preco = this._utils.detectAmount(c.preco);
  }

  cadPesquisa(){
    let exp = {};
    let doneExp = [];
    try{
      this.cartoes.forEach( (c) => {
        if (c.rupturaTotal == false){
          if (c.preco.length < 1 || c.data_vencimento.length < 1 || (c.estoque == false && c.prateleira == false && c.rupturaTotal == false) ){
            throw exp;
          }
        }
        let data = '';

        c.data_vencimento.forEach(element => {
          data += element.value+';';
        });

        let quantidade = '';

        c.quantidade.forEach(element => {
          quantidade += element.value+';';
        });

        this._http.post(
          'app/insertPesquisa.php',
          {
            pro_id: c.pro_id,
            data_vencimento:  data,
            data_vencimento_estoque: c.data_vencimento_estoque,
            preco: c.preco,
            nfrentes: c.n_frentes,
            nfrentes_concorrente: c.n_frentes_concorrente,
            nmaxfrentes: c.n_max_frentes,
            quantidade: quantidade,
            ruptura: c.rupturaTotal == true ? '1' : '0',
            estoque: c.estoque == true ? '1' : '0',
            prateleira: c.prateleira == true ? '1' : '0',
            rede: this.pageParams.params.rede,
            cliente: this.pageParams.params.cliente,
            promotor: this.pageParams.params.promotor,
            filial: this.pageParams.params.filial,
            data: this.pageParams.params.data,
            hora: this.pageParams.params.hora
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            doneExp.push('true');
          }else if (response.status == '0x101'){
            doneExp.push('false');
          }
        });
      });

      setTimeout( () => {
        if ('false' in doneExp){
          window.alert('KSTrade - Cadastro de Pesquisa\r\n\r\nA pesquisa não foi cadastrada, por favor contate-nos');
        }else{
          window.alert('KSTrade - Cadastro de Pesquisa\r\n\r\nPesquisa cadastrada com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/dashboard']);
          }, 1500);
        }
      },1500);
    }catch(exp){
      window.alert('KSTrade - Cadastro de Pesquisa\r\n\r\nPreencha todos os campos!');
    }
  }
}
