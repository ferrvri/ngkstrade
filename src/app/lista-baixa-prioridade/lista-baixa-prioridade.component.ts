import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';

declare var showDialog: any;
declare var html2canvas: any;
declare var $: any;

@Component({
  selector: 'app-lista-baixa-prioridade',
  templateUrl: './lista-baixa-prioridade.component.html',
  styleUrls: ['./lista-baixa-prioridade.component.css']
})
export class ListaBaixaPrioridadeComponent implements OnInit {

  filiais : any = [];
  resultsFilterFiliais = [];

  emptyResult: boolean = false;
  doneLoading: boolean =  false;

  jobImage: string = ''; 
  jobTitle: string = '';
  queryFiliais:string = '';
  queryPromotor: string = '';

  enableImprimir: boolean = false;
  p: number = 0; 

  constructor(private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    $('#dataInit').datepicker();
    $('#dataEnd').datepicker();
    let result = [];
    if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador'){
      this.doneLoading = false;
      this._http.post(
        'selectRedes.php',
        {}
      ).subscribe( (response1) => {
        if (response1.status == '0x104'){
          this.filiais = [];
          if (response1.result !== undefined || response1.result.length > 0){
            response1.result.forEach( (e1) => {
              let redes = {
                nome: e1.RED_Nome,
                id: e1.RED_ID,
                content: [],
                visible: true
              };
    
              this._http.post(
                'selectFilial.php',
                {
                  id: redes.id,
                  type: 'pendente'
                }
              ).subscribe( (response2) => {
                if (response2.status == '0x104'){
                  response2.result.forEach(element => {
                    JSON.parse(localStorage.getItem('clientes')).forEach(elemCli => {
                      this._http.post(
                        'selectBaixaPrioridade.php',
                        {
                          unique: true,
                          id: element.RFIL_ID,
                          cli_id: elemCli.Cli_ID,
                          dataone: new Date(Date.now()).toISOString().substring(0,10),
                          datatwo: new Date(Date.now()).toISOString().substring(0,10)
                        }
                      ).subscribe( (response3) => {
                        if (response3.status == '0x104'){
                          if (response3.result.length > 0){
                            element.jobs = response3.result;
                            redes.content.push(element);
                          }
                        }
                      });
                    });
                  });
                  this.filiais.push(redes);
                  setTimeout( () => {
                    this.doneLoading = true;
                    this.resultsFilterFiliais = this.filiais;
                  }, 5000);
                }
              });
            });
          }else if (response1.status == '0x101'){
            this.emptyResult = true;
          }
        }
      });    
    }else{
      this.doneLoading = false;
      this._http.post(
        'selectRedes.php',
        {}
      ).subscribe( (response1) => {
        if (response1.status == '0x104'){
          this.filiais = [];
          if (response1.result !== undefined || response1.result.length > 0){
            response1.result.forEach( (e1) => {
              let redes = {
                nome: e1.RED_Nome,
                id: e1.RED_ID,
                content: [],
                visible: true
              };
    
              this._http.post(
                'selectFilial.php',
                {
                  id: redes.id,
                  type: 'pendente'
                }
              ).subscribe( (response2) => {
                if (response2.status == '0x104'){
                  response2.result.forEach(element => {
                    this._http.post(
                      'selectBaixaPrioridade.php',
                      {
                        id: element.RFIL_ID,
                        dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2] + '-' +
                        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '-' +
                        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
                        datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2] + '-' +
                        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '-' +
                        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
                      }
                    ).subscribe( (response3) => {
                      if (response3.status == '0x104'){
                        if (response3.result.length > 0){
                          element.jobs = response3.result;
                          redes.content.push(element);
                        }
                      }
                    });
                  });
                  this.filiais.push(redes);
                  setTimeout( () => {
                    this.doneLoading = true;
                    this.resultsFilterFiliais = this.filiais;
                  }, 5000);
                }
              })
            });
          }else if (response1.status == '0x101'){
            this.emptyResult = true;
          }
        }
      });    
    }
  }


  showfilial(){
    console.log(this.filiais);
  }

  openImagemJob(j){
    this.jobImage = 'uploads/'+j.JOB_Foto;
    this.jobTitle = 'Cliente: '+ j.Cli_Nome + ' - Data / Hora: ' + j.JOB_Data_Hora_Input2 + '- Observação: '+ j.JOB_Obs;

    showDialog({
      title: this.jobTitle,
      image: this.jobImage,
      close: {
        title: 'Fechar',
        onClick: null
      }
    });
  }

  filterArrayFiliais(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterFiliais = arr.filter( (e) => {
      return regexp.test(e.nome);
    });
  }

  searchFiliais(string){
    if (string.length < 1){
      this.enableImprimir = false;
    }else{
      this.enableImprimir = true;
    }
    this.filterArrayFiliais(string, this.filiais);
  }

  makePDF() {
    document.getElementById('bProdutividadeTable').style.display = 'block';
    html2canvas(document.getElementById('bProdutividadeTable')).then( (canvas) => {
      let w = window.open("",'_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="'+(<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1)+'" /></body></html>');
      document.getElementById('bProdutividadeTable').style.display = 'none';
    });
  }

  alreadySearch: boolean = false;

  searchProdutividade(){
    this.filiais = [];
    this.doneLoading = false;
    this.alreadySearch = true;
    if ((<HTMLInputElement>document.getElementById('dataInit')).value != '' && (<HTMLInputElement>document.getElementById('dataEnd')).value != ''){
      if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador'){
        this.doneLoading = false;
        this._http.post(
          'selectRedes.php',
          {}
        ).subscribe( (response1) => {
          if (response1.status == '0x104'){
            this.filiais = [];
            if (response1.result !== undefined || response1.result.length > 0){
              response1.result.forEach( (e1) => {
                let redes = {
                  nome: e1.RED_Nome,
                  id: e1.RED_ID,
                  content: [],
                  visible: true
                };
      
                this._http.post(
                  'selectFilial.php',
                  {
                    id: redes.id,
                    type: 'pendente'
                  }
                ).subscribe( (response2) => {
                  if (response2.status == '0x104'){
                    response2.result.forEach(element => {
                      JSON.parse(localStorage.getItem('clientes')).forEach(elemCli => {
                        this._http.post(
                          'selectBaixaPrioridade.php',
                          {
                            unique: true,
                            id: element.RFIL_ID,
                            cli_id: elemCli.Cli_ID,
                            dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2] + '-' +
                            (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '-' +
                            (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
                            datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2] + '-' +
                            (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '-' +
                            (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
                          }
                        ).subscribe( (response3) => {
                          if (response3.status == '0x104'){
                            if (response3.result.length > 0){
                              element.jobs = response3.result;
                              redes.content.push(element);
                            }
                          }
                        });

                      });
                    });
                    this.filiais.push(redes);
                    setTimeout( () => {
                      this.doneLoading = true;
                      this.resultsFilterFiliais = this.filiais;
                      $('#dataInit').datepicker();
                      $('#dataEnd').datepicker();
                    }, 5000);
                  }
                });
              });
            }else if (response1.status == '0x101'){
              this.emptyResult = true;
            }
          }
        });    
      }else{
        this.doneLoading = false;
        this._http.post(
          'selectRedes.php',
          {}
        ).subscribe( (response1) => {
          if (response1.status == '0x104'){
            this.filiais = [];
            if (response1.result !== undefined || response1.result.length > 0){
              response1.result.forEach( (e1) => {
                let redes = {
                  nome: e1.RED_Nome,
                  id: e1.RED_ID,
                  content: [],
                  visible: true
                };
      
                this._http.post(
                  'selectFilial.php',
                  {
                    id: redes.id,
                    type: 'pendente'
                  }
                ).subscribe( (response2) => {
                  if (response2.status == '0x104'){
                    response2.result.forEach(element => {
                      this._http.post(
                        'selectBaixaPrioridade.php',
                        {
                          id: element.RFIL_ID,
                          dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2] + '-' +
                          (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '-' +
                          (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
                          datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2] + '-' +
                          (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '-' +
                          (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
                        }
                      ).subscribe( (response3) => {
                        if (response3.status == '0x104'){
                          if (response3.result.length > 0){
                            element.jobs = response3.result;
                            redes.content.push(element);
                          }
                        }
                      });
                    });
                    this.filiais.push(redes);
                    setTimeout( () => {
                      this.doneLoading = true;
                      this.resultsFilterFiliais = this.filiais;
                    }, 5000);
                  }
                })
              });
            }else if (response1.status == '0x101'){
              this.emptyResult = true;
            }
          }
        });    
      }
    }
  }

  clearData(){
     this.alreadySearch = false;
     this.ngOnInit();
    (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
  }

  setDateOne(){
    if((<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] === undefined){
      (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    }else{
      (<HTMLInputElement>document.getElementById('dataInit')).value = 
      (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '/' +
      (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0] + '/' +
      (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2];
    }
  }

  setDateTwo(){
    if((<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] === undefined){
      (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
    }else{
      (<HTMLInputElement>document.getElementById('dataEnd')).value = 
      (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '/' +
      (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0] + '/' +
      (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2];
    }
  }

  queryPromotores: string = '';

  searchPromotores(q){
    if (q.length < 1){
      this.enableImprimir = false;
      this.resultsFilterFiliais = this.filiais;
    }else{
      this.enableImprimir = true;
      this.filterArrayPromotores(q, this.filiais);
    }
  }

  filterArrayPromotores(model, arr){
    this.resultsFilterFiliais = [];
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    arr.forEach(e1 => {
      let redes = {
        nome: e1.nome,
        id: e1.id,
        content: [],
        visible: true,
        atendimentoTotal: 0
      };
      
      e1.content.forEach(element => {
        element.jobs.forEach(e2 => {
          if (regexp.test(e2.PRO_Nome)){
            redes.content.push(element);
          } 
        });
        // if (regexp.test(element.PRO_Nome)){
        //   console.log(element.PRO_Nome);
        //   redes.content.push(element);
        // }else{
        //   console.log(element.PRO_Nome);
        // }
      });
      redes.atendimentoTotal = redes.content.length || 0;
      this.resultsFilterFiliais.push(redes);

      // console.log(this.resultsFilterFiliais);
    });   
  }

}
