import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaBaixaPrioridadeComponent } from './lista-baixa-prioridade.component';

describe('ListaBaixaPrioridadeComponent', () => {
  let component: ListaBaixaPrioridadeComponent;
  let fixture: ComponentFixture<ListaBaixaPrioridadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaBaixaPrioridadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaBaixaPrioridadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
