import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule } from '@angular/router';
import { ReactiveFormsModule} from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


// import {HTMLDialogElement} from '@types/react';

import { AppComponent } from './app.component';
import { LoginComponent } from '../app/login/login.component';
import { MainComponent } from '../app/main/main.component';
import { FormsModule } from '@angular/forms';
import { LoadingComponent } from './loading/loading.component';
import { ExampleComponent } from './example/example.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CadastroClienteComponent } from './cadastro-cliente/cadastro-cliente.component';
import { ListaClientesComponent } from './lista-clientes/lista-clientes.component';
import { CadRedeComponent } from './cad-rede/cad-rede.component';
import { ListaRedesComponent } from './lista-redes/lista-redes.component';
import { EditarClienteComponent } from './editar-cliente/editar-cliente.component';
import { EditarRedeComponent } from './editar-rede/editar-rede.component';
import { CadFilialComponent } from './cad-filial/cad-filial.component';
import { ListaFilialComponent } from './lista-filial/lista-filial.component';
import { JobsComponent } from './jobs/jobs.component';
import { ListaJobsComponent } from './lista-jobs/lista-jobs.component';
import { CadPromotorComponent } from './cad-promotor/cad-promotor.component';
import { ListaPromotorComponent } from './lista-promotor/lista-promotor.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ListaUsuarioComponent } from './lista-usuario/lista-usuario.component';
import { CadContaComponent } from './cad-conta/cad-conta.component';
import { ListaContasPagarComponent } from './lista-contas-pagar/lista-contas-pagar.component';
import { CadContatoComponent } from './cad-contato/cad-contato.component';
import { ListaContatoComponent } from './lista-contato/lista-contato.component';
import { InnerLoadComponent } from './inner-load/inner-load.component';
import { CadServicoComponent } from './cad-servico/cad-servico.component';
import { ListaServicoComponent } from './lista-servico/lista-servico.component';
import { MensagensComponent } from './mensagens/mensagens.component';
import { ViewmensagemComponent } from './viewmensagem/viewmensagem.component';
import { ValorJobComponent } from './valor-job/valor-job.component';
import { TabelaValoresComponent } from './tabela-valores/tabela-valores.component';
import { CadContasReceberComponent } from './cad-contas-receber/cad-contas-receber.component';
import { ListaContasReceberComponent } from './lista-contas-receber/lista-contas-receber.component';
import { OtrComponent } from './otr/otr.component';
import { LoginClienteComponent } from './cliente/login-cliente/login-cliente.component';
import { DashboardClienteComponent } from './cliente/dashboard-cliente/dashboard-cliente.component';
import { MainClienteComponent } from './cliente/main-cliente/main-cliente.component';
import { RedesComponent } from './cliente/redes/redes.component';
import { FiliaisComponent } from './cliente/filiais/filiais.component';
import { RegistroVendedorComponent } from './registro-vendedor/registro-vendedor.component';
import { ViewMessageVendedorComponent } from './cliente/view-message-vendedor/view-message-vendedor.component';
import { VendedorMensagensComponent } from './cliente/vendedor-mensagens/vendedor-mensagens.component';
import { ListaJobsPendentesComponent } from './lista-promotores-produtividade/lista-jobs-pendentes.component';
import { ListaBaixaPrioridadeComponent } from './lista-baixa-prioridade/lista-baixa-prioridade.component';
import { PromotorProdutividadeComponent } from './promotor-produtividade/promotor-produtividade.component';
import { CadRotinaComponent } from './cad-rotina/cad-rotina.component';
import { ListaRotinaComponent } from './lista-rotina/lista-rotina.component';
import { InatividadeCoordComponent } from './inatividade-coord/inatividade-coord.component';
import { OnLongPressDirective } from './on-long-press.directive';

import {NgxPaginationModule} from 'ngx-pagination';
import { PromotorLoginComponent } from './promotor-login/promotor-login.component';
import { ImagesGalleryComponent } from './images-gallery/images-gallery.component';
import { AddNewsComponent } from './add-news/add-news.component';
import { ListaNoticiaComponent } from './lista-noticia/lista-noticia.component';
import { PesquisaComponent } from './pesquisa/pesquisa.component';
import { ListaPesquisaComponent } from './lista-pesquisa/lista-pesquisa.component';
import { PesquisaJobComponent } from './pesquisa-job/pesquisa-job.component';
import { UtilsService } from './utils.service';
import { QuizComponent } from './quiz/quiz.component';
import { ListaQuizComponent } from './lista-quiz/lista-quiz.component';
import { QuizUserComponent } from './quiz-user/quiz-user.component';
import { PesquisaClienteComponent } from './cliente/pesquisa-cliente/pesquisa-cliente.component';
import { CadGrupoNoticiaComponent } from './cad-grupo-noticia/cad-grupo-noticia.component';
import { ListaRotasComponent } from './lista-rotas/lista-rotas.component';
import { AtendimentoEmergencialComponent } from './atendimento-emergencial/atendimento-emergencial.component';
import { ListaComentariosComponent } from './lista-comentarios/lista-comentarios.component';
import { CadComentariosComponent } from './cad-comentarios/cad-comentarios.component';
import { ListaComentariosDefinidosComponent } from './lista-comentarios-definidos/lista-comentarios-definidos.component';
import { MasterAreaComponent } from './master-area/master-area.component';
import { BooksComponent } from './cliente/books/books.component';
import { CadBookComponent } from './cad-book/cad-book.component';
import { ComentariosJobsComponent } from './comentarios-jobs/comentarios-jobs.component';
import { PedidosEmetasComponent } from './pedidos-emetas/pedidos-emetas.component';
import { GeolocationServiceService } from './geolocation-service.service';
import { LogsComponent } from './logs/logs.component';
import { ExcelService } from './excel.service';
import { LixeiraComponent } from './lixeira/lixeira.component';
import { HttpService } from './http.service';


export const ROUTES = [
  //{path: '', pathMatch: "full", component: LoadingComponent},
  {path: 'clienteLogin', component: LoginClienteComponent},
  {path: 'cliente', component: MainClienteComponent, children: [
      // {path: '', component: MainClienteComponent, pathMatch: 'full'},
      {path: 'dashboard', component: DashboardClienteComponent},
      {path: 'redes', component: RedesComponent},
      {path: 'filiais', component: FiliaisComponent},
      {path: 'viewMensagem', component: ViewMessageVendedorComponent},
      {path: 'mensagens', component: VendedorMensagensComponent},
      {path: 'pesquisa', component: PesquisaClienteComponent},
      {path: 'cadQuizUser', component: QuizUserComponent},
      {path: 'cadBook', component: CadBookComponent}
    ]
  },
  {path: 'loading', component: LoadingComponent},
  {path: 'login', component: LoginComponent},
  {path: 'main', component: MainComponent, children: [
      {path: 'innerload', component: InnerLoadComponent},
      {path: 'dashboard', component: DashboardComponent},
      {path: 'cadastroCliente', component: CadastroClienteComponent},
      {path: 'listaClientes', component: ListaClientesComponent},
      {path: 'editarCliente', component: EditarClienteComponent},
      {path: 'cadastrarRede', component: CadRedeComponent},
      {path: 'listaRedes', component: ListaRedesComponent},
      {path: 'editarRede', component: EditarRedeComponent},
      {path: 'cadFilial', component: CadFilialComponent},
      {path: 'listaFilial', component: ListaFilialComponent},
      {path: 'novoJob', component: JobsComponent},
      {path: 'listaJobs', component: ListaJobsComponent},
      {path: 'listaPendentes', component: ListaJobsPendentesComponent},
      {path: 'listaBaixaP', component: ListaBaixaPrioridadeComponent},
      {path: 'valorJob', component: ValorJobComponent},
      {path: 'listaValorJob', component: TabelaValoresComponent},
      {path: 'cadPromotor', component: CadPromotorComponent},
      {path: 'listaPromotor', component: ListaPromotorComponent},
      {path: 'promotorLogin', component: PromotorLoginComponent},
      {path: 'pProdutividade', component: PromotorProdutividadeComponent},
      {path: 'cadUsuario', component: UsuariosComponent},
      {path: 'listaUsuario', component: ListaUsuarioComponent},
      {path: 'cadConta', component: CadContaComponent},
      {path: 'listaContas', component: ListaContasPagarComponent},
      {path: 'cadContato', component: CadContatoComponent},
      {path: 'listaContato', component: ListaContatoComponent},
      {path: 'cadServico', component: CadServicoComponent},
      {path: 'listaServico', component: ListaServicoComponent},
      {path: 'mensagens', component: MensagensComponent},
      {path: 'viewMensagem', component: ViewmensagemComponent},
      {path: 'cadContasReceber', component: CadContasReceberComponent},
      {path: 'listaContasReceber', component: ListaContasReceberComponent},
      {path: 'cadOTR', component: OtrComponent},
      {path: 'registroVendedor', component: RegistroVendedorComponent},
      {path: 'cadRotina', component: CadRotinaComponent},
      {path: 'listaRotina', component: ListaRotinaComponent},
      {path: 'inatividadeCoord', component: InatividadeCoordComponent},
      {path: 'addNoticia', component: AddNewsComponent},
      {path: 'listaNoticia', component: ListaNoticiaComponent},
      {path: 'cadGrupoNoticia', component: CadGrupoNoticiaComponent},
      {path: 'pesquisa', component: PesquisaComponent},
      {path: 'listaPesquisa', component: ListaPesquisaComponent},
      {path: 'cadPesquisaJob', component: PesquisaJobComponent},
      {path: 'quiz', component: QuizComponent},
      {path: 'listaQuiz', component: ListaQuizComponent},
      {path: 'cadQuizUser', component: QuizUserComponent},
      {path: 'listaRotas', component: ListaRotasComponent},
      {path: 'atendimentos', component: AtendimentoEmergencialComponent},
      {path: 'listarComentarios', component: ListaComentariosComponent},
      {path: 'listaComentariosDef', component: ListaComentariosDefinidosComponent},
      {path: 'cadComentariosDef', component: CadComentariosComponent},
      {path: 'cadBook', component: CadBookComponent},
      {path: 'comentJobs', component: ComentariosJobsComponent},

      {path: 'PedidosEMetas', component: PedidosEmetasComponent},
      {path: 'logs', component: LogsComponent},

      {path: 'lixeira', component: LixeiraComponent},
      
      //master for admin
      {path: 'master', component: MasterAreaComponent}
    ]
  }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    LoadingComponent,
    ExampleComponent,
    DashboardComponent,
    CadastroClienteComponent,
    ListaClientesComponent,
    CadRedeComponent,
    ListaRedesComponent,
    EditarClienteComponent,
    EditarRedeComponent,
    CadFilialComponent,
    ListaFilialComponent,
    JobsComponent,
    ListaJobsComponent,
    CadPromotorComponent,
    ListaPromotorComponent,
    UsuariosComponent,
    ListaUsuarioComponent,
    CadContaComponent,
    ListaContasPagarComponent,
    CadContatoComponent,
    ListaContatoComponent,
    InnerLoadComponent,
    CadServicoComponent,
    ListaServicoComponent,
    MensagensComponent,
    ViewmensagemComponent,
    ValorJobComponent,
    TabelaValoresComponent,
    CadContasReceberComponent,
    ListaContasReceberComponent,
    OtrComponent,
    LoginClienteComponent,
    DashboardClienteComponent,
    MainClienteComponent,
    RedesComponent,
    FiliaisComponent,
    RegistroVendedorComponent,
    ViewMessageVendedorComponent,
    VendedorMensagensComponent,
    ListaJobsPendentesComponent,
    ListaBaixaPrioridadeComponent,
    PromotorProdutividadeComponent,
    CadRotinaComponent,
    ListaRotinaComponent,
    InatividadeCoordComponent,
    OnLongPressDirective,
    PromotorLoginComponent,
    ImagesGalleryComponent,
    AddNewsComponent,
    ListaNoticiaComponent,
    PesquisaComponent,
    ListaPesquisaComponent,
    PesquisaJobComponent,
    QuizComponent,
    ListaQuizComponent,
    QuizComponent,
    QuizUserComponent,
    PesquisaClienteComponent,
    CadGrupoNoticiaComponent,
    ListaRotasComponent,
    AtendimentoEmergencialComponent,
    ListaComentariosComponent,
    CadComentariosComponent,
    ListaComentariosDefinidosComponent,
    MasterAreaComponent,
    BooksComponent,
    CadBookComponent,
    ComentariosJobsComponent,
    PedidosEmetasComponent,
    LogsComponent,
    LixeiraComponent
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    RouterModule.forRoot(ROUTES, {useHash: true}),
    NgxPaginationModule
  ],
  providers: [ExcelService, UtilsService, GeolocationServiceService, HttpService],
  bootstrap: [AppComponent]
})
export class AppModule { }
