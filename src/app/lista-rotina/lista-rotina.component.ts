import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-lista-rotina',
  templateUrl: './lista-rotina.component.html',
  styleUrls: ['./lista-rotina.component.css']
})
export class ListaRotinaComponent implements OnInit {

  resultsFilterPromotores: any = [];
  queryPromotor: string = '';

  promotores: any = []
  doneLoading: boolean = false;
  emptyResult: boolean = false;

  selectAll: boolean = false;

  userData: any = [];

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    this._http.post(
      'selectRotina.php',
      {
        all: true
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          console.log(element);
          element.menuRow = false;
          element.content.forEach(element2 => {
            element2.dias_semana = '';
            element2.aux_semana.split(';').forEach((element3, index) => {
              if (index == (element2.aux_semana.split(';').length - 1)) {
                element2.dias_semana += element3
              } else {
                element2.dias_semana += element3 + ' / ';
              }
            });
          });
          this.promotores.push(element);
        });
        this.resultsFilterPromotores = this.promotores;
        this.doneLoading = true;
      }
    });
  }

  filterArrayPromotores(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultsFilterPromotores = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
  }


  searchPromotor(string) {
    if (string.length < 1) {
      this.resultsFilterPromotores = this.promotores;
    } else {
      this.filterArrayPromotores(string, this.promotores);
    }
  }

  deleteSelected() {
    let done = [];
    let cc = confirm('KSTrade Merchandising\r\n\r\nDeseja realmente remover os selecionados?');
    if (cc == true) {
      this.resultsFilterPromotores.forEach((element, index) => {
        element.content.forEach((element2, index2) => {
          if ((<HTMLInputElement>document.getElementById('rotinaTR-' + index + '-' + index2)).checked == true) {
            this._http.post(
              'deleteRotina.php',
              {
                id: element2.aux_pro_id,
                //log
                usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
                usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
                usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
                nome_pc: this._geo.GetComputerName(),
                latlong: this.latLong,
                funcao: 'Deleção - Rotina selecionada[' + element2.aux_pro_id + ']'
              }
            ).subscribe((response) => {
              if (response.status == '0x104') {
                done.push('true');
              } else {
                done.push('false');
              }
            });
          }
        });
      });

      if ('false' in done) {
        window.alert('KSTrade Merchandising\r\n\r\nErro ao remover!');
      } else {
        window.alert('KSTrade Merchandising\r\n\r\nRemovido com sucesso!');
        this._router.navigate(['/main/innerload']);
        setTimeout(() => {
          this._router.navigate(['/main/listaRotina']);
        }, 1500);
      }
    }
  }

  deleteRotina(c, fI) {
    let cc = confirm('KSTrade Merchandising\r\n\r\nDeseja realmente remover esta rotina?');
    if (cc == true) {
      this._http.post(
        'deleteRotina.php',
        {
          id: c.aux_pro_id,
          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Rotina[' + c.aux_pro_id + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising\r\n\r\nRemovido com sucesso!');
          this.resultsFilterPromotores.forEach((element, index) => {
            console.log(element);
            element.content.forEach((element, index2) => {
              if (index2 == fI) {
                console.log('ok');
                this.resultsFilterPromotores[index].content.splice(index2, 1);
              } else {
                console.log('not');
              }
            });
          });
        }
      });
    }
  }

  removeAllRotina(data, index) {
    let r = []
    let cc = confirm('KSTrade Merchandising\r\n\r\nDeseja realmente remover esta rotina?');

    if (cc == true) {
      data.content.forEach(element => {
        this._http.post(
          'deleteRotina.php',
          {
            id: element.aux_pro_id,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Deleção - Rotina completa[' + element.aux_pro_id + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            r.push(true)
          } else {
            r.push(false)
          }
        })
      })
    }

    let x = {}
    try {
      r.forEach((e) => {
        if (e == false) {
          throw x;
        }
      });
      window.alert('KSTrade Merchandising - Rotinas\r\n\r\Rotina removida com sucesso!')
      this.resultsFilterPromotores.splice(index, 1)
    } catch (x) {
      window.alert('KSTrade Merchandising - Rotinas\r\n\r\nErro ao remover Rotina.')
    }
  }

}
