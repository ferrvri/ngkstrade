import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRotinaComponent } from './lista-rotina.component';

describe('ListaRotinaComponent', () => {
  let component: ListaRotinaComponent;
  let fixture: ComponentFixture<ListaRotinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRotinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRotinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
