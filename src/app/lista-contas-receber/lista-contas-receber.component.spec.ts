import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaContasReceberComponent } from './lista-contas-receber.component';

describe('ListaContasReceberComponent', () => {
  let component: ListaContasReceberComponent;
  let fixture: ComponentFixture<ListaContasReceberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaContasReceberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaContasReceberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
