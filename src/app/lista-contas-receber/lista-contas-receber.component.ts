import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var html2canvas: any;

@Component({
  selector: 'app-lista-contas-receber',
  templateUrl: './lista-contas-receber.component.html',
  styleUrls: ['./lista-contas-receber.component.css']
})
export class ListaContasReceberComponent implements OnInit {

  queryContas: string = '';
  servicoFilter : string = 'Selecione';

  resultsFilterContas: any = [];
  contasReceber: any = [];
  servicos: any = [];

  pageParams: any;

  emptyResult:boolean = false;
  filterService:boolean = false;

  totalContasReceber:number = 0;
  p: number = 0; 

  selectAll:boolean = false;

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });
    
    if (this.pageParams.params.page == 'recebido'){
      this._http.post(
        'selectContasReceber.php',
        {
          kind: 'recebido',
          normal: true
        }
      ).subscribe( (response) => {
        this.contasReceber = [];
        if(response.status == '0x104'){
          this.totalContasReceber = 0;
          response.result.forEach(element => {
            let o = {
              FORR_ID: element.FORR_ID,
              FORR_Nome: element.FORR_Nome,
              FORR_Servico_ID: element.FORR_Servico_ID,
              RFIL_ID : element.RFIL_ID,
              RFIL_Nome: element.RFIL_Nome,
              TIPS_Nome: element.TIPS_Nome,
              FORR_Valor: element.FORR_Valor,
              FORR_Status: element.FORR_Status,
              FORR_Obs: element.FORR_Obs,
              FORR_Loja: element.FORR_Loja,
              FORR_notaFiscal: element.FORR_notaFiscal,
              FORR_Data: element.FORR_Data,
              menuRow: false
            }
            this.contasReceber.push(o);
            // if (o.FORR_Valor.split(',')[0].length == 3){
            //   this.totalContasReceber += parseFloat(o.FORR_Valor.replace(',', '.'));
            // }else if (o.FORR_Valor.split(',')[0].length == 1){
            //   this.totalContasReceber += parseFloat(o.FORR_Valor.replace(',', '').replace('.00', ',00'));
            // }else{
            //   this.totalContasReceber += parseFloat(o.FORR_Valor);
            // }
            this.totalContasReceber += parseFloat(o.FORR_Valor.replace(',', ''));
          });
          this.resultsFilterContas = this.contasReceber;
        }else if(response.status == '0x101'){
          this.emptyResult = true;
        }
      });
    }else if (this.pageParams.params.page == 'areceber'){
      this._http.post(
        'selectContasReceber.php',
        {
          kind: 'areceber',
          normal: true
        }
      ).subscribe( (response) => {
        this.contasReceber = [];
        if(response.status == '0x104'){
          this.totalContasReceber = 0;
          response.result.forEach(element => {
            let o = {
              FORR_ID: element.FORR_ID,
              FORR_Nome: element.FORR_Nome,
              FORR_Servico_ID: element.FORR_Servico_ID,
              RFIL_ID : element.RFIL_ID,
              RFIL_Nome: element.RFIL_Nome,
              TIPS_Nome: element.TIPS_Nome,
              FORR_Valor: element.FORR_Valor,
              FORR_Status: element.FORR_Status,
              FORR_Obs: element.FORR_Obs,
              FORR_Loja: element.FORR_Loja,
              FORR_notaFiscal: element.FORR_notaFiscal,
              FORR_Data: element.FORR_Data,
              menuRow: false
            }
            console.log(o.FORR_Valor);
            this.contasReceber.push(o);
            // if (o.FORR_Valor.split(',')[0].length == 3){
            //   this.totalContasReceber += parseFloat(o.FORR_Valor.replace(',', '.'));
            // }else if (o.FORR_Valor.split(',')[0].length == 1){
            //   this.totalContasReceber += parseFloat(o.FORR_Valor.replace(',', '').replace('.00', ',00'));
            // }else{
            //   this.totalContasReceber += parseFloat(o.FORR_Valor);
            // }
            this.totalContasReceber += parseFloat(o.FORR_Valor.replace(',', ''));
          });
          this.resultsFilterContas = this.contasReceber;
        }else if(response.status == '0x101'){
          this.emptyResult = true;
        }
      });
    }
  }

  enableFilterService(){
    if (!this.filterService){
      this.filterService = true;
      this._http.post(
        'selectServicos.php',
        {}
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          this.servicos = [];
          response.result.forEach(element => {
            this.servicos.push(element);
          });
        }else if (response.status == '0x101'){
          window.alert('Erro ao selecionar filtro de servicos');
        }
      });
    }else{
      this.filterService = false;
      this.ngOnInit();
    }
  }

  setServicoFilter(s){
    this.totalContasReceber = 0;
    this.servicoFilter = s.TIPS_Nome;
    this.filterArrayServicos(this.servicoFilter, this.contasReceber);
  }

  filterArrayServicos(model, arr){
    this.totalContasReceber = 0;
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterContas = arr.filter( (e) => {
      return regexp.test(e.TIPS_Nome);
    });
    this.resultsFilterContas.forEach(element => {
      this.totalContasReceber += parseFloat(element.FORR_Valor.replace(',', ''));
    });
  }

  changeState(r, index){
    switch(r){
      case 'recebido':{
        let c = confirm("KSTrade - Contas a receber\r\n\r\nDeseja alterar para 'Recebido'?");
        if (c == true){
          this._http.post(
            'editContaReceber.php',
            {
              status: 'Recebido',
              id: this.contasReceber[index].FORR_ID
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              window.alert('KSTrade - Contas a receber\r\n\r\nAlterado com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout( () => {
                this._router.navigate(['/main/listaContasReceber', {page: 'areceber'}]);
              }, 1000);
            }else if (response.status == '0x101'){
              window.alert('Erro na edição de conta');
            }
          });
        }
        break;
      }
      case 'areceber':{
        let c = confirm("KSTrade - Contas a receber\r\n\r\nDeseja alterar para 'A receber'?");
        if (c == true){
          this._http.post(
            'editContaReceber.php',
            {
              status: 'A receber',
              id: this.contasReceber[index].FORR_ID
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              window.alert('KSTrade - Contas a receber\r\n\r\nAlterado com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout( () => {
                this._router.navigate(['/main/listaContasReceber', {page: 'recebido'}]);
              }, 1000);
            }else if (response.status == '0x101'){
              window.alert('Erro na edição de conta');
            }
          });
        }
        break;
      }
    }
  }

  filterArrayContas(model, arr){
    this.totalContasReceber = 0;
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterContas = arr.filter( (e) => {
      return regexp.test(e.FORR_Nome);
    });
    this.resultsFilterContas.forEach(element => {
      // this.totalContasReceber += parseFloat(element.FORR_Valor);
      this.totalContasReceber += parseFloat(element.FORR_Valor.replace(',', ''));
    });
  }

  searchContas(string){
    this.filterArrayContas(string, this.contasReceber);
  }

  removeConta(c): void{
    let conf = confirm('KSTrade - Contas a receber\r\n\r\nDeseja realmente remover?');
    if (conf == true){
      this._http.post(
        'editContaReceber.php',
        {
          unique: true,
          id: c.FORR_ID
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade - Contas a receber\r\n\r\nConta removida com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaContasReceber', {page: 'areceber'}]);
          }, 1500);
        }else if (response.status == '0x101'){
          window.alert('Erro na remoção de conta!');
        }
      });
    }
  }

  makePDF() {
    document.getElementById('contasReceberTable').style.display = 'block';
    html2canvas(document.getElementById('contasReceberTable')).then( (canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("",'_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="'+(<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1)+'" /></body></html>');
      document.getElementById('contasReceberTable').style.display = 'none';
    });
  }

  pagarTodosConta(){
    let c = confirm('KSTrade - Lista de contas a pagar\r\n\r\nDeseja realmente marcar todos como recebido?');
    if (c == true){
      for (let i = 0; i < this.resultsFilterContas.length; i++){
        if ( (<HTMLInputElement>document.getElementById('contaReceberCheckBox_'+i)).checked == true){
          this._http.post(
            'editContaReceber.php',
            {
              status: 'Recebido',
              id: this.contasReceber[i].FORR_ID
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              console.log(0);
            }
          });
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaContasReceber', {page: 'areceber'}]);
          }, 1500);
        }
      }
    }
  }

  abrirTodosConta(){
    let c = confirm('KSTrade - Lista de contas a pagar\r\n\r\nDeseja realmente marcar todos como aberto?');
    if (c == true){
      for (let i = 0; i < this.resultsFilterContas.length; i++){
        if ( (<HTMLInputElement>document.getElementById('contaReceberCheckBox_'+i)).checked == true){
          this._http.post(
            'editContaReceber.php',
            {
              status: 'A receber',
              id: this.contasReceber[i].FORR_ID
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              console.log(0);
            }
          });
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaContasReceber', {page: 'areceber'}]);
          }, 1500);
        }
      }
    }
  }

  isSelectedAllContas: boolean = false;

  selectAllContas(){
    if (this.isSelectedAllContas == true){
      for (let i = 0; i < this.resultsFilterContas.length; i++){
        (<HTMLInputElement>document.getElementById('contaReceberCheckBox_'+i)).checked = false;
      }
    }else{
      this.isSelectedAllContas = true;
      for (let i = 0; i < this.resultsFilterContas.length; i++){
        (<HTMLInputElement>document.getElementById('contaReceberCheckBox_'+i)).checked = true;
      }
    }
  }

}
