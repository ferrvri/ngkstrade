import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-lista-servico',
  templateUrl: './lista-servico.component.html',
  styleUrls: ['./lista-servico.component.css']
})
export class ListaServicoComponent implements OnInit {

  emptyResult:boolean = false;

  queryServicos: string = '';

  servicos:any = [];
  resultsFilterServicos: any = [];

  constructor(private _http: HttpService, private _router:Router) { }

  ngOnInit() {
    this._http.post(
      'selectServicos.php',
      {
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          let o = {
            TIPS_ID: element.TIPS_ID,
            TIPS_Nome: element.TIPS_Nome,
            menuRow : false
          };
          this.servicos.push(o);
        });
        this.filterArrayServicos(this.queryServicos, this.servicos);
      }else if (response.status == '0x101'){
        this.emptyResult = true;
      }
    });
  }

  deleteServico(s):void{
    let c = confirm('KSTrade - Serviços\r\n\r\nDeseja realmente excluir?');
    if(c == true){
      this._http.post(
        'deleteServico.php',
        {
          id: s.TIPS_ID
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade - Serviços\r\n\r\Serviço excluido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaServico']);
          }, 1000);
        }else if (response.status == '0x101'){
          window.alert('Erro na exclusão do serviço');
        }
      });
    }
  }

  searchServicos(string){
    this.filterArrayServicos(string, this.servicos);
  }

  filterArrayServicos(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterServicos = arr.filter( (e) => {
      return regexp.test(e.TIPS_Nome);
    });
  }

}
