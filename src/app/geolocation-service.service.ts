import { Injectable } from '@angular/core';

declare var ActiveXObject: any;

@Injectable()
export class GeolocationServiceService {

  result = {
    lat: 0,
    lng: 0
  };

  constructor() {
    navigator.geolocation.getCurrentPosition(resp => {
      this.result = { lng: resp.coords.longitude, lat: resp.coords.latitude };
    });
  }

  getPosition(cb?) {
    cb(this.result);
  }

  GetComputerName(cb?) {
    let c = '';
    try {
      var network = new ActiveXObject('WScript.Network');
      c = network.computerName;
    }
    catch (e) { }

    return c;
  }

}
