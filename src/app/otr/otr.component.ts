import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-otr',
  templateUrl: './otr.component.html',
  styleUrls: ['./otr.component.css']
})
export class OtrComponent implements OnInit {

  redeId: number = 0;
  filialId: number = 0;
  promotorId: number = 0;
  coordenadorId: number = 0;

  filialOk: boolean;
  coordOk: boolean;

  pageParams:any;
  
  promotores: any = [];
  redes: any = [];
  filiais: any = [];

  promotor: string = 'Selecione';
  coordenador: string = '';
  loja: string = '';
  descricao: string = '';
  data: string = '';
  anexo: string = '';
  rede: string = 'Selecione';
  filial: string = 'Selecione';
  
  toUploadImage:string = '';
  nameOfFile:string = '';

  errorDescricao = {required: null};
  errorPromotor = {required: null};
  // errorLoja = {required: null};
  errorCoordenador = {required: null};
  errorData = {required: null};
  errorAnexo = {required: null};
  errorRede = {required: null};
  errorFilial = {required: null};

  cadastroOTR: FormGroup = new FormGroup( {
    // loja: new FormControl(this.loja, [Validators.required, Validators.minLength(6)]),
    promotor: new FormControl(this.loja, [Validators.required, Validators.minLength(6)]),
    coordenador: new FormControl(this.loja, [Validators.required, Validators.minLength(6)]),
    descricao: new FormControl(this.loja, [Validators.required, Validators.minLength(6)]),
    data: new FormControl(this.loja, [Validators.required, Validators.minLength(6)]),
    rede: new FormControl(this.rede, [Validators.required, Validators.minLength(6)]),
    filial: new FormControl(this.filial, [Validators.required, Validators.minLength(6)])
  } );

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    $('#inputDataOtr').datepicker();

    $('.dropify').dropify();

    // Used events
    var drEvent = $('#input-file-events').dropify();

    drEvent.on('dropify.beforeClear', function(event, element) {
        return confirm("Você deseja realmente apagar \"" + element.file.name + "\" ?");
    });

    drEvent.on('dropify.afterClear', function(event, element) {
        alert('Arquivo apagado');
    });

    drEvent.on('dropify.errors', function(event, element) {
        console.log('Erro!');
    });

    var drDestroy = $('#input-file-to-destroy').dropify();
    drDestroy = drDestroy.data('dropify');
    $('#toggleDropify').on('click', function(e) {
        e.preventDefault();
        if (drDestroy.isDropified()) {
            drDestroy.destroy();
        } else {
            drDestroy.init();
        }
    })

    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    this._http.post(
      'selectPromotor.php',
      {
        clienteId: this.pageParams.params.cliente
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          this.promotores.push(element);
        });
        this._http.post(
          'selectRedes.php',
          {}
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            response.result.forEach(element => {
              this.redes.push(element);
            });
          }else if (response.status == '0x101'){
            window.alert('Erro na seleção de redes');
          }
        });
      }else if (response.status == '0x101'){
        window.alert('Erro na seleção de promotor');
      }
    });
  }

  setPromotor(p, index):void{
    this.promotor = p.PRO_Nome;
    this.promotorId = p.PRO_ID;
    this._http.post(
      'selectCoordenadores.php',
      {
        forUser: this.promotores[index].PRO_ID
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.coordenador = response.result[0].Usr_Nome;
        this.coordenadorId = response.result[0].Usr_ID;
        setTimeout( () => {
          this.coordOk = true;
        }, 500);
      }else if (response.status == '0x101'){
        window.alert('Erro na selecao de coordenador para o promotor');
      }
    });
  }

  setRede(r, index){
    this.rede = r.RED_Nome;
    this.redeId = r.RED_ID
    this._http.post(
      'selectFilial.php',
      {
        id: this.redeId
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.filiais = [];
        response.result.forEach(element => {
          this.filiais.push(element)
        });
        setTimeout( () => {
          this.filialOk = true;
        }, 500);
      }else if (response.status == '0x101'){
        window.alert('Erro ao selecionar Filiais desta rede');
      }
    });
  }

  setFilial(f, index){
    this.filial = f.RFIL_Nome;
    this.filialId = f.RFIL_ID;
  }

  validation():any{
    try{
      if (this.coordenador.length < 1){
        this.errorCoordenador.required = true;
      }else{
        this.errorCoordenador.required = null;
      }

      if (this.descricao.length < 1){
        this.errorDescricao.required = true;
      }else{
        this.errorDescricao.required = null;
      }

      if (this.promotor.length < 1){
        this.errorPromotor.required = true;
      }else{
        this.errorPromotor.required = null;
      }

      if (this.data.length < 1){
        this.errorData.required = true;
      }else{
        this.errorData.required = null;
      }

      if (this.toUploadImage.length < 1){
        this.errorAnexo.required = true;
      }else{
        this.errorAnexo.required = null;
      }

      if (this.rede.length < 1 || this.rede == 'Selecione'){
        this.errorRede.required = true;
      }else{
        this.errorRede.required = null;
      }

      if (this.filial.length < 1 || this.filial == 'Selecione'){
        this.errorFilial.required = true;
      }else{
        this.errorFilial.required = null;
      }
      return true;
    }catch(e){
      console.log(e.toString());
      return false;
    }
  }

  uploadChanges(event){
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      this.nameOfFile = event.target.files[0].name;
      reader.readAsDataURL(file);
      reader.onload = () => {
          this.toUploadImage =  reader.result.toString().split(',')[1];
      };
    }
  }

  cadOTR():void{
    if (this.validation() && this.errorCoordenador.required == null && this.errorDescricao.required == null
    && this.errorPromotor.required == null && this.errorData.required == null &&
    this.errorRede.required == null && this.errorFilial.required == null){
      this._http.post(
        'insertOTR.php',
        {
          descricao: this.descricao,
          loja: this.loja,
          coordenador: this.coordenadorId,
          data: this.data,
          promotor: this.promotorId,
          filial: this.filialId,
          title: this.nameOfFile,
          anexo: this.toUploadImage
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade - Cadastro de OTR\r\n\r\nOcorrência em tempo real cadastrada com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaClientes', {page: 'search', cliente: ''}]);
          }, 1000);
        }else if (response.status == '0x101'){
          window.alert('Erro de inserção ao banco');
        }else if (response.status == '0x102'){
          window.alert('KSTrade - Cadastro de OTR\r\n\r\nO arquivo já existe no sistema!');
        }else if(response.status == '0x103'){
          window.alert('KSTrade - Cadastro de OTR\r\n\r\nOcorreu um erro no salvamento do arquivo.');
        }
      });
    }
  }

  setData(){
    if ((<HTMLInputElement>document.getElementById('inputDataOtr')).value === undefined || (<HTMLInputElement>document.getElementById('inputDataOtr')).value == ''){
      (<HTMLInputElement>document.getElementById('inputDataOtr')).value = '';
    }else{
      this.data = (<HTMLInputElement>document.getElementById('inputDataOtr')).value.split('/')[1] +'/' + (<HTMLInputElement>document.getElementById('inputDataOtr')).value.split('/')[0] +'/' + (<HTMLInputElement>document.getElementById('inputDataOtr')).value.split('/')[2];
    }   
  }

}
