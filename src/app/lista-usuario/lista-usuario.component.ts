import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';
import { environment } from '../../environments/environment';

declare var html2canvas: any;
declare var showDialog: any;

@Component({
  selector: 'app-lista-usuario',
  templateUrl: './lista-usuario.component.html',
  styleUrls: ['./lista-usuario.component.css']
})
export class ListaUsuarioComponent implements OnInit {

  userData = [];
  usuarios = [];
  emptyResult: boolean = false;
  p: number = 0;

  queryUsuarios = '';
  resultsFilterUsuarios = [];

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat + ';' + c.lng)
    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    this._http.post(
      'selectUsuarios.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        console.log(response);
        response.result.forEach((e) => {
          let o = {
            Usr_Nome: e.Usr_Nome,
            Usr_Login: e.Usr_Login,
            Usr_Senha: e.Usr_Senha,
            Usr_Nivel: e.Usr_Nivel,
            Usr_ID: e.Usr_ID,
            Usr_Status: e.Usr_Status,
            Usr_Email: e.Usr_Email,
            Usr_Avatar: environment.apiUrl+'uploads/users/'+e.Usr_Avatar || '',
            Usr_Net_Status: e.Usr_Net_Status,
            menuRow: false
          }
          this.usuarios.push(o);
        });
        this.resultsFilterUsuarios = this.usuarios;
      } else if (response.status == '0x101') {
        this.emptyResult = true;
      }
    });
  }

  showProfilePic(usuario) {
    showDialog({
      subtitle: usuario.Usr_Nome,
      images: [usuario.Usr_Avatar]
    });
  }

  removeUsuario(usuario) {
    let c = confirm('KSTrade - Remover Usuario\r\n\r\nDeseja realmente remover esse usuario?');
    if (c == true) {
      this._http.post(
        'deleteUsuario.php',
        {
          id: usuario.Usr_ID,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Usuario[' + usuario.Usr_ID + ' / ' + usuario.Usr_Nome + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising\r\n\r\nUsuario removido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaUsuario']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert('Erro na remoção do usuario');
        }
      });
    }
  }

  activateUser(userId): void {
    let c = confirm('KSTrade - Ativar Usuario\r\n\r\nDeseja realmente ativar o usuario?');
    if (c == true) {
      this._http.post(
        'activeUser.php',
        {
          id: userId,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Ativação - Usuario[' + userId + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('Usuario ativado!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaUsuario']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert('Erro na ativação do usuario');
        }
      });
    }
  }

  inactivateUser(userId): void {
    let c = confirm('KSTrade - Inativar Usuario\r\n\r\nDeseja realmente inativar o usuario?');
    if (c == true) {
      this._http.post(
        'inactiveUser.php',
        {
          id: userId,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Inativação - Usuario[' + userId + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('Usuario inativado!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaUsuario']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert('Erro na inativação do usuario');
        }
      });
    }
  }

  enviarAcesso(u) {
    let c = confirm('KSTrade - Enviar acesso\r\n\r\nDeseja realmente enviar as credenciais de acesso para este usuario?')
    if (c == true) {
      if (u.Usr_Email !== null && u.Usr_Email != 'null') {
        this._http.post(
          'enviarAcesso.php',
          {
            nome: u.Usr_Nome,
            login: u.Usr_Login,
            email: u.Usr_Email
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade - Envio de acesso\r\n\r\nE-mail enviado com sucesso!');
          } else {
            window.alert('Ocorreu um erro no envio de e-mail por favor, verifique com o desenvolvedor');
          }
        });
      } else {
        window.alert('KSTrade - Envio de e-mail\r\n\r\nVocê precisa cadastrar o e-mail deste usuário!');
      }
    }
  }

  makePDF() {
    document.getElementById('usuarioTable').style.display = 'block';
    html2canvas(document.getElementById('usuarioTable')).then((canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("", '_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
      document.getElementById('usuarioTable').style.display = 'none';
    });
  }

  searchUsuarios(string) {
    if (string.length < 1) {
      this.resultsFilterUsuarios = this.usuarios;
    } else {
      this.filterArrayUsuarios(string, this.usuarios);
    }
  }

  filterArrayUsuarios(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterUsuarios = arr.filter((e) => {
      return regexp.test(e.Usr_Nome);
    });
  }
}
