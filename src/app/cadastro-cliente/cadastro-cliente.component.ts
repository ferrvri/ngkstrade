import { Component, OnInit } from '@angular/core';
import { FormGroup, AbstractControl, ValidatorFn, FormControl, Validators } from '@angular/forms';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-cadastro-cliente',
  templateUrl: './cadastro-cliente.component.html',
  styleUrls: ['./cadastro-cliente.component.css']
})
export class CadastroClienteComponent implements OnInit {
  coordIndex: number = 0;

  coordenadorOK = false;
  toUploadImage = [];
  coordenadores = [];
  estados = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

  errorNotExistsCep: boolean = false;
  errorNomeFantasia = { required: null };
  errorRazaoSocial = { required: null };
  errorCnpj = { required: null };
  errorTelefoneFixo = { required: null };
  errorTelefoneMovel = { required: null };
  errorEmail = { required: null };
  errorCoordenador = { required: null };
  errorCep = { required: null };
  errorEndereco = { required: null };
  errorCidade = { required: null };
  errorBairro = { required: null };
  errorEstado = { required: null };
  errorNumero = { required: null };
  errorAnexo = {required: null};

  nomeFantasia: string = "";
  razaoSocial: string = "";
  cnpj: string = "";
  telefoneFixo: string = "";
  telefoneMovel: string = "";
  email: string = "";
  coordenador: string = "Selecione";
  cep: string = "";
  endereco: string = "";
  numero: string = "";
  cidade: string = "";
  bairro: string = "";
  estado: string = "Selecione";
  anexo = '';

  cadastroCliente: FormGroup = new FormGroup({
    nomeFantasia: new FormControl(this.nomeFantasia, [Validators.required, Validators.minLength(6)]),
    razaoSocial: new FormControl(this.razaoSocial, [Validators.required, Validators.minLength(6)]),
    cnpj: new FormControl(this.cnpj, [Validators.required, Validators.minLength(6)]),
    telefoneFixo: new FormControl(this.telefoneFixo, [Validators.required, Validators.minLength(6)]),
    telefoneMovel: new FormControl(this.telefoneMovel, [Validators.required, Validators.minLength(6)]),
    email: new FormControl(this.email, [Validators.required, Validators.minLength(6)]),
    coordenador: new FormControl(this.coordenador, [Validators.required, Validators.minLength(6)]),
    cep: new FormControl(this.cep, [Validators.required, Validators.minLength(6)]),
    endereco: new FormControl(this.endereco, [Validators.required, Validators.minLength(6)]),
    numero: new FormControl(this.numero, [Validators.required, Validators.minLength(6)]),
    cidade: new FormControl(this.cidade, [Validators.required, Validators.minLength(6)]),
    bairro: new FormControl(this.bairro, [Validators.required, Validators.minLength(6)]),
    estado: new FormControl(this.estado, [Validators.required, Validators.minLength(6)])
  });

  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat +';'+c.lng)
    jQuery('.mydatepicker, #datepicker').datepicker();

    $('.dropify').dropify();

    // Used events
    var drEvent = $('#input-file-events').dropify();

    drEvent.on('dropify.beforeClear', function (event, element) {
      return confirm("Você deseja realmente apagar \"" + element.file.name + "\" ?");
    });

    drEvent.on('dropify.afterClear', function (event, element) {
      alert('Arquivo apagado');
    });

    drEvent.on('dropify.errors', function (event, element) {
      console.log('Erro!');
    });

    var drDestroy = $('#input-file-to-destroy').dropify();
    drDestroy = drDestroy.data('dropify');
    $('#toggleDropify').on('click', function (e) {
      e.preventDefault();
      if (drDestroy.isDropified()) {
        drDestroy.destroy();
      } else {
        drDestroy.init();
      }
    })

    $('#inputCnpj').mask('00.000.000/0000-00');
    $('#inputTelefoneFixo').mask('(00) 0000-0000');
    $('#inputTelefoneMovel').mask('(00) 0 0000-0000');
    $('#inputCEP').mask('00000-000');

    this._http.post(
      'selectCoordenadores.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.coordenadores.push(element);
        });
        this.resultFilterCoordenador = this.coordenadores;
      } else {
        window.alert('Não foi possível buscar os coordenadores disponiveis');
      }
    });
  }

  validation() {
    try {

      if (this.razaoSocial.length < 1) {
        this.errorRazaoSocial = { required: true };
      } else {
        this.errorRazaoSocial = { required: null };
      }
      if (this.nomeFantasia.length < 1) {
        this.errorNomeFantasia = { required: true }
      } else {
        this.errorNomeFantasia = { required: null };
      }
      if (this.cnpj.length < 1) {
        this.errorCnpj = { required: true };
      } else {
        this.errorCnpj = { required: null };
      }
      if (this.telefoneFixo.length < 1) {
        this.errorTelefoneFixo = { required: true }
      } else {
        this.errorTelefoneFixo = { required: null };
      }
      if (this.telefoneMovel.length < 1) {
        this.errorTelefoneMovel = { required: true };
      } else {
        this.errorTelefoneMovel = { required: null };
      }
      if (this.email.length < 1) {
        this.errorEmail = { required: true }
      } else {
        this.errorEmail = { required: null };
      }
      if (this.coordenador.length < 1) {
        this.errorCoordenador = { required: true };
      } else {
        this.errorCoordenador = { required: null };
      }
      if (this.cep.length < 1) {
        this.errorCep = { required: true };
      } else {
        this.errorCep = { required: null };
      }
      if (this.endereco.length < 1) {
        this.errorEndereco = { required: true };
      } else {
        this.errorEndereco = { required: null };
      }
      if (this.numero.length < 1) {
        this.errorNumero = { required: true }
      } else {
        this.errorNumero = { required: null };
      }
      if (this.cidade.length < 1) {
        this.errorCidade = { required: true };
      } else {
        this.errorCidade = { required: null };
      }
      if (this.bairro.length < 1) {
        this.errorBairro = { required: true }
      } else {
        this.errorBairro = { required: null };
      }
      if (this.estado.length < 1) {
        this.errorEstado = { required: true };
      } else {
        this.errorEstado = { required: null };
      }
      if (this.anexo.length < 1){
        this.errorAnexo.required = true;
      }else{
        this.errorAnexo.required = false;
      }

      return true;
    } catch (e) {
      console.log(e.toString());
    }
  }

  removeFiles() {
    $(".dropify-clear").click();
    this.toUploadImage = [];
  }

  uploadChanges(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      //this.nameOfFile = event.target.files[0].name;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.toUploadImage.push(reader.result.toString().split(',')[1]);
      };
    }
  }

  nameOfFile: string = '';


  cadCliente(): void {
    if (this.validation() && (this.errorBairro.required == null && this.errorCep.required == null &&
      this.errorCidade.required == null && this.errorCnpj.required == null && this.errorCoordenador.required == null &&
      this.errorEmail.required == null && this.errorEndereco.required == null && this.errorEstado.required == null &&
      this.errorNomeFantasia.required == null && this.errorNumero.required == null && this.errorRazaoSocial.required == null &&
      this.errorTelefoneFixo.required == null)) {

      this.nameOfFile = Math.ceil(Math.random() * 1000) + this.nomeFantasia + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg'
      let nameOfFile = this.nameOfFile.trim();

      this._http.post(
        'insertCliente.php',
        {
          nomeFantasia: this.nomeFantasia,
          razaoSocial: this.razaoSocial,
          telefoneFixo: this.telefoneFixo,
          telefoneMovel: this.telefoneMovel,
          email: this.email,
          cep: this.cep,
          endereco: this.endereco,
          numero: this.numero,
          cidade: this.cidade,
          estado: this.estado,
          cnpj: this.cnpj,
          bairro: this.bairro,
          coordenador: this.coordIndex,
          title: nameOfFile,
          anexo: this.toUploadImage[0],
          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Inserção - Cliente[' + this.nomeFantasia + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Cadastro de Cliente\r\n\r\nCliente cadastrado com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/cadastroCliente']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert('Erro de inserção ao banco');
        } else if (response.status == '0x102') {
          window.alert('KSTrade - Cadastro de cliente\r\n\r\nCNPJ já cadastrado!');
        }
      })
    }
  }

  checkCep(cep: string): void {
    if (cep.length < 1) {
      this.errorCep = { required: true };
    } else {
      this._http.post(
        'https://viacep.com.br/ws/' + cep.replace('-', '').replace('.', '') + '/json',
        {}
      ).subscribe((response) => {
        if (response.localidade) {
          this.estado = response.uf;
          this.bairro = response.bairro;
          this.cidade = response.localidade;
          this.endereco = response.logradouro;
          this.errorNotExistsCep = false;
          document.getElementById('inputNumeroCadCliente').focus();
        } else {
          this.errorNotExistsCep = true;
          this.estado = ""
          this.bairro = ""
          this.cidade = ""
          this.endereco = ""
        }
      });
    }
  }

  setEstado(e): void {
    this.estado = e;
  }

  setCoordenador(c): void {
    this.coordenador = c.Usr_Nome;
    this.coordIndex = c.Usr_ID;
    this.coordenadorOK = false;
  }

  resultFilterCoordenador: any = [];

  filterCoordenador(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterCoordenador = this.coordenadores;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterCoordenador = arr.filter((e) => {
        return regexp.test(e.Usr_Nome.split('')[0]);
      });
    }
  }

}
