import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.css']
})
export class LogsComponent implements OnInit {

  p: number;

  logs = []
  userData: any = [];

  emptyResult = false;
  alreadySearch = false;

  constructor(private _http: HttpService,) { }

  ngOnInit() {
    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    this._http.post(
      'selectLogs.php',
      {
        datas: [
          new Date(new Date().getFullYear(), new Date().getMonth(), 0).toISOString().split('T')[0],
          new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().split('T')[0]
        ]
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.logs.push(element);
        });
      }
    });
  }

  searchLog(datainit, dataend) {
    this._http.post(
      'selectLogs.php',
      {
        datas: [
          datainit,
          dataend
        ]
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.logs = response.result;
      } else {
        this.logs = [];
        this.emptyResult = true;
      }
    });
  }


  clearData(datainit, dataend) {
    datainit = '';
    dataend = '';
  }

  searchLogs(logs){
    if (logs.length < 1){
      this.ngOnInit();
    }else{
      this.logs = this.logs.filter(l => {
        return l.log_funcao.indexOf(logs) != -1
      });
    }
  }
  
}
