import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InatividadeCoordComponent } from './inatividade-coord.component';

describe('InatividadeCoordComponent', () => {
  let component: InatividadeCoordComponent;
  let fixture: ComponentFixture<InatividadeCoordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InatividadeCoordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InatividadeCoordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
