import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

declare var $: any;

@Component({
  selector: 'app-inatividade-coord',
  templateUrl: './inatividade-coord.component.html',
  styleUrls: ['./inatividade-coord.component.css']
})
export class InatividadeCoordComponent implements OnInit {

  coordenadores = [];
  resultsCoord = [];
  emptyResult: boolean = false;
  queryCoordenador: string = '';

  constructor( private _http: HttpService, ) { }

  ngOnInit() {
    $('#dataContasPagarInit').datepicker();
    $('#dataContasPagarEnd').datepicker();
    this._http.post(
      'selectCoordenadores.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          let o = {
            coordid: element.Usr_ID,
            coordNome: element.Usr_Nome,
            Usr_Net_Status: element.Usr_Net_Status,
            content: [],
            pageNumber: 0
          }

          this._http.post(
            'selectInatCoordenadores.php',
            {
              coord: element.Usr_ID
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              response.result.forEach(element2 => {
                let data1 = new Date(element2.ina_hora_saida).getTime();
                let data2 = new Date(element2.ina_hora_volta).getTime();
                let secs = (data2 - data1) / 1000;
                element2.ina_tempo_minuto = Math.floor(secs / 60);
                element2.ina_tempo_segundos = secs - element2.ina_tempo_minuto * 60;
                element2.ina_hora_saida = element2.ina_hora_saida.substring(11, 16);
                element2.ina_hora_volta = element2.ina_hora_volta.substring(11, 16);
                o.content.push(element2);
              });
            }
          });

          this.coordenadores.push(o);
        });
        console.log(this.coordenadores);
        this.resultsCoord = this.coordenadores;
      }
    });
  }


  setDateOne(){
    (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value = (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] +'/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0] +'/' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2]
  }

  setDateTwo(){
    (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value = (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] +'/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0] +'/' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2]
  }

  clearData(){
    (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value = '';
    (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value = '';
    this.alreadySearch = false;
    this.resultsCoord = this.coordenadores;
  }

  alreadySearch: boolean = false;

  search(){
    this.alreadySearch = true;
    this.resultsCoord = [];
    this._http.post(
      'selectCoordenadores.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          let o = {
            coordid: element.Usr_ID,
            coordNome: element.Usr_Nome,
            content: [],
            pageNumber: 0
          }

          this._http.post(
            'selectInatCoordenadores.php',
            {
              coord: element.Usr_ID,
              filter: 'data',
              dataone: (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[2] +'-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[1] +'-' + (<HTMLInputElement>document.getElementById('dataContasPagarInit')).value.split('/')[0],
              datatwo: (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[2] +'-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[1] +'-' + (<HTMLInputElement>document.getElementById('dataContasPagarEnd')).value.split('/')[0]
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              response.result.forEach(element2 => {
                o.content.push(element2);
              });
            }
          });

          this.resultsCoord.push(o);
        });
      }
    });
  }

  filterArrayCoordenadores(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsCoord = arr.filter( (e) => {
      return regexp.test(e.coordNome);
    });
  }

  searchCoordenador(string){
    if (string.length < 1){
      this.resultsCoord = this.coordenadores;
    }else{
      this.filterArrayCoordenadores(string, this.coordenadores);
    }
  }
}
