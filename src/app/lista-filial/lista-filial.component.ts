import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { PromotorProdutividadeComponent } from '../promotor-produtividade/promotor-produtividade.component';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var html2canvas: any;
declare var jsPDF: any;
declare var $: any;

@Component({
  selector: 'app-lista-filial',
  templateUrl: './lista-filial.component.html',
  styleUrls: ['./lista-filial.component.css']
})
export class ListaFilialComponent implements OnInit {

  filiais = [];
  emptyResult = false;

  resultsFilterFiliais: any = [];
  queryFiliais: string = '';
  queryCidadeFiliais: string = '';

  enableTotal:boolean = false;
  totalAtendimento:number = 0;

  pageParams: any;
  userData = []

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    this.userData.push(JSON.parse(localStorage.getItem('session'))[0])
    
    if (this.pageParams.params.rede){
      let redes = {
        nome: '',
        id: 0,
        content: [],
        visible: true,
        atendimentoTotal: 0
      };

      let result = [];
      
      this._http.post(
        'selectRedes.php',
        {
          unique: true,
          id: this.pageParams.params.rede
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          redes.nome = response.result[0].RED_Nome;
          redes.id = response.result[0].RED_ID;
          this._http.post(
            'selectFilial.php',
            {
              id: redes.id
            }
          ).subscribe( (response2) => {
            if (response2.status == '0x104'){
                response2.result.forEach(element => {
                  element.menuRow = false;
                  redes.content.push(element);
                });
              // redes.cidade = response2.result.RFIL_Cidade;
            }else{
              return;
            }
            this.filiais.push(redes);
            this.filterArrayFiliais(this.queryFiliais, this.filiais);
          }); 
        }
      });
    }else{
      let result = [];
      
      this._http.post(
        'selectRedeInFilial.php',
        {}
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          this.filiais = response.result;
          this.emptyResult = false;
          this.filterArrayFiliais(this.queryFiliais, this.filiais);
        }else{
          this.emptyResult = true
        }
      });

      // this._http.post(
      //   'selectRedes.php',
      //   {}
      // ).subscribe( (response1) => {
      //   if (response1.status == '0x104'){
      //     if (response1.result !== undefined || response1.result.length > 0){
      //       response1.result.forEach( (e1) => {
      //         let redes = {
      //           nome: e1.RED_Nome,
      //           id: e1.RED_ID,
      //           content: [],
      //           visible: true,
      //           atendimentoTotal: 0
      //         };
    
      //         this._http.post(
      //           'selectFilial.php',
      //           {
      //             id: redes.id
      //           }
      //         ).subscribe( (response2) => {
      //           if (response2.status == '0x104'){
      //              response2.result.forEach(element => {
      //                element.menuRow = false;
      //                redes.content.push(element);
      //              });
      //             // redes.cidade = response2.result.RFIL_Cidade;
      //           }else{
      //             return;
      //           }
      //           this.filiais.push(redes);
      //           this.filterArrayFiliais(this.queryFiliais, this.filiais);
      //         }); 
      //       });
      //     }else if (response1.status == '0x101'){
      //       this.emptyResult = true;
      //     }
      //   }
      // });
    }
  }

  openFilial(index, f, event):void{
    if (f.content.length > 0){
      if (f.visible == true){
        f.visible = false;
      }else{
        f.visible = true;
      }
    }
  }

  deleteFilial(f):void{
    let confirmation = confirm('KSTrade - Lista de Filiais\r\n\r\nDeseja realmente excluir?');
    if (confirmation == true){
      this._http.post(
        'deleteFilial.php',
        {
          id: f.RFIL_ID,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Filial[' + f.RFIL_ID + ']'
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('Removido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaFilial']);
          }, 1000);
        }else if (response.status == '0x101'){
          window.alert('Erro inesperado!');
        }
      });
    }
  }

  filterArrayFiliais(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterFiliais = arr.filter( (e) => {
      return regexp.test(e.nome);
    });
  }

  searchFiliais(string){
    if (string.length < 1){
      this.enableTotal = false;
    }else{
      this.enableTotal = true;
      this.resultsFilterFiliais.forEach(element => {
        element.atendimentoTotal = element.content.length;
      });
    }
    this.filterArrayFiliais(string, this.filiais);
    this.pdfReady = true;
  }

  filterArrayCidadeFiliais(model, arr){
    this.resultsFilterFiliais = [];
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    arr.forEach(e1 => {
      let redes = {
        nome: e1.nome,
        id: e1.id,
        content: [],
        visible: true,
        atendimentoTotal: 0
      };
      
      e1.content.forEach(element => {
        if (regexp.test(element.RFIL_Cidade)){
          redes.content.push(element);
        }
      });
      redes.atendimentoTotal = redes.content.length || 0;
      this.resultsFilterFiliais.push(redes);

      // console.log(this.resultsFilterFiliais);
    });    

  }

  searchCidadeFiliais(string){
    if (string.length < 1){
      this.enableTotal = false;
    }else{
      this.enableTotal = true;
    }
    this.filterArrayCidadeFiliais(string, this.filiais);
    this.pdfReady = true;
  }

  pdfReady = false;

  makePDF() {
    document.getElementById('filialTable').style.display = 'block';
    if (this.pdfReady == true){
      console.log('ok');
      html2canvas(document.getElementById('filialTable')).then( (canvas) => {
        let w = window.open("",'_blank');
        w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="'+(<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1)+'" /></body></html>');
        document.getElementById('filialTable').style.display = 'none';
      });
    }
  }
}
