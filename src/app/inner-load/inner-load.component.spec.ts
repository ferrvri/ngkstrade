import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InnerLoadComponent } from './inner-load.component';

describe('InnerLoadComponent', () => {
  let component: InnerLoadComponent;
  let fixture: ComponentFixture<InnerLoadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InnerLoadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InnerLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
