import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilsService } from '../utils.service';
import { GeolocationServiceService } from '../geolocation-service.service';
import { environment } from '../../environments/environment';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css']
})
export class UsuariosComponent implements OnInit {

  pageParams: any;

  userData = [];

  errorNomeCompleto = { required: null };
  errorNivel = { required: null };
  errorUsuario = { required: null };
  errorSenha = { required: null };
  errorEmail = { required: null };
  errorAnexo = { required: null };

  nomeCompleto: string = "";
  nivel: string = "Selecione";
  usuario: string = "";
  senha: string = "";
  status: string = "Ativo";
  email: string = '';
  anexo: string = '';

  nivelOK: boolean = false;
  statusOK: boolean = false;

  clientes: any = [];
  cliente: string = 'Selecione';
  clienteIndex: number = 0;
  clienteOK: boolean = false;

  redes: any = [];
  toUploadImage: any = [];

  clientes_res = [{
    'Cli_ID': 0,
    'Cli_Nome': '',
    activated: false
  }];

  rede_res = [{
    'RED_ID': 0,
    'RED_Nome': '',
    'meta': '',
    activated: false
  }];

  cadastroUsuario: FormGroup = new FormGroup({
    nomeCompleto: new FormControl(this.nomeCompleto, [Validators.required, Validators.minLength(6)]),
    usuario: new FormControl(this.usuario, [Validators.required, Validators.minLength(6)]),
    senha: new FormControl(this.senha, [Validators.required, Validators.minLength(6)]),
    nivel: new FormControl(this.nivel, [Validators.required, Validators.minLength(6)]),
    status: new FormControl(this.status, [Validators.required, Validators.minLength(6)]),
    // cliente: new FormControl(this.cliente, [Validators.required, Validators.minLength(6)]),
    email: new FormControl(this.email, [Validators.required, Validators.minLength(6)]),
    anexo: new FormControl(this.email, [Validators.required, Validators.minLength(6)])
  });

  latLong = '';

  allRedesSelected = false;

  constructor(
    private _geo: GeolocationServiceService,
    private utils: UtilsService,
    private _http: HttpService,
    private _activatedRoute: ActivatedRoute,
    private _router: Router
  ) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat + ';' + c.lng);

    jQuery('.mydatepicker, #datepicker').datepicker();

    $('.dropify').dropify();

    // Used events
    var drEvent = $('#input-file-events').dropify();

    drEvent.on('dropify.beforeClear', function (event, element) {
      return confirm("Você deseja realmente apagar \"" + element.file.name + "\" ?");
    });

    drEvent.on('dropify.afterClear', function (event, element) {
      alert('Arquivo apagado');
    });

    drEvent.on('dropify.errors', function (event, element) {
      console.log('Erro!');
    });

    var drDestroy = $('#input-file-to-destroy').dropify();
    drDestroy = drDestroy.data('dropify');
    $('#toggleDropify').on('click', function (e) {
      e.preventDefault();
      if (drDestroy.isDropified()) {
        drDestroy.destroy();
      } else {
        drDestroy.init();
      }
    })

    this._http.post(
      'selectClientes.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.clientes.push(element);
        });
      } else if (response.status == '0x101') {
        window.alert('Erro na seleção de clientes');
      }
    });

    this._http.post(
      'selectRedes.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.redes.push(element);
        });
      } else if (response.status == '0x101') {
        window.alert('Erro na seleção de clientes');
      }
    });

    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar' && this.pageParams.params.usrId.length > 0) {

      this._http.post(
        'selectUsuarios.php',
        {
          unique: true,
          id: this.pageParams.params.usrId
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.nomeCompleto = response.result[0].Usr_Nome;
          this.usuario = response.result[0].Usr_Login;
          this.nivel = response.result[0].Usr_Nivel;
          this.status = response.result[0].Usr_Status;
          this.email = response.result[0].Usr_Email;
          if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Admin') {
            this.senha = response.result[0].Usr_Senha;
          } else {
            this.senha = '******';
          }
          this.usr_id = parseInt(response.result[0].Usr_ID);
          console.log(response.result[0].Usr_Avatar);
          if (response.result[0].Usr_Avatar.length > 0 && response.result[0].Usr_Avatar != 'null' && response.result[0].Usr_Avatar != "NULL" && response.result[0].Usr_Avatar != null) {
            console.log(response.result[0].Usr_Avatar);
            this.toUploadImage.push(environment.apiUrl+'uploads/users/' + response.result[0].Usr_Avatar);
          }

          this.notEdit = true;
        } else if (response.status == '0x101') {
          window.alert('Erro ao selecionar usuario');
        }
      });
    }
  }

  removeFiles() {
    $(".dropify-clear").click();
    this.toUploadImage = [];
    this.notEdit = false;
  }

  applyMoneyMask(c) {
    c.meta = this.utils.detectAmount(c.meta);
  }

  uploadChanges(event) {
    this.notEdit = false;
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      //this.nameOfFile = event.target.files[0].name;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.toUploadImage.push(reader.result.toString().split(',')[1]);
      };
    }
  }

  nameOfFile: string = '';

  usr_id: number = 0;

  setCliente(c, cI) {
    this.cliente = c.Cli_Nome;
    this.clienteIndex = c.Cli_ID;

    this.clientes_res[cI].activated = false;
    this.clientes_res[cI].Cli_ID = c.Cli_ID;
    this.clientes_res[cI].Cli_Nome = c.Cli_Nome;

    if (this.nivel == 'Vendedor' && this.pageParams.params.page == 'editar') {
      this._http.post(
        'selectRedesVendedor.php',
        {
          ven_id: this.usr_id,
          cli_id: c.Cli_ID
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          response2.result.forEach((element) => {
            this.redes.forEach((element2, index) => {
              if (element2.RED_ID == element) {
                (<HTMLInputElement>document.getElementById('rede_checkbox_' + index)).checked = true;
              }
            });
          });
        }
      });
    }
  }

  setRedeMeta(c, cI) {
    this.rede_res[cI].activated = false;
    this.rede_res[cI].RED_ID = c.RED_ID;
    this.rede_res[cI].RED_Nome = c.RED_Nome;
  }

  addRedeMeta() {
    this.rede_res.push({
      'RED_ID': 0,
      'RED_Nome': '',
      'meta': '',
      activated: false
    });
  }

  validation() {
    try {
      if (this.nomeCompleto.length < 1) {
        this.errorNomeCompleto = { required: true };
      } else {
        this.errorNomeCompleto = { required: null };
      }
      if (this.nivel.length < 1) {
        this.errorNivel = { required: true }
      } else {
        this.errorNivel = { required: null };
      }
      if (this.usuario.length < 1) {
        this.errorUsuario = { required: true };
      } else {
        this.errorUsuario = { required: null };
      }
      if (this.senha.length < 1) {
        this.errorSenha = { required: true }
      } else {
        this.errorSenha = { required: null };
      }
      if (this.email.length < 1) {
        this.errorEmail = { required: true }
      } else {
        this.errorEmail = { required: null };
      }
      if (this.toUploadImage.length < 1) {
        this.errorAnexo = { required: true }
      } else {
        this.errorAnexo = { required: null }
      }
      if (this.nomeCompleto == 'Administrador' || this.usuario == 'Administrador') {
        window.alert('Não é possivel cadastrar este usuário')
        return false;
      }
      return true;
    } catch (e) {
      console.log(e.toString());
    }
  }

  notEdit: boolean = false;
  pressionado: boolean = false;
  cadUsuario(): void {
    this.pressionado = true
    if (this.pageParams.params.page == 'editar' && this.pageParams.params.usrId.length > 0) {
      if (this.validation() && (this.errorNivel.required == null && this.errorNomeCompleto.required == null &&
        this.errorSenha.required == null && this.errorUsuario.required == null && this.errorEmail.required == null) && this.pageParams.params.page == 'editar') {

        this.nameOfFile = Math.ceil(Math.random() * 1000) + this.nomeCompleto + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg'
        let nameOfFile = this.nameOfFile.trim();

        let ob = {
          id: this.pageParams.params.usrId,
          nomeCompleto: this.nomeCompleto,
          nivel: this.nivel,
          usuario: this.usuario,
          senha: this.senha,
          status: this.status,
          email: this.email,
          title: nameOfFile,
          anexo: this.toUploadImage[0],

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Edição de Usuario - Usuario[' + this.pageParams.params.usrId + ']'
        }

        if (this.toUploadImage.length == 1 && this.notEdit == true) {
          ob.anexo = '';
          ob.title = '';
        }

        this._http.post(
          'editarUsuario.php',
          ob
        ).subscribe((response) => {
          if (response.status == '0x104') {
            if (this.nivel == 'Vendedor') {
              let i: number = 0;
              for (i = 0; i < this.redes.length; i++) {
                if ((<HTMLInputElement>document.getElementById('rede_checkbox_' + i)).checked === true) {
                  this.clientes_res.forEach((element) => {
                    this._http.post(
                      'insertVendedor.php',
                      {
                        type: 'update',
                        usr_id: this.usr_id,
                        rede: this.redes[i].RED_ID,
                        cliente: element.Cli_ID
                      }
                    ).subscribe((res) => {
                      console.log('going');
                    });
                  });
                }
                if (i == (this.redes.length - 1)) {
                  window.alert('KSTrade - Cadastro de Usuario\r\n\r\nUsuario editado com sucesso!');
                  this._router.navigate(['/main/innerload']);
                  setTimeout(() => {
                    this._router.navigate(['/main/cadUsuario']);
                  }, 1000);
                } else {
                  console.log(this.redes.length);
                  console.log(i);
                }
              }
            } else if (this.nivel == 'Representante') {

              this.clientes_res.forEach((e, index) => {
                this._http.post(
                  'insertRepresentanteCliente.php',
                  {
                    cliente_id: e.Cli_ID,
                    usuario_id: this.usr_id
                  }
                ).subscribe((res) => {
                  if (res.status == '0x104') {
                    console.log('ok');
                  }
                })

                if (index == (this.clientes_res.length - 1)) {
                  this.rede_res.forEach((element, ii) => {
                    this._http.post(
                      'insertRepresentanteMeta.php',
                      {
                        rede_id: element.RED_ID,
                        usuario_id: this.usr_id,
                        valor: element.meta.replace('.', '')
                      }
                    ).subscribe((res) => {
                      if (res.status == '0x104') {
                        console.log('ok meta');
                        if (ii == this.rede_res.length - 1) {
                          window.alert('KSTrade - Edição de Usuario\r\n\r\nUsuario editado com sucesso!');
                          this._router.navigate(['/main/innerload']);
                          setTimeout(() => {
                            this._router.navigate(['/main/cadUsuario']);
                          }, 1000);
                        }
                      }
                    })
                  })
                }
              });

            } else {
              window.alert('KSTrade - Edição de Usuario\r\n\r\nUsuario editado com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout(() => {
                this._router.navigate(['/main/cadUsuario']);
              }, 1000);
            }
            this.pressionado = false;
          } else if (response.status == '0x101') {
            window.alert('Erro de inserção ao banco');
          }
        });
      }
    } else {
      if (this.validation() && (this.errorNivel.required == null && this.errorNomeCompleto.required == null &&
        this.errorSenha.required == null && this.errorUsuario.required == null && this.errorEmail.required == null)) {
        this.nameOfFile = Math.ceil(Math.random() * 1000) + this.nomeCompleto + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg'
        let nameOfFile = this.nameOfFile.trim();

        this._http.post(
          'insertUsuario.php',
          {
            nomeCompleto: this.nomeCompleto,
            nivel: this.nivel,
            usuario: this.usuario,
            senha: this.senha,
            email: this.email,
            title: nameOfFile,
            anexo: this.toUploadImage[0],

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Cadastro de Usuario - Usuario[' + this.pageParams.params.usrId + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            if (this.nivel == 'Vendedor') {
              let i: number = 0;
              for (i = 0; i < this.redes.length; i++) {
                if ((<HTMLInputElement>document.getElementById('rede_checkbox_' + i)).checked === true) {
                  this.clientes_res.forEach((element) => {
                    this._http.post(
                      'insertVendedor.php',
                      {
                        rede: this.redes[i].RED_ID,
                        cliente: element.Cli_ID
                      }
                    ).subscribe((res) => {
                      console.log('going');
                    })
                  });
                }
                if (i == (this.redes.length - 1)) {
                  console.log('ok');
                  window.alert('KSTrade - Cadastro de Usuario\r\n\r\nUsuario cadastrado com sucesso!');
                  this._router.navigate(['/main/innerload']);
                  setTimeout(() => {
                    this._router.navigate(['/main/cadUsuario']);
                  }, 1000);
                } else {
                  console.log(this.redes.length);
                  console.log(i);
                }
              }
            } else if (this.nivel == 'Representante') {
              this._http.post(
                'insertConta.php',
                {
                  descricao: '$#COMISSÃO VICTOR TI, REPRESENTANTES',
                  dataVencimento: new Date(new Date().getFullYear(), (new Date().getDate() > 10 ? new Date().getMonth() + 1 : new Date().getMonth()), 10).toISOString().split('T')[0],
                  valor: '50,00'
                }
              ).subscribe((r) => {
                if (r.status == '0x104') {
                  this.clientes_res.forEach((e, index) => {
                    this._http.post(
                      'insertRepresentanteCliente.php',
                      {
                        cliente_id: e.Cli_ID,
                        usuario_id: response.id
                      }
                    ).subscribe((res) => {
                      if (res.status == '0x104') {
                        console.log('ok');
                      }
                    })

                    if (index == (this.clientes_res.length - 1)) {
                      this.rede_res.forEach((element, ii) => {
                        this._http.post(
                          'insertRepresentanteMeta.php',
                          {
                            rede_id: element.RED_ID,
                            usuario_id: response.id,
                            valor: element.meta
                          }
                        ).subscribe((res) => {
                          if (res.status == '0x104') {
                            console.log('ok meta');
                            if (ii == this.rede_res.length - 1) {
                              window.alert('KSTrade - Cadastro de Usuario\r\n\r\nUsuario cadastrado com sucesso!');
                              this._router.navigate(['/main/innerload']);
                              setTimeout(() => {
                                this._router.navigate(['/main/cadUsuario']);
                              }, 1000);
                            }
                          }
                        })
                      })
                    }
                  });
                }
              });
            } else {
              window.alert('KSTrade - Cadastro de Usuario\r\n\r\nUsuario cadastrado com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout(() => {
                this._router.navigate(['/main/cadUsuario']);
              }, 1000);
            }
            this.pressionado = false;
          } else if (response.status == '0x101') {
            window.alert('Erro de inserção ao banco');
          } else if (response.status == '0x102') {
            window.alert('KSTrade - Essa foto já está cadastrada a esse usuário!');
          }
        });
      }
    }
  }

  setNivel(nivel) {
    this.nivel = nivel;
    console.log(this.nivel);
    this.nivelOK = false;
  }

  setStatus(status) {
    this.statusOK = false;
    this.status = status;
  }

  addCliente() {
    this.clientes_res.push({
      Cli_ID: 0,
      Cli_Nome: '',
      activated: false
    });
  }

  removeCliente(index) {
    this.clientes_res.splice(index, 1);
  }

  removeRedeMeta(index) {
    this.rede_res.splice(index, 1);
  }

  selectAllRedes() {
    this.allRedesSelected = true;
    this.redes.forEach((_, index) => {
      let checkbox = document.getElementById('rede_checkbox_' + index) as HTMLInputElement
      if (checkbox) {
        checkbox.setAttribute('checked', 'true');
        checkbox.value = 'true';
      }
    })
  }

  unselectAllRedes() {
    this.allRedesSelected = false;
    this.redes.forEach((_, index) => {
      let checkbox = document.getElementById('rede_checkbox_' + index) as HTMLInputElement
      if (checkbox) {
        checkbox.removeAttribute('checked');
        checkbox.value = 'false';
      }
    })
  }
}
