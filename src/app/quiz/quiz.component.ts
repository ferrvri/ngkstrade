import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '../../../node_modules/@angular/router';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements OnInit {

  diasdasemana: any = [];
  days: any = [
    {name : 'Seg', activated: false},
    {name : 'Ter', activated: false},
    {name : 'Qua', activated: false},
    {name : 'Qui', activated: false},
    {name : 'Sex', activated: false},
    {name : 'Sab', activated: false}
  ];

  tipo:string = 'Selecionar';
  tipoOK:boolean = false;
  tipoIndex: number = 0;

  questaoTitle:string = '';
  questao:string = '';

  tempo:string = '20';

  questoes = [
    {questao: 'a', questao_text: '', activated: false, required: false},
    {questao: 'b', questao_text: '', activated: false, required: false},
    {questao: 'c', questao_text: '', activated: false, required: false},
    {questao: 'd', questao_text: '', activated: false, required: false},
    {questao: 'e', questao_text: '', activated: false, required: false}
  ]
  quizquestao = [];

  tipos = [
    {tipo: 0, tipo_nome: 'Promotores'},
    {tipo: 1, tipo_nome: 'Coordenadores Internos/Externos'},
    {tipo: 2, tipo_nome: 'Clientes/Vendedores'}
  ]

  errorTipo: any = {required: null};
  errorTitle: any = {required: null};
  errorTempo: any = {required: null};
  errorQuestoes: any = {required: null};

  constructor( private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute ) { }

  ngOnInit() {

  }

  selectThisDay(day){
    if (day.activated == true){
      day.activated = false;
      this.diasdasemana.forEach( (element, index) => {
        if (day.name == element){
          this.diasdasemana.splice(index,1);
        }
      });
    }else{
      day.activated = true;
      this.diasdasemana.push(day.name);
    }
  }

  selectThisQuestion(questao){
    if (questao.activated == true){
      questao.activated = false;
      this.quizquestao.forEach( (element, index) => {
        if (questao.questao == element){
          this.quizquestao.splice(index,1);
        }
      });
    }else{
      questao.activated = true;
      this.questoes.forEach( (e) => {
        if (e.activated == true && e.questao != questao.questao){
          e.activated = false;
        }
      })

      this.quizquestao[0] = questao.questao_text;
    }
  }

  setTipo(t){
    this.tipo = t.tipo_nome;
    this.tipoIndex = t.tipo;
    this.tipoOK = false;
  }

  validation(){
    if(this.tipo.length < 1 || this.tipo == 'Selecionar'){
      this.errorTipo.required = true;
    }
    if (this.questaoTitle.length < 1){
      this.errorTitle.required = true;
    }
    if (this.tempo.length < 1){
      this.errorTempo.required = true;
    }
    try{
      let exp = {};
      this.questoes.forEach( (e) => {
        if (e.questao_text == '' || e.questao_text.length < 1){
          throw exp;
        }
      })
    }catch(exp){
      this.errorQuestoes.required = true;
    }
    return true;
  }

  cadQuiz(){
    let dias = '';
    this.diasdasemana.forEach(element => {
      dias += element+';'
    });

    let qs = '';
    this.questoes.forEach( (element) => {
      qs += element.questao_text+';'
    });

    if (this.validation() && this.errorTempo.required == null && this.errorTipo.required == null 
      && this.errorTitle.required == null && this.errorQuestoes.required == null) {
        this._http.post(
          'insertQuiz.php',
          {
            title: this.questaoTitle,
            questoes: qs,
            correta: this.quizquestao[0],
            tempo: this.tempo,
            tipo: this.tipoIndex,
            dias_semana: dias
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade Merchandising - Quiz\r\n\r\nQuiz cadastrado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout( () => {
              this._router.navigate(['/main/quiz']);
            }, 1500);
          }else{
            window.alert('KSTrade Merchandising - Quiz\r\n\r\nErro ao cadastrar Quiz.');
          }
        });
    }
  }

}
