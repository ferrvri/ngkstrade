import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'images-gallery',
  templateUrl: './images-gallery.component.html',
  styleUrls: ['./images-gallery.component.css']
})
export class ImagesGalleryComponent implements OnInit {

  @Input() images: any = [];

  constructor() { }

  ngOnInit() {
    console.log('ok');
    console.log(this.images);
  }

}
