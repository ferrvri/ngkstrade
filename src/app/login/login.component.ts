import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  usuario: string = "";
  senha: string = "";

  error : any = {};

  formLogin: FormGroup = new FormGroup( {
    login: new FormControl(this.usuario, [Validators.required, Validators.minLength(6)]),
    senha: new FormControl(this.senha, [Validators.required, Validators.minLength(6)])
  } );

  expired = 'false';
  
  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _activatedRoute: ActivatedRoute, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this.expired = localStorage.getItem('expired');
    if (localStorage.getItem('session') && JSON.parse(localStorage.getItem('session'))[0].Usr_Nome){
      this._router.navigate(['main']);
    }else if (localStorage.getItem('session') && JSON.parse(localStorage.getItem('session'))[0].Cli_Nome){
      this._router.navigate(['cliente']);
    }
  }

  logIn(form):void{
    if (form.value.login.length < 1 || form.value.senha.length < 1){
      this.error = {title: '0x105', msg: 'Preencha todos os campos'};
      setTimeout( () => {
        this.error = {title: '', msg: ''};
      }, 1500)
    }else{
      this._http.post(
        'login.php',
        {
          usuario: form.value.login,
          senha: form.value.senha,

          //log
          usuario_id: '0',
          usuario_nome: '-',
          usuario_nivel: '-',
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Novo login[' + form.value.login + ' / ' + form.value.senha + ']'
        }
      ).subscribe( (response) => {
        if(response.status == '0x104' && response.result[0].Usr_Status == 'Ativo' && (response.result[0].Usr_Nivel == 'Admin' || response.result[0].Usr_Nivel == 'Coordenador' || response.result[0].Usr_Nivel == 'Auxiliar' || response.result[0].Usr_Nivel == 'Externo' || response.result[0].Usr_Nivel == 'Financeiro' || response.result[0].Usr_Nivel == 'Representante')){
          localStorage.setItem('session', JSON.stringify(response.result));
          if (localStorage.getItem('expired')){
            localStorage.removeItem('expired')
          }
          // this._router.navigate(['main/dashboard']);
          this._router.navigate(['loading']);
          setTimeout(() => {
            // this._router.navigate(['/main']);
            window.location.reload();
            // window.location.href = '/sistema/#/main';
          }, 1500);
        }else if (response.status == '0x104' && response.result[0].Usr_Status == 'Inativo'){
          window.alert('Seu acesso foi inativado, por favor contate-nos');
        }else if(response.status == '0x101'){
          window.alert('Usuário ou senha incorretos!');
        }else if (response.result.length < 1){
          window.alert('Erro inesperado ocorreu!');
        }else if (response.result[0].Usr_Nivel == 'Vendedor'){ 
          window.alert('Acesse a área de cliente!');
        }
      });
    }
  }

  keydownLogin(e): void{
    switch (e.keyCode || e.which){
      case 13:
        this.logIn(this.formLogin);
      break;
    }    
  }

  passwordInputFocus(e){
    switch (e.keyCode || e.which){
      case 13:
        document.getElementById('passInput').focus();
      break;
    }  
  }

}
