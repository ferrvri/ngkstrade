import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';


import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
const swal: SweetAlert = _swal as any;

@Component({
  selector: 'app-master-area',
  templateUrl: './master-area.component.html',
  styleUrls: ['./master-area.component.css']
})
export class MasterAreaComponent implements OnInit {

  canSelect: boolean = false;
  emptyResult: boolean = false;
  enableTotal: boolean = false;
  loggedIn: boolean = false;

  resultFilterPromotores = [];
  promotores = [];
  usuarios = [];

  tempToPrintList: any = [];

  queryPromotores: string = '';
  queryPromotoresTel: string = '';
  queryPromotoresConta: string = '';
  queryPromotoresFil: string = '';

  totalAtendimento: number = 0;

  doneLoading: boolean = false;

  userData: any = [];

  senhaDigitada = '';

  pageParams;

  constructor(private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    console.log(this.pageParams)

    this._http.post(
      'selectPromotor2.php',
      {
        group: true
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {

        this.resultFilterPromotores = this.promotores = response.result.filter((a) => {
          return a.PRO_Nome.indexOf('PJ') !== -1
        });

        this._http.post(
          'selectUsuarios.php',
          {
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.usuarios = response.result;
          } else if (response.status == '0x101') {
            this.emptyResult = true;
          }
        });
        setTimeout(() => {
          this.doneLoading = true;
        }, 5000);
      } else if (response.status == '0x101') {
        this.emptyResult = true;
      }
    });

    let div = document.createElement('div');
    div.innerHTML = `
        <input type="password" class="form-control" style="width: 80%" placeholder="Senha" id="secretInput"/>
    `
    div.style.display = 'table';
    div.style.margin = '10px auto';
    div.style.height = '120px';

    div.getElementsByClassName('form-control')[0].addEventListener('change', (ev) => {
      let data = (ev.target as any).value
      this._http.post(
        'login.php',
        {
          usuario: 'admin',
          senha: data
        }
      ).subscribe((response) => {
        if (response.status == '0x104' && response.result[0].Usr_Status == 'Ativo' && (response.result[0].Usr_Nivel == 'Admin')) {
          this.loggedIn = true;
          let btn = document.getElementsByClassName('swal-button swal-button--confirm')[0] as any;
          (btn as any).click();
        } else {
          let btn = document.getElementsByClassName('swal-button swal-button--confirm')[0] as any;
          (btn as any).click();
          setTimeout(() => {
            swal({
              title: 'Sem permissão de acesso',
              icon: 'error'
            }).then(() => {
              this._router.navigate(['/main/dashboard']);
            });
          }, 150);
        }
      });
    })

    swal({
      title: 'Digite a senha para prosseguir',
      content: div as any
    })

    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);

  }

  filterArrayPromotores(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
  }

  filterArrayPromotoresConta(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.PRO_Conta);
    });
  }

  filterArrayPromotoresTelefone(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.PRO_Telefone);
    });
  }

  filterArrayPromotoresFilial(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultFilterPromotores = arr.filter((e) => {
      return regexp.test(e.RFIL_Nome);
    });
  }

  searchPromotores(p, param) {
    switch (param) {
      case 'nome':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotores(p, this.promotores);
        break;
      case 'conta':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotoresConta(p, this.promotores);
        break;
      case 'telefone':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotoresTelefone(p, this.promotores);
        break;
      case 'filial':
        if (p.length < 1) {
          this.enableTotal = false;
          this.totalAtendimento = 0;
        } else {
          if (this.resultFilterPromotores.length == 1) {
            this.enableTotal = true;
            this.totalAtendimento = this.resultFilterPromotores[0].content.length;
          }
        }
        this.filterArrayPromotoresFilial(p, this.promotores);
        break;
    }
  }

  deletePromotor(p, kind, firstindex?, secondindex?) {
    let c = confirm('KSTrade - Lista de Promotores\r\n\r\nDeseja realmente remover?');
    if (c == true) {
      let o = {
        id: 0,
        nome: ''
      }

      switch (kind) {
        case 'total': {
          o.nome = p.PRO_Nome
          delete o.id
          break;
        }
        case 'partial': {
          o.id = p.PRO_ID
          delete o.nome
          break;
        }
      }

      this._http.post(
        'deletePromotor.php',
        o
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Lista de promotores\r\n\r\nRemovido com sucesso!');
          if (kind == 'partial') {
            this.resultFilterPromotores[firstindex].content.splice(secondindex, 1);
          } else {
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/listaPromotor']);
            }, 1000);
          }
        } else {
          window.alert('Erro na remocao de promotor!');
        }
      });
    }
  }

  deleteSelected(kind) {
    switch (kind) {
      case 'redes': {
        let r = []
        let c = confirm('KSTrade - Lista de Promotores\r\n\r\nDeseja realmente remover os selecionados?');
        if (c == true) {
          this.resultFilterPromotores.forEach((elem1, index1) => {
            elem1.content.forEach((elem2, index2) => {
              if ((<HTMLInputElement>document.getElementById('pro-' + index1 + '-' + index2)).checked == true) {
                this._http.post(
                  'deletePromotor.php',
                  {
                    id: elem2.PRO_ID
                  }
                ).subscribe((response) => {
                  if (response.status == '0x104') {
                    r.push('true');
                  } else {
                    r.push('false');
                  }
                });
              }
              if (index1 == (this.promotores.length - 1)) {
                if ('false' in r) {
                  window.alert('Erro na remocao de promotor!');
                } else {
                  window.alert('KSTrade - Lista de promotores\r\n\r\nRemovido com sucesso!');
                  this.resultFilterPromotores[index1].content.splice(index2, 1);
                }
              }
            });
          });

        }
        break;
      }
      case 'promotores': {
        let r = []
        let c = confirm('KSTrade - Lista de Promotores\r\n\r\nDeseja realmente remover os selecionados?');
        if (c == true) {
          this.resultFilterPromotores.forEach((elem1, index1) => {
            if ((<HTMLInputElement>document.getElementById('selectPRO_' + index1)) !== null && (<HTMLInputElement>document.getElementById('selectPRO_' + index1)).checked == true) {
              this._http.post(
                'deletePromotor.php',
                {
                  nome: elem1.PRO_Nome
                }
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  r.push('true');
                  this.resultFilterPromotores.splice(index1, 1);
                } else {
                  r.push('false');
                }
              });
            }

            if (index1 == (this.promotores.length - 1)) {
              if ('false' in r) {
                window.alert('Erro na remoção de promotor!');
              } else {
                window.alert('KSTrade - Lista de promotores\r\n\r\nRemovido com sucesso!');
              }
            }
          });
        }
        break;
      }
    }

  }

  inativatePromotor(fc) {
    let c = confirm('KSTrade - Lista de promotores\r\n\r\nDeseja realmente inativar esse promotor?');
    if (c == true) {
      this._http.post(
        'inativatePromotor.php',
        {
          pro_id: fc.PRO_ID,
          pro_nome: fc.PRO_Nome
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Lista de promotores\r\n\r\nInativado com sucesso!');
        }
      });
    }
  }

  proOn: boolean = false;

  proOnline(arr) {
    if (this.proOn) {
      this.resultFilterPromotores = this.promotores;
      this.proOn = false;
    } else {
      this.proOn = true;
      this.resultFilterPromotores = arr.filter((e) => {
        return (e.PRO_Net_Status == '1' || e.PRO_Net_Status === true) ? true : false;
      });
    }
  }


  makeDisconnect(promotor) {
    this._http.post(
      'app/updateNetStatus.php',
      {
        status: 'mDisconnect',
        email: promotor.PRO_Email
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        swal({
          title: 'Comando enviado com sucesso!'
        })

        promotor.menuRow = false
      }
    });
  }

  disconnectUsers() {
    this.usuarios.forEach( (user) => {
      this._http.post(
        'updateUsrNetStatus.php',
        {
          id: user.Usr_ID,
          status: '3'
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          console.log('disconnected', user.Usr_Nome);
        }
      });
    })
  }
}
