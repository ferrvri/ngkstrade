import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadBookComponent } from './cad-book.component';

describe('CadBookComponent', () => {
  let component: CadBookComponent;
  let fixture: ComponentFixture<CadBookComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadBookComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadBookComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
