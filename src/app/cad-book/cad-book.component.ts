import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute } from '@angular/router';

declare var PptxGenJS: any;

@Component({
  selector: 'app-cad-book',
  templateUrl: './cad-book.component.html',
  styleUrls: ['./cad-book.component.css']
})
export class CadBookComponent implements OnInit {

  contents: any = [];
  resultsFilterContents: any = [];

  redes = [];

  rede_selected = false;
  doneLoading = true;
  pageParams;

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    })

    this._http.post(
      'selectJobsOfCliente.php',
      {
        only_redes: true,
        cli_id: this.pageParams.params.Cli_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.redes = response.result;
      }
    })
  }

  genBook(nome, obj) {
    var pptx = new PptxGenJS();

    document.getElementById('caption').innerHTML = 'Gerando o Book...'
    pptx.setAuthor('Sistema KSTRADE');
    pptx.setCompany('KSTrade Merchandising');
    pptx.setSubject('BOOK '+nome);
    pptx.setTitle('Book');

    pptx.setLayout('LAYOUT_16x9');

    pptx.addNewSlide().addImage({
      path: './assets/books/book-1.png', x: 0, y: 0, w: 10.0, h: 5.625
    })

    pptx.addNewSlide().addImage({
      path: './assets/books/init_text.png', x: 0, y: 0, w: 10.0, h: 5.625
    })

    let intro = pptx.addNewSlide();

    intro.addShape(pptx.shapes.RECTANGLE, { x: 0.0, y: 0.0, w: 10.0, h: 5.625, fill: '000000' });

    intro.addImage({
      path: 'uploads/logos/' + this.pageParams.params.Cli_Logo, x: 3.34375, y: 2.8125 - (1.5625 / 2), w: 3.125, h: 1.5625
    })

    this._http.post(
      // 'selectJobsOfCliente.php',
      'selectJobsOfCliente.php',
      {
        cli_id: this.pageParams.params.Cli_ID,
        rede_id: obj.RED_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        let result_full = response.result.filiais.filter((a) => {
          return a.content.length > 0;
        });

        result_full.forEach((element, index) => {

          let slide = pptx.addNewSlide();

          slide.addImage({
            path: './assets/books/bg_simple.jpg', x: 0, y: 0, w: 10.0, h: 5.625
          });

          slide.addText('Pag. ' + (index + 1), { x: 9.0, y: 0.150, fontFace: 'Arial', fontSize: 10, color: 'ffffff' });

          slide.addText(nome + ' - ' + element.RFIL_Nome, { shape: pptx.shapes.RECTANGLE, align: 'c', color: 'ffffff', x: 0, y: 0, w: 10.0, h: 2, lineSize: 1 });
          // slide.addText(, { x: 2.75, y: 0.725, fontFace: 'Arial', fontSize: 16, color: 'ffffff' });

          let res_port = [];
          let res_land = [];

          res_port = element.content.filter((a) => {
            return a.isPort && a.isPort == true
          })

          res_land = element.content.filter((v) => {
            return v.isLand && v.isLand == true
          });

          if (res_port.length > 0 || res_land.length > 0){
            if (res_port.length > res_land.length) {
              if (res_port.length > 0 && res_port.length == 2) {
                slide.addImage({
                  path: 'uploads/' + res_port[0].JOB_Foto, x: 2.7, y: 1.35, w: 2.225, h: 3.455
                });
                slide.addImage({
                  path: 'uploads/' + res_port[1].JOB_Foto, x: 4.975, y: 1.35, w: 2.225, h: 3.455
                });
              } else if (res_port.length > 0 && res_port.length == 1) {
                slide.addImage({
                  path: 'uploads/' + res_port[0].JOB_Foto, x: 3.8875, y: 1.35, w: 2.225, h: 3.455
                });
              } else {
                for (let i = 0; i < 4; i++) {
                  if (res_port[i] !== undefined) {
                    slide.addImage({
                      path: 'uploads/' + res_port[i].JOB_Foto, x: 0.25 + (2.35 * i), y: 1.35, w: 2.225, h: 3.455
                    });
                  }
                }
              }
            } else {
              if (res_land.length > 0 && res_land.length == 1) {
                slide.addImage({
                  path: 'uploads/' + res_land[0].JOB_Foto, x: 2.875, y: 1.35, w: 4.25, h: 2.85
                });
              } else {
                for (let i = 0; i < 2; i++) {
                  if (res_land[i] !== undefined) {
                    slide.addImage({
                      path: 'uploads/' + res_land[i].JOB_Foto, x: 0.5 + (4.5 * i), y: 1.35, w: 4.25, h: 2.85
                    });
                  }
                }
              }
            }
          }

          slide.addImage({
            path: 'uploads/logos/' + this.pageParams.params.Cli_Logo, x: 8.625, y: 4, w: 1.125, h: 1.0
          })

          // let result_offen = response.result.filiais.filter((a) => {
          //   return a.content.length < 1;
          // });


          // let offsetTop = 1.35;

          // result_offen.forEach(ofElem => {
          //   slide.addText(nome + ' - ' + ofElem.RFIL_Nome, { x: 1.75, y: offsetTop, fontFace: 'Arial', fontSize: 16, color: 'ffffff' });
          // });

          // offsetTop += 0.5;
        });

        pptx.addNewSlide().addImage({
          path: './assets/books/final.png', x: 0, y: 0, w: 10.0, h: 5.625
        });

        document.getElementById('caption').innerHTML = 'Salve o book!'
        setTimeout(() => {
          document.getElementById('caption').innerHTML = 'Selecione a rede para gerar o Book'
        }, 1500);

        pptx.save('BOOK-' + nome + new Date().getFullYear() + '_' + (new Date().getMonth() + 1));
      } else if (response.status == '0x101') {
        window.alert('KSTrade Merchandising - Gerar Book \r\n\r\n O book não foi gerado! Jobs insuficientes.')
      }
    })
  }

}
