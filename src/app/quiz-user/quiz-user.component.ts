import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '../../../node_modules/@angular/router';
import { HttpService } from '../http.service';

@Component({
  selector: 'app-quiz-user',
  templateUrl: './quiz-user.component.html',
  styleUrls: ['./quiz-user.component.css']
})
export class QuizUserComponent implements OnInit {

  questionTitle: string = '';
  questionTime: string = '';

  pageParams: any;

  options = [];

  userData = [];

  constructor( private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router ) { }

  ngOnInit() {
    if (localStorage.getItem('session')){
      this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    }
    
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });  

    this._http.post(
      'selectQuiz.php',
      {
        unique: true,
        id: this.pageParams.params.per_id
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.questionTime = response.result[0].PER_Tempo;
        this.questionTitle = response.result[0].PER_Title;
        
        response.result[0].PER_Text.split(';').forEach(element => {
          this.options.push(element);
        });

        if (this._router.url == '/main/cadQuizUser' || this._router.url == '/cliente/cadQuizUser' ){
          this.tick(parseInt(this.questionTime));
        }
      }
    });
  }

  tick(time){
    if (time > 0){
      setTimeout( () => {
        this.questionTime = ''+ (time-1);
        this.tick(time-1);
      }, 1000)
    }else if (time == 0 || time < 0){
      this._http.post(
        'insertQuizResposta.php',
        {
          timed: true,
          per_id: this.pageParams.params.per_id,
          usr_id: this.userData[0].Usr_ID
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade Merchandising - Quiz\r\n\r\nVocê não respondeu a tempo!');
          this._router.navigate(['/main/dashboard']);
        }
      });
    }
  }

  setOption(op){
    let c = confirm('KSTrade Merchandising - Quiz\r\n\r\nConfirmar?');
    if (c == true){
      this._http.post(
        'insertQuizResposta.php',
        {
          per_id: this.pageParams.params.per_id,
          usr_id: this.userData[0].Usr_ID,
          option: op,
          time: this.questionTime
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade Merchandising - Quiz\r\n\r\nResposta enviada!');
          this._router.navigate(['/main/dashboard']);
        }else{
          window.alert('KSTrade Merchandising - Quiz\r\n\r\nProblema ao enviar sua resposta');
        }
      });
    }
  }
}
