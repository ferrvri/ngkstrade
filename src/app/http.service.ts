import { HttpClient, HttpHeaders } from "@angular/common/http";
import { HttpParamsOptions } from "@angular/common/http/src/params";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs/Observable";
import { environment } from "../environments/environment";

@Injectable()
export class HttpService {
    
    constructor(
        private _httpClient: HttpClient
    ) {}

    post (url, body, options?): Observable<any>{
        console.log(url, environment.apiUrl)
        return this._httpClient.post(
            environment.apiUrl + url,
            body,
            options
        );
    }

    get (url, options?): Observable<any>{
        return this._httpClient.get(
            environment.apiUrl + url,
            options
        );
    }    
}