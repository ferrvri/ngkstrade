import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AtendimentoEmergencialComponent } from './atendimento-emergencial.component';

describe('AtendimentoEmergencialComponent', () => {
  let component: AtendimentoEmergencialComponent;
  let fixture: ComponentFixture<AtendimentoEmergencialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AtendimentoEmergencialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AtendimentoEmergencialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
