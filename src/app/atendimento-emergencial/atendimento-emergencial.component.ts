import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

declare var showDialog: any;

@Component({
  selector: 'app-atendimento-emergencial',
  templateUrl: './atendimento-emergencial.component.html',
  styleUrls: ['./atendimento-emergencial.component.css']
})
export class AtendimentoEmergencialComponent implements OnInit {

  redes = []
  filiais = []
  
  p;
  atendimentos = []
  resultsFilterAtendimentos = []

  queryClientes: string = '';
  emptyResult: boolean = false;

  constructor( private _http: HttpService, ) { }

  ngOnInit() {
    this._http.post(
      'selectRedes.php',
      {
        all: true
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.redes = response.result;
      }
    });

    this._http.post(
      'selectAtendimentosE.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          element.menuRow = false;
        });
        this.atendimentos = this.resultsFilterAtendimentos = response.result;
      }
    });
  }

  openDetalhes(atendimento){
    atendimento.menuRow = false
    console.log(atendimento)
    showDialog({
      normalTitle: atendimento.aten_detalhe,
      type: 'normal',
      positive: true
    });
  }

  searchRede(string) {
    if (string.length < 1) {
      this.resultsFilterAtendimentos = this.atendimentos;
    } else {
      this.filterArrayRede(string, this.resultsFilterAtendimentos);
    }
  }

  filterArrayRede(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterAtendimentos = arr.filter((e) => {
      return regexp.test(e.RED_Nome);
    });
  }

  searchFilial(string) {
    if (string.length < 1) {
      this.resultsFilterAtendimentos = this.atendimentos;
    } else {
      this.filterArrayFilial(string, this.resultsFilterAtendimentos);
    }
  }


  filterArrayFilial(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterAtendimentos = arr.filter((e) => {
      return regexp.test(e.RFIL_Nome);
    });
  }

  solicitar(atendimento){
    // let c = confirm("KSTrade Merchandising\r\n\r\nDeseja enviar um e-mail para os administradores?");
    // if (c == true){
    //   this._http.post(
    //     'sendEmailAtendimento.php',
    //     {
    //       aten_id: atendimento.aten_id
    //     }
    //   ).subscribe( (response) => {
    //     if (response.status == '0x104'){
    //       window.alert('KSTrade Merchandising\r\n\r\nEmail enviado com sucesso!')
    //     }
    //   });
    // }

    this._http.post(
      'updateAtendimento.php',
      {
        aten_id: atendimento.aten_id
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        window.alert('KSTrade Merchandising\r\n\r\nAtendimento solicitado com sucesso!')
        this.resultsFilterAtendimentos = this.atendimentos = []
        this.ngOnInit()
      }
    });
  }
}
