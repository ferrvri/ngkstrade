import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
import { environment } from '../../environments/environment';
const swal: SweetAlert = _swal as any;

declare var showDialog: any;

@Component({
  selector: 'app-comentarios-jobs',
  templateUrl: './comentarios-jobs.component.html',
  styleUrls: ['./comentarios-jobs.component.css']
})
export class ComentariosJobsComponent implements OnInit {

  redes = []
  filiais = []
  images = []
  
  otrTitle = '';
  otrImage = '';

  p;
  comentarios = []
  resultsFilterComentarios = []
  userData: any;

  queryClientes: string = '';
  emptyResult: boolean = false;

  constructor( private _http: HttpService, ) { }

  ngOnInit() {
    this.userData = JSON.parse(localStorage.getItem('session'));
    this._http.post(
      'selectRedes.php',
      {
        all: true
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.redes = response.result;
      }
    });

    this._http.post(
      'selectComentariosJob.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.resultsFilterComentarios = this.comentarios = response.result;
      }
    });
  }

  openDetalhes(atendimento){
    atendimento.menuRow = false
    let div = document.createElement('div');
    div.innerHTML = `
      <h2 style="display: table; margin: 5px auto">${atendimento.nome_usuario}</h2>
      <h5 style="display: table; margin: 5px auto">${atendimento.comentario}</h5>

      <small>${atendimento.data_comentario}</small>
    `;

    swal({
      content: (div as any),
      buttons: ['OK']
    })
  }

  searchRede(string) {
    if (string.length < 1) {
      this.resultsFilterComentarios = this.comentarios;
    } else {
      this.filterArrayRede(string, this.resultsFilterComentarios);
    }
  }

  filterArrayRede(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterComentarios = arr.filter((e) => {
      return regexp.test(e.RED_Nome);
    });
  }

  searchFilial(string) {
    if (string.length < 1) {
      this.resultsFilterComentarios = this.comentarios;
    } else {
      this.filterArrayFilial(string, this.resultsFilterComentarios);
    }
  }


  filterArrayFilial(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterComentarios = arr.filter((e) => {
      return regexp.test(e.RFIL_Nome);
    });
  }

  openJOBImage(j) {
    this._http.post(
      'selectFotosExtras.php',
      {
        job_id: j.JOB_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.otrImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.otrTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;

        this.images = [];
        this.images.push(environment.apiUrl+'uploads/' + j.JOB_Foto);

        response.result.forEach(element => {
          this.images.push(environment.apiUrl+'uploads/' + element.JFOT_Foto);
        });

        if (
            (j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
            (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
            (j.JOB_Latitude === null && j.JOB_Longitude === null)
          ) {
          showDialog({
            title: this.otrTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.otrTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      } else {
        this.otrImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.otrTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;

        if ((j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
          (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
          (j.JOB_Latitude === null && j.JOB_Longitude === null)) {
          showDialog({
            title: this.otrTitle,
            image: this.otrImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.otrTitle,
            image: this.otrImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      }
    });
  }

}
