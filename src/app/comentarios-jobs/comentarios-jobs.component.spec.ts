import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ComentariosJobsComponent } from './comentarios-jobs.component';

describe('ComentariosJobsComponent', () => {
  let component: ComentariosJobsComponent;
  let fixture: ComponentFixture<ComentariosJobsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ComentariosJobsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ComentariosJobsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
