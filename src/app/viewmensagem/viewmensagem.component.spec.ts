import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewmensagemComponent } from './viewmensagem.component';

describe('ViewmensagemComponent', () => {
  let component: ViewmensagemComponent;
  let fixture: ComponentFixture<ViewmensagemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewmensagemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewmensagemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
