import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-viewmensagem',
  templateUrl: './viewmensagem.component.html',
  styleUrls: ['./viewmensagem.component.css']
})
export class ViewmensagemComponent implements OnInit {

  userData: any = [];
  pageParams: any;
  message:any = [];

  corpoMensagem: string = '';

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.msg.length > 0){
      setInterval( () => {
        if (this.pageParams.params.toPromo){
          this._http.post(
            'selectMensagens.php',
            {
              uniquePro: true,
              deid: this.pageParams.params.deid,
              paraid: this.pageParams.params.paraid
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              let r = [];
                response.result.forEach(element => {
                  r.push(element);
                });
                
                if (r.length > this.message.length){
                  this.message = r;
                
                  if (r[r.length -1].deId != this.userData[0].Usr_ID){
                    (<HTMLAudioElement>document.getElementById('message_tone_msg')).play();
                    document.getElementById('msgIcon').className =  "heartbit";
                  }
                }else{
                  document.getElementById('msgIcon').className =  "";
                }
              }
          });
        }else{
          this._http.post(
            'selectMensagens.php',
            {
              unique: true,
              deid: this.pageParams.params.deid,
              paraid: this.pageParams.params.paraid
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              let r = [];
              response.result.forEach(element => {
                r.push(element);
              });
              
              if (r.length > this.message.length){
                this.message = r;
              
                if (r[r.length -1].deId != this.userData[0].Usr_ID){
                  (<HTMLAudioElement>document.getElementById('message_tone_msg')).play();
                  document.getElementById('msgIcon').className =  "heartbit";
                }
              }
            }else if (response.status == '0x101'){
              // window.alert('Erro na seleção de mensagem');
            }
          });
        }
      }, 2000);
      setTimeout( () => {
        document.getElementById('message_content').scrollTop = document.getElementById('message_content').scrollHeight;
      }, 2250);
    }
  }

  deleteMsg(m):void{
    let c = confirm('KSTrade - Mensagens\r\n\r\nDeseja realmente excluir?');
    if (c == true){
      this._http.post(
        'deleteMensagem.php',
        {
          id: m
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade - Mensagens\r\n\r\nMensagem excluida com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/viewMensagem']);
          }, 1000);
        }else if (response.status == '0x101'){
          window.alert('Erro na deleção da mensagem');
        }
      });
    }
  }

  sendMensagem(){
    if (this.corpoMensagem.length > 0){
      if (this.pageParams.params.toPromo){
        this._http.post(
          'sendMensagem.php',
          {
            deid: this.pageParams.params.deid,
            paraid: this.pageParams.params.paraid,
            mensagem: this.corpoMensagem,
            status: 'Nao Lido',
            dataenvio: new Date().getUTCDate().toString() + '/' + (parseInt(new Date().getUTCMonth().toString().length == 1 ? '0'+new Date().getUTCMonth().toString(): new Date().getUTCMonth().toString()) + 1) + '/' + new Date().getUTCFullYear().toString(),
            timestamp: Date.now().valueOf().toString().substring(0,10)
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            this.message = [];
            this.ngOnInit();
            this.corpoMensagem = '';
          }else if (response.status == '0x101'){
            window.alert('Erro no envio da mensagem');
          }
        });
      }else{
        this._http.post(
          'sendMensagem.php',
          {
            deid: this.pageParams.params.paraid,
            paraid: this.pageParams.params.deid,
            mensagem: this.corpoMensagem,
            status: 'Nao Lido',
            dataenvio: new Date().getUTCDate().toString() + '/' + (parseInt(new Date().getUTCMonth().toString().length == 1 ? '0'+new Date().getUTCMonth().toString(): new Date().getUTCMonth().toString()) + 1) + '/' + new Date().getUTCFullYear().toString(),
            timestamp: Date.now().valueOf().toString().substring(0,10)
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            this.message = [];
            this.ngOnInit();
            this.corpoMensagem = '';
          }else if (response.status == '0x101'){
            window.alert('Erro no envio da mensagem');
          }
        });
      }
    }
  }

}
