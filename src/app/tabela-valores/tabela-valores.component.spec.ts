import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TabelaValoresComponent } from './tabela-valores.component';

describe('TabelaValoresComponent', () => {
  let component: TabelaValoresComponent;
  let fixture: ComponentFixture<TabelaValoresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TabelaValoresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TabelaValoresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
