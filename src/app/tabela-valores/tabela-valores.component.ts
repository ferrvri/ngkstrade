import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var html2canvas: any;
declare var $: any;

@Component({
  selector: 'app-tabela-valores',
  templateUrl: './tabela-valores.component.html',
  styleUrls: ['./tabela-valores.component.css']
})
export class TabelaValoresComponent implements OnInit {

  emptyResult: boolean = false;
  enableSelect: boolean = false;

  queryRedes: string = '';
  valoresJob: any = [];

  editall = false;

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    $(".maskMoney").maskMoney();
    let result = [];
    this._http.post(
      'selectClientes.php',
      {}
    ).subscribe((response1) => {
      if (response1.status == '0x104') {
        response1.result.forEach((e1) => {

          let o = {
            nome: e1.Cli_Nome,
            id: e1.Cli_ID,
            content: [],
            visible: true
          };

          this._http.post(
            'selectValoresTabela.php',
            {
              id: o.id
            }
          ).subscribe((response2) => {
            if (response2.status == '0x104') {
              response2.result.forEach(element => {
                element.edited = false
                o.content.push(element);
              });;
            } else if (response2.status == '0x101') {
              o.content = [];
            }
            this.valoresJob.push(o);
            this.resultsFilterRedes = this.valoresJob;
          });
        });
      } else if (response1.status == '0x101') {
        this.emptyResult = true;
      }
    });
  }

  openValoresJob(index, v, event): void {
    if (v.content.length > 0) {
      if (v.visible == true) {
        v.visible = false;
        document.getElementById('mainTdJob-' + index).style.borderLeft = 'none';
      } else {
        v.visible = true;
        document.getElementById('mainTdJob-' + index).style.borderLeft = '2px solid #940000';
      }
    }
  }

  deleteValor(content) {
    let c = confirm('KSTrade - Valores Job\r\n\r\nDeseja realmente excluir este valor?');
    if (c == true) {
      this._http.post(
        'deleteValor.php',
        {
          id: content.VJOB_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade - Valores Job\r\n\r\nValor excluido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaValorJob']);
          }, 1000);
        } else if (response.status == '0x101') {
          window.alert('Erro na deleção de valor');
        }
      });
    }
  }

  deleteSelected() {
    let verr = false;
    let c = confirm('KSTrade - Valores Job\r\n\r\nDeseja realmente excluir este valor?');
    if (c == true) {
      this.resultsFilterRedes.forEach((element, index) => {
        element.content.forEach((element2, Findex) => {
          if ((<HTMLInputElement>document.getElementById('valores-tbl-chk-' + index + '-' + Findex)).checked == true) {
            this._http.post(
              'deleteValor.php',
              {
                id: this.resultsFilterRedes[index].content[Findex].VJOB_ID
              }
            ).subscribe((response) => {
              if (response.status == '0x104') {
                verr = true;
              } else if (response.status == '0x101') {
                verr = false;
              }
            });
          }
        });
        if (index == this.resultsFilterRedes.length - 1) {
          window.alert('KSTrade - Valores Job\r\n\r\nValor excluido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaValorJob']);
          }, 1000);
        }
      })
    }
  }

  resultsFilterRedes: any = [];

  searchRede(string) {
    if (string.length < 1) {
      this.resultsFilterRedes = this.valoresJob;
    } else {
      this.filterRedes(this.valoresJob, this.queryRedes);
    }
  }

  filterRedes(arr, model) {
    this.resultsFilterRedes = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    arr.forEach(e1 => {

      let valores = {
        nome: e1.nome,
        id: e1.id,
        content: [],
        visible: true
      };

      e1.content.forEach(element => {
        if (regexp.test(element.RED_Nome)) {
          valores.content.push(element);
        }
      });

      this.resultsFilterRedes.push(valores);

      // console.log(this.resultsFilterFiliais);
    });
  }

  queryCliente: string = '';
  searchCliente(string) {
    if (string.length < 1) {
      this.resultsFilterRedes = this.valoresJob;
    } else {
      this.filterClientes(this.queryCliente, this.valoresJob);
    }
  }

  filterClientes(model, arr) {
    this.resultsFilterRedes = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    arr.forEach(e1 => {

      let valores = {
        nome: e1.nome,
        id: e1.id,
        content: e1.content,
        visible: true
      };


      if (regexp.test(valores.nome)) {
        this.resultsFilterRedes.push(valores);
      }

      // console.log(this.resultsFilterFiliais);
    });
  }

  makePDF() {
    document.getElementById('valoresTable').style.display = 'block';
    html2canvas(document.getElementById('valoresTable')).then((canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("", '_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
      document.getElementById('valoresTable').style.display = 'none';
    });
  }

  saveAll() {
    let verr = false;
    let c = confirm('KSTrade - Valores Job\r\n\r\nDeseja realmente salvar os valores?');
    if (c == true) {
      this.resultsFilterRedes.forEach((element, index) => {
        element.content.filter(e => e.edited == true).forEach((element2, Findex) => {
          console.log(element2);
          this._http.post(
            'insertValorJob.php',
            {
              type: 'update',
              valor: element2.VJOB_Valor,
              id: element2.VJOB_ID
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              if (Findex == element.content.filter(e => e.edited == true).length - 1) {
                window.alert('KSTrade - Valores Job\r\n\r\nValores editados com sucesso!');
                this._router.navigate(['/main/innerload']);
                setTimeout(() => {
                  this._router.navigate(['/main/listaValorJob']);
                }, 1000);
              }
            } else if (response.status == '0x101') {
              window.alert('Erro na inserção do valor');
            }
          });
        })
      })
    }
  }

  showEdit() {
    this.editall = !this.editall;
    setTimeout(() => { $(".maskMoney").maskMoney(); }, 1000);
  }
}