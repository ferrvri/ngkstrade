import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadServicoComponent } from './cad-servico.component';

describe('CadServicoComponent', () => {
  let component: CadServicoComponent;
  let fixture: ComponentFixture<CadServicoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadServicoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadServicoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
