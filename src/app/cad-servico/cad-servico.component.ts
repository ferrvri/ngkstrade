import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-cad-servico',
  templateUrl: './cad-servico.component.html',
  styleUrls: ['./cad-servico.component.css']
})
export class CadServicoComponent implements OnInit {

  pageParams:any; 

  servico: string = '';

  errorServico = {required: null};

  cadastroServico: FormGroup = new FormGroup({
    servico: new FormControl(this.servico, [Validators.required, Validators.minLength(6)])
  });

  constructor(private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if(this.pageParams.params.page == 'editar' && this.pageParams.params.id.length > 0){
      this._http.post(
        'selectServicos.php',
        {
          id: this.pageParams.params.id
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          this.servico = response.result[0].TIPS_Nome;
        }else if (response.status == '0x101'){
          window.alert('Erro ao selecionar o serviço!');
        }
      });
    }
  }

  validation():boolean{
    try{
      if (this.servico.length < 0){
        this.errorServico = {required: true}
      }else{
        this.errorServico = {required: null}
      }

      return true;
    }catch(e){
      console.log(e.toString());
    }
  }

  cadServico():void{
    if(this.pageParams.params.page == 'editar' && this.pageParams.params.id.length > 0){
      if (this.validation() && this.errorServico.required == null){
        this._http.post(
          'insertServico.php',
          {
            unique: true,
            id: this.pageParams.params.id,
            servico: this.servico
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Edição de Serviço\r\n\r\nServico editado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadServico']);
            }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro na edição do serviço!');
          }
        });
      }
    }else{
      if (this.validation() && this.errorServico.required == null){
        this._http.post(
          'insertServico.php',
          {
            servico: this.servico
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Cadastro de Serviço\r\n\r\nServiço cadastrado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadServico']);
            }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro na inserção do serviço!');
          }
        });
      }
    }
  }

}
