import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-lista-comentarios',
  templateUrl: './lista-comentarios.component.html',
  styleUrls: ['./lista-comentarios.component.css']
})
export class ListaComentariosComponent implements OnInit {

  p;
  comentarios = []
  pageParams;

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _router: Router, private _http: HttpService, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng);

    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data
    })
    this._http.post(
      'app/selectComentarios.php',
      {
        new_id: this.pageParams.params.app_id
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          element.menuRow = false;
        });
        this.comentarios = response.result;
      }
    });
  }

  deleteComentario(c) {
    let cc = confirm("KSTrade Merchandising \r\n\r\n Deseja realmente excluir o comentario?");
    if (cc == true) {
      this._http.post(
        'app/deleteComentario.php',
        {
          comentario_id: c.comentario_id,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Comentario[' + c.comentario_id + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising\r\n\r\nExcluido com sucesso!');
          this._router.navigate(['/main/listaNoticia']);
        }
      });
    }
  }
}
