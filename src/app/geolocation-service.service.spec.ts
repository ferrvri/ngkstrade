import { TestBed, inject } from '@angular/core/testing';

import { GeolocationServiceService } from './geolocation-service.service';

describe('GeolocationServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GeolocationServiceService]
    });
  });

  it('should be created', inject([GeolocationServiceService], (service: GeolocationServiceService) => {
    expect(service).toBeTruthy();
  }));
});
