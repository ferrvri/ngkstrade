import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';

declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-cad-conta',
  templateUrl: './cad-conta.component.html',
  styleUrls: ['./cad-conta.component.css']
})

export class CadContaComponent implements OnInit {

  pageParams: any;

  errorDescricao = {required: null};
  errorData = {required: null};
  errorValor = {required: null};
  

  descricao: string = "";
  datavencimento: string = "";
  valor: string = "";

  cadastroConta: FormGroup = new FormGroup({
    descricao: new FormControl(this.descricao, [Validators.required, Validators.minLength(6)]),
    datavencimento: new FormControl(this.datavencimento, [Validators.required, Validators.minLength(6)]),
    valor: new FormControl(this.valor, [Validators.required, Validators.minLength(6)])
  });

  constructor(private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    $('#inputDataCadConta').datepicker();
    $("#valorInputConta").maskMoney();
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar' && this.pageParams.params.id.length > 0){
      this._http.post(
        'selectContas.php',
        {
          kind: 'pagar',
          unique: true,
          id: this.pageParams.params.id
        }
      ).subscribe( (response) => {
        console.log(response);
        if(response.status == '0x104'){
          this.descricao = response.result[0].CONP_Descricao;
          this.datavencimento = response.result[0].conp_data_vencimento.substring(0,10).split('-')[2] + '/'+response.result[0].conp_data_vencimento.substring(0,10).split('-')[1]+'/'+response.result[0].conp_data_vencimento.substring(0,10).split('-')[0];
          this.valor = response.result[0].CONP_Valor;
        }else if(response.status == '0x101'){
          window.alert('Erro na seleção de conta');
        }
      });
    }
  } 

  validation(){
    try{

      if (this.descricao.length < 1){
        this.errorDescricao = {required: true};
      }else{
        this.errorDescricao = {required: null};
      }
      if ((<HTMLInputElement>document.getElementById('valorInputConta')).value.length < 1){
        this.errorValor = {required: true}
      }else{
        this.errorValor = {required: null};
      }
      if (this.datavencimento.length < 1){
        this.errorData = {required: true};
      }else{
        this.errorData = {required: null};
      }
      return true;
    }catch(e){
      console.log(e.toString());
    }
  }

  cadConta():void{
    if (this.pageParams.params.page == 'editar' && this.pageParams.params.id.length > 0){
      if(this.validation() && (this.errorValor.required == null && this.errorDescricao.required == null &&
        this.errorData.required == null)){
          this._http.post(
            'insertConta.php', 
            {
              type: 'update',
              id: this.pageParams.params.id,
              descricao: this.descricao,
              dataVencimento: this.datavencimento,
              valor: (<HTMLInputElement>document.getElementById('valorInputConta')).value
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              window.alert('KSTrade - Contas a pagar\r\n\r\nConta atualizada com sucesso!');
              this._router.navigate(['/main/innerload']);
                setTimeout(() => {
                  this._router.navigate(['/main/listaContas', {page: 'pagar'}]);
                }, 1000);
            }else if (response.status == '0x101'){
              window.alert('Erro de inserção ao banco');
            }
          })
        }
    }else{
      if(this.validation() && (this.errorValor.required == null && this.errorDescricao.required == null &&
      this.errorData.required == null)){
        this._http.post(
          'insertConta.php', 
          {
            descricao: this.descricao,
            dataVencimento: this.datavencimento,
            valor: (<HTMLInputElement>document.getElementById('valorInputConta')).value
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Contas a pagar\r\n\r\nConta cadastrada com sucesso!');
            this._router.navigate(['/main/innerload']);
              setTimeout(() => {
                this._router.navigate(['/main/cadConta']);
              }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro de inserção ao banco');
          }
        })
      }
    }
  }

  setData(){
    if ((<HTMLInputElement>document.getElementById('inputDataCadConta')).value === undefined || (<HTMLInputElement>document.getElementById('inputDataCadConta')).value == ''){
      (<HTMLInputElement>document.getElementById('inputDataCadConta')).value = '';
    }else{
      this.datavencimento = (<HTMLInputElement>document.getElementById('inputDataCadConta')).value.split('/')[1] +'/' + (<HTMLInputElement>document.getElementById('inputDataCadConta')).value.split('/')[0] +'/' + (<HTMLInputElement>document.getElementById('inputDataCadConta')).value.split('/')[2];
    }   
  }

}
