import { Component, OnInit } from '@angular/core';
import { RedesComponent } from '../cliente/redes/redes.component';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-cad-rotina',
  templateUrl: './cad-rotina.component.html',
  styleUrls: ['./cad-rotina.component.css']
})
export class CadRotinaComponent implements OnInit {

  clientes = []
  cliente: string = '';
  clienteIndex: number = 0;
  clienteOK: boolean = false;

  redes = []
  rede: string = '';
  redeIndex: number = 0;
  redeOK: boolean = false;

  filiais = []
  filial: string = '';
  filialIndex: number = 0;
  filialOK: boolean = false;

  promotorIndex: number = 0;
  promotor: string = '';
  promotores: any = []
  promotorOK: boolean = false;

  errorCliente = { required: null };
  errorRede = { required: null };
  errorFilial = { required: null };
  errorPromotor = { required: null };

  pageParams: any;

  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _router: Router, private _activatedRoute: ActivatedRoute, private _http: HttpService,) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar') {
      this._http.post(
        'selectAllOfRotina.php',
        {
          rede: this.pageParams.params.redeid,
          filial: this.pageParams.params.filialid,
          clienteid: this.pageParams.params.clienteid,
          promotor: this.pageParams.params.promotor
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.cliente = response.result[0].Cli_Nome;
          this.clienteIndex = response.result[0].Cli_ID;
          this.rede = response.result[0].RED_Nome;
          this.redeIndex = response.result[0].RED_ID;
          this.filial = response.result[0].RFIL_Nome;
          this.filialIndex = response.result[0].RFIL_ID;
          this.promotor = response.result[0].PRO_Nome;
          this.promotorIndex = response.result[0].PRO_ID;

          let days = response.result[0].aux_semana.split(';');
          this.days.forEach((element, index) => {
            days.forEach(d => {
              if (d == element.name) {
                element.activated = true;
                this.rotina.push(element.name);
              }
            });
          });

          this._http.post(
            'selectFilial.php',
            {
              id: this.redeIndex
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              response.result.forEach(element => {
                this.filiais.push(element);
              });
              this.resultFilterFiliais = this.filiais;

              this._http.post(
                'selectPromotorOfRede.php',
                {
                  rede: this.redeIndex,
                  filial: this.filialIndex
                }
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  this.promotores = [];
                  response.result.forEach(element => {
                    this.promotores.push(element);
                  });
                  this.resultFilterPromotores = this.promotores;
                }
              });
            }
          });
        }
      });
    }

    this._http.post(
      'selectRedes.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.redes.push(element)
        });
        this.resultFilterRedes = this.redes;
      }
    });
  }

  resultFilterClientes: any = [];

  filterClientes(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterClientes = this.clientes;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterClientes = arr.filter((e) => {
        return regexp.test(e.Cli_Nome.split('')[0]);
      });
    }
  }

  resultFilterRedes: any = [];

  filterRedes(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterRedes = this.redes;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterRedes = arr.filter((e) => {
        return regexp.test(e.RED_Nome.split('')[0]);
      });
    }
  }

  resultFilterFiliais: any = [];

  filterFilial(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterFiliais = this.filiais;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterFiliais = arr.filter((e) => {
        return regexp.test(e.RFIL_Nome.split('')[0]);
      });
    }
  }

  resultFilterPromotores: any = [];

  filterPromotores(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterPromotores = this.promotores;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterPromotores = arr.filter((e) => {
        return regexp.test(e.PRO_Nome.split('')[0]);
      });
    }
  }

  rotina: any = [];
  days: any = [
    { name: 'Seg', activated: false },
    { name: 'Ter', activated: false },
    { name: 'Qua', activated: false },
    { name: 'Qui', activated: false },
    { name: 'Sex', activated: false },
    { name: 'Sab', activated: false }
  ];

  selectThisDay(day) {
    if (day.activated == true) {
      day.activated = false;
      this.rotina.forEach((element, index) => {
        if (day.name == element) {
          this.rotina.splice(index, 1);
        }
      });
    } else {
      day.activated = true;
      this.rotina.push(day.name);
    }
    console.log(this.rotina)
  }

  setCliente(c) {
    this.cliente = c.Cli_Nome;
    this.clienteIndex = c.Cli_ID;
    this.clienteOK = false;

    this._http.post(
      'selectPromotorOfRede.php',
      {
        rede: this.redeIndex,
        filial: this.filialIndex
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.promotores = [];
        response.result.forEach(element => {
          this.promotores.push(element);
        });
        this.resultFilterPromotores = this.promotores;
      } else {
        this.resultFilterPromotores = this.promotores = [];
      }
    });
  }

  setRede(r) {
    this.rede = r.RED_Nome;
    this.redeIndex = r.RED_ID;
    this.redeOK = false;

    this._http.post(
      'selectFilial.php',
      {
        id: this.redeIndex
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.filiais = [];
        response.result.forEach(element => {
          this.filiais.push(element);
        });
        this.resultFilterFiliais = this.filiais;
      } else {
        this.resultFilterFiliais = this.filiais = [];
      }
    });
  }

  setFilial(r) {
    this.filial = r.RFIL_Nome;
    this.filialIndex = r.RFIL_ID;
    this.filialOK = false;

    this._http.post(
      'selectClientes.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.clientes = [];
        response.result.forEach(element => {
          this._http.post(
            'selectClientesOfRotina.php',
            {
              rede: this.redeIndex,
              filial: this.filialIndex,
              clienteid: element.Cli_ID
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              response.result.forEach(element2 => {
                this.clientes.push(element2)
              });
              this.resultFilterClientes = this.clientes;
            }
          });
        });
      }
    });
  }

  setPromotor(p) {
    this.promotor = p.PRO_Nome;
    this.promotorIndex = p.PRO_ID;
    this.promotorOK = false;
  }

  validation() {
    try {
      if (this.rede.length < 1) {
        this.errorRede = { required: true };
      } else {
        this.errorRede = { required: null };
      }
      if (this.filial.length < 1) {
        this.errorFilial = { required: true };
      } else {
        this.errorFilial = { required: null };
      }
      if (this.promotor.length < 1) {
        this.errorPromotor = { required: true };
      } else {
        this.errorPromotor = { required: null };
      }
      return true;
    } catch (e) {
      console.log(e.toString());
    }
  }

  cadRotina() {
    if (this.pageParams.params.page == 'editar') {
      if (this.validation() == true && this.errorRede.required == null && this.errorPromotor.required == null &&
        this.errorFilial.required == null && this.errorCliente.required == null) {
        let dias = '';
        this.rotina.forEach(element => {
          dias += element + ';'
        });
        this._http.post(
          'insertRotina.php',
          {
            type: 'update',
            pro_id: this.pageParams.params.pro_id,
            dias: dias,
            cliente: this.clienteIndex,
            rede: this.redeIndex,
            filial: this.filialIndex,
            promotor: this.promotorIndex,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Rotina[' + this.pageParams.params.pro_id + ' / ' +this.clienteIndex + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade Merchandising\r\n\r\nRotina editada com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/listaRotina']);
            }, 1500);
          }
        });
      }
    } else {
      if (this.validation() == true && this.errorRede.required == null && this.errorPromotor.required == null &&
        this.errorFilial.required == null && this.errorCliente.required == null) {
        let dias = '';
        this.rotina.forEach(element => {
          dias += element + ';'
        });

        this._http.post(
          'insertRotina.php',
          {
            dias: dias,
            cliente: this.clienteIndex,
            rede: this.redeIndex,
            filial: this.filialIndex,
            promotor: this.promotorIndex,
            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Inserção - Rotina[' + this.promotorIndex + ' / ' +this.clienteIndex + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade Merchandising\r\n\r\nRotina cadastrada com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadRotina']);
            }, 1500);
          } else if (response.status == '0x102') {
            window.alert('KSTrade Merchandising\r\n\r\nRotina já cadastrada a esse promotor!');
          }
        });
      }
    }
  }


}
