import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadRotinaComponent } from './cad-rotina.component';

describe('CadRotinaComponent', () => {
  let component: CadRotinaComponent;
  let fixture: ComponentFixture<CadRotinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadRotinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadRotinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
