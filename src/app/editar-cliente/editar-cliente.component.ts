import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ReactiveFormsModule, FormControl, FormControlName, FormGroup, Validators } from '@angular/forms';
import { HttpService } from '../http.service';
import { GeolocationServiceService } from '../geolocation-service.service';
import { environment } from '../../environments/environment';

declare var $: any;
declare var jQuery: any;


@Component({
  selector: 'app-editar-cliente',
  templateUrl: './editar-cliente.component.html',
  styleUrls: ['./editar-cliente.component.css']
})
export class EditarClienteComponent implements OnInit {

  cliente: any = [];
  toUploadImage = [];
  anexo = '';
  errorAnexo = { required: null }

  coordIndex: number = 0;
  coordenadorOK: boolean = false;
  fotoUrl = '';
  coordenadores = [];
  estados = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

  errorNotExistsCep: boolean = false;
  errorNomeFantasia = { required: null };
  errorRazaoSocial = { required: null };
  errorCnpj = { required: null };
  errorTelefoneFixo = { required: null };
  errorTelefoneMovel = { required: null };
  errorEmail = { required: null };
  errorCoordenador = { required: null };
  errorCep = { required: null };
  errorEndereco = { required: null };
  errorCidade = { required: null };
  errorBairro = { required: null };
  errorEstado = { required: null };
  errorNumero = { required: null };

  nomeFantasia: string = "";
  razaoSocial: string = "";
  cnpj: string = "";
  telefoneFixo: string = "";
  telefoneMovel: string = "";
  email: string = "";
  coordenador: string = "Selecione";
  cep: string = "";
  endereco: string = "";
  numero: string = "";
  cidade: string = "";
  bairro: string = "";
  estado: string = "Selecione";

  editarCliente: FormGroup = new FormGroup({
    nomeFantasia: new FormControl(this.nomeFantasia, [Validators.required, Validators.minLength(6)]),
    razaoSocial: new FormControl(this.razaoSocial, [Validators.required, Validators.minLength(6)]),
    cnpj: new FormControl(this.cnpj, [Validators.required, Validators.minLength(6)]),
    telefoneFixo: new FormControl(this.telefoneFixo, [Validators.required, Validators.minLength(6)]),
    telefoneMovel: new FormControl(this.telefoneMovel, [Validators.required, Validators.minLength(6)]),
    email: new FormControl(this.email, [Validators.required, Validators.minLength(6)]),
    coordenador: new FormControl(this.coordenador, [Validators.required, Validators.minLength(6)]),
    cep: new FormControl(this.cep, [Validators.required, Validators.minLength(6)]),
    endereco: new FormControl(this.endereco, [Validators.required, Validators.minLength(6)]),
    numero: new FormControl(this.numero, [Validators.required, Validators.minLength(6)]),
    cidade: new FormControl(this.cidade, [Validators.required, Validators.minLength(6)]),
    bairro: new FormControl(this.bairro, [Validators.required, Validators.minLength(6)]),
    estado: new FormControl(this.estado, [Validators.required, Validators.minLength(6)])
  });

  latLong = '';


  constructor(private _geo: GeolocationServiceService, private _activedRoute: ActivatedRoute, private _http: HttpService, private _router: Router) { }

  removeFiles() {
    $(".dropify-clear").click();
    this.toUploadImage = [];
  }

  uploadChanges(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      //this.nameOfFile = event.target.files[0].name;
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.toUploadImage.push(reader.result.toString().split(',')[1]);
      };
    }
  }

  nameOfFile: string = '';

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    jQuery('.mydatepicker, #datepicker').datepicker();

    $('.dropify').dropify();

    // Used events
    var drEvent = $('#input-file-events').dropify();

    drEvent.on('dropify.beforeClear', function (event, element) {
      return confirm("Você deseja realmente apagar \"" + element.file.name + "\" ?");
    });

    drEvent.on('dropify.afterClear', function (event, element) {
      alert('Arquivo apagado');
    });

    drEvent.on('dropify.errors', function (event, element) {
      console.log('Erro!');
    });

    var drDestroy = $('#input-file-to-destroy').dropify();
    drDestroy = drDestroy.data('dropify');
    $('#toggleDropify').on('click', function (e) {
      e.preventDefault();
      if (drDestroy.isDropified()) {
        drDestroy.destroy();
      } else {
        drDestroy.init();
      }
    })

    $('#inputCnpjEdit').mask('00.000.000/0000-00');
    $('#inputTelefoneFixoEdit').mask('(00) 0000-0000');
    $('#inputTelefoneMovelEdit').mask('(00) 0 0000-0000');
    $('#inputCEPEdit').mask('00000-000');
    if (this.getParams()) {
      this._http.post(
        'selectCoordenadores.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          response.result.forEach(element => {
            this.coordenadores.push(element);
          });

          this._http.post(
            'selectClientes.php',
            {
              unique: true,
              id: this.cliente[0].params.id
            }
          ).subscribe((response) => {
            this.nomeFantasia = response.result[0].Cli_Nome;
            this.razaoSocial = response.result[0].Cli_RazaoSocial;
            this.telefoneFixo = response.result[0].Cli_TelefoneFixo;
            this.telefoneMovel = response.result[0].Cli_TelefoneMovel;
            this.email = response.result[0].Cli_Email;
            this.cep = response.result[0].Cli_CEP;
            this.endereco = response.result[0].Cli_Endereco;
            this.numero = response.result[0].Cli_Numero;
            this.cidade = response.result[0].Cli_Cidade;
            this.estado = response.result[0].Cli_Estado;
            this.cnpj = response.result[0].Cli_CNPJ;
            this.bairro = response.result[0].Cli_Bairro;
            if (response.result[0].Cli_Logo != null && response.result[0].Cli_Logo.length > 0) {
              this.fotoUrl = environment.apiUrl+'uploads/logos/' + response.result[0].Cli_Logo;
            }

            this.coordenadores.forEach((e) => {
              if (e.Usr_ID == response.result[0].Cli_Coordenador_ID) {
                this.coordenador = e.Usr_Nome;
                this.coordIndex = e.Usr_ID;
              }
            });
          });
        } else {
          window.alert('Não foi possível buscar os coordenadores disponiveis');
        }
      });
    }
  }

  getParams(): any {
    this._activedRoute.paramMap.subscribe((data) => {
      this.cliente.push(data);
    });

    if (this.cliente.length < 1) {
      return false;
    } else {
      return true;
    }
  }

  validation() {
    try {

      if (this.razaoSocial.length < 1) {
        this.errorRazaoSocial = { required: true };
      } else {
        this.errorRazaoSocial = { required: null };
      }
      if (this.nomeFantasia.length < 1) {
        this.errorNomeFantasia = { required: true }
      } else {
        this.errorNomeFantasia = { required: null };
      }
      if (this.cnpj.length < 1) {
        this.errorCnpj = { required: true };
      } else {
        this.errorCnpj = { required: null };
      }
      if (this.telefoneFixo.length < 1) {
        this.errorTelefoneFixo = { required: true }
      } else {
        this.errorTelefoneFixo = { required: null };
      }
      if (this.telefoneMovel.length < 1) {
        this.errorTelefoneMovel = { required: true };
      } else {
        this.errorTelefoneMovel = { required: null };
      }
      if (this.email.length < 1) {
        this.errorEmail = { required: true }
      } else {
        this.errorEmail = { required: null };
      }
      if (this.coordenador.length < 1) {
        this.errorCoordenador = { required: true };
      } else {
        this.errorCoordenador = { required: null };
      }
      if (this.cep.length < 1) {
        this.errorCep = { required: true };
      } else {
        this.errorCep = { required: null };
      }
      if (this.endereco.length < 1) {
        this.errorEndereco = { required: true };
      } else {
        this.errorEndereco = { required: null };
      }
      if (this.numero.length < 1) {
        this.errorNumero = { required: true }
      } else {
        this.errorNumero = { required: null };
      }
      if (this.cidade.length < 1) {
        this.errorCidade = { required: true };
      } else {
        this.errorCidade = { required: null };
      }
      if (this.bairro.length < 1) {
        this.errorBairro = { required: true }
      } else {
        this.errorBairro = { required: null };
      }
      if (this.estado.length < 1) {
        this.errorEstado = { required: true };
      } else {
        this.errorEstado = { required: null };
      }
      return true;
    } catch (e) {
      console.log(e.toString());
    }
  }

  editCliente(): void {
    let confirmation = window.confirm("KSTrade - Editar Clientes\r\n\r\nDeseja realmente editar?");
    if (confirmation == true) {
      if (this.validation() && (this.errorBairro.required == null && this.errorCep.required == null &&
        this.errorCidade.required == null && this.errorCnpj.required == null && this.errorCoordenador.required == null &&
        this.errorEmail.required == null && this.errorEndereco.required == null && this.errorEstado.required == null &&
        this.errorNomeFantasia.required == null && this.errorNumero.required == null && this.errorRazaoSocial.required == null &&
        this.errorTelefoneFixo.required == null)) {

        this.nameOfFile = Math.ceil(Math.random() * 1000) + this.nomeFantasia + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg'
        let nameOfFile = this.nameOfFile.trim();

        this._http.post(
          'editCliente.php',
          {
            Cli_ID: this.cliente[0].params.id,
            nomeFantasia: this.nomeFantasia,
            razaoSocial: this.razaoSocial,
            telefoneFixo: this.telefoneFixo,
            telefoneMovel: this.telefoneMovel,
            email: this.email,
            cep: this.cep,
            endereco: this.endereco,
            numero: this.numero,
            cidade: this.cidade,
            estado: this.estado,
            cnpj: this.cnpj,
            bairro: this.bairro,
            coordenador: this.coordIndex,
            title: nameOfFile,
            anexo: this.toUploadImage[0],
            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Cliente[' + this.cliente[0].params.id + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('Cliente editado com sucesso!');
            this._router.navigate(['/main/listaClientes']);
          } else if (response.status == '0x101') {
            window.alert('Erro de conexao ao banco');
          }
        });
      }
    }
  }

  checkCep(cep: string): void {
    if (cep.length < 1) {
      this.errorCep = { required: true };
    } else {
      this._http.post(
        'https://viacep.com.br/ws/' + cep.replace('-', '').replace('.', '') + '/json',
        {}
      ).subscribe((response) => {
        if (response.localidade) {
          this.estado = response.uf;
          this.bairro = response.bairro;
          this.cidade = response.localidade;
          this.endereco = response.logradouro;
          this.errorNotExistsCep = false;
        } else {
          this.errorNotExistsCep = true;
          this.estado = ""
          this.bairro = ""
          this.cidade = ""
          this.endereco = ""
        }
      });
    }
  }

  setEstado(e): void {
    this.estado = e;
  }

  setCoordenador(c: number): void {
    this.coordIndex = this.coordenadores[c].Usr_ID;
    this.coordenador = this.coordenadores[c].Usr_Nome;
    this.coordenadorOK = false;
  }
}
