import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../http.service';
import { GeolocationServiceService } from '../geolocation-service.service';
import { ExcelService } from '../excel.service';

declare var showDialog: any;
declare var html2canvas: any;
declare var $: any;

@Component({
  selector: 'app-lista-jobs-pendentes',
  templateUrl: './lista-jobs-pendentes.component.html',
  styleUrls: ['./lista-jobs-pendentes.component.css']
})
export class ListaJobsPendentesComponent implements OnInit {

  contents: any = [];
  resultsFilterContents: any = [];

  clientes = [];
  filiais = [];
  redes = [];
  promotores = [];

  emptyResult: boolean = false;
  doneLoading: boolean = true;
  cli_selected = false;
  pro_selected = false;

  jobImage: string = '';
  jobTitle: string = '';

  userData: any;

  meses = [
    {
      mes: 'Janeiro',
      numero: 1
    },
    {
      mes: 'Fevereiro',
      numero: 2
    },
    {
      mes: 'Março',
      numero: 3
    },
    {
      mes: 'Abril',
      numero: 4
    },
    {
      mes: 'Maio',
      numero: 5
    },
    {
      mes: 'Junho',
      numero: 6
    },
    {
      mes: 'Julho',
      numero: 7
    },
    {
      mes: 'Agosto',
      numero: 8
    },
    {
      mes: 'Setembro',
      numero: 9
    },
    {
      mes: 'Outubro',
      numero: 10
    },
    {
      mes: 'Novembro',
      numero: 11
    },
    {
      mes: 'Dezembro',
      numero: 12
    }
  ]

  anos = []

  clienteIndex;

  valorTotalPromotor = 0;

  latLong = '';

  constructor(private _excel: ExcelService, private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._geo.getPosition( c => this.latLong = c.lat+';'+c.lng);
    
    let ano = 2017;
    this.anos.push(ano)
    for (let i = 1; i <= 50; i++) {
      this.anos.push(ano+i)
    }

    this.userData = JSON.parse(localStorage.getItem('session'));

    if (this.userData[0].Usr_Nivel == 'Coordenador') {
      this._http.post(
        'selectClientes.php',
        {
          coordenador: true,
          coordId: this.userData[0].Usr_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this._http.post('selectRedes.php', {})
            .subscribe(r => { this.redes = r.result; });

          this.clientes = response.result;
        }
      });
    } else {
      this._http.post(
        'selectClientes.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.clientes = response.result;
          this._http.post('selectRedes.php', {})
            .subscribe(r => {
              this.redes = r.result;
              this._http.post('selectPromotor.php', {
                group: true
              }).subscribe(r => {
                this.promotores = r.result;
              });
            });
        }
      });
    }
  }

  getSemanasMes() {
    let results = [];
    let _first_day = new Date(new Date().getFullYear(), new Date().getMonth(), 1);

    for (let i = 0; i <= 4; i++) {
      let lim = {
        init: '',
        end: ''
      }

      switch (_first_day.getDay()) {
        case 0: {
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 1))
          break;
        }
        case 1: {
          lim.init = _first_day.toISOString().split('T')[0];
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 5))
          lim.end = _first_day.toISOString().split('T')[0]
          break;
        }
        case 2: {
          lim.init = _first_day.toISOString().split('T')[0];
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 4))
          lim.end = _first_day.toISOString().split('T')[0]
          break;
        }

        case 3: {
          lim.init = _first_day.toISOString().split('T')[0];
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 3))
          lim.end = _first_day.toISOString().split('T')[0]
          break;
        }

        case 4: {
          lim.init = _first_day.toISOString().split('T')[0];
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 2))
          lim.end = _first_day.toISOString().split('T')[0]
          break;
        }

        case 5: {
          lim.init = _first_day.toISOString().split('T')[0];
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 1))
          lim.end = _first_day.toISOString().split('T')[0]
          break;
        }

        case 6: {
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 2))
          lim.init = _first_day.toISOString().split('T')[0];
          _first_day = new Date(_first_day.setDate(_first_day.getDate() + 5))
          lim.end = _first_day.toISOString().split('T')[0];
          break;
        }
      }

      results.push(lim);
    }
    return results;
  }

  openImagemJob(j) {
    this.jobImage = 'uploads/' + j.JOB_Foto;
    this.jobTitle = 'Cliente: ' + j.Cli_Nome + ' - Data / Hora: ' + j.JOB_Data_Hora_Input2 + '- Observação: ' + j.JOB_Obs;

    showDialog({
      title: this.jobTitle,
      image: this.jobImage,
      close: {
        title: 'Fechar',
        onClick: null
      }
    });
  }

  filterArrayFiliais(model, arr, cidade?) {
    this.doneLoading = false;
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    var regexpcid = new RegExp((".*" + cidade.split("").join('.*') + ".*"), "i");
    let r = [];
    if (arr.length >= 1) {
      arr.forEach(arrEl => {
        let o = {
          cli_id: arrEl.cli_id,
          cli_nome: arrEl.cli_nome,
          content: []
        }

        if (arrEl.content && arrEl.content.length > 0) {
          arrEl.content.forEach(element => {
            if (regexp.test(element.RFIL_Nome) && regexpcid.test(element.RFIL_Cidade)) {
              o.content.push(element);
            }
          });
          if (o.content.length > 0) {
            r.push(o);
          }
        }
      });
    }
    setTimeout(() => {
      this.resultsFilterContents = r;
      this.doneLoading = true;
    }, 1500);
  }

  filial_selected = false
  searchFiliais(string, cidade?) {
    this.filial_selected = true
    if (string.length < 1) {
      // this.pdfReady = false;
      this.resultsFilterContents = this.contents;
      this.filterArrayFiliais(string, this.resultsFilterContents, cidade);
    } else {
      if (this.resultsFilterContents.length != this.contents.length) {
        this.resultsFilterContents = this.contents;
      }

      this.filterArrayFiliais(string, this.resultsFilterContents, cidade);
      // this.pdfReady = true;
    }
  }

  rede_selected = false
  searchRede(string, id?) {
    this.rede_selected = true
    if (string.length < 1) {
      // this.pdfReady = false;
      this.resultsFilterContents = this.contents;
      this.filterArrayRedes(string, this.resultsFilterContents, id);
    } else {
      this.resultsFilterContents = this.contents;

      this.filterArrayRedes(string, this.resultsFilterContents, id);
      // this.pdfReady = true;

      this._http.post(
        'selectFilialInRede.php',
        {
          nomeRede: string
        }
      ).subscribe((response3) => {
        if (response3.status == '0x104') {
          this.filiais = response3.result;
        }
      });
    }
  }

  filterArrayRedes(model, arr, id?) {
    this.doneLoading = false;
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    let r = [];
    if (arr.length >= 1) {
      arr.forEach(arrEl => {
        let o = {
          cli_id: arrEl.cli_id,
          cli_nome: arrEl.cli_nome,
          content: []
        }

        if (arrEl.content && arrEl.content.length > 0) {
          arrEl.content.forEach(element => {
            if (regexp.test(element.RED_Nome)) {
              o.content.push(element);
            }
          });
          if (o.content.length > 0) {
            r.push(o);
          }
        }
      });
    }
    setTimeout(() => {
      this._http.post('selectFilial.php', { type: 'pendente', id: id })
        .subscribe(r => { this.filiais = r.result; });

      this.resultsFilterContents = r;
      this.doneLoading = true;
    }, 1500);
  }

  filterArrayPromotores(model, arr) {
    this.doneLoading = false;
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    let r = [];
    if (arr.length >= 1) {
      arr.forEach(arrEl => {
        let o = {
          cli_id: arrEl.cli_id,
          cli_nome: arrEl.cli_nome,
          content: []
        }

        if (arrEl.content && arrEl.content.length > 0) {
          
          arrEl.content.forEach(element => {
            if (regexp.test(element.PRO_Nome)) {
              o.content.push(element);
            }
          });

          if (o.content.length > 0) {
            r.push(o);
          }

        }
      });
    }
    setTimeout(() => {
      this.resultsFilterContents = r;
      this.doneLoading = true;
    }, 1500);
  }

  searchPromotores(string) {
    if (string.length < 1) {
      // this.pdfReady = false;
      this.filterArrayPromotores(string, this.contents);
      this.resultsFilterContents = this.contents;
    } else {
      if (this.resultsFilterContents.length != this.contents.length) {
        this.resultsFilterContents = this.contents;
      }

      this.filterArrayPromotores(string, this.contents);
      // this.pdfReady = true;
    }
  }

  searchCliente(string, c) {
    if (c.Cli_ID == -1 || c.Cli_ID == '-1') {
      this.contents = [];
      this.resultsFilterContents = [];
      this.clienteIndex = 0;
      this.cli_selected = false;
    } else {
      this.contents = [];

      this._http.post(
        'selectProdutividade2.php',
        {
          cli_id: parseInt(c.Cli_ID),
          mes: new Date().getMonth() + 1,
          ano: new Date().getFullYear()
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          this.contents.push(response2.result);
          this.resultsFilterContents = this.contents;
          this.emptyResult = false;
          this.cli_selected = true;
          this.clienteIndex = parseInt(c.Cli_ID);

          this.resultsFilterContents[0].content.sort( (a, b) => {
            return a.RFIL_Nome > b.RFIL_Nome
          });

          this._http.post(
            'selectRedesInCliente.php',
            {
              clienteNome: c.Cli_Nome
            }
          ).subscribe((response2) => {
            if (response2.status == '0x104') {
              this.redes = response2.result

              this._http.post(
                'selectPromotor2.php',
                {
                  group: true
                }
              ).subscribe((response3) => {
                if (response3.status == '0x104') {
                  this.promotores = response3.result
                }
              });
            }
          });
        }
      });
    }
  }

  filterArrayClientes(model, arr) {
    this.doneLoading = false;
    var regexp = new RegExp((".*" + model + ".*"), "i");
    let r = [];
    arr.forEach(e1 => {
      if (regexp.test(e1.cli_nome)) {
        r.push(e1);
      }
    });

    setTimeout(() => {
      this.doneLoading = true;
      this.resultsFilterContents = r;
    }, 1500);
  }

  searchPromotorCliente(string, obj){
    if (string.length < 1) {
      // this.pdfReady = false;
      this.filterArrayPromotores(string, this.contents);
      this.resultsFilterContents = this.contents;
    } else {
      if (this.resultsFilterContents.length != this.contents.length) {
        this.resultsFilterContents = this.contents;
      }

      this.filterArrayPromotores(string, this.contents);
    }
  }

  searchPromotorMensal(){
    let promotor = (document.getElementById('promotor_select') as HTMLInputElement).value
    let value = (document.getElementById('mes_select') as HTMLInputElement).value
    let ano = parseInt((document.getElementById('ano_select') as HTMLInputElement).value)

    this.contents = [];
    this.valorTotalPromotor = 0;
    this.resultsFilterContents = [];

    if (promotor.split(':')[0] == '-1') {
      this.pro_selected = false;
      this.cli_selected = false;
    } else {
      this._http.post(
        'selectProdutividade2.php',
        {
          pro_nome: promotor.split(':')[1],
          mes: value,
          ano: ano == 0 ? new Date().getFullYear(): ano
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          this.contents.push(response2.result);
          this.resultsFilterContents = this.contents;
          this.emptyResult = false;
          this.pro_selected = true;

          this.resultsFilterContents.forEach(element => {
            element.content.forEach(element2 => {
              this.valorTotalPromotor += parseFloat(element2.valorTotal);
            });
          });

          this.resultsFilterContents.forEach(element => {
            element.content = element.content.sort( (a, b) => {
              return a.RFIL_Nome > b.RFIL_Nome
            })
          });
        }
      });
    }
  }

  updateRow(cli_id, obj, vis?, type?) {

    switch (vis) {
      case 1: {
        if (type !== undefined && type == 'down'){
          obj.visita1--;
        }else{
          obj.visita1++;
        }

        break;
      }
      case 2: {
         if (type !== undefined && type == 'down'){
          obj.visita2--;
        }else{
          obj.visita2++;
        }
        break;
      }
      case 3: {
        if (type !== undefined && type == 'down'){
          obj.visita3--;
        }else{
          obj.visita3++;
        }
        break;
      }
      case 4: {
        if (type !== undefined && type == 'down'){
          obj.visita4--;
        }else{
          obj.visita4++;
        }
        break;
      }
    }

    this._http.post(
      'insertResultadoRotina.php',
      {
        cliente: cli_id,
        rede: obj.RED_ID,
        filial: obj.RFIL_ID,
        promotor: obj.PRO_ID,
        mes: new Date().getMonth() + 1,
        v1: obj.visita1,
        v2: obj.visita2,
        v3: obj.visita3,
        v4: obj.visita4,

        //log
        usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
        usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
        usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
        nome_pc: this._geo.GetComputerName(),
        latlong: this.latLong,
        funcao: (type === undefined ? 'Aumento de produtividade' : 'Diminuição de produtividade') +' - Promotor[' + obj.PRO_Nome + ' / '+ obj.PRO_ID + ']'
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        console.log('ok');
      }
    });
  }

  searchProdutividade(mes) {
    console.log('ok')
    let ano = parseInt((document.getElementById('anoSelect') as HTMLInputElement).value)
    this.doneLoading = false;
    if (mes == 0) {
      this._http.post(
        'selectProdutividade2.php',
        {
          cli_id: this.clienteIndex,
          mes: new Date().getMonth() + 1,
          ano: ano == 0 ? new Date().getFullYear(): ano
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          console.log(response2);
          this.contents = []
          this.contents.push(response2.result);
          this.resultsFilterContents = this.contents;
          this.emptyResult = false;
          this.cli_selected = true;
          this.doneLoading = true;
        }
      });
    } else {
      console.log('passou')
      this._http.post(
        'selectProdutividade2.php',
        {
          cli_id: this.clienteIndex,
          mes: mes,
          ano: ano == 0 ? new Date().getFullYear(): ano
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          console.log(response2);
          this.contents = []
          this.contents.push(response2.result);
          this.resultsFilterContents = this.contents;
          this.emptyResult = false;
          this.cli_selected = true;
          this.doneLoading = true;
        }
      });
    }
  }

  searchProdutividadeAno() {
    let mes = parseInt((document.getElementById('mesSelect') as HTMLInputElement).value)
    let ano = parseInt((document.getElementById('anoSelect') as HTMLInputElement).value)
    
    this.doneLoading = false;
    if (mes == 0) {
      this._http.post(
        'selectProdutividade2.php',
        {
          cli_id: this.clienteIndex,
          mes: new Date().getMonth() + 1,
          ano: ano == 0 ? new Date().getFullYear(): ano
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          console.log(response2);
          this.contents = []
          this.contents.push(response2.result);
          this.resultsFilterContents = this.contents;
          this.emptyResult = false;
          this.cli_selected = true;
          this.doneLoading = true;
        }
      });
    } else {
      console.log('passou')
      this._http.post(
        'selectProdutividade2.php',
        {
          cli_id: this.clienteIndex,
          mes: mes,
          ano: ano == 0 ? new Date().getFullYear(): ano
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          console.log(response2);
          this.contents = []
          this.contents.push(response2.result);
          this.resultsFilterContents = this.contents;
          this.emptyResult = false;
          this.cli_selected = true;
          this.doneLoading = true;
        }
      });
    }
  }

  searchPromotor(string, obj) {
    this.contents = [];
    this.valorTotalPromotor = 0;
    this.resultsFilterContents = [];

    if (obj.PRO_ID == -1 || obj.PRO_ID == '-1') {
      this.pro_selected = false;
      this.cli_selected = false;
    } else {
      this._http.post(
        'selectProdutividade2.php',
        {
          pro_nome: string,
          mes: new Date().getMonth() + 1,
          ano: new Date().getFullYear()
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          this.contents.push(response2.result);
          this.resultsFilterContents = this.contents;
          console.log(this.resultsFilterContents);
          this.emptyResult = false;
          this.pro_selected = true;

          this.resultsFilterContents.forEach(element => {
            element.content.forEach(element2 => {
              this.valorTotalPromotor += parseFloat(element2.valorTotal);
            });
          });

          this.resultsFilterContents.forEach(element => {
            element.content = element.content.sort( (a, b) => {
              return a.RFIL_Nome > b.RFIL_Nome
            })
          });

          console.log(this.resultsFilterContents);
        }
      });
    }
  }

  makePDF() {
    html2canvas(document.getElementById('tableProdutividade')).then((canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("", '_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
    });
  }
  
  makeExcel(){
    let r = this.resultsFilterContents[0].content;
    r.push({'ValorTotal': this.valorTotalPromotor});

    this._excel.exportAsExcelFile(r, 'ListaProdutividade_'+new Date(Date.now()).toISOString().split('T')[0]);
  }

  moverLixeira(promotor, index){
    let cc = confirm('KSTrade Merchandising - Listagem de Produtividade \r\n\r\nDeseja realmente mover esse promotor para a lixeira?');
    if (cc){
      this._http.post(
        'moverParaLixeira.php',
        {
          red_id: promotor.RED_ID,
          rfil_id: promotor.RFIL_ID,
          pro_id: promotor.PRO_ID,
          cli_nome: promotor.Cli_Nome,
          valor: promotor.valorTotal,
          usuario: this.userData[0].Usr_Nome
        }
      ).subscribe((response) => {
        if (response.status == '0x104'){
          this.resultsFilterContents[0].content.splice(index, 1)
        }
      });
    }
  }

  removeRow(ctn, content, index, contentIndex){
    console.log(content)
    let cc = confirm('KSTrade Merchandising - Listagem de Produtividade \r\n\r\nDeseja realmente remover? Essa exclusão não terá retorno');
    if (cc){
      this._http.post(
        'removerResultadoRotina.php',
        {
          id: content.id_aux_r_rotina
        }
      ).subscribe((response) => {
        if (response.status == '0x104'){
          this.resultsFilterContents[index].content.splice(contentIndex, 1)
        }
      });
    }
  }

}
