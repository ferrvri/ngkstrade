import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaJobsPendentesComponent } from './lista-jobs-pendentes.component';

describe('ListaJobsPendentesComponent', () => {
  let component: ListaJobsPendentesComponent;
  let fixture: ComponentFixture<ListaJobsPendentesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaJobsPendentesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaJobsPendentesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
