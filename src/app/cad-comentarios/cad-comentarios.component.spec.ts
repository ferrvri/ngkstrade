import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadComentariosComponent } from './cad-comentarios.component';

describe('CadComentariosComponent', () => {
  let component: CadComentariosComponent;
  let fixture: ComponentFixture<CadComentariosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadComentariosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadComentariosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
