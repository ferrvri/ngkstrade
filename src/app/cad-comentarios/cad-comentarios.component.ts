import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

@Component({
  selector: 'app-cad-comentarios',
  templateUrl: './cad-comentarios.component.html',
  styleUrls: ['./cad-comentarios.component.css']
})
export class CadComentariosComponent implements OnInit {

  title: string = '';
  errorTitle = {required: null}


  latLong = ''
  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
  }

  validation (){
    if (this.title.length < 1){
      this.errorTitle.required = true
    }

    if (this.errorTitle.required == null){
      return true;
    }

    return false;
  }

  cadComentario(){
    if (this.validation()){
      this._http.post(
        'insertComentarioDefinido.php',
        {
          title: this.title,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Inserção - Comentário Pré-definido'
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade Merchandising - Cadastro de Comentarios\r\n\r\nComentário cadastrado com sucesso!')
          this._router.navigate(['/main/listaComentariosDef'])
        }else{
          window.alert('KSTrade Merchandising - Cadastro de Comentarios\r\n\r\nErro ao cadastrar comentário!')
        }
      });
    }
  }
}

