import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadContatoComponent } from './cad-contato.component';

describe('CadContatoComponent', () => {
  let component: CadContatoComponent;
  let fixture: ComponentFixture<CadContatoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadContatoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadContatoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
