import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-cad-contato',
  templateUrl: './cad-contato.component.html',
  styleUrls: ['./cad-contato.component.css']
})

export class CadContatoComponent implements OnInit {

  pageParams: any;

  nome: string = '';
  telefoneFixo: string = '';
  telefoneMovel: string = '';
  email: string = '';
  obs: string = '';

  errorNome = { required: null };
  errorTelefoneFixo = { required: null };
  errorTelefoneMovel = { required: null };
  errorEmail = { required: null };
  errorObs = { required: null };

  latLong = '';

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  cadastroContato: FormGroup = new FormGroup({
    nome: new FormControl(this.nome, [Validators.required, Validators.minLength(6)]),
    telefoneFixo: new FormControl(this.nome, [Validators.required, Validators.minLength(6)]),
    telefoneMovel: new FormControl(this.nome, [Validators.required, Validators.minLength(6)]),
    email: new FormControl(this.nome, [Validators.required, Validators.minLength(6)]),
    obs: new FormControl(this.nome, [Validators.required, Validators.minLength(6)])
  });

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)

    $('#inputTelefoneFixoCadContato').mask('(00) 0000-0000');
    $('#inputTelefoneMovelCadContato').mask('(00) 0 0000-0000');

    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar' && this.pageParams.params.conId.length > 0) {
      this._http.post(
        'selectContatos.php',
        {
          unique: true,
          id: this.pageParams.params.conId
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.nome = response.result[0].CON_Nome;
          this.telefoneFixo = response.result[0].CON_Telefone_Fixo;
          this.telefoneMovel = response.result[0].CON_Telefone_Movel;
          this.email = response.result[0].CON_Email;
          this.obs = response.result[0].CON_Observacoes;
        } else if (response.status == '0x101') {
          window.alert('Erro na seleção do contato!');
        }
      });
    }
  }

  validation() {
    try {
      if (this.nome.length < 1) {
        this.errorNome = { required: true };
      } else {
        this.errorNome = { required: null };
      }
      if (this.telefoneFixo.length < 1) {
        this.errorTelefoneFixo = { required: true }
      } else {
        this.errorTelefoneFixo = { required: null };
      }
      if (this.telefoneMovel.length < 1) {
        this.errorTelefoneMovel = { required: true };
      } else {
        this.errorTelefoneMovel = { required: null };
      }
      if (this.obs.length < 1) {
        this.errorObs = { required: true };
      } else {
        this.errorObs = { required: null };
      }
      if (this.email.length < 1) {
        this.errorEmail = { required: true };
      } else {
        this.errorEmail = { required: null };
      }
      return true;
    } catch (e) {
      console.log(e.toString());
    }
  }

  cadContato(): void {
    if (this.pageParams.params.page == 'editar' && this.pageParams.params.conId.length > 0) {
      if (this.validation() && (this.errorEmail.required == null && this.errorNome.required == null && this.errorObs.required == null
        && this.errorTelefoneFixo.required == null && this.errorTelefoneMovel.required == null)) {
        this._http.post(
          'editarContato.php',
          {
            id: this.pageParams.params.conId,
            nome: this.nome,
            telefoneMovel: this.telefoneMovel,
            telefoneFixo: this.telefoneFixo,
            email: this.email,
            obs: this.obs,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Contato['+this.pageParams.params.conId+']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade - Editar Contatos\r\n\r\nContato editado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadContato']);
            }, 1000);
          } else if (response.status == '0x101') {
            window.alert('Erro no cadastro do contato!');
          }
        });
      }
    } else {
      if (this.validation() && (this.errorEmail.required == null && this.errorNome.required == null && this.errorObs.required == null
        && this.errorTelefoneFixo.required == null && this.errorTelefoneMovel.required == null)) {
        this._http.post(
          'insertContato.php',
          {
            nome: this.nome,
            telefoneMovel: this.telefoneMovel,
            telefoneFixo: this.telefoneFixo,
            email: this.email,
            obs: this.obs,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Inserção - Contato['+this.nome+']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade - Cadastro de Contatos\r\n\r\nContato cadastrado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadContato']);
            }, 1000);
          } else if (response.status == '0x101') {
            window.alert('Erro no cadastro do contato!');
          }
        });
      }
    }
  }



}
