import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../http.service';

import { Chart } from 'chart.js';

@Component({
  selector: 'app-pedidos-emetas',
  templateUrl: './pedidos-emetas.component.html',
  styleUrls: ['./pedidos-emetas.component.css']
})
export class PedidosEmetasComponent implements OnInit {

  @ViewChild("chart") chartC: ElementRef;

  private chart: Chart;

  chartOpts = {
    type: "doughnut",
    data: {
      labels: ["Meta já atingida", "Valor restante"],
      datasets: [
        {
          data: [],
          backgroundColor: [
            "#8c1515",
            "rgba(27, 55, 44, 0.25)"
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      circumference: 4 * Math.PI
    }
  }

  pageParams;
  emptyResult = false;

  redes = [];
  pedidos = [];
  clientes = [];

  chart_obj;

  rede_selected = 0;

  constructor(private _activatedRoute: ActivatedRoute, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    })

    this._http.post(
      'app/selectRedesRepresentante.php',
      {
        usuario_id: this.pageParams.params.usu_id
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.redes = response.result;
      }
    });
  }


  searchRede(id, obj) {
    this._http.post(
      'app/selectClientesRepresentante.php',
      {
        usuario_id: this.pageParams.params.usu_id
      }
    ).subscribe((response2) => {
      if (response2.status == '0x104') {
        this.clientes = response2.result;
        this.rede_selected = obj;
      }
    });
  }

  searchCliente(id, obj) {
    this._http.post(
      'app/selectMeta.php',
      {
        usuario: this.pageParams.params.usu_id,
        rede: (this.rede_selected as any).RED_ID,
        cliente: obj.Cli_ID,
        dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 0).toISOString().substring(0, 10),
        datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
      }
    ).subscribe((response2) => {
      if (response2.status == '0x104') {
        this.chart_obj = {
          percent: response2.percent,
          meta: response2.meta,
          valores: response2.valores
        };

        this._http.post(
          'selectPedidos.php',
          {
            usuario_id: this.pageParams.params.usu_id,
            rede_id: (this.rede_selected as any).RED_ID,
            cliente_id: obj.Cli_ID
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            this.pedidos = response.result;

            this.chartOpts.data.datasets[0].data[0] = this.chart_obj.valores;
            this.chartOpts.data.datasets[0].data[1] = this.chart_obj.meta;

            this.chart = new Chart(this.chartC.nativeElement, this.chartOpts);
          }else{
            this.pedidos = []
            delete this.chartOpts.data.datasets[0].data[0];
            delete this.chartOpts.data.datasets[0].data[1];
          }
        });
      }
    });
  }


}
