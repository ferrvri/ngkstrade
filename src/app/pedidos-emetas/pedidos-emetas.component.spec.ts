import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PedidosEmetasComponent } from './pedidos-emetas.component';

describe('PedidosEmetasComponent', () => {
  let component: PedidosEmetasComponent;
  let fixture: ComponentFixture<PedidosEmetasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PedidosEmetasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PedidosEmetasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
