import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, Validators, FormControl, FormGroup } from '@angular/forms';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var $: any;
declare var jQuery: any;

@Component({
  selector: 'app-cad-filial',
  templateUrl: './cad-filial.component.html',
  styleUrls: ['./cad-filial.component.css']
})

export class CadFilialComponent implements OnInit {
  pageParams: any;

  filialIndex: number = 0;
  redIndex: number = 0;

  redeOK: boolean = false;
  estadoOK: boolean = false;

  redes = [];
  estados = ['AC', 'AL', 'AP', 'AM', 'BA', 'CE', 'DF', 'ES', 'GO', 'MA', 'MT', 'MS', 'MG', 'PA', 'PB', 'PR', 'PE', 'PI', 'RJ', 'RN', 'RS', 'RO', 'RR', 'SC', 'SP', 'SE', 'TO'];

  errorNotExistsCep: boolean = false;
  errorNomeFilial = { required: null };
  errorTelefoneFixo = { required: null };
  errorRede = { required: null };
  errorCep = { required: null };
  errorEndereco = { required: null };
  errorCidade = { required: null };
  errorBairro = { required: null };
  errorEstado = { required: null };
  errorNumero = { required: null };
  errorVisita = { required: null };

  nomeFilial: string = "";
  telefoneFixo: string = "";
  rede: string = "Selecione";
  cep: string = "";
  endereco: string = "";
  numero: string = "";
  cidade: string = "";
  bairro: string = "";
  estado: string = "Selecione";
  numeroVisita: string = '';

  latLong = '';

  cadastroFilial: FormGroup = new FormGroup({
    nomeFilial: new FormControl(this.nomeFilial, [Validators.required, Validators.minLength(6)]),
    telefoneFixo: new FormControl(this.telefoneFixo, [Validators.required, Validators.minLength(6)]),
    rede: new FormControl(this.rede, [Validators.required, Validators.minLength(6)]),
    cep: new FormControl(this.cep, [Validators.required, Validators.minLength(6)]),
    endereco: new FormControl(this.endereco, [Validators.required, Validators.minLength(6)]),
    numero: new FormControl(this.numero, [Validators.required, Validators.minLength(6)]),
    cidade: new FormControl(this.cidade, [Validators.required, Validators.minLength(6)]),
    bairro: new FormControl(this.bairro, [Validators.required, Validators.minLength(6)]),
    estado: new FormControl(this.estado, [Validators.required, Validators.minLength(6)]),
    numeroVisita: new FormControl(this.numeroVisita, [Validators.required, Validators.minLength(6)])
  });

  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat + ';' + c.lng)

    $('#inputCEPFilial').mask('00000-000');
    $('#inputTelefoneFixoFilial').mask('(00) 0 0000 0000');

    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar') {
      this._http.post(
        'selectFilial.php',
        {
          unique: true,
          id: this.pageParams.params.id
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.redIndex = response.result[0].RFIL_Rede_ID;
          this.rede = response.result[0].RED_Nome;
          this.nomeFilial = response.result[0].RFIL_Nome;
          this.telefoneFixo = response.result[0].RFIL_Telefone;
          this.cep = response.result[0].RFIL_CEP;
          this.endereco = response.result[0].RFIL_Endereco;
          this.bairro = response.result[0].RFIL_Bairro;
          this.cidade = response.result[0].RFIL_Cidade;
          this.estado = response.result[0].RFIL_Estado;
          this.numero = response.result[0].RFIL_Numero;
          this.numeroVisita = response.result[0].RFIL_Numero_Visitas;
        } else {
          window.alert('Erro na seleção de filial');
        }
      });
    } else {
      this._http.post(
        'selectRedes.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          response.result.forEach(element => {
            this.redes.push(element);
          });
          this.resultFilterRedes = this.redes;
        } else {
          window.alert('Não foi possível buscar as redes disponiveis');
        }
      });
    }

  }

  validation() {
    try {

      if (this.nomeFilial.length < 1) {
        this.errorNomeFilial = { required: true };
      } else {
        this.errorNomeFilial = { required: null };
      }
      if (this.telefoneFixo.length < 1) {
        this.errorTelefoneFixo = { required: true }
      } else {
        this.errorTelefoneFixo = { required: null };
      }
      if (this.rede.length < 1 || this.rede == 'Selecione') {
        this.errorRede = { required: true };
      } else {
        this.errorRede = { required: null };
      }
      if (this.cep.length < 1) {
        this.errorCep = { required: true };
      } else {
        this.errorCep = { required: null };
      }
      if (this.endereco.length < 1) {
        this.errorEndereco = { required: true };
      } else {
        this.errorEndereco = { required: null };
      }
      if (this.numero.length < 1) {
        this.errorNumero = { required: true }
      } else {
        this.errorNumero = { required: null };
      }
      if (this.cidade.length < 1) {
        this.errorCidade = { required: true };
      } else {
        this.errorCidade = { required: null };
      }
      if (this.bairro.length < 1) {
        this.errorBairro = { required: true }
      } else {
        this.errorBairro = { required: null };
      }
      if (this.estado.length < 1 || this.estado == 'Selecione') {
        this.errorEstado = { required: true };
      } else {
        this.errorEstado = { required: null };
      }

      if (this.numeroVisita.length < 1) {
        this.errorVisita = { required: true };
      } else {
        this.errorVisita = { required: null };
      }
      return true;
    } catch (e) {
      console.log(e.toString());
    }
  }

  cadFilial(): void {
    if (this.pageParams.params.page == 'editar') {
      if (this.validation() && (this.errorBairro.required == null && this.errorCep.required == null &&
        this.errorCidade.required == null && this.errorEndereco.required == null && this.errorEstado.required == null &&
        this.errorNumero.required == null && this.errorNomeFilial.required == null &&
        this.errorTelefoneFixo.required == null && this.errorVisita.required == null)) {
        this._http.post(
          'insertFilial.php',
          {
            type: 'update',
            nomeFilial: this.nomeFilial,
            telefoneFixo: this.telefoneFixo,
            cep: this.cep,
            endereco: this.endereco,
            numero: this.numero,
            cidade: this.cidade,
            estado: this.estado,
            bairro: this.bairro,
            rede: this.redIndex,
            numeroVisita: this.numeroVisita,
            id: this.pageParams.params.id,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Filial[' + this.pageParams.params.id + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade - Edição de Filial\r\n\r\nFilial editada com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/listaFilial']);
            }, 1000);
          } else if (response.status == '0x101') {
            window.alert('Erro de alteração no banco');
          }
        });
      }
    } else {
      if (this.validation() && (this.errorBairro.required == null && this.errorCep.required == null &&
        this.errorCidade.required == null && this.errorEndereco.required == null && this.errorEstado.required == null && this.errorNumero.required == null && this.errorNomeFilial.required == null &&
        this.errorTelefoneFixo.required == null && this.errorVisita.required == null)) {
        this._http.post(
          'insertFilial.php',
          {
            nomeFilial: this.nomeFilial,
            telefoneFixo: this.telefoneFixo,
            cep: this.cep,
            endereco: this.endereco,
            numero: this.numero,
            cidade: this.cidade,
            estado: this.estado,
            bairro: this.bairro,
            rede: this.redIndex,
            numeroVisita: this.numeroVisita,

            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Inserção - Filial[' + this.nomeFilial + ']'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            window.alert('KSTrade - Cadastro de Filial\r\n\r\nFilial cadastrada com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout(() => {
              this._router.navigate(['/main/cadFilial']);
            }, 1000);
          } else if (response.status == '0x101') {
            window.alert('Erro de inserção ao banco');
          } else if (response.status == '0x102') {
            window.alert('KSTrade - Cadastro de Filial\r\n\r\nJá existe uma filial cadastrada com esse nome!');
          }
        })
      }
    }
  }

  checkCep(cep: string): void {
    if (cep.length < 1) {
      this.errorCep = { required: true };
    } else {
      this._http.post(
        'https://viacep.com.br/ws/' + cep.replace('-', '').replace('.', '') + '/json',
        {}
      ).subscribe((response) => {
        if (response.localidade) {
          this.estado = response.uf;
          this.bairro = response.bairro;
          this.cidade = response.localidade;
          this.endereco = response.logradouro;
          this.errorNotExistsCep = false;
          document.getElementById('inputNumeroCadFilial').focus();
        } else {
          this.errorNotExistsCep = true;
          this.estado = ""
          this.bairro = ""
          this.cidade = ""
          this.endereco = ""
        }
      });
    }
  }

  setEstado(e): void {
    this.estado = e;
    this.estadoOK = false;
  }

  setRede(e): void {
    this.rede = e.RED_Nome;
    this.redIndex = e.RED_ID;
    this.redeOK = false;
  }

  resultFilterRedes: any = [];

  filterRede(event: KeyboardEvent, model, arr) {
    if (event.keyCode == 8) {
      this.resultFilterRedes = this.redes;
    } else {
      var regexp = new RegExp((".*" + event.key.split("") + ".*"), "i");
      this.resultFilterRedes = arr.filter((e) => {
        return regexp.test(e.RED_Nome.split('')[0]);
      });
    }
  }

}
