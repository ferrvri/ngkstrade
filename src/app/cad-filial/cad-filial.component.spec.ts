import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadFilialComponent } from './cad-filial.component';

describe('CadFilialComponent', () => {
  let component: CadFilialComponent;
  let fixture: ComponentFixture<CadFilialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadFilialComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadFilialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
