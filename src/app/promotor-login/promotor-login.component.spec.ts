import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotorLoginComponent } from './promotor-login.component';

describe('PromotorLoginComponent', () => {
  let component: PromotorLoginComponent;
  let fixture: ComponentFixture<PromotorLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotorLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotorLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
