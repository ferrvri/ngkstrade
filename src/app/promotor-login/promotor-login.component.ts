import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute } from '@angular/router';

declare var showDialog: any;

@Component({
  selector: 'app-promotor-login',
  templateUrl: './promotor-login.component.html',
  styleUrls: ['./promotor-login.component.css']
})
export class PromotorLoginComponent implements OnInit {

  pageParams: any;

  email: string = '';
  senha: string = '';

  doneLoading: boolean = false;


  errorEmail = {required: null}
  errorSenha = {required: null}

  constructor(private _activatedRoute: ActivatedRoute, private _http: HttpService,) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      console.log(data);
      this.pageParams = data;
    });

    this._http.post(
      'selectPromotor.php',
      {
        uniqueName: true,
        nome: this.pageParams.params.proNome
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        if (response.result[0].PRO_Email != '' && response.result[0].PRO_Senha != ''){
          this.email = response.result[0].PRO_Email;
          this.doneLoading = true;
        }
      }
    });
  }

  validation(){
    if (this.email.length < 1){
      this.errorEmail.required = true;
    }
    if (this.senha.length < 1){
      this.errorSenha.required = true;
    }
  }

  cadLogin(){
    this.validation();
    if (this.errorEmail.required == null && this.errorSenha.required == null){
      console.log(0)
      this._http.post(
        'generateLoginPromotor.php',
        {
          nome: this.pageParams.params.proNome,
          email: this.email,
          senha: this.senha
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          showDialog({
            type: 'normal',
            title: 'Login gerado com sucesso!',
            text: `E-mail: ${this.email}\nSenha: ${this.senha}\nCódigo de confirmação: ${response.codigo}`,
          });
        }
      });
    }
  }

}
