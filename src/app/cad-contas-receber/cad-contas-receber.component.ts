import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

declare var $:any;
declare var jQuery:any;

@Component({
  selector: 'app-cad-contas-receber',
  templateUrl: './cad-contas-receber.component.html',
  styleUrls: ['./cad-contas-receber.component.css']
})
export class CadContasReceberComponent implements OnInit {

  pageParams: any;

  descricao: string = '';
  servico: string = 'Selecione';
  filial: string = 'Selecione';
  rede: string = 'Selecione';
  valor: string = '';
  loja: string = '';
  notafiscal: string = '';
  observacao: string = '';
  data: string = '';

  servicoId: number = 0;
  filialId: number = 0;
  redeId: number = 0;

  servicos: any = [];
  filiais: any = [];
  redes: any = [];

  filialOK: boolean = false;
  redeOK: boolean = false;
  servicoOK: boolean = false;

  errorDescricao: any = {required: null};
  errorServico: any = {required: null};
  errorValor: any = {required: null};
  errorLoja: any = {required: null};
  errorNotaFiscal: any = {required: null};
  errorObservacao: any = {required: null};
  errorData: any = {required: null};
  errorFilial: any = {required: null};
  errorRede: any = {required: null};

  cadastroContaReceber: FormGroup = new FormGroup({
    servico: new FormControl(this.servico, [Validators.minLength(6), Validators.required]),
    descricao: new FormControl(this.descricao, [Validators.minLength(6), Validators.required]),
    valor: new FormControl(this.valor, [Validators.minLength(6), Validators.required]),
    // rede: new FormControl(this.observacao, [Validators.minLength(6), Validators.required]),
    // filial: new FormControl(this.loja, [Validators.minLength(6), Validators.required]),
    notafiscal: new FormControl(this.notafiscal, [Validators.minLength(6), Validators.required]),
    data: new FormControl(this.data, [Validators.minLength(6), Validators.required]),
    obs: new FormControl(this.observacao, [Validators.minLength(6), Validators.required])
  });

  constructor(private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    $("#dataInputReceber").datepicker();
    $("#valorInputReceber").maskMoney();
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar'){
      this._http.post(
        'selectContasReceber.php',
        {
          kind: 'areceber',
          unique: true,
          id: this.pageParams.params.id
        }
      ).subscribe( (response) => {
        if(response.status == '0x104'){
          this.servico = response.result[0].TIPS_Nome;
          this.descricao = response.result[0].FORR_Nome;
          this.valor = response.result[0].FORR_Valor;
          this.notafiscal = response.result[0].FORR_notaFiscal;
          this.data = response.result[0].FORR_Data;
          this.observacao = response.result[0].FORR_Obs;
          this.servicoId = response.result[0].FORR_Servico_ID;
        }else if (response.status == '0x101'){
          window.alert('Erro na selecao de conta');
        }
      });
    }else{
      this._http.post(
        'selectServicos.php',
        {}
      ).subscribe( (response) => {
        if(response.status == '0x104'){
          response.result.forEach(element => {
            this.servicos.push(element);
          });
          this.resultFilterServicos = this.servicos;
          // this._http.post(
          //   'selectRedes.php',
          //   {}
          // ).subscribe( (response) => {
          //   if (response.status == '0x104'){
          //     response.result.forEach(element => {
          //       this.redes.push(element);
          //     });
          //   }else if (response.status == '0x101'){
          //     window.alert('Erro na seleção de redes');
          //   }
          // });
        }else if(response.status == '0x101'){
          window.alert('Erro na seleção dos servicos');
        }
      });
    }
  }

  validation(){
    try{
      if (this.servico.length < 1 || this.servico == 'Selecione'){
        this.errorServico = {required: true};
      }else{
        this.errorServico = {required: null};
      }
      if ((<HTMLInputElement>document.getElementById('valorInputReceber')).value.length < 1){
        this.errorValor = {required: true};
      }else{
        this.errorValor = {required: null};
      }
      if (this.descricao.length < 1){
        this.errorDescricao = {required: true};
      }else{
        this.errorDescricao = {required: null};
      }
      if (this.notafiscal.length < 1){
        this.errorNotaFiscal = {required: true};
      }else{
        this.errorNotaFiscal = {required: null};
      }
      if (this.observacao.length < 1){
        this.errorObservacao = {required: true};
      }else{
        this.errorObservacao = {required: null};
      }
      if (this.data.length < 1){
        this.errorData.required = true;
      }else{
        this.errorData.required = null;
      }
      // if (this.filial.length < 1){
      //   this.errorFilial.required = true;
      // }else{
      //   this.errorFilial.required = null;
      // }
      // if (this.rede.length < 1){
      //   this.errorRede.required = true;
      // }else{
      //   this.errorRede.required = null;
      // }
      return true;
    }catch(e){
      console.log(e.toString());
      return false;
    }
  }

  setData(){
    if ((<HTMLInputElement>document.getElementById('dataInputReceber')).value === undefined || (<HTMLInputElement>document.getElementById('dataInputReceber')).value == ''){
      this.data = '';
    }else{
      this.data = (<HTMLInputElement>document.getElementById('dataInputReceber')).value.split('/')[1]+'/'+(<HTMLInputElement>document.getElementById('dataInputReceber')).value.split('/')[0]+'/'+(<HTMLInputElement>document.getElementById('dataInputReceber')).value.split('/')[2]
    }
  }

  cadContaReceber():void{
    if (this.pageParams.params.page == 'editar'){
      if(this.validation() && this.errorDescricao.required == null && this.errorServico.required == null
      && this.errorValor.required == null && this.errorLoja.required == null && this.errorObservacao.required == null
      && this.errorNotaFiscal.required == null && this.errorData.required == null && this.errorFilial.required == null &&
      this.errorRede.required == null){
        this._http.post(
          'insertContaReceber.php',
          {
            type: 'update',
            id: this.pageParams.params.id,
            valor: (<HTMLInputElement>document.getElementById('valorInputReceber')).value,
            descricao: this.descricao,
            servicoid: this.servicoId,
            // filial: this.filialId,
            // rede: this.redeId,
            obs: this.observacao,
            notafiscal: this.notafiscal,
            data: this.data
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Edição de conta\r\n\r\nConta a receber editada com suceso!');
            this._router.navigate(['/main/innerload']);
            setTimeout( () => {
              this._router.navigate(['/main/listaContasReceber', {page: 'areceber'}]);
            }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro na inserção de contas a receber');
          }else if (response.status == '0x102'){
            window.alert('KSTrade - Contas a receber\r\n\r\nA nota fiscal inserida já existe!');
          }
        });
      }
    }else{
      if(this.validation() && this.errorDescricao.required == null && this.errorServico.required == null
      && this.errorValor.required == null && this.errorLoja.required == null && this.errorObservacao.required == null
      && this.errorNotaFiscal.required == null && this.errorData.required == null && this.errorFilial.required == null &&
      this.errorRede.required == null){
        this._http.post(
          'insertContaReceber.php',
          {
            valor: (<HTMLInputElement>document.getElementById('valorInputReceber')).value,
            descricao: this.descricao,
            servicoid: this.servicoId,
            // filial: this.filialId,
            // rede: this.redeId,
            obs: this.observacao,
            notafiscal: this.notafiscal,
            data: this.data
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Cadastro de conta\r\n\r\nConta a receber cadastrada com suceso!');
            this._router.navigate(['/main/innerload']);
            setTimeout( () => {
              this._router.navigate(['/main/cadContasReceber']);
            }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro na inserção de contas a receber');
          }else if (response.status == '0x102'){
            window.alert('KSTrade - Contas a receber\r\n\r\nA nota fiscal inserida já existe!');
          }
        });
      }
    }
  }

  setServico(s):void{
    this.servico = s.TIPS_Nome;
    this.servicoId = s.TIPS_ID;
    this.servicoOK = false;
  }

  setFilial(f):void{
    this.filial = f.RFIL_Nome;
    this.filialId = f.RFIL_ID;
    this.filialOK = false;
  }

  setRedes(r):void{
    this.rede = r.RED_Nome;
    this.redeId = r.RED_ID;
    this._http.post(
      'selectFilial.php',
      {
        id: this.redeId
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.filiais = [];
        response.result.forEach(element => {
          this.filiais.push(element);
        });
        this.redeOK = false;
      }else if (response.status == '0x101'){
        window.alert('Erro ao selecionar filiais');
      }
    });
  }

  resultFilterServicos : any = [];

  filterServico(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterServicos = this.servicos;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterServicos = arr.filter( (e) => {
        return regexp.test(e.TIPS_Nome.split('')[0]);
      });
    }
  }

  

}
