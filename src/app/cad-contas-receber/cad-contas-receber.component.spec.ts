import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadContasReceberComponent } from './cad-contas-receber.component';

describe('CadContasReceberComponent', () => {
  let component: CadContasReceberComponent;
  let fixture: ComponentFixture<CadContasReceberComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadContasReceberComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadContasReceberComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
