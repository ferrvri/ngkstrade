import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ValorJobComponent } from './valor-job.component';

describe('ValorJobComponent', () => {
  let component: ValorJobComponent;
  let fixture: ComponentFixture<ValorJobComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValorJobComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ValorJobComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
