import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { ActivatedRoute, Router } from '@angular/router';

declare var $:any;
declare var jQuery: any;

@Component({
  selector: 'app-valor-job',
  templateUrl: './valor-job.component.html',
  styleUrls: ['./valor-job.component.css']
})
export class ValorJobComponent implements OnInit {

  rede: string = "Selecione";
  filial: string = "Selecione";

  redeOK: boolean = false;
  filialOK: boolean = false;

  redes: any = [];
  filiais: any = [];

  redeId : number = 0;
  filialId : number = 0;
  jobId: number = 0;

  valor: string = "";

  errorFilial = {required: null};
  errorRede = {required: null};
  errorValor = {required: null};

  pageParams:any;

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    $("#valorJobInput").maskMoney();
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });
    if (this.pageParams.params.page == 'editar' && this.pageParams.params.cliId.length > 0){
      this._http.post(
        'selectValoresTabela.php',
        {
          id: this.pageParams.params.cliId,
          jobId: this.pageParams.params.job
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          this.rede = response.result[0].RED_Nome;
          this.redeId = response.result[0].RED_ID;
          this.filial = response.result[0].RFIL_Nome;
          this.filialId = response.result[0].RFIL_ID;
          this.valor = response.result[0].VJOB_Valor;
          document.getElementById('redeSelect').setAttribute('disabled', 'true');
          document.getElementById('filSelect').setAttribute('disabled', 'true');
        }else if (response.status == '0x101'){
          window.alert('Erro ao selecionar redes');
        }
      });
    }else{
      this._http.post(
        'selectRedes.php',
        {}
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          response.result.forEach(element => {
            this.redes.push(element);
          });
          this.resultFilterRedes = this.redes;
        }else if (response.status == '0x101'){
          window.alert('Erro ao selecionar redes');
        }
      });
    }
  }

  setRede(i):void{
    this.rede = i.RED_Nome;
    this.redeId = i.RED_ID;
    this._http.post(
      'selectFilial.php',
      {
        id: i.RED_ID
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.filiais = [];
        this.filial = "Selecione";
        response.result.forEach(element => {
          this.filiais.push(element);
        });
        this.resultFilterFiliais = this.filiais;
        this.redeOK = false;
      }else if (response.status == '0x101'){
        window.alert('Erro ao selecionar filiais');
      }
    });
  }

  setFilial(i):void{
    this.filial = i.RFIL_Nome;
    this.filialId = i.RFIL_ID;
    this.filialOK = false;
  }
  
  validation():any{
    try{
      if (this.rede.length < 1 || this.rede == 'Selecione'){
        this.errorRede.required = true;
      }else{
        this.errorRede.required = null;
      }
      if (this.filial.length < 1 || this.filial == 'Selecione'){
        this.errorFilial.required = true;
      }else{
        this.errorFilial.required = null;
      }
      if ((<HTMLInputElement>document.getElementById("valorJobInput")).value.length < 1){
        this.errorValor.required = true;
      }else{
        this.errorValor.required = null;
      }
      return true;
    }catch(e){
      console.log(e.toString());
      return false;
    }
  }

  cadValor(){
    if (this.validation() && this.errorFilial.required == null && this.errorRede.required == null 
    && this.errorValor.required == null){
      if (this.pageParams.params.page == 'editar'){
        this._http.post(
          'insertValorJob.php',
          {
            type: 'update',
            valor: (<HTMLInputElement>document.getElementById("valorJobInput")).value,
            id: this.pageParams.params.job
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Edição de valores\r\n\r\nValor editado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout( () => {
              this._router.navigate(['/main/listaClientes', {page: 'search', cliente: ''}]);
            }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro na inserção do valor');
          }
        });
      }else{
        console.log('rede - > '+ this.redeId);
        console.log('filial - > '+ this.filialId);
        console.log('cliente -> '+ this.pageParams.params.cliente);
        this._http.post(
          'insertValorJob.php',
          {
            type: 'normal',
            clienteid: this.pageParams.params.cliente,
            redeid: this.redeId,
            filialid: this.filialId,
            valor: (<HTMLInputElement>document.getElementById("valorJobInput")).value
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Cadastro de valores\r\n\r\nValor cadastrado com sucesso!');
            this._router.navigate(['/main/innerload']);
            setTimeout( () => {
              this._router.navigate(['/main/listaClientes', {page: 'search', cliente: ''}]);
            }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro na inserção do valor');
          }else if (response.status == '0x105'){
            window.alert('KSTrade - Valores Job\r\n\r\nValor já cadastrado!');
          }
        });
      }
    }
  }

  

  resultFilterFiliais : any = [];

  filterFilial(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterFiliais = this.filiais;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterFiliais = arr.filter( (e) => {
        return regexp.test(e.RFIL_Nome.split('')[0]);
      });
    }
  }

  resultFilterRedes : any = [];

  filterRede(event: KeyboardEvent, model, arr){
    if (event.keyCode == 8){
      this.resultFilterRedes = this.redes;
    }else{
      var regexp = new RegExp((".*"+event.key.split("")+ ".*"), "i");
      this.resultFilterRedes = arr.filter( (e) => {
        return regexp.test(e.RED_Nome.split('')[0]);
      });
    }
  }
}
