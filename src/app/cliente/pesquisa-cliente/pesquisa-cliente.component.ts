import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';
import { ExcelService } from '../../excel.service';

@Component({
  selector: 'app-pesquisa-cliente',
  templateUrl: './pesquisa-cliente.component.html',
  styleUrls: ['./pesquisa-cliente.component.css']
})
export class PesquisaClienteComponent implements OnInit {

  userData = [];
  pesquisas = [];
  resultsFilterPesquisas = [];

  dict = {
    pes_data2: "Data realizacao",
    PRO_Nome: "Promotor",
    RED_Nome: "Rede",
    RFIL_Nome: "Filial",
    pes_prateleira: "Em Area de venda?",
    pes_estoque: "Em Estoque?",
    pes_rupturatotal: "Em Ruptura total?",
    pes_produto_id: "Produto",
    pes_numero_frentes: "Número de Frentes"
  }

  p: number = 0;

  constructor(
    private _http: HttpService,
    private _excel: ExcelService
  ) { }

  ngOnInit() {
    if (localStorage.getItem('session')) {
      this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
      this._http.post(
        'selectPesquisas_2.php',
        {
          clientes: this.userData[0].clientes,
          dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
          datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.resultsFilterPesquisas = this.pesquisas = response.result;
        }
      });
    }
  }

  setDateOnePesquisa() {
    if ((<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value =
        (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[2];
    }
  }

  setDateTwoPesquisa() {
    if ((<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value =
        (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[2];
    }
  }

  alreadySearch: boolean = false;

  searchPesquisas(init, end) {
    this.resultsFilterPesquisas = [];
    this.alreadySearch = true;
    console.log(init, end)
    // this._http.post(
    //   'selectPesquisas.php',
    //   {
    //     // type: 'jobs'
    //   }
    // ).subscribe((response) => {
    //   if (response.status == '0x104') {
    //     response.result.forEach(element => {
    //       element.content.forEach(element2 => {
    //         element2.pesquisas = [];
    //         this._http.post(
    //           'selectPesquisas.php',
    //           {
    //             pesquisa: true,
    //             pro_id: element2.pro_id,
    //             dataone: (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[2]
    //               + '-' + (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[1]
    //               + '-' + (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[0],
    //             datatwo:
    //               (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[2]
    //               + '-' + (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[1]
    //               + '-' + (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[0]
    //           }
    //         ).subscribe((response2) => {
    //           if (response2.status == '0x104') {
    //             response2.result.forEach(element3 => {
    //               element2.pesquisas.push(element3);
    //             });
    //           }
    //         });
    //       });

    //       this.resultsFilterPesquisas.push(element);
    //     });
    //   }
    // });

    this._http.post(
      'selectPesquisas_2.php',
      {
        clientes: this.userData[0].clientes,
        dataone: init,
        datatwo: end
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.resultsFilterPesquisas = this.pesquisas = response.result;
      }
    });
  }

  queryClientes: string = '';
  filterArrayClientes(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterPesquisas = arr.filter((e) => {
      return regexp.test(e.cliente_nome);
    });
  }

  clearDataPesquisa() {
    this.alreadySearch = false;
    this.resultsFilterPesquisas = this.pesquisas;
    (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value = '';
    (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value = '';
  }


  queryProdutos: string = '';

  filterArrayProdutos(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    // this.resultsFilterPesquisas = arr.filter( (e) => {
    //   return regexp.test(e.cliente_nome);
    // });


    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach(element2 => {
        if (regexp.test(element2.pro_nome)) {
          o.content.push(element2);
        }
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  searchClientes(string) {
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayClientes(string, this.pesquisas);
    }
  }

  searchProdutos(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayProdutos(string, this.pesquisas);
    }
  }

  searchPesquisaRede(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaRedes(string, this.pesquisas);
    }
  }

  queryPesquisaRedes: string = '';

  filterArrayPesquisaRedes(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana,
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RED_Nome)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  searchPesquisaFilial(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaFilial(string, this.pesquisas);
    }
  }

  daysBetween(date1, date2) {
    date2 = date2.split('-')[0] + '-' + date2.split('-')[2] + '-' + date2.split('-')[1];
    console.log(date1, date2);
    //Get 1 day in milliseconds
    let one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    let date1_ms = new Date(date1).getTime();
    let date2_ms = new Date(date2).getTime();

    // Calculate the difference in milliseconds
    let difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
  }

  queryPesquisaFilial: string = '';

  filterArrayPesquisaFilial(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana,
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RFIL_Nome)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  queryPesquisaCidade: string = '';

  searchPesquisaCidade(string) {
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaCidade(string, this.pesquisas);
    }
  }

  filterArrayPesquisaCidade(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana || ";",
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RFIL_Cidade)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }


  sendCritcEmail(ff, f) {
    let c = confirm('KSTrade Merchandising\r\n\r\nDeseja enviar o e-mail');
    if (c == true) {
      this._http.post(
        'enviarCriticEmail.php',
        {
          rede: f.RED_ID,
          redeNome: f.RED_Nome,
          filial: f.RFIL_ID,
          filialNome: f.RFIL_Nome,
          cliente: f.JOB_Cliente_ID,
          "obj": { "produto": ff.pro_nome, "rede": f.RED_Nome, "filial": f.RFIL_Nome, "datavencimento": f.pes_data_vencimento }
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising\r\n\r\nE-mail enviado com sucesso!');
        } else {
          window.alert('KSTrade Merchandising\r\n\r\nUm erro ocorreu por favor contate-nos!');
        }
      });
    }
  }

  onlyCrt: boolean = false;

  onlyCritic() {
    if (!this.onlyCrt) {
      this.onlyCrt = true;
      this.resultsFilterPesquisas = [];
      this.pesquisas.forEach((element) => {
        let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
        element.content.forEach((element2, index) => {
          o.content.push({
            pesquisas: [],
            pro_dias_semana: element2.pro_dias_semana || ";",
            pro_id: element2.pro_id,
            pro_nome: element2.pro_nome
          });

          element2.pesquisas.forEach(element3 => {
            if (element3.hasDif) {
              o.content[index].pesquisas.push(element3);
            }
          });
        });
        this.resultsFilterPesquisas.push(o);
      });
    } else {
      this.onlyCrt = false;
      this.resultsFilterPesquisas = this.pesquisas;
    }
  }

  makeExcel() {
    let r = [];

    r.push({ '': this.resultsFilterPesquisas[0].cliente_nome });

    this.resultsFilterPesquisas[0].content.forEach(element => {
      element.pesquisas.forEach(element2 => {
        let content = {}
        for (let k in element2) {
          if (
            (k.indexOf('pes') != -1 ||
              k.indexOf('PRO_Nome') != -1 ||
              k.indexOf('RED_Nome') != -1 ||
              k.indexOf('pes_produto_id') != -1 ||
              k.indexOf('RFIL_Nome') != -1) &&
            k.indexOf('pes_job') == -1 &&
            k.indexOf('pro_id') == -1
          ) {
            if (element2[k] !== null && this.dict[k] !== undefined && k != 'pes_data' && k != 'pesquisas' && k != 'pes_id' && k != 'pes_status' && k != 'pes_numero_max_frentes') {
              if (k == 'pes_produto_id') {
                content[this.dict[k]] = element.pro_id == element2[k] ? element.pro_nome : 'SEM PRODUTO'
              } else if (k == 'pes_numero_frentes') {
                content[this.dict[k]] = element2[k]
              } else {
                content[this.dict[k]] = element2[k] == 0 ? 'Não' : element2[k] == 1 ? 'Sim' : element2[k]
              }
            }
          }
        }
        r.push(content)
      });
    });

    this._excel.exportAsExcelFile(r, 'Pesquisas' + new Date(Date.now()).toISOString().split('T')[0]);
  }

}
