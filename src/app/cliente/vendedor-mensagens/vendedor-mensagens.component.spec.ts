import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendedorMensagensComponent } from './vendedor-mensagens.component';

describe('VendedorMensagensComponent', () => {
  let component: VendedorMensagensComponent;
  let fixture: ComponentFixture<VendedorMensagensComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendedorMensagensComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendedorMensagensComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
