import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-vendedor-mensagens',
  templateUrl: './vendedor-mensagens.component.html',
  styleUrls: ['./vendedor-mensagens.component.css']
})
export class VendedorMensagensComponent implements OnInit {

  usuarioDestinario: string = 'Selecione um destinatario';
  mensagemCorpo: string = '';
  prioridade: string = 'Selecione';

  usuarioOK:boolean = false;

  pageParams: any;

  usuarioIdDestino:string = '';

  usuarios: any = [];

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });
    if (this.pageParams.params.type == 'reply'){
      this._http.post(
        'selectUsuarios.php',
        {
          unique: true,
          id: this.pageParams.params.to
        }
      ).subscribe( (response) => {
        if(response.status == '0x104'){
          this.usuarioDestinario = response.result[0].Usr_Nome;
          this.usuarioIdDestino = response.result[0].Usr_ID;
          document.getElementById('destino').setAttribute('disabled', 'false');
        }else if (response.status == '0x101'){
          window.alert('Erro ao selecionar usuario');
        }
      });
    }else if(this.pageParams.params.type == 'vendedor'){
      if (this.usuarios.length < 1){
        this._http.post(
          'selectUsuariosMsg.php',
          {
            type: 'vendedor'
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            response.result.forEach(element => {
              if(JSON.parse(localStorage.getItem('session'))[0].Usr_Nome != element.Usr_Nome){
                this.usuarios.push(element);
              }
            });
          }else if (response.status == '0x104'){
            window.alert('Erro ao selecionar usuarios para mensagem');
          }
        });
      }
    }else{
      if (this.usuarios.length < 1){
        this._http.post(
          'selectUsuariosMsg.php',
          {}
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            response.result.forEach(element => {
              if(JSON.parse(localStorage.getItem('session'))[0].Usr_Nome != element.Usr_Nome){
                this.usuarios.push(element);
              }
            });
          }else if (response.status == '0x104'){
            window.alert('Erro ao selecionar usuarios para mensagem');
          }
        });
      }
    }
  }

  setUsuario(u):void{
    this.usuarioDestinario = this.usuarios[u].Usr_Nome;
    this.usuarioIdDestino = this.usuarios[u].Usr_ID;
    console.log(this.usuarioIdDestino);
    this.usuarioOK = false;
  }

  sendMensagem():void{
    if ((this.mensagemCorpo.length < 1 || this.mensagemCorpo === undefined) ||(this.usuarioDestinario == "Selecione um destinatario") ){
      window.alert('Preencha todos os campos!');
    }else{
      if(this.pageParams.params.type == 'reply'){
        this._http.post(
          'sendMensagem.php',
          {
            deid: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            paraid: this.pageParams.params.to,
            mensagem: this.mensagemCorpo,
            status: 'Nao Lido',
            dataenvio: new Date().getUTCDate().toString() + '/' + (parseInt(new Date().getUTCMonth().toString().length == 1 ? '0'+new Date().getUTCMonth().toString(): new Date().getUTCMonth().toString()) + 1) + '/' + new Date().getUTCFullYear().toString()
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Mensagens\r\n\r\nMensagem respondida a: \r\n\t'+ this.usuarioDestinario);
            this._router.navigate(['/main/innerload']);
            setTimeout( () => {
              this._router.navigate(['/cliente/dashboard']);
            }, 1000);
          }else if (response.status == '0x101'){
            window.alert('Erro no envio da mensagem');
          }
        });
      }else{
        this._http.post(
          'sendMensagem.php',
          {
            deid: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            paraid: this.usuarioIdDestino,
            mensagem: this.mensagemCorpo,
            status: 'Nao Lido',
            dataenvio: new Date().getUTCDate().toString() + '/' + (parseInt(new Date().getUTCMonth().toString().length == 1 ? '0'+new Date().getUTCMonth().toString(): new Date().getUTCMonth().toString()) + 1) + '/' + new Date().getUTCFullYear().toString()
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade - Mensagens\r\n\r\nMensagem enviada a: \r\n\t'+ this.usuarioDestinario);
          }else if (response.status == '0x101'){
            window.alert('Erro no envio da mensagem');
          }
        });
      }
    }
  }

  setPrioridade(p):void{
    this.prioridade = p;
  }


}
