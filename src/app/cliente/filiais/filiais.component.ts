import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-filiais',
  templateUrl: './filiais.component.html',
  styleUrls: ['./filiais.component.css']
})
export class FiliaisComponent implements OnInit {

  filiais = [];
  emptyResult = false;

  resultsFilterFiliais: any = [];
  queryFiliais: string = '';
  queryCidadeFiliais: string = '';

  constructor(private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    let result = [];
    this._http.post(
      'cliente/selectRedes.php',
      {
        id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
      }
    ).subscribe( (response1) => {
      if (response1.status == '0x104'){
        response1.result.forEach( (e1) => {
          let redes = {
            nome: e1.RED_Nome,
            id: e1.RED_ID,
            content: [],
            visible: true
          };

          this._http.post(
            'cliente/selectFilial.php',
            {
              id: redes.id
            }
          ).subscribe( (response2) => {
            if (response2.result.length > 0){
              redes.content = response2.result;
            }else{
              redes.content = [];
            }
            this.filiais.push(redes);
            this.filterArrayFiliais(this.queryFiliais, this.filiais);
          }); 
        });
      }else if (response1.status == '0x101'){
        this.emptyResult = true;
      }
    });
  }

  openFilial(index, f, event):void{
    if (f.content.length > 0){
      if (f.visible == true){
        f.visible = false;
      }else{
        f.visible = true;
      }
    }
  }

  deleteFilial(f):void{
    let confirmation = confirm('KSTrade - Lista de Filiais\r\n\r\nDeseja realmente excluir?');
    if (confirmation == true){
      this._http.post(
        'deleteFilial.php',
        {
          id: f.RFIL_ID
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('Removido com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/listaFilial']);
          }, 1000);
        }else if (response.status == '0x101'){
          window.alert('Erro inesperado!');
        }
      });
    }
  }

  filterArrayFiliais(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterFiliais = arr.filter( (e) => {
      return regexp.test(e.nome);
    });
  }

  searchFiliais(string){
    this.filterArrayFiliais(string, this.filiais);
  }

  filterArrayCidadeFiliais(model, arr){
    this.resultsFilterFiliais = [];
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    arr.forEach(e1 => {
      let redes = {
        nome: e1.nome,
        id: e1.id,
        content: [],
        visible: true
      };
      
      e1.content.forEach(element => {
        if (regexp.test(element.RFIL_Cidade)){
          redes.content.push(element);
        }
      });
      this.resultsFilterFiliais.push(redes);
      // console.log(this.resultsFilterFiliais);
    });    

  }

  searchCidadeFiliais(string){
    this.filterArrayCidadeFiliais(string, this.filiais);
  }


}
