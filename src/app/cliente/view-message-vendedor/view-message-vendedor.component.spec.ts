import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewMessageVendedorComponent } from './view-message-vendedor.component';

describe('ViewMessageVendedorComponent', () => {
  let component: ViewMessageVendedorComponent;
  let fixture: ComponentFixture<ViewMessageVendedorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewMessageVendedorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewMessageVendedorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
