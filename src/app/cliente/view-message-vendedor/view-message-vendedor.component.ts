import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-view-message-vendedor',
  templateUrl: './view-message-vendedor.component.html',
  styleUrls: ['./view-message-vendedor.component.css']
})
export class ViewMessageVendedorComponent implements OnInit {

  pageParams: any;
  message:any = [];

  constructor(private _http: HttpService, private _activatedRoute: ActivatedRoute, private _router: Router) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.msg.length > 0){
      this._http.post(
        'selectMensagens.php',
        {
          unique: true,
          id: this.pageParams.params.msg
        }
      ).subscribe( (response) => {
        console.log(response);
        if (response.status == '0x104'){
          response.result.forEach(element => {
            this.message.push(element);
          });
        }else if (response.status == '0x101'){
          window.alert('Erro na seleção de mensagem');
        }
      });
    }
  }

  deleteMsg(m):void{
    let c = confirm('KSTrade - Mensagens\r\n\r\nDeseja realmente excluir?');
    if (c == true){
      this._http.post(
        'deleteMensagem.php',
        {
          id: m
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade - Mensagens\r\n\r\nMensagem excluida com sucesso!');
          this._router.navigate(['/main/innerload']);
          setTimeout( () => {
            this._router.navigate(['/main/viewMensagem']);
          }, 1000);
        }else if (response.status == '0x101'){
          window.alert('Erro na deleção da mensagem');
        }
      });
    }
  }

}
