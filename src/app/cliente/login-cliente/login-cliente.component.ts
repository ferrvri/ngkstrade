import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

// declare var Date: any;

@Component({
  selector: 'app-login-cliente',
  templateUrl: './login-cliente.component.html',
  styleUrls: ['./login-cliente.component.css']
})
export class LoginClienteComponent implements OnInit {

  constructor(private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    if (!localStorage.getItem('session') && !JSON.parse(localStorage.getItem('session'))[0] !== undefined){
      this._router.navigate(['login']);
    }else if (!localStorage.getItem('session') && !JSON.parse(localStorage.getItem('session'))[0].Cli_Nome){
      this._router.navigate(['clienteLogin']);
    }
  }

  usuario: string = "";
  senha: string = "";

  error : any = {};

  formLogin: FormGroup = new FormGroup({
    login: new FormControl(this.usuario, [Validators.required, Validators.minLength(6)]),
    senha: new FormControl(this.senha, [Validators.required, Validators.minLength(6)])
  });

  logIn(form):void{
    if (form.value.login.length < 1 || form.value.senha.length < 1){
      this.error = {title: '0x105', msg: 'Preencha todos os campos'};
      setTimeout( () => {
        this.error = {title: '', msg: ''};
      }, 1500)
    }else{
      this._http.post(
        'cliente/login.php',
        {
          usuario: form.value.login,
          senha: form.value.senha
        }
      ).subscribe( (response) => {
        if(response.status == '0x104'){
          let data = response.result[0];
          data.expires = new Date(new Date(Date.now()).setHours(+24)).toISOString();
          let arr = []
          arr.push(data);
          localStorage.setItem('session', JSON.stringify(arr));
          // this._router.navigate(['main/dashboard']);
          this._router.navigate(['loading']);
          setTimeout( () => {
            // this._router.navigate(['cliente']);
            window.location.href = '/sistema/#/cliente/dashboard';
            // window.location.href = '/#/cliente/dashboard';
          }, 1500);
        }else if(response.status == '0x101'){
          window.alert('Usuário ou senha incorretos!');
        }else if (response.result.length < 1){
          window.alert('Erro inesperado ocorreu!');
        }
      });
    }
  }

  returnToMain():void{
    window.location.href = "/sistema/#/login";
  }

  keydownLogin(e): void{
    switch (e.keyCode || e.which){
      case 13:
        this.logIn(this.formLogin);
      break;
    }    
  }

  passwordInputFocus(e){
    switch (e.keyCode || e.which){
      case 13:
        document.getElementById('passInput').focus();
      break;
    }  
  }
}
