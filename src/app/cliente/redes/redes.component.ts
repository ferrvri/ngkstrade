import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';

@Component({
  selector: 'app-redes',
  templateUrl: './redes.component.html',
  styleUrls: ['./redes.component.css']
})
export class RedesComponent implements OnInit {

  redes:any = [];
  emptyResult: boolean;

  queryRedes: string = '';
  resultsFilterRedes: any = [];
  
  constructor(private _http: HttpService,) { }

  ngOnInit() {  
    this._http.post(
      'cliente/selectRedes.php',
      {
        id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
      }
    ).subscribe( (response) => {
      if(response.status == '0x104'){
        response.result.forEach(element => {
          let o = {
            RED_ID: element.RED_ID,
            RED_Nome: element.RED_Nome,
            menuRow: false
          }
          this.redes.push(o);
        });
        this.filterArrayRedes(this.queryRedes, this.redes);
      }else if (response.status == '0x101'){
        this.emptyResult = true;
      }else if (response.result.length < 1){
        this.emptyResult = true;
      }
    });
   
  }

  filterArrayRedes(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterRedes = arr.filter( (e) => {
      return regexp.test(e.RED_Nome);
    });
  }

  searchRedes(e, string){
    this.filterArrayRedes(this.queryRedes, this.redes);
  }

}
