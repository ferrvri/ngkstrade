import { Component, OnInit } from '@angular/core';
import { HttpService } from '../../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { BlockOverflowProperty } from 'csstype';
import { environment } from '../../../environments/environment';

declare var showDialog: any;
declare var $: any;

@Component({
  selector: 'app-dashboard-cliente',
  templateUrl: './dashboard-cliente.component.html',
  styleUrls: ['./dashboard-cliente.component.css']
})
export class DashboardClienteComponent implements OnInit {

  p: number = 0;
  pesquisas = [];
  resultsFilterPesquisas = [];

  c = null

  otrs: any = [];
  userData: any = [];
  resultsJob: any = [];
  mensagens: any = [];
  checkins = [];

  emptyResult: boolean = false;
  showOTRImage: boolean = false;
  doneLoading: boolean = false;
  doneJobsLoad: boolean = false;
  filtroDataCheck: boolean = false;

  otrTitle: string = '';
  otrImage: string = '';
  filterFilial: string = '';
  filterRede: string = '';

  constructor(private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }

  date_sort_desc(date1, date2) {
    // if (new Date(date1.JOB_Data_JOB) > new Date(date2.JOB_Data_JOB)) return -1;
    // if (new Date(date1.JOB_Data_JOB) < new Date(date2.JOB_Data_JOB)) return 1;
    // return 0;

    return (<any>new Date(date2.JOB_Data_Hora_Input).getTime()) - (<any>new Date(date1.JOB_Data_Hora_Input).getTime())
  };

  ngOnInit(type?: string) {
    if (type && type == 'jobs') {
      let r = [];
      this.doneJobsLoad = false;
      this.userData = JSON.parse(localStorage.getItem('session'));
      this._http.post(
        'cliente/selectRedes.php',
        {
          id: this.userData[0].Usr_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          response.result.forEach(element => {
            this.userData[0].clientes.forEach(element2 => {
              this._http.post(
                'cliente/selectJobs.php',
                {
                  id: element2.aux_cliente_id,
                  rede_id: element.RED_ID,
                  dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                  datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().substring(0, 10)
                }
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  response.result.forEach((e) => {
                    r.push(e);
                  });
                }
              });
            });
          });
          setTimeout(() => {
            this.otrs = r;
            this.resultsJob = this.otrs;
            this.doneJobsLoad = true;
            this.resultsJob.sort(this.date_sort_desc);
          }, 3000);
        }
      });
    } else {
      if (localStorage.getItem('session')) {
        this.userData = JSON.parse(localStorage.getItem('session'));
        $('#dataInitPesquisa').datepicker();
        $('#dataEndPesquisa').datepicker();
        // let c = null;
        this.getInfos();
      }
    }
  }

  getInfos() {
    if (this.userData[0].Usr_Nivel == 'Vendedor' || this.userData[0].Usr_Nivel == 'Externo') {
      this._http.post(
        'selectQuiz.php',
        {
          tipo: 2
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this._http.post(
            'selectQuiz.php',
            {
              already: true,
              per_id: response.result[0].PER_ID,
              usr_id: this.userData[0].Usr_ID
            }
          ).subscribe((response2) => {
            if (response2.status != '0x102') {
              let days = ['Dom', 'Seg', 'Ter', 'Qua', 'Qui', 'Sex', 'Sab'];
              let dayName = days[new Date().getDay()];
              let arr = [];
              setTimeout(() => {
                if (arr.length > 0) {
                  this._router.navigate(['/cliente/cadQuizUser', { per_id: arr }]);
                } else {
                  this._router.navigate(['/cliente/dashboard']);
                }
              }, 1000);

              response.result.forEach(element => {
                let elem = element.PER_Dias_Semana.split(';');
                elem.forEach(element2 => {
                  if (element2 == dayName) {

                    clearInterval(this.c);
                    arr.push(element.PER_ID);
                  }
                });
              });
            }
          });
        }
      });

      if (!this.filtroDataCheck && !this.alreadySearch) {
        let r = [];

        this._http.post(
          'cliente/selectRedes.php',
          {
            id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            response.result.forEach(element => {
              this.userData[0].clientes.forEach(element2 => {
                this._http.post(
                  'cliente/selectJobs.php',
                  {
                    id: element2.aux_cliente_id,
                    rede_id: element.RED_ID,
                    dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                    datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().substring(0, 10)
                  }
                ).subscribe((response) => {
                  if (response.status == '0x104') {
                    response.result.forEach((e) => {
                      r.push(e);
                    });
                  } else {
                    this.doneJobsLoad = true;
                  }
                });
              });
            });
          }
          setTimeout(() => {
            if (r.length > this.otrs.length || true) {
              this.otrs = r;
              this.doneLoading = true;
              this.resultsJob = this.otrs;
              this.resultsJob.sort(this.date_sort_desc);
              this.doneJobsLoad = true;
              // this.getInfos();
            }
          }, 7000);
        });
        if (!this.filtroPromotorCheckIn && !this.alreadyFiltered) {
          this._http.post(
            'selectCheckin.php',
            {
              forCliente: true,
              id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              this.checkins = [];
              this.checkins = response.result;
            }
          });
        }
      }
    } else {
      if (!this.filtroDataCheck && !this.alreadySearch) {
        let r = [];

        this._http.post(
          'cliente/selectRedes.php',
          {
            id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            response.result.forEach(element => {
              this.userData[0].clientes.forEach(element2 => {
                this._http.post(
                  'cliente/selectJobs.php',
                  {
                    id: element2.aux_cliente_id,
                    rede_id: element.RED_ID,
                    dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().substring(0, 10),
                    datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 0).toISOString().substring(0, 10)
                  }
                ).subscribe((response2) => {
                  if (response2.status == '0x104') {
                    response2.result.forEach((e) => {
                      r.push(e);
                    });
                  } else {
                    this.doneJobsLoad = true;
                  }
                });
              });
            });
          }
        });
        setTimeout(() => {
          if (r.length > this.otrs.length) {
            this.otrs = r;
            this.doneLoading = true;
            this.resultsJob = this.otrs;
            this.resultsJob.sort(this.date_sort_desc);
            this.doneJobsLoad = true;
            // this.getInfos();
          }
        }, 7000);

        if (!this.filtroPromotorCheckIn) {
          this._http.post(
            'selectCheckin.php',
            {}
          ).subscribe((response) => {
            if (response.status == '0x104') {
              this.checkins = [];
              this.checkins = response.result;
            }
          });
        }
      }
    }
  }

  images = []

  openJOBImage(j) {
    this._http.post(
      'selectFotosExtras.php',
      {
        job_id: j.JOB_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.otrImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.otrTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;

        this.images = [];
        this.images.push(environment.apiUrl+'uploads/' + j.JOB_Foto);

        response.result.forEach(element => {
          this.images.push(environment.apiUrl+'uploads/' + element.JFOT_Foto);
        });

        if ((j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
          (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
          (j.JOB_Latitude === null && j.JOB_Longitude === null)) {
          showDialog({
            title: this.otrTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.otrTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      } else {
        this.otrImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.otrTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;
        console.log(this.otrImage);

        if ((j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
          (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
          (j.JOB_Latitude === null && j.JOB_Longitude === null)) {
          showDialog({
            title: this.otrTitle,
            image: this.otrImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.otrTitle,
            image: this.otrImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      }
    });
  }

  closeOTRImage() {
    // (<HTMLDialogElement>document.querySelector('#otrImageDialog')).close();
    this.showOTRImage = false;
    // this.showOTRImage = false;
    // this.otrImage = '';
    // document.getElementById('body').style.overflow =  "auto";
  }

  filterArrayRede(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsJob = arr.filter((e) => {
      return regexp.test(e.RED_Nome);
    });
  }

  filterArrayFilial(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsJob = arr.filter((e) => {
      return regexp.test(e.RFIL_Nome);
    });
  }

  enableFilter(model): void {
    if (model == true) {
      if (this.resultsJob.length != this.otrs.length) {
        this.ngOnInit('jobs');
      }
      model = false;
      this.ngOnInit();
    } else {
      model = true;
      clearInterval(this.c);
    }
  }

  openMensagem(m): void {
    this._router.navigate(['/main/innerload']);
    setTimeout(() => {
      this._router.navigate(['/cliente/viewMensagem', { msg: m.MSG_ID }])
    }, 1000);
  }

  clearData() {
    this.resultsJob = [];
    this.ngOnInit();
    (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
  }

  setDateOne() {
    if ((<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataInit')).value =
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2];
    }
  }

  setDateTwo() {
    if ((<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataEnd')).value =
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2];
    }
  }

  searchJob() {
    clearInterval(this.c);
    this.resultsJob = [];
    let r = [];
    this.doneJobsLoad = false;
    this._http.post(
      'cliente/selectRedes.php',
      {
        id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          this.userData[0].clientes.forEach(element2 => {
            this._http.post(
              'cliente/selectJobs.php',
              {
                id: element2.aux_cliente_id,
                rede_id: element.RED_ID,
                dataone: (<HTMLInputElement>document.getElementById('dataInit')).value,
                datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value
              }
            ).subscribe((response) => {
              if (response.status == '0x104') {
                response.result.forEach((e) => {
                  r.push(e);
                });
                this.doneJobsLoad = true;
              } else {
                this.doneJobsLoad = true;
              }
            });
          });
          setTimeout(() => {
            this.otrs = r;
            this.resultsJob = this.otrs;
            this.resultsJob.sort(this.date_sort_desc);
            this.emptyResult = false;
          }, 1500);
        });
      }
    });
  }

  clearDataPesquisa() {
    this.alreadySearch = false;
    this.resultsFilterPesquisas = this.pesquisas;
    (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value = '';
    (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value = '';
  }

  setDateOnePesquisa() {
    if ((<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value =
        (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[2];
    }
  }

  setDateTwoPesquisa() {
    if ((<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value =
        (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[2];
    }
  }

  alreadySearch: boolean = false;

  searchPesquisas() {
    this.resultsFilterPesquisas = [];
    this.alreadySearch = true;
    this._http.post(
      'selectPesquisas.php',
      {
        // type: 'jobs'
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          element.content.forEach(element2 => {
            element2.pesquisas = [];
            this._http.post(
              'selectPesquisas.php',
              {
                pesquisa: true,
                pro_id: element2.pro_id,
                dataone: (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[2]
                  + '-' + (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[1]
                  + '-' + (<HTMLInputElement>document.getElementById('dataInitPesquisa')).value.split('/')[0],
                datatwo:
                  (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[2]
                  + '-' + (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[1]
                  + '-' + (<HTMLInputElement>document.getElementById('dataEndPesquisa')).value.split('/')[0]
              }
            ).subscribe((response2) => {
              if (response2.status == '0x104') {
                response2.result.forEach(element3 => {
                  element2.pesquisas.push(element3);
                });
              }
            });
          });

          this.resultsFilterPesquisas.push(element);
        });
      }
    });
  }

  queryClientes: string = '';
  filterArrayClientes(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterPesquisas = arr.filter((e) => {
      return regexp.test(e.cliente_nome);
    });
  }

  queryProdutos: string = '';

  filterArrayProdutos(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    // this.resultsFilterPesquisas = arr.filter( (e) => {
    //   return regexp.test(e.cliente_nome);
    // });


    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach(element2 => {
        if (regexp.test(element2.pro_nome)) {
          o.content.push(element2);
        }
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  searchClientes(string) {
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayClientes(string, this.pesquisas);
    }
  }

  searchProdutos(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayProdutos(string, this.pesquisas);
    }
  }

  searchPesquisaRede(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaRedes(string, this.pesquisas);
    }
  }

  queryPesquisaRedes: string = '';

  filterArrayPesquisaRedes(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana,
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RED_Nome)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  searchPesquisaFilial(string) {
    this.alreadySearch = true;
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaFilial(string, this.pesquisas);
    }
  }

  daysBetween(date1, date2) {
    date2 = date2.split('-')[0] + '-' + date2.split('-')[2] + '-' + date2.split('-')[1];
    console.log(date1, date2);
    //Get 1 day in milliseconds
    let one_day = 1000 * 60 * 60 * 24;

    // Convert both dates to milliseconds
    let date1_ms = new Date(date1).getTime();
    let date2_ms = new Date(date2).getTime();

    // Calculate the difference in milliseconds
    let difference_ms = date2_ms - date1_ms;

    // Convert back to days and return
    return Math.round(difference_ms / one_day);
  }

  queryPesquisaFilial: string = '';

  filterArrayPesquisaFilial(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana,
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RFIL_Nome)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }

  queryPesquisaCidade: string = '';

  searchPesquisaCidade(string) {
    if (string.length < 1) {
      this.resultsFilterPesquisas = this.pesquisas;
    } else {
      this.filterArrayPesquisaCidade(string, this.pesquisas);
    }
  }

  filterArrayPesquisaCidade(model, arr) {
    this.resultsFilterPesquisas = [];
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");

    arr.forEach(element => {
      let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
      element.content.forEach((element2, index) => {
        o.content.push({
          pesquisas: [],
          pro_dias_semana: element2.pro_dias_semana || ";",
          pro_id: element2.pro_id,
          pro_nome: element2.pro_nome
        });
        element2.pesquisas.forEach(element3 => {
          if (regexp.test(element3.RFIL_Cidade)) {
            o.content[index].pesquisas.push(element3);
          }
        });
      });
      this.resultsFilterPesquisas.push(o);
    });
  }


  sendCritcEmail(ff, f) {
    let c = confirm('KSTrade Merchandising\r\n\r\nDeseja enviar o e-mail');
    if (c == true) {
      this._http.post(
        'enviarCriticEmail.php',
        {
          rede: f.RED_ID,
          redeNome: f.RED_Nome,
          filial: f.RFIL_ID,
          filialNome: f.RFIL_Nome,
          cliente: f.JOB_Cliente_ID,
          "obj": { "produto": ff.pro_nome, "rede": f.RED_Nome, "filial": f.RFIL_Nome, "datavencimento": f.pes_data_vencimento }
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          window.alert('KSTrade Merchandising\r\n\r\nE-mail enviado com sucesso!');
        } else {
          window.alert('KSTrade Merchandising\r\n\r\nUm erro ocorreu por favor contate-nos!');
        }
      });
    }
  }

  onlyCrt: boolean = false;

  onlyCritic() {
    if (!this.onlyCrt) {
      this.onlyCrt = true;
      this.resultsFilterPesquisas = [];
      this.pesquisas.forEach((element) => {
        let o = { cliente_id: element.cliente_id, cliente_nome: element.cliente_nome, dias_semana: element.dias_semana, content: [] };
        element.content.forEach((element2, index) => {
          o.content.push({
            pesquisas: [],
            pro_dias_semana: element2.pro_dias_semana || ";",
            pro_id: element2.pro_id,
            pro_nome: element2.pro_nome
          });

          element2.pesquisas.forEach(element3 => {
            if (element3.hasDif) {
              o.content[index].pesquisas.push(element3);
            }
          });
        });
        this.resultsFilterPesquisas.push(o);
      });
    } else {
      this.onlyCrt = false;
      this.resultsFilterPesquisas = this.pesquisas;
    }
  }


  filtroPromotorCheck = false;
  filtroPromotorCheckIn = false;
  filterPromotoresStr = '';
  filterPromotoresStrCheckin = '';

  filterPromotores(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultsJob = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
  }

  filterPromotoresCheckin(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.checkins = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
  }

  alreadyFiltered = false;
  filterEntrada(type) {
    this.alreadyFiltered = true;
    this.checkins = this.checkins.filter((a) => {
      if (a.type == type) {
        return true;
      }
    });
  }

}
