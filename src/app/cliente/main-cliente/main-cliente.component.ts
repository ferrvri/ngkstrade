import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpService } from '../../http.service';

declare var Date: any;
declare var $: any;

@Component({
  selector: 'app-main-cliente',
  templateUrl: './main-cliente.component.html',
  styleUrls: ['./main-cliente.component.css']
})
export class MainClienteComponent implements OnInit {

  yearNow = new Date().getFullYear();
  userData : any = [];

  userImage: string = '';

  constructor(private _router: Router, private _http: HttpService,) { }

  ngOnInit() {
    this._router.navigate(['/cliente/dashboard']);
    if (!localStorage.getItem('session')){
      console.log('ok');
      this._router.navigate(['login']);
    }else{
      this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
      this.userImage = 'defaultuser.png';

      if ((this.userData[0].Usr_Avatar === null || this.userData[0].Usr_Avatar === 'null' || this.userData[0].Usr_Avatar === 'NULL')){
        this.userImage = './assets/defaultuser.png';  
      }else{
        this.userImage = 'uploads/users/'+this.userData[0].Usr_Avatar;
      }

      var date1 = new Date(new Date.now());
      var date2 = new Date(JSON.parse(localStorage.getItem('session'))[0].expires.substring(0,10));
      var timeDiff = Math.abs(date2.getTime() - date1.getTime());
      var diffDays = timeDiff / (1000 * 3600 * 24); 
      
      if (diffDays >= 1){
        window.alert('Sessão expirada!');
        localStorage.removeItem('session');
        localStorage.removeItem('local');
        this._router.navigate(['login']);
      }    
      
      let interval = setInterval( _ => {
        this._http.post('cliente/selectClienteStatus.php', {
          cliente_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
        }).subscribe( (res: any) => {
          if (res.status == '0x104'){
            localStorage.removeItem('session')
            clearInterval(interval);
            window.location.reload();
          }
        })
      }, 10000)
    }
  }

  logOut():void{
    localStorage.removeItem('session');
    this._router.navigate(['loading']);
    setTimeout( () => {
      this._router.navigate(['login']);
    }, 1500);
  }

  closeMenu(){
    $('#menu-order').click();
  }

}
