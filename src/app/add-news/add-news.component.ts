import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router, ActivatedRoute } from '@angular/router';
import { GeolocationServiceService } from '../geolocation-service.service';
import { environment } from '../../environments/environment';

declare var $: any;
declare var escape: any;

@Component({
  selector: 'app-add-news',
  templateUrl: './add-news.component.html',
  styleUrls: ['./add-news.component.css']
})
export class AddNewsComponent implements OnInit {

  titulo: string = '';
  corpo: string = '';

  hasgroup = false;

  groupoOK = false;
  grupo = 'Selecionar grupo';
  grupo_obj: any = {};
  grupos = [];
  tempo = 15;

  cardSimples: boolean = false;
  cardImage: boolean = false;

  toUploadImage: any = [];

  pageParams: any;

  hasNewImage = false;


  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router, private _activatedRoute: ActivatedRoute) { }


  decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }


  ngOnInit() {
    this._geo.getPosition(c => { this.latLong = c.lat + ';' + c.lng });

    setTimeout(() => {
      $('.dropify').dropify();

      // Used events
      var drEvent = $('#input-file-events').dropify();

      drEvent.on('dropify.beforeClear', function (event, element) {
        return confirm("Você deseja realmente apagar \"" + element.file.name + "\" ?");
      });

      drEvent.on('dropify.afterClear', function (event, element) {
        alert('Arquivo apagado');
      });

      drEvent.on('dropify.errors', function (event, element) {
        console.log('Erro!');
      });

      var drDestroy = $('#input-file-to-destroy').dropify();
      drDestroy = drDestroy.data('dropify');
      $('#toggleDropify').on('click', function (e) {
        e.preventDefault();
        if (drDestroy.isDropified()) {
          drDestroy.destroy();
        } else {
          drDestroy.init();
        }
      })
    }, 300);

    this._activatedRoute.paramMap.subscribe((data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar') {
      this._http.post(
        'app/selectNews.php',
        {
          unique: true,
          new_id: this.pageParams.params.new_id
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          console.log('editar', response.result[0]);
          if (response.result[0].app_type == 'image') {
            this.cardImage = true;
            this.toUploadImage.push({ id: null, foto: environment.apiUrl+'uploads/news/' + response.result[0].app_image });
            // this.toUploadImage.push(response.result[0].app_image)

            if (response.result[0].app_fotos_extras.length > 0) {

              response.result[0].app_fotos_extras.forEach(element => {
                this.toUploadImage.push({ id: element.new_foto_id, foto: environment.apiUrl+'uploads/news/' + element.new_foto })
              });

            }
          } else {
            this.cardSimples = true;
          }

          this.titulo = this.decode_utf8(response.result[0].app_title);
          this.corpo = this.decode_utf8(response.result[0].app_text);
          this.tempo = response.result[0].app_timestamp;
        }
      });
    }

  }

  check(arg) {
    switch (arg) {
      case 'sim':
        this.cardImage = false;
        break;
      case 'img':
        this.cardSimples = false;
        setTimeout(() => {
          this.ngOnInit();
        }, 200);
        break;
    }
  }

  getGroups() {
    this._http.post(
      'app/selectNews.php',
      {
        only_group: true
      }
    ).subscribe((response) => {
      console.log(response);
      this.grupos = response.result;
    })
  }

  setGrupo(g) {
    this.grupo = g.group_nome;
    this.grupo_obj = g;
    this.groupoOK = false;
  }

  uploadChanges(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        if (this.pageParams.params.page == 'editar') {
          this.hasNewImage = true
          this.toUploadImage.push({ id: null, foto: (reader.result + '') });
        } else {
          this.toUploadImage.push((reader.result + '').split(',')[1]);
        }
      };
    }
  }

  removeFiles() {
    this.toUploadImage = [];
    $('.dropify-clear').click();
  }

  cadNoticia() {
    if (this.pageParams.params.page == 'editar') {
      if ((this.titulo.length < 1 || this.titulo === undefined)
        || (this.corpo.length < 1 || this.corpo === undefined)) {
        window.alert('KSTrade - Adicionar noticia\r\n\r\nVocê deve preenchar os campos para adicionar!');
      } else {
        if (this.hasNewImage) {
          let news_fotos = []

          this.toUploadImage.forEach(element => {
            if (typeof element == 'object') {
              if (element.id == null && element.foto.indexOf('data:image/') != -1) {
                news_fotos.push(element.foto.split(',')[1])
              }
            }
          });

          let nameOfFile = '';

          if (this.cardImage) {
            nameOfFile = 'noticia-' + Math.ceil(Math.random() * 1000) + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg'
          }

          let o = {
            update: true,
            new_image: news_fotos.length > 0 ? true : false,
            id: this.pageParams.params.new_id,
            type: this.cardImage == true ? 'image' : this.cardSimples == true ? 'simple' : '',
            titulo: this.titulo,
            corpo: this.corpo,
            tempo: this.tempo,
            anexo: news_fotos,
            foto: nameOfFile,
            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Noticia[' + this.pageParams.params.new_id + ']'
          };

          this._http.post(
            'insertNoticia.php',
            o
          ).subscribe((response) => {
            if (response.status == '0x104') {
              window.alert('KSTrade - Adicionar noticia\r\n\r\Editada com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout(() => {
                this._router.navigate(['/main/listaNoticia']);
              }, 1000);
            }
          });

        } else {

          let o = {
            update: true,
            new_image: false,
            id: this.pageParams.params.new_id,
            type: this.cardImage == true ? 'image' : this.cardSimples == true ? 'simple' : '',
            titulo: this.titulo,
            corpo: this.corpo,
            tempo: this.tempo,
            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Edição - Noticia[' + this.pageParams.params.new_id + ']'
          };

          this._http.post(
            'insertNoticia.php',
            o
          ).subscribe((response) => {
            if (response.status == '0x104') {
              window.alert('KSTrade - Adicionar noticia\r\n\r\Editada com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout(() => {
                this._router.navigate(['/main/listaNoticia']);
              }, 1000);
            }
          });
        }
      }
    } else {
      if ((this.titulo.length < 1 || this.titulo === undefined)
        || (this.corpo.length < 1 || this.corpo === undefined)) {
        window.alert('KSTrade - Adicionar noticia\r\n\r\nVocê deve preenchar os campos para adicionar!');
      } else {
        let nameOfFile = '';
        if (this.cardImage) {
          nameOfFile = 'noticia-' + Math.ceil(Math.random() * 1000) + new Date(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + new Date().getDate()).toISOString().substring(0, 10) + '.jpg'
        }

        this._http.post(
          'insertNoticia.php',
          {
            type: this.cardImage == true ? 'image' : this.cardSimples == true ? 'simple' : '',
            titulo: this.titulo,
            corpo: this.corpo,
            tempo: this.tempo,
            anexo: this.toUploadImage,
            foto: nameOfFile,
            //log
            usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
            usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
            usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
            nome_pc: this._geo.GetComputerName(),
            latlong: this.latLong,
            funcao: 'Inserção - Noticia'
          }
        ).subscribe((response) => {
          if (response.status == '0x104') {
            if (this.hasgroup == true) {
              console.log(this.grupo_obj);
              this._http.post(
                'insertNoticiaGrupo.php',
                {
                  not_id: response.id,
                  id: this.grupo_obj.group_id,

                  //log
                  usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
                  usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
                  usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
                  nome_pc: this._geo.GetComputerName(),
                  latlong: this.latLong,
                  funcao: 'Inserção - Noticia Grupo'
                }
              ).subscribe((response) => {
                if (response.status == '0x104') {
                  window.alert('KSTrade - Adicionar noticia\r\n\r\nAdicionada com sucesso!');
                  this._router.navigate(['/main/innerload']);
                  setTimeout(() => {
                    this._router.navigate(['/main/addNoticia']);
                  }, 1000);
                }
              });
            } else {
              window.alert('KSTrade - Adicionar noticia\r\n\r\nAdicionada com sucesso!');
              this._router.navigate(['/main/innerload']);
              setTimeout(() => {
                this._router.navigate(['/main/addNoticia']);
              }, 1000);
            }
          }
        });
      }
    }
  }

  removeImage(index, c?) {
    if (this.pageParams.params.page == 'editar' && this.hasNewImage == false) {
      this._http.post(
        'removeFotoExtraNews.php',
        {
          id: c.id,

           //log
           usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
           usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
           usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
           nome_pc: this._geo.GetComputerName(),
           latlong: this.latLong,
           funcao: 'Deleção - Noticia Foto Extra'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.toUploadImage.splice(index, 1);
        }
      });
    } else {
      this.toUploadImage.splice(index, 1);
      if (index == 0) {
        $(".dropify-clear").click();
      }
    }
  }
}
