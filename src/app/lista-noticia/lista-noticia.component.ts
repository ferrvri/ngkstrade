import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

declare var escape: any;
declare var $: any;

import * as _swal from 'sweetalert';
import { SweetAlert } from 'sweetalert/typings/core';
import { GeolocationServiceService } from '../geolocation-service.service';
import { environment } from '../../environments/environment';
const swal: SweetAlert = _swal as any;

@Component({
  selector: 'app-lista-noticia',
  templateUrl: './lista-noticia.component.html',
  styleUrls: ['./lista-noticia.component.css']
})
export class ListaNoticiaComponent implements OnInit {

  cards : any = [];
  noticia: any = {
    grupos: []
  };

  latLong = '';
  constructor( private _geo: GeolocationServiceService, private _router: Router, private _http: HttpService, private _sanitization: DomSanitizer ) { }

  decode_utf8(s) {
    return decodeURIComponent(escape(s));
  }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this._http.post(
      'app/selectNews.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.cards = [];
        response.result.forEach(element => {
          console.log(element.app_image);
          element.app_title = this.decode_utf8(element.app_title);
          element.app_text = this.decode_utf8(element.app_text);
          element.app_image = this._sanitization.bypassSecurityTrustStyle(`url(${environment.apiUrl}uploads/news/${element.app_image}) center / cover`);
          this.cards.push(element);
        });
      }
      
      this.noticia.grupos = [];
        this._http.post(
          'app/selectNews.php',
          {
            hasgroup: true
          }
        ).subscribe( (response2) => {
          if (response2.status == '0x104'){
            response2.result.forEach(element => {
              element.content.forEach(el => {
                el.activated = false;
                el.app_title = this.decode_utf8(el.app_title);
                el.app_text = this.decode_utf8(el.app_text);
                el.app_image = this._sanitization.bypassSecurityTrustStyle(`url(${environment.apiUrl}uploads/news/${el.app_image}) center / cover`);
              });
              this.noticia.grupos.push(element);
            });
            console.log(this.noticia)
          }
        });
    });
  }

  removeNew(n, index, type, sIndex?){
    let c = confirm('KSTrade Merchandising - Lista de noticias\r\n\r\nDeseja realmente remover esta noticia?');
    if (c == true){
      this._http.post(
        'removeNew.php',
        {
          new_id: n.app_id,

           //log
           usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
           usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
           usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
           nome_pc: this._geo.GetComputerName(),
           latlong: this.latLong,
           funcao: 'Deleção - Noticia[' + n.app_id + ']'
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade Merchandising - Lista de noticias\r\n\r\nRemovido com sucesso!');
          if (type == 's') {
            this.cards.splice(index, 1)
          }else if (type == 'g'){
            this.noticia.grupos[index].content.splice(sIndex, 1)
          }
        }
      });
    }
  }

  deleteGroup(n, index){
    let c = confirm('KSTrade Merchandising - Lista de noticias\r\n\r\nDeseja realmente remover este grupo?');
    if (c == true){
      this._http.post(
        'removeGroupNews.php',
        {
          group_id: n.group_id,

           //log
           usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
           usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
           usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
           nome_pc: this._geo.GetComputerName(),
           latlong: this.latLong,
           funcao: 'Deleção - Grupo de noticia[' + n.group_id + ']'
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          window.alert('KSTrade Merchandising - Lista de noticias\r\n\r\nRemovido com sucesso!');
          this.noticia.grupos.splice(index, 1);
        }
      });
    }
  }

  upgradeAccordion(child, index){
    if(child.activated == true){
      document.getElementById('accordion_'+index).className = 
      document.getElementById('accordion_'+index).className.replace('resizeDown', '');
      document.getElementById('accordion_'+index).className += 'resizeUp';
    }else{
      document.getElementById('accordion_'+index).className = 
      document.getElementById('accordion_'+index).className.replace('resizeUp', '');
      document.getElementById('accordion_'+index).className += 'resizeDown';
    }

    child.activated = !child.activated;
  }

  comentarios(data){
    this._router.navigate(['/main/listarComentarios', {app_id: data.app_id, app_title: data.app_title}]);
  }

  showFotosExtras(c){
    console.log(c)

    let div = document.createElement('div');
    div.innerHTML = `
      <div class="mdl-grid">
    `;

    c.app_fotos_extras.forEach( (element, index) => {
      div.innerHTML += 
      `<img src="${environment.apiUrl}uploads/news/${element.new_foto}" style="display: inline-table; margin: 4px; float: left" class="mdl-cell--6-col"/>`
      
      if (index == (c.app_fotos_extras.length -1)){
        div.innerHTML += '</div>'
      }
    });

    swal({
      title: c.app_title + ' - Fotos extras',
      content: div as any,
      closeOnClickOutside: true,
      // buttons: []
    })
  }

}
