import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var html2canvas: any;
declare var jsPDF: any;

@Component({
  selector: 'app-lista-redes',
  templateUrl: './lista-redes.component.html',
  styleUrls: ['./lista-redes.component.css']
})
export class ListaRedesComponent implements OnInit {

  userData = []
  p: number = 0;

  redes: any = [];
  emptyResult: boolean;

  queryRedes: string = '';
  resultsFilterRedes: any = [];

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService,) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat+';'+c.lng)
    this.userData = JSON.parse(localStorage.getItem('session'));
    this._http.post(
      'selectRedes.php',
      {}
    ).subscribe((response) => {
      if (response.status == '0x104') {
        response.result.forEach(element => {
          let o = {
            RED_ID: element.RED_ID,
            RED_Nome: element.RED_Nome,
            menuRow: false
          }
          this.redes.push(o);
        });
        this.filterArrayRedes(this.queryRedes, this.redes);
      } else if (response.status == '0x101') {
        this.emptyResult = true;
      } else if (response.result.length < 1) {
        this.emptyResult = true;
      }
    });

  }

  filterArrayRedes(model, arr) {
    var regexp = new RegExp((".*" + model.split("").join('.*') + ".*"), "i");
    this.resultsFilterRedes = arr.filter((e) => {
      return regexp.test(e.RED_Nome);
    });
  }

  searchRedes(e, string) {
    this.filterArrayRedes(this.queryRedes, this.redes);
  }

  makePDF() {
    document.getElementById('redesTable').style.display = 'block';
    html2canvas(document.getElementById('redesTable')).then((canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("", '_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onLoad="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
      document.getElementById('redesTable').style.display = 'none';
    });
  }

  removeRede(id, index) {
    let c = confirm("KSTrade Merchandising - Listagem de Redes\r\n\r\nDeseja realmente remover a rede?")

    if (c == true) {
      this._http.post(
        'deleteRede.php',
        {
          id: id,
          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Rede[' + id + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.resultsFilterRedes.splice(index, 1);
        }
      });
    }
  }


}
