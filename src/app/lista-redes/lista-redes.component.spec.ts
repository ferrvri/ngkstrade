import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRedesComponent } from './lista-redes.component';

describe('ListaRedesComponent', () => {
  let component: ListaRedesComponent;
  let fixture: ComponentFixture<ListaRedesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRedesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRedesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
