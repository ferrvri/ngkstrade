import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { HttpService } from '../http.service';

declare var html2canvas: any;
declare var $: any;

@Component({
  selector: 'app-promotor-produtividade',
  templateUrl: './promotor-produtividade.component.html',
  styleUrls: ['./promotor-produtividade.component.css']
})
export class PromotorProdutividadeComponent implements OnInit {

  resultsProdutividade: any = [];
  resultsFilterProdutividade: any = [];

  queryPromotor:string = '';

  emptyResult: boolean = false;
  doneLoading: boolean = false;
  p: number = 0; 

  constructor(private _router: Router, private _http: HttpService, private _activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    $('#dataInit').datepicker();
    $('#dataEnd').datepicker();
    this._http.post(
      'selectPromotor.php',
      {
        group: true
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        let results = [];
        response.result.forEach(element => {
          this._http.post(
            'selectRanking.php',
            {
              nome: element.PRO_Nome,
              dataone: new Date(Date.now()).toISOString().substring(0,10),
              datatwo: new Date(Date.now()).toISOString().substring(0,10)
            }
          ).subscribe( (response) => {
            if (response.status == '0x104'){
              response.result.forEach(elem => {
                elem.pro_nome = element.PRO_Nome;
                elem.pro_status = element.PRO_Status;
                elem.pro_telefone = element.PRO_Telefone;
                results.push(elem);
              });
              results.sort( (a, b) => {
                return b.total-a.total;
              });
            }
          });
        });
        this.resultsProdutividade = this.resultsFilterProdutividade = results;
        this.doneLoading = true;
      }
    });

    
  }

  filterArrayPromotores(model, arr){
    var regexp = new RegExp((".*"+model.split("").join('.*')+ ".*"), "i");
    this.resultsFilterProdutividade = arr.filter( (e) => {
      return regexp.test(e.pro_nome);
    });
  }

  searchPromotor(p){
    if (p.length < 1) this.resultsFilterProdutividade = this.resultsProdutividade;

    this.filterArrayPromotores(p, this.resultsFilterProdutividade);
  }

  makePDF() {
    document.getElementById('tablePromotorProdutividade').style.display = 'block';
    html2canvas(document.getElementById('tablePromotorProdutividade')).then( (canvas) => {
      console.log((<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1));
      let w = window.open("",'_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="'+(<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1)+'" /></body></html>');
      document.getElementById('tablePromotorProdutividade').style.display = 'none';
    });
  }

  alreadySearch: boolean = false;

  setDateOne(){
    if ((<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] === undefined){
      (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    }else{
      (<HTMLInputElement>document.getElementById('dataInit')).value = 
      (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '/' +
      (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0] + '/' +
      (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2];
    }
  }

  setDateTwo(){
    if ((<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] === undefined){
      (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
    }else{
      (<HTMLInputElement>document.getElementById('dataEnd')).value = 
      (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '/' +
      (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0] + '/' +
      (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2];
    }
  }

  searchProdutividade(){
    this.doneLoading = false;
    this.alreadySearch = true;
    if( (<HTMLInputElement>document.getElementById('dataInit')).value.length > 0 && (<HTMLInputElement>document.getElementById('dataEnd')).value.length > 0){
      this._http.post(
        'selectPromotor.php',
        {
          group: true
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          let results = [];
          response.result.forEach(element => {
            this._http.post(
              'selectRanking.php',
              {
                nome: element.PRO_Nome,
                dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2] + '-' +
                (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '-' +
                (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0],
                datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2] + '-' +
                (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '-' +
                (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0]
              }
            ).subscribe( (response) => {
              if (response.status == '0x104'){
                response.result.forEach(elem => {
                  elem.pro_nome = element.PRO_Nome;
                  elem.pro_status = element.PRO_Status;
                  elem.pro_telefone = element.PRO_Telefone;
                  results.push(elem);
                });
                results.sort( (a, b) => {
                  return b.total-a.total;
                });
              }
            });
          });
          this.resultsProdutividade = this.resultsFilterProdutividade = results;
          this.doneLoading = true;
        }
      });
    }else{
      (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
      (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    }     
  }

  clearData(){
    this.alreadySearch = false;
    (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
  }

}
