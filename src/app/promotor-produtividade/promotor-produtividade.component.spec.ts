import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PromotorProdutividadeComponent } from './promotor-produtividade.component';

describe('PromotorProdutividadeComponent', () => {
  let component: PromotorProdutividadeComponent;
  let fixture: ComponentFixture<PromotorProdutividadeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PromotorProdutividadeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PromotorProdutividadeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
