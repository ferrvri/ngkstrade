import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaRotasComponent } from './lista-rotas.component';

describe('ListaRotasComponent', () => {
  let component: ListaRotasComponent;
  let fixture: ComponentFixture<ListaRotasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListaRotasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaRotasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
