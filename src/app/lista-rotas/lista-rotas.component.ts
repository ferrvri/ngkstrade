import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';

declare var showDialog: any;

@Component({
  selector: 'app-lista-rotas',
  templateUrl: './lista-rotas.component.html',
  styleUrls: ['./lista-rotas.component.css']
})
export class ListaRotasComponent implements OnInit {

  promotores = []
  promotorSelected = false;
  doneLoading = true;
  emptyResult = false;
  rotas = []
  rotasResults = [];

  promotorID = 0;

  constructor( private _http: HttpService, ) { }

  ngOnInit() {
    this._http.post(
      'selectPromotor.php',
      {
        group: true
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          if (element.PRO_Nome.indexOf('PJ') != -1){
            this.promotores.push(element)
          }
        });
        // this.promotores = response.result;
      }
    });
  }

  searchPromotor(promotor){
    this.promotorSelected = true
    this.promotorID = parseInt(promotor);
    this._http.post(
      'rota/selectRota.php',
      {
        promotor_id: parseInt(promotor),
        dataInit: new Date(new Date().getFullYear(), new Date().getMonth(), 1).toISOString().split('T')[0], 
        dataFim: new Date(new Date().getFullYear(), new Date().getMonth() +1, 1).toISOString().split('T')[0] 
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.emptyResult = false;
        this.rotasResults = this.rotas = response.result
      }else{
        this.emptyResult = true;
      }
    })
  }

  clearData (){
    this.rotasResults = this.rotas;
  }

  searchRota(init, end){
    this._http.post(
      'rota/selectRota.php',
      {
        promotor_id: this.promotorID,
        dataInit: init, 
        dataFim: end 
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        this.emptyResult = false;
        this.rotasResults = this.rotas = response.result
      }else{
        this.emptyResult = true;
      }
    })
  }

  openInfos(rota_id){
    this._http.post(
      'rota/selectAuxRota.php',
      {
        rota_id: rota_id 
      }
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        let myCoords = []
        response.result.forEach(element => {
          myCoords.push({
            lat: element.aux_pro_rota_lat,
            long: element.aux_pro_rota_long
          });
        });

        showDialog({
          forLocal: true,
          local: myCoords
        });
      }
    })
  }

}
