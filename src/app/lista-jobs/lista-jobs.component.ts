import { Component, OnInit } from '@angular/core';
import { HttpService } from '../http.service';
import { Router } from '@angular/router';
import { _ } from 'core-js';
import { environment } from '../../environments/environment';
import { GeolocationServiceService } from '../geolocation-service.service';

declare var $: any;
declare var dialogPolyfill: any;
declare var HTMLDialogElement: any;
declare var showDialog: any;
declare var html2canvas: any;

@Component({
  selector: 'app-lista-jobs',
  templateUrl: './lista-jobs.component.html',
  styleUrls: ['./lista-jobs.component.css']
})
export class ListaJobsComponent implements OnInit {
  HTMLDialogElement: any;
  emptyResult = false;
  showJobImage = false;

  jobImage: string = "";
  jobTitle: string = "";

  jobs = [];
  resultsFilterJobs: any = [];

  queryJobs: string = '';

  totalJobs: number = 0;
  p: number = 0;

  userData = [];
  // Filtros novos
  clientes = [];
  redes = [];
  filiais = [];
  promotores = [];

  latLong = '';
  constructor(private _geo: GeolocationServiceService, private _http: HttpService, private _router: Router) { }

  ngOnInit() {
    this._geo.getPosition(c => this.latLong = c.lat + ';' + c.lng)
    this.userData.push(JSON.parse(localStorage.getItem('session'))[0]);
    // $('#dataInit').datepicker();
    // $('#dataEnd').datepicker();
    if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador') {
      this._http.post(
        'selectClientes.php',
        {
          coordenador: true,
          coordId: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.clientes = response.result;

          this.emptyResult = true;

          this._http.post(
            'selectJobs.php',
            {
              clientes: response.result,
              filter: 'data',
              dataone: new Date(new Date().getFullYear(), new Date().getMonth(), 0).toISOString().substring(0, 10),
              datatwo: new Date(new Date().getFullYear(), new Date().getMonth() + 1, 1).toISOString().substring(0, 10)
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              response.result.forEach((e) => {
                this.jobs.push(e);
              });

              this.resultsFilterJobs = this.jobs;
              this.resultsFilterJobs.sort(this.date_sort_desc);
              this.emptyResult = false;
            }
          });
        }
      });

      this._http.post(
        'selectTotais.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          if (this.totalJobs != response.result.totalJobs) {
            this.totalJobs = response.result.totalJobs;
          }
        }
      });
    } else {
      this._http.post(
        'selectClientes.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.clientes = response.result;
          this._http.post(
            'selectRedes.php',
            {}
          ).subscribe((response2) => {
            if (response2.status == '0x104') {
              this.redes = response2.result
              this._http.post(
                'selectFilial.php',
                {
                  all: true
                }
              ).subscribe((response3) => {
                if (response3.status == '0x104') {
                  this.filiais = response3.result;
                  this._http.post(
                    'selectPromotor.php',
                    {
                      group: true
                    }
                  ).subscribe((response4) => {
                    if (response4.status == '0x104') {
                      this.promotores = response4.result;
                    }
                  });
                }
              });
            }
          });
        }
      });

      this._http.post(
        'selectJobs.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          response.result.forEach((e) => {
            this.jobs.push(e);
          });
          this.filterArrayJobs(this.queryJobs, this.jobs);
        } else if (response.status == '0x101') {
          this.emptyResult = true;
        }
      });

      this._http.post(
        'selectTotais.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.totalJobs = response.result.totalJobs;
        }
      });
    }
  }

  images = [];

  openImagemJob(j) {
    this._http.post(
      'selectFotosExtras.php',
      {
        job_id: j.JOB_ID
      }
    ).subscribe((response) => {
      if (response.status == '0x104') {
        this.jobImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.jobTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;

        this.images = [];
        this.images.push(environment.apiUrl+'uploads/' + j.JOB_Foto);

        response.result.forEach(element => {
          this.images.push(environment.apiUrl+'uploads/' + element.JFOT_Foto);
        });

        if ((j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
          (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
          (j.JOB_Latitude === null && j.JOB_Longitude === null)) {
          showDialog({
            title: this.jobTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.jobTitle,
            images: this.images,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      } else {
        this.jobImage = environment.apiUrl+'uploads/' + j.JOB_Foto;
        this.jobTitle = j.Cli_Nome + ' - ' + j.JOB_Data_Hora_Input2 + ' - ' + j.JOB_Obs;

        if ((j.JOB_Latitude == '' && j.JOB_Longitude == '') ||
          (j.JOB_Latitude == 'null' && j.JOB_Longitude == 'null') ||
          (j.JOB_Latitude === null && j.JOB_Longitude === null)) {
          showDialog({
            title: this.jobTitle,
            image: this.jobImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        } else {
          showDialog({
            coords: [j.JOB_Latitude, j.JOB_Longitude],
            title: this.jobTitle,
            image: this.jobImage,
            infos: [j.JOB_PRO_Modelo, j.JOB_PRO_Plataforma, j.JOB_PRO_Versao, j.JOB_PRO_Manufact],
            Usr_Nivel: this.userData[0].Usr_Nivel,
            Usr_Nome: this.userData[0].Usr_Nome,
            JOB_ID: j.JOB_ID
          });
        }
      }
    });
  }

  closeJobImage() {
    // (<HTMLDialogElement>document.querySelector('#jobImageDialog')).close();
    this.showJobImage = false;
    this.jobImage = '';
    this.jobTitle = '';
  }

  date_sort_desc(date1, date2) {
    // if (new Date(date1.JOB_Data_JOB) > new Date(date2.JOB_Data_JOB)) return -1;
    // if (new Date(date1.JOB_Data_JOB) < new Date(date2.JOB_Data_JOB)) return 1;
    // return 0;

    console.log('passing');
    return (<any>new Date(date2.JOB_Data_Hora_Input).getTime()) - (<any>new Date(date1.JOB_Data_Hora_Input).getTime())
  };

  filterArrayJobs(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultsFilterJobs = arr.filter((e) => {
      return regexp.test(e.Cli_Nome);
    });
  }

  searchJobs(string) {
    if (string.length < 1) {
      this.pdfReady = false;
      this.resultsFilterJobs = this.jobs;
    } else {
      this.pdfReady = true;
      this.filterArrayJobs(string, this.resultsFilterJobs);

      this._http.post(
        'selectRedesInCliente.php',
        {
          clienteNome: string
        }
      ).subscribe((response2) => {
        if (response2.status == '0x104') {
          this.redes = response2.result
        }
      });
    }
  }

  queryPromotor: string = '';
  filterArrayPromotor(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultsFilterJobs = arr.filter((e) => {
      return regexp.test(e.PRO_Nome);
    });
  }

  searchPromotor(string) {
    if (string.length < 1) {
      this.pdfReady = false;
    } else {
      this.pdfReady = true;
    }
    this.filterArrayPromotor(string, this.resultsFilterJobs);
  }

  pdfReady = false;
  makePDF() {
    document.getElementById('jobsTable').style.display = 'block';
    html2canvas(document.getElementById('jobsTable')).then((canvas) => {
      let w = window.open("", '_blank');
      w.document.write('<html><head><title>KSTrade Imprimir</title></head><body onload="window.print();window.close();"><img src="' + (<HTMLCanvasElement>canvas).toDataURL('image/jpeg', 1) + '" /></body></html>');
      document.getElementById('jobsTable').style.display = 'none';
    });
  }

  alreadySearch: boolean = false;

  clearData() {
    this.alreadySearch = false;
    this.jobs = []
    this.ngOnInit();
    (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
  }

  setDateOne() {
    if ((<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataInit')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataInit')).value =
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataInit')).value.split('/')[2];
    }
  }

  setDateTwo() {
    if ((<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] === undefined) {
      (<HTMLInputElement>document.getElementById('dataEnd')).value = '';
    } else {
      (<HTMLInputElement>document.getElementById('dataEnd')).value =
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[1] + '/' +
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[0] + '/' +
        (<HTMLInputElement>document.getElementById('dataEnd')).value.split('/')[2];
    }
  }

  searchJob() {
    this.alreadySearch = true;
    if (JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel == 'Coordenador') {
      this._http.post(
        'selectClientes.php',
        {
          coordenador: true,
          coordId: JSON.parse(localStorage.getItem('session'))[0].Usr_ID
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.jobs = [];
          this.emptyResult = true;
          this.clientes = response.result;
          
          this._http.post(
            'selectJobs.php',
            {
              clientes: response.result,
              filter: 'data',
              dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.trim(),
              datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.trim()
            }
          ).subscribe((response) => {
            if (response.status == '0x104') {
              response.result.forEach((e) => {
                this.jobs.push(e);
              });

              this.resultsFilterJobs = this.jobs;
              this.resultsFilterJobs.sort(this.date_sort_desc);
              this.emptyResult = false;
            }
          });

          setTimeout(() => {
            this.resultsFilterJobs = this.jobs;
            this.resultsFilterJobs.sort(this.date_sort_desc);
            this.emptyResult = false;
          }, 1500);
        }
      });
    } else {
      this._http.post(
        'selectJobs.php',
        {
          filter: 'data',
          kind: 'pagar',
          dataone: (<HTMLInputElement>document.getElementById('dataInit')).value.trim(),
          datatwo: (<HTMLInputElement>document.getElementById('dataEnd')).value.trim()
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.jobs = [];
          this.emptyResult = false;
          response.result.forEach((e) => {
            this.jobs.push(e);
          });
          this.filterArrayJobs(this.queryJobs, this.jobs);
        } else if (response.status == '0x101') {
          this.emptyResult = true;
        }
      });
      this._http.post(
        'selectTotais.php',
        {}
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this.totalJobs = response.result.totalJobs;
        }
      });
    }
  }


  redeQuery: string = '';

  searchRede(string) {
    if (string.length < 1) {
      this.resultsFilterJobs = this.jobs;
    } else {
      this.filterArrayRede(string, this.resultsFilterJobs);
      this._http.post(
        'selectFilialInRede.php',
        {
          nomeRede: string
        }
      ).subscribe((response3) => {
        if (response3.status == '0x104') {
          this.filiais = response3.result;

          this._http.post(
            'selectPromotor.php',
            {
              group: true
            }
          ).subscribe((response4) => {
            if (response4.status == '0x104') {
              this.promotores = response4.result;
            }
          });
        }
      });
    }
  }

  filterArrayRede(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultsFilterJobs = arr.filter((e) => {
      return regexp.test(e.RED_Nome);
    });
  }


  filialQuery: string = '';

  searchFilial(string) {
    if (string.length < 1) {
      this.resultsFilterJobs = this.jobs;
    } else {
      this.filterArrayFilial(string, this.resultsFilterJobs);
    }
  }


  filterArrayFilial(model, arr) {
    var regexp = new RegExp((".*" + model + ".*"), "i");
    this.resultsFilterJobs = arr.filter((e) => {
      return regexp.test(e.RFIL_Nome);
    });
  }

  excluirJob(j) {
    let c = confirm('KSTrade Merchandising - Lista de Jobs\r\n\r\nDeseja realmente apagar o Job?');
    if (c == true) {
      this._http.post(
        'removeJob.php',
        {
          job_id: j.JOB_ID,

          //log
          usuario_id: JSON.parse(localStorage.getItem('session'))[0].Usr_ID,
          usuario_nome: JSON.parse(localStorage.getItem('session'))[0].Usr_Nome,
          usuario_nivel: JSON.parse(localStorage.getItem('session'))[0].Usr_Nivel,
          nome_pc: this._geo.GetComputerName(),
          latlong: this.latLong,
          funcao: 'Deleção - Job[' + j.JOB_ID + ']'
        }
      ).subscribe((response) => {
        if (response.status == '0x104') {
          this._router.navigate(['/main/innerload']);
          setTimeout(() => {
            this._router.navigate(['/main/listaJobs'])
          }, 1500);
        } else {
          window.alert('KSTrade Merchandising - Lista de Jobs\r\n\r\nErro ao excluir job!');
        }
      });
    }
  }

}
