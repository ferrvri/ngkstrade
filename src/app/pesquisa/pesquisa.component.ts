import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../http.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-pesquisa',
  templateUrl: './pesquisa.component.html',
  styleUrls: ['./pesquisa.component.css']
})
export class PesquisaComponent implements OnInit {

  nomeProduto: string = '';

  n_vezes: string = '';

  clientes:any = [];
  cliente: string = 'Selecione o Cliente';
  clienteIndex: number = 0;
  clienteOK: boolean = false;

  pageParams: any;

  errorNomeProduto = {required: null};
  errorCliente = {required: null};
  errorNumeroVezes = {required: null}

  produtos = [
    {
      value: '',
      index: 1,
      valueConcorrente: ''
    }
  ]

  constructor( private _router: Router, private _http: HttpService, private _activatedRoute: ActivatedRoute ) { }

  ngOnInit() {
    this._activatedRoute.paramMap.subscribe( (data) => {
      this.pageParams = data;
    });

    if (this.pageParams.params.page == 'editar'){
      this._http.post(
        'selectPesquisas.php',
        {
          cliente: this.pageParams.params.cliente,
        }
      ).subscribe( (response) => {
        if (response.status == '0x104'){
          this.produtos = [];
          this.n_vezes = response.result[0].numero_vezes;
          this.cliente = response.result[0].cliente_nome;
          this.clienteIndex = response.result[0].cliente_id;

          response.result.forEach( (element) => {
            element.content.forEach( (element1, index1) => {
              this.produtos.push({
                value: element1.pro_nome,
                index: element1.pro_id,
                valueConcorrente: element1.pro_concorrente_nome
              });
            });
          });

          let days = response.result[0].dias_semana.split(';');
          this.days.forEach( (element, index) => {
            days.forEach(d => {
              if (d == element.name){
                element.activated = true;
                this.diasdasemana.push(element.name);
              }
            });
          });
        }
      });
    }

    this._http.post(
      'selectClientes.php',
      {}
    ).subscribe( (response) => {
      if (response.status == '0x104'){
        response.result.forEach(element => {
          this.clientes.push(element);
        });
      }
    });
  }


  validation(): boolean{
    try{
      if (this.cliente == 'Selecione o Cliente' || this.cliente.length < 1){
        this.errorCliente.required = true;
      }
      if (this.n_vezes == '' || this.n_vezes.length < 1){
        this.errorNumeroVezes.required = true;
      }
      return true;
    }catch(e){
      return false;
    }
  }

  cadPesquisa(){
    if (this.pageParams.params.page == 'editar'){
      if (this.validation() == true && this.errorNumeroVezes.required == null && this.errorCliente.required == null && this.errorNomeProduto.required == null){  
        let diassemana = '';
        
        this.diasdasemana.forEach(element => {
          diassemana += element+';';
        });

        this._http.post(
          'insertPesquisa.php',
          {
            type: 'update',
            cliente: this.clienteIndex,
            produtos: this.produtos,
            dias: diassemana,
            vezes: this.n_vezes
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nEditada com sucesso!');
            this._router.navigate( ['/main/innerload'] );
            setTimeout( () => {
              this._router.navigate( ['/main/pesquisa'] );
            }, 1500);
          }else if (response.status == '0x101'){
            window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nErro na edição de pesquisa!');
          }
        });
      }
    }else{
      if (this.validation() == true && this.errorCliente.required == null && this.errorNomeProduto.required == null){
        let diassemana = '';
        
        this.diasdasemana.forEach(element => {
          diassemana += element+';';
        });

        this._http.post(
          'insertPesquisa.php',
          {
            cliente: this.clienteIndex,
            produtos: this.produtos,
            dias: diassemana,
            vezes: this.n_vezes
          }
        ).subscribe( (response) => {
          if (response.status == '0x104'){
            window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nCadastrado com sucesso!');
            this._router.navigate( ['/main/innerload'] );
            setTimeout( () => {
              this._router.navigate( ['/main/pesquisa'] );
            }, 1500);
          }else if (response.status == '0x101'){
            window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nErro no cadastramento de pesquisa!');
          }
        });
      }
    }
  }

  // cadPesquisa(){
  //   if (this.pageParams.params.page == 'editar'){
  //     if (this.validation() == true && this.errorCliente.required == null && this.errorNomeProduto.required == null){  
  //       let diassemana = '';
        
  //       this.diasdasemana.forEach(element => {
  //         diassemana += element+';';
  //       });

  //       this._http.post(
  //         'insertPesquisa.php',
  //         {
  //           type: 'update',
  //           cliente: this.clienteIndex,
  //           produtos: this.produtos,
  //           dias: diassemana
  //         }
  //       ).subscribe( (response) => {
  //         if (response.status == '0x104'){
  //           window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nEditada com sucesso!');
  //           this._router.navigate( ['/main/innerload'] );
  //           setTimeout( () => {
  //             this._router.navigate( ['/main/pesquisa'] );
  //           }, 1500);
  //         }else if (response.status == '0x101'){
  //           window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nErro na edição de pesquisa!');
  //         }
  //       });
  //     }
  //   }else{
  //     if (this.validation() == true && this.errorCliente.required == null && this.errorNomeProduto.required == null){
  //       let diassemana = '';
        
  //       this.diasdasemana.forEach(element => {
  //         diassemana += element+';';
  //       });

  //       this._http.post(
  //         'insertPesquisa.php',
  //         {
  //           cliente: this.clienteIndex,
  //           produtos: this.produtos,
  //           dias: diassemana
  //         }
  //       ).subscribe( (response) => {
  //         if (response.status == '0x104'){
  //           window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nCadastrado com sucesso!');
  //           this._router.navigate( ['/main/innerload'] );
  //           setTimeout( () => {
  //             this._router.navigate( ['/main/pesquisa'] );
  //           }, 1500);
  //         }else if (response.status == '0x101'){
  //           window.alert('KSTrade Merchandising - Cadastro de Pesquisa\r\n\r\nErro no cadastramento de pesquisa!');
  //         }
  //       });
  //     }
  //   }
  // }


  setCliente(c){
    this.cliente = c.Cli_Nome;
    this.clienteIndex = c.Cli_ID;
    this.clienteOK = false;
  }

  addProduto(){
    this.produtos.push({
      value: '',
      valueConcorrente: '',
      index: this.produtos.length+1
    });
  }

  removeProduto(index){
    this.produtos.splice(index, 1);
  }

  diasdasemana: any = [];
  days: any = [
    {name : 'Seg', activated: false},
    {name : 'Ter', activated: false},
    {name : 'Qua', activated: false},
    {name : 'Qui', activated: false},
    {name : 'Sex', activated: false},
    {name : 'Sab', activated: false}
  ];

  selectThisDay(day){
    if (day.activated == true){
      day.activated = false;
      this.diasdasemana.forEach( (element, index) => {
        if (day.name == element){
          this.diasdasemana.splice(index,1);
        }
      });
    }else{
      day.activated = true;
      this.diasdasemana.push(day.name);
    }
  }
}
