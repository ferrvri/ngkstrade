var beforeScroll = 0;
var diag_index = 0;
var images = [];
var opt = null;

function sendComentario() {
    console.log('ok');
    var val = $('#comentario_box').val();
    if (val.length < 1) {
        $('#comentario_box').css('border', '4px solid #d50000')
        $("#comentar").append($('<span id="alert_spam" style="display: table; margin: 10px auto">Preencha o campo para comentar!</span>'))
    } else {
        if ($('#comentar > alert_spam')) {
            $('#alert_spam').remove();
            $('#comentario_box').css('border', '1px solid #ececec')
        }

        $.ajax({
            method: 'post',
            url: './assets/kstrade_php/insertComentarioJob.php',
            data: {
                job_id: opt.JOB_ID,
                nome: opt.Usr_Nome,
                comentario: val
            },
            success: (response) => {
                response = JSON.parse(response)
                if (response.status == '0x104'){
                    $('#envia_comentario').css('display', 'none')
                    $("#comentar").append($('<span id="alert_spam" style="display: table; margin: 10px auto">Comentário enviado com sucesso!</span>'))
                }
            }
        })
    }
}

function showLoading() {
    // remove existing loaders
    $('.loading-container').remove();
    $('<div id="orrsLoader" class="loading-container"><div><div class="mdl-spinner mdl-js-spinner is-active"></div></div></div>').appendTo("body");

    componentHandler.upgradeElements($('.mdl-spinner').get());
    setTimeout(function () {
        $('#orrsLoader').css({ opacity: 1 });
    }, 1);
}

function hideLoading() {
    $('#orrsLoader').css({ opacity: 0 });
    setTimeout(function () {
        $('#orrsLoader').remove();
    }, 400);
}

function openCity(cityName) {
    var i;
    var x = document.getElementsByClassName("city");
    for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
    }
    document.getElementById(cityName).style.display = "block";
    if (cityName == 'local') {
        document.getElementById(cityName).style.height = "550px";
        if (document.getElementById('prev_button') && document.getElementById('next_button')) {
            document.getElementById('prev_button').style.display = 'none';
            document.getElementById('next_button').style.display = 'none';
        }
    } else if (cityName == 'infos') {
        document.getElementById(cityName).style.height = "600px";
        if (document.getElementById('prev_button') && document.getElementById('next_button')) {
            document.getElementById('prev_button').style.display = 'none';
            document.getElementById('next_button').style.display = 'none';
        }
    }else if (cityName == 'comentar'){
        document.getElementById('prev_button').style.display = 'none';
        document.getElementById('next_button').style.display = 'none';
    } else {
        document.getElementById(cityName).style.height = "600px";
        if (document.getElementById('prev_button') && document.getElementById('next_button')) {
            document.getElementById('prev_button').style.display = 'table';
            document.getElementById('next_button').style.display = 'table';
        }
    }
}

function incrementDiag(len) {
    if (diag_index < len) {
        document.getElementById('prev_button').removeAttribute('disabled');
        diag_index++;
    } else {
        diag_index = len;
        document.getElementById('next_button').setAttribute('disabled', 'true');
        document.getElementById('prev_button').removeAttribute('disabled');
    }

    document.getElementById('imageDiag').src = images[diag_index];
}

function decrementDiag() {
    if (diag_index <= 1) {
        document.getElementById('next_button').removeAttribute('disabled');
        document.getElementById('prev_button').setAttribute('disabled', 'true');
        diag_index = 0;
    } else {
        document.getElementById('next_button').removeAttribute('disabled');
        diag_index--;
    }

    document.getElementById('imageDiag').src = images[diag_index];
}

function showDialog(options) {


    options = $.extend({
        id: 'orrsDiag',
        type: null,
        title: null,
        normalTitle: null,
        text: null,
        image: null,
        subtitle: null,
        images: [],
        coords: null,
        forLocal: false,
        local: [],
        infos: [],
        infosImages: [],
        neutral: false,
        negative: false,
        positive: false,
        close: false,
        cancelable: true,
        contentStyle: null,
        onLoaded: false,
        onHidden: false,
        hideOther: true,
        Usr_Nome: '',
        Usr_Nivel: '',
        JOB_ID: 0
    }, options);

    opt = options;

    if (!options.forLocal) {
        var x = document.getElementsByClassName("city");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
    }

    diag_index = 0;
    images = options.images;
    document.body.scrollTop = 0;
    document.getElementById('body').overflowY = 0;
    document.getElementById('mainHTML').overflowY = 0;
    beforeScroll = document.documentElement.scrollTop;
    document.documentElement.scrollTop = 0;
    document.getElementById('body').style.overflowY = 'hidden';
    document.getElementById('mainHTML').style.overflowY = 'hidden';

    if (options.hideOther) {
        // remove existing dialogs
        $('#orrsDiag').remove();
        $('.dialog-container').remove();
        $(document).unbind("keyup.dialog");
    }

    if (options.normalTitle != null && options.type == 'normal') {
        console.log('passing here');
        var dialog = $('#' + options.id);
        $(`<div id="${options.id}" class="dialog-container" >
            <div class="mdl-card mdl-shadow--16dp" id="${options.id}_content" style="margin: 15% auto; width: 25%; height: 27%">
            </div>
        </div>`).appendTo("body");
        var dialog = $('#' + options.id);
        var content = dialog.find('.mdl-card');
        $(`
            <div id="content" class="w3-container city" style="overflow: auto; position: relative; top: 20px;">

            </div>`).appendTo(content);
        if (options.contentStyle != null) content.css(options.contentStyle);

        $(` <p>${options.normalTitle}</p>
            `).appendTo(dialog.find("#content"));
    } else {

        if (options.type != null) {
            $(`<div id="${options.id}" class="dialog-container" >
                    <div class="mdl-card mdl-shadow--16dp" id="${options.id}_content" style="text-align: center; height: 200px; width: 330px; margin-top: 15%;">
                    </div>
                </div>`).appendTo("body");
            var dialog = $('#' + options.id);
            var content = dialog.find('.mdl-card');
            if (options.contentStyle != null) content.css(options.contentStyle);
            if (options.title != null) {
                $(` <h5 style="display: table; margin: auto; font-weight: bold;margin-bottom: 25px;margin-top: 10px;">${options.title}</h5>`).appendTo(content);
            }
            if (options.text != null) {
                $(`<p style="font-size: 14px;">${options.text.split('\n')[0]}</p>
                    <p style="font-size: 14px;">${options.text.split('\n')[1]}</p>
                    <p style="font-size: 14px;">${options.text.split('\n')[2]}</p>`).appendTo(content);
            }
        } else {
            if (options.forLocal == true) {
                $(`<div id="${options.id}" class="dialog-container" >
                    <div class="mdl-card mdl-shadow--16dp" id="${options.id}_content">
                    </div>
                </div>`).appendTo("body");
                var dialog = $('#' + options.id);
                var content = dialog.find('.mdl-card');
                $(`<div class="w3-bar w3-white" style="height: 55px;" id="navbar">
                    <button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" id="bugbutton" onclick="openCity('local')">Localização</button>
                    <button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px; display: none" onclick="openCity('content')">Imagem</button>
                    <button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px; display: none" onclick="openCity('infos')">Informações</button>
                </div>
            
                <div id="content" class="w3-container city" style="overflow: auto; position: relative; top: 20px;">
                    
                </div>
                <div id="local" class="w3-container city" style="margin-top: 10px; margin-bottom: 10px;">
                    
                </div>
                <div id="infos" class="w3-container city" style="display: none; margin-top: 10px; margin-bottom: 10px;">
                    
                </div>
                <div id="comentar" class="w3-container city" style="display: none; margin-top: 10px; margin-bottom: 10px;">
                    <div style="display: table; margin: auto; text-align: center">
                        <p>O seu comentário será enviado a nossa equipe, toda resposta, solicitação e cobranças deverão ser acompanhadas pela mesma.</p>

                        <textarea id="comentario_box" col="4" class="form-control" placeholder="Digite um comentário.."></textarea>
                        <button id="envia_comentario" onclick="sendComentario()" class="btn btn-info col-6" style="display: table; margin: 5px auto;">
                            <span>Enviar</span>
                        </button>
                    </div>
                </div>`).appendTo(content);

                if (options.contentStyle != null) content.css(options.contentStyle);

                var map = new google.maps.Map(document.getElementById('local'), {
                    zoom: 10,
                    center: new google.maps.LatLng(options.local[0].lat, options.local[0].long),
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                });

                var infowindow = new google.maps.InfoWindow();

                var marker, i;

                for (i = 0; i < options.local.length; i++) {
                    marker = new google.maps.Marker({
                        position: new google.maps.LatLng(options.local[i].lat, options.local[i].long),
                        map: map
                    });

                    google.maps.event.addListener(marker, 'click', (function (marker, i) {
                        return function () {
                            infowindow.setContent("Rota - " + i);
                            infowindow.open(map, marker);
                        }
                    })(marker, i));
                }

                setTimeout(() => {
                    $('#local').css('overflow', 'auto')
                    $('#bugbutton').click()
                }, 500);

                if (options.Usr_Nivel == 'Vendedor' || options.Usr_Nivel == 'Admin') {
                    $(`<button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('comentar')">Comentar</button>`).appendTo(dialog.find('#navbar'))
                }
            } else {

                if (options.coords != null) {
                    console.log('Test1 -> ', options.infos);
                    $(`<div id="${options.id}" class="dialog-container" >
                        <div class="mdl-card mdl-shadow--16dp" id="${options.id}_content">
                        </div>
                    </div>`).appendTo("body");
                    var dialog = $('#' + options.id);
                    var content = dialog.find('.mdl-card');
                    $(`<div class="w3-bar w3-white" style="height: 55px;" id="navbar">
                            <button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('content')">Imagem</button>
                            <button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('local')">Localização</button>
                            <button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('infos')">Informações</button>
                        </div>
                    
                        <div id="content" class="w3-container city" style="overflow: auto; position: relative; top: 20px;">
                            
                        </div>
                        <div id="local" class="w3-container city" style="margin-top: 10px; margin-bottom: 10px;">
                            
                        </div>
                        <div id="infos" class="w3-container city" style="display: none; margin-top: 10px; margin-bottom: 10px;">
                            
                        </div>
                        <div id="comentar" class="w3-container city" style="display: none; margin-top: 10px; margin-bottom: 10px;">
                            <div style="display: table; margin: auto; text-align: center">
                                <p>O seu comentário será enviado a nossa equipe, toda resposta, solicitação e cobranças deverão ser acompanhadas pela mesma.</p>

                                <textarea id="comentario_box" col="4" class="form-control" placeholder="Digite um comentário.."></textarea>
                                <button id="envia_comentario" onclick="sendComentario()" class="btn btn-info col-6" style="display: table; margin: 5px auto;">
                                    <span>Enviar</span>
                                </button>
                            </div>
                        </div>`).appendTo(content);
                    if (options.contentStyle != null) content.css(options.contentStyle);
                    if (options.title != null) {
                        $(` <h5 style="display: table; margin: auto; font-weight: bold;">${options.title.split('-')[0]}</h5>
                            <span style="display: table; margin: auto; font-size: 14px; margin-top: 5px;color: #000""> Data / Hora:</span>
                            <span style="display: table; margin: auto; margin-top: 2px; font-size: 14px;color: #000""> ${options.title.split('-')[1]}</span>
                            <span style="display: table; margin: auto; font-size: 14px; margin-top: 5px;color: #000""> Observações: </span>
                            <p style="text-align: center; color: #000">${options.title.split('-')[2]}</p>
                            <div id="dialog_image" style="margin-bottom: 20px;overflow-y: auto;">
                            </div>
                        `).appendTo(dialog.find("#content"));
                    }
                    if (options.text != null) {
                        $('<p>' + options.text + '</p>').appendTo(dialog.find('#dialog_image'));
                    } else if (options.text == null && options.image != null) {
                        $('<img src="' + options.image + '" style="position: relative; margin: 20px auto; display: table; width: 100%; position: relative;"/>').appendTo(dialog.find('#dialog_image'));
                    } else if (options.text == null && options.image != null && options.coords != null) {
                        $('<img src="' + options.image + '" style="position: relative; margin: 20px auto; display: table; width: 100%; position: relative;"/>').appendTo(dialog.find('#dialog_image'));
                    }

                    if (images.length > 1) {
                        $(` <button disabled id="prev_button" onclick="decrementDiag()" class="arrowButton mdl-button mdl-js-button mdl-button--fab" style="float: left;position: absolute;top: 60%;left: 5px;z-index: 15;"> < </button>
                            <button id="next_button" onclick="incrementDiag(${options.images.length - 1})"  class="arrowButton mdl-button mdl-js-button mdl-button--fab" style="float: right;position: absolute;top: 60%;right: 5px;z-index: 15;"> > </button>`
                        ).appendTo("#" + options.id + "_content");
                    }

                    if (options.images.length > 0) {
                        $(`
                            <div>
                                <img id="imageDiag" src="${images[0]}" style="position: relative; margin: 20px auto; display: table; width: 100%; position: relative;"/>
                            </div>
                        `).appendTo(dialog.find('#dialog_image'));
                    }

                    var exp = {};
                    try {
                        options.infos.forEach((e) => {
                            if (e === null || e == 'null' || e == '') {
                                throw exp;
                            }
                        });

                        $(`
                        <div>
                            <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 22px; font-weight: bold;">Informações adquiridas do aparelho:</span>
                            <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos possíveis fraudes, o sistema adquire informações abertas do aparelho.</span>
                            <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Modelo do aparelho</span>
                            <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 16px;">${options.infos[0]}</span>
                            <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Sistema operacional e versão</span>
                            <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 18px;">${options.infos[1]} - ${options.infos[2]}</span>
                            <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Fabricante</span>
                            <span style="display: table; margin: auto; font-size: 18px;">${options.infos[3]}</span>
                            <div id="infos_images"></div>
                        </div>
                        `).appendTo(dialog.find('#infos'));

                        if (options.infosImages.length > 0) {
                            options.infosImages.forEach((el) => {
                                $(`
                                <img src="${el}"  style="display: table; margin-top: 30px; margin-bottom: 20px; height: 200px; width: 600px;"/>
                               `).appendTo(dialog.find('#infos_images'));
                            });
                        }
                    } catch (exp) {
                        $(`
                        <div>
                            <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 22px; font-weight: bold;">Sem informações!</span>
                            <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos possíveis fraudes, o sistema adquire informações abertas do aparelho.</span>
                        </div>
                        `).appendTo(dialog.find('#infos'));
                    }

                    var myLatlng = new google.maps.LatLng(parseFloat(options.coords[0]), parseFloat(options.coords[1]));

                    var map = new google.maps.Map(document.getElementById('local'), {
                        center: myLatlng,
                        zoom: 18
                    });

                    var marker = new google.maps.Marker({
                        title: options.title,
                        position: myLatlng,
                        map: map
                    });

                    if (options.Usr_Nivel == 'Vendedor' || options.Usr_Nivel == 'Admin') {
                        $(`<button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('comentar')">Comentar</button>`).appendTo(dialog.find('#navbar'))
                    }
                } else {
                    console.log('null coords');
                    var dialog = $('#' + options.id);
                    $(`<div id="${options.id}" class="dialog-container" >
                            <div class="mdl-card mdl-shadow--16dp" id="${options.id}_content">
                            </div>
                        </div>`).appendTo("body");
                    var dialog = $('#' + options.id);
                    var content = dialog.find('.mdl-card');
                    $(`<div class="w3-bar w3-white" style="height: 55px;" id="navbar" >
                                <button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('content')">Imagem</button>
                            </div>
                        
                            <div id="content" class="w3-container city" style="overflow: auto; position: relative; top: 20px;">
                                
                            </div>
                            <div id="infos" class="w3-container city" style="display: none; margin-top: 10px; margin-bottom: 10px;">
                                
                            </div>
                            <div id="comentar" class="w3-container city" style="display: none; margin-top: 10px; margin-bottom: 10px;">
                                <div style="display: table; margin: auto; text-align: center">
                                    <p>O seu comentário será enviado a nossa equipe, toda resposta, solicitação e cobranças deverão ser acompanhadas pela mesma.</p>

                                    <textarea id="comentario_box" col="4" class="form-control" placeholder="Digite um comentário.."></textarea>
                                    <button id="envia_comentario" onclick="sendComentario()" class="btn btn-info col-6" style="display: table; margin: 5px auto;">
                                        <span>Enviar</span>
                                    </button>
                                </div>
                            </div>`).appendTo(content);
                    if (options.contentStyle != null) content.css(options.contentStyle);

                    if (options.title != null) {
                        $(` <h5 style="display: table; margin: auto; font-weight: bold;">${options.title.split('-')[0]}</h5>
                                <span style="display: table; margin: auto; font-size: 14px; margin-top: 5px;color: #000""> Data / Hora:</span><br />
                                <span style="display: table; margin: auto; margin-top: 2px; font-size: 14px;color: #000""> ${options.title.split('-')[1]}</span>
                                <span style="display: table; margin: auto; font-size: 14px; margin-top: 5px;color: #000""> Observações: </span><br />
                                <p style="text-align: center; color: #000">${options.title.split('-')[2]}</p>
                                <div id="dialog_image" style="margin-bottom: 20px;overflow-y: auto;">
                                </div>
                            `).appendTo(dialog.find("#content"));
                    }

                    if (options.subtitle != null) {
                        $(` <h5 style="display: table; margin: auto; font-weight: bold;">${options.subtitle}</h5>
                                <div id="dialog_image" style="margin-top: 15px; margin-bottom: 20px;overflow-y: auto;">
                                </div>
                            `).appendTo(dialog.find("#content"));
                    }

                    if (options.text != null) {
                        $('<p>' + options.text + '</p>').appendTo(content);
                    } else if (options.text == null && options.image != null) {
                        $('<img src="' + options.image + '" style="position: relative; margin: 20px auto; display: table; width: 100%; position: relative;"/>').appendTo(dialog.find("#dialog_image"));
                    }

                    if (images.length > 1) {
                        $(` <button disabled id="prev_button" onclick="decrementDiag()" class="arrowButton mdl-button mdl-js-button mdl-button--fab" style="float: left;position: absolute;top: 60%;left: 5px;z-index: 15;"> < </button>
                                <button id="next_button" onclick="incrementDiag(${options.images.length - 1})"  class="arrowButton mdl-button mdl-js-button mdl-button--fab" style="float: right;position: absolute;top: 60%;right: 5px;z-index: 15;"> > </button>`
                        ).appendTo("#" + options.id + "_content");
                    }

                    if (options.images.length > 0) {
                        $(`
                                <div>
                                    <img id="imageDiag" src="${images[0]}" style="position: relative; margin: 20px auto; display: table; width: 100%; position: relative;"/>
                                </div>
                            `).appendTo(dialog.find('#dialog_image'));
                    }

                    if (options.infos != null && options.infos.length > 0) {
                        $(`<button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('infos')">Informações</button>`).appendTo(dialog.find('#navbar'))
                        var exp = {};
                        try {
                            options.infos.forEach((e) => {
                                if (e === null || e == 'null' || e == '') {
                                    throw exp;
                                }
                            });

                            $(`
                                <div>
                                    <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 22px; font-weight: bold;">Informações adquiridas do aparelho:</span>
                                    <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos possíveis fraudes, o sistema adquire informações abertas do aparelho.</span>
                                    <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Modelo do aparelho</span>
                                    <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 16px;">${options.infos[0]}</span>
                                    <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Sistema operacional e versão</span>
                                    <span style="display: table; margin: auto; margin-bottom: 15px; font-size: 18px;">${options.infos[1]} - ${options.infos[2]}</span>
                                    <span style="display: table; margin: auto; margin-bottom: 5px; font-size: 18px; font-weight: bold;">Fabricante</span>
                                    <span style="display: table; margin: auto; font-size: 18px;">${options.infos[3]}</span>
                                    <div id="infos_images"></div>
                                </div>
                                `).appendTo(dialog.find('#infos'));
                        } catch (exp) {
                            $(`
                                <div>
                                    <span style="display: table; margin: auto; margin-top: 30px; margin-bottom: 10px; font-size: 22px; font-weight: bold;">Sem informações!</span>
                                    <span style="display: table; margin: auto; margin-bottom: 20px; font-size: 12px;">Para evitarmos possíveis fraudes, o sistema adquire informações abertas do aparelho.</span>
                                </div>
                                `).appendTo(dialog.find('#infos'));
                        }
                    }

                    if (options.Usr_Nivel == 'Vendedor' || options.Usr_Nivel == 'Admin') {
                        $(`<button class="w3-bar-item w3-button" style="display: table; margin: auto; width: 120px;" onclick="openCity('comentar')">Comentar</button>`).appendTo(dialog.find('#navbar'))
                    }
                }
            }
        }
    }

    if (options.neutral || options.negative || options.positive || options.close) {
        var buttonBar = $('<div class="mdl-card__actions dialog-button-bar"></div>');
        if (options.neutral) {
            options.neutral = $.extend({
                id: 'neutral',
                title: 'Neutral',
                onClick: null
            }, options.neutral);
            var neuButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect" id="' + options.neutral.id + '">' + options.neutral.title + '</button>');
            neuButton.click(function (e) {
                e.preventDefault();
                if (options.neutral.onClick == null || !options.neutral.onClick(e))
                    hideDialog(dialog, options.onHidden)
            });
            neuButton.appendTo(buttonBar);
        }
        if (options.negative) {
            options.negative = $.extend({
                id: 'negative',
                title: 'Cancelar',
                onClick: null
            }, options.negative);
            var negButton = $('<button class="mdl-button mdl-js-button mdl-js-ripple-effect" id="' + options.negative.id + '">' + options.negative.title + '</button>');
            negButton.click(function (e) {
                e.preventDefault();
                if (options.negative.onClick == null || !options.negative.onClick(e))
                    hideDialog(dialog, options.onHidden)
            });
            negButton.appendTo(buttonBar);
        }
        if (options.positive) {
            options.positive = $.extend({
                id: 'positive',
                title: 'OK',
                onClick: null
            }, options.positive);
            var posButton = $('<button class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (options.positive.onClick == null || !options.positive.onClick(e))
                    hideDialog(dialog, options.onHidden)
            });
            posButton.appendTo(buttonBar);
        }
        if (options.close) {
            options.positive = $.extend({
                id: 'close',
                title: 'Fechar',
                onClick: null
            }, options.positive);
            var posButton = $('<button class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect" id="' + options.positive.id + '">' + options.positive.title + '</button>');
            posButton.click(function (e) {
                e.preventDefault();
                if (options.close.onClick == null || !options.close.onClick(e))
                    hideDialog(dialog, options.onHidden);
            });
            posButton.appendTo(buttonBar);
        }
        buttonBar.appendTo(content);
    }
    componentHandler.upgradeDom();
    if (options.cancelable) {
        dialog.click(function () {
            hideDialog(dialog, options.onHidden);
        });
        $(document).bind("keyup.dialog", function (e) {
            if (e.which == 27)
                hideDialog(dialog, options.onHidden);
        });
        content.click(function (e) {
            e.stopPropagation();
        });
    }
    setTimeout(function () {
        dialog.css({ opacity: 1 });
        if (options.onLoaded)
            options.onLoaded();
    }, 1);


    function hideDialog(dialog, callback) {
        $(document).unbind("keyup.dialog");
        dialog.css({ opacity: 0 });
        setTimeout(function () {
            dialog.remove();
            if (callback)
                callback();
        }, 400);
        document.getElementById('body').style.overflowY = 'auto';
        document.documentElement.scrollTop = beforeScroll;

        document.getElementById('mainHTML').style.overflowY = 'auto';
        document.getElementById('mainHTML').scrollTop = beforeScroll;
    }
}